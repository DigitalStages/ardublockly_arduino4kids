# ArduBlockly_Arduino4Kids

Dies ist die Übungsanleitung für den Kurs ["ArduBlockly - Arduino für Kids"](https://gitlab.com/DigitalStages/ardublockly_arduino4kids)

# Index <a name="index"></a>

1. [Vorbereitung](#preparation)

2. [Installation](#installation)

    2.1. [Installationsprozedur für Windows, Mac, Linux PC](#installationPC)

    2.2. [Installationsprozedur für Android Tablet](#installationTablet)

3. [Übungen](#tutorials)

    3.1. [Hallo mein Name ist Arduino](#let-me-introduce)

    3.2. [Mein Blinker](#my-blinker)

    3.3. [Es werde Licht](#let-there-be-light)

    3.4. [Mehr Farben](#more-colors)

    3.5. [Knopf Spass](#button-fun)

    3.6. [Beat Meister](#let-beat-a-rhythm)

    3.7. [Mini Piano](#mini-piano)

    3.8. [Sam sagt 'Beweg deinen Arm!'](#sam-says-move-your-arm)

    3.9. [Mehr spielt die Musik!](#lets-make-some-more-music)

    3.10. [Hört sich an wie ein Handy!](#sounds-like-a-phone)

    3.11. [Wie heiss ist es?](#how-hot-is-it)

    3.12. [Links, Rechts, Vor und Zurück](#left-right-back-forth)

    3.13. [Die Starlight Show](#star-light-show)

    3.14. [Animierte Animatoren](#get-animated)

    3.15. [Alles dreht sich](#round-and-round-it-goes)

    3.16. [Rot, Gelb, Grün....und los!](#red,yellow,green...and-go)

    3.17. [Wie spät ist es?](#what-time-is-it)

    3.18. [Wie tief ist es?](#how-deep-is-it)

    3.19. [Wie ein Schreikopf](#lets-scream)

    3.20. [3 mal das Gleiche bitte](#3-times-the-same)

    3.21. [Du bist eingeladen](#you-shall-pass)

    3.22. [Blink Blink Blink](#blink-blink-blink)

    3.23. [Hell, Heller, Leuchtend hell](#brighter,lighter,light-brighter)

    3.24. [9.8.7.6.5.4.3.2.1.0...Start!](#countdown...9...0)

    3.25. [Und 1, 2, 3, 4](#1..2..3..4)

    3.26. [Lass uns etwas Wind machen](#lets-make-some-wind)

    3.27. [Tik Tak Tok](#tic-tac-toc)

    3.28. [Brumm!](#brummm)

    3.29. [Zu guter Letzt](#last-but-not-least)


4. [Roboterprojekt](#robotproject)

    4.1. [Vorbereitung](#rp-preparation)

    4.2. [Zusammenbau: Teil 1](#rp-assemblyP1)

    4.3. [Zusammenbau: Teil 2](#rp-assemblyP2)

    4.4. [Zusammenbau: Teil 3](#rp-assemblyP3)

    4.5. [Zusammenbau: Teil 4](#rp-assemblyP4)

    4.6. [Zusammenbau: Teil 5](#rp-assemblyP5)

    4.7. [Zusammenbau: Teil 6](#rp-assemblyP6)


---

#### [Zurück zum Index](#index)

## 1. Vorbereitung <a name="preparation"></a>

* Um an dem Kurs teilnehmen zu können, benötigen du den Zugang zu einem PC oder Android Tablet und ein ELEGOO "The Most Complete Starter Kit" Arduino Projekt Kit. Wenn du ein Tablet benutzen möchtest, benötigst du zusätzlich einen Mini USB <> USB Adapter.
* Falls du deinen eigenen Laptop oder Tablet mitbringst, solltest du auch die Software installiert haben, damit wir sofort loslegen können.


### 1.1. ELEGOO "The Most Complete Starter Kit" Arduino Projekt
<br>

<img src="images/ElegooTheMostCompleteStarterKit.jpg" width="960"/>

#### Produktinformation

Das Ultimate Starter Kit für Arduino Elegoo eignet sich ideal für Kinder, Jugendliche und Erwachsene. Das umfangreiche Kit wird mit viel Zubehör geliefert. 63 verschiedene Komponenten und über 200 Teile gehören dazu. Das neuste UNO R3 Entwicklungsboard ist ebenfalls dabei. Ihr Projekt ist so einfach umsetzbar, denn selbst das LCD1602 Modul erfordert kein Löten. Das deutsche Tutorial umfasst über 30 Lektionen inklusive Programmen und Erläuterungen.

<br>

### 1.2. Hardware für Android Tablets

Um den Arduino mit dem Tablet zu verbinden, benötigt man einen OTG Adapter USB A Buchse auf Micro USB B Stecker.

<img src="images/Parts/OTG_USB_Adapter.jpg" width="512"/>

<br>

### 1.3. Software für PC, Mac und Linux Computer
* [ArduBlockly Software](https://gitlab.com/DigitalStages/ardublockly)
* [Arduino Software](https://www.arduino.cc/en/Main/Software_) >> Damit wir den Code auf den Arduino hochladen können
* [Python](https://www.python.org/downloads/) >> Damit wir ArduBlockly im Webbrowser benutzen können

<br>

### 1.4. Software für Android Tablets

* [ArduBlockly Software](https://gitlab.com/DigitalStages/ardublockly)
* [ArduinoDroid Software](https://play.google.com/store/apps/details?id=name.antonsmirnov.android.arduinodroid2) >> Damit wir den Code auf den Arduino hochladen können
* [Pydroid3 - Python](https://play.google.com/store/apps/details?id=ru.iiec.pydroid3) >> Damit wir ArduBlockly im Webbrowser benutzen können
* [File Manager](https://play.google.com/store/apps/details?id=com.alphainventor.filemanager) >> Damit wir Dateien auswählen und kopieren können

<br>


---

#### [Zurück zum Index](#index)

## 2. Installation <a name="installationPC"></a>

Installationshilfe, damit es einfacher ist ArduBlockly auf deinem Laptop/PC oder Tablet zum Laufen zu bringen. Dies scheint ein wenig kompliziert, so wir gehen Schritt-für-Schritt durch die Prozedur von Installation bis zur Konfiguration.

### 2.1. Installationsprozedur für Windows, Mac, Linux PC <a name="installation"></a>

1. Installiere die Arduino Software mit der Installationsdatei, welche du vom oberen Link für dein Computersystem heruntergeladen hast.

2. Starte das Programm und wähle im Menü ***Sketch >> Include Library >> Add .ZIP Library***

  <img src="images/RunArdublockly/RunArdublockly_ImportLibraries_1.png" width="960">

----

3. Suche das ArduBlockly Verzeichnis und wähle die erste Zipdatei unter **arduino-libraries** und drücke OK.

  <img src="images/RunArdublockly/RunArdublockly_ImportLibraries_2.png" width="960"/>

----

4. Wenn alles geklappt hat, sollte es eine Meldung wie folgt geben:

  <img src="images/RunArdublockly/RunArdublockly_ImportLibraries_3.png" width="640"/>

----

5. Wiederhole dies mit allen Zip Dateien im Libraries Verzeichnis. Diese sind notwendig für die Module und Sensoren vom Arduino Kit.

6. Schließe das Arduino Programm und installiere Python mit der Installationsdatei, welche du für dein Operationssystem heruntergeladen hast.

#### Starte ArduBlockly

  **Windows:**
  ***Wenn du Python erfolgreich installiert hast, solltest du ArduBlockly durch einfaches Doppelklicken der start.py Datei starten können.***

  **Mac and Linux:**

1. Öffne das *ardublockly* Verzeichnis mit dem Dateimanager deines Computers.
2. Öffne ein Terminalfenster und gebe "python3 " (Leerzeichen nicht vergessen) ein.
3. Dann ziehe die **start.py** Datei in das Terminalfenster.

  <img src="images/RunArdublockly/RunArdublockly_1.png" width="960"/>

----

4. Die Kommandozeile sollte so ähnlich aussehen wie im folgenden Bildbeispiel.

>!DAS ARDUINO BOARD SOLLTE BEVOR DAS PROGRAMM GESTARTED WIRD, ÜBER USB AN DEN COMPUTER ANGESCHLOSSEN WERDEN!

  Drücke danach ENTER

  <img src="images/RunArdublockly/RunArdublockly_2.png" width="1024">

----

5. Der Terminal sollte so ähnlich aussehen (abhängig vom Operationssystem) wie folgendes Bild:

  <img src="images/RunArdublockly/RunArdublockly_3.png" width="640"/>

----

6. Nach wenigen Augenblicken sollte sich der Webbrowser öffnen und die Ardublockly IDE erscheint.

  <img src="images/RunArdublockly/RunArdublockly_4.png" width="960"/>

----

7. Drücke auf das Symbol oben links, welches das Hauptmenü öffnet. Wähle **Settings**.

  <img src="images/RunArdublockly/RunArdublockly_5.png" width="960"/>

----

8. Addiere den Installationspfad zur bereits installierten Arduino IDE Software. Falls du ein spezielles Sketchverzeichnis angeben möchtest, in welchem deine Blöcke automatisch gespeichert werden, kannst du dies unter **Sketch Folder** einrichten.

  <img src="images/RunArdublockly/RunArdublockly_6.png" width="512"/>

----

9. Als Nächstes wähle das zu verwendete Arduino Board aus. In unserem Falle ist dies **Uno**.

  <img src="images/RunArdublockly/RunArdublockly_7.png" width="512"/>

----

10. Die nächste Einstellung sollte gemacht werden, während das Board über USB am Computer angeschlossen ist, um es in den Einstellungen sehen zu können. **ttyACM0** oder **ttyUSB0** sind unter Linux übliche Bezeichnungen für das Arduino Board.

  <img src="images/RunArdublockly/RunArdublockly_8.png" width="512"/>

----

11. Drücke auf das Doppelpfeilsymbol unten links. Dies wird den Arduino Terminal zeigen.

  <img src="images/RunArdublockly/RunArdublockly_9.png" width="128"/>

----

12. Gehe mit der Maus über das Dreieck oder Haken bis du ***Verifiziere den Sketch*** (Verify the Sketch) als Pop-up Nachricht liest. Drücke das entsprechende Symbol.

  <img src="images/RunArdublockly/RunArdublockly_10.png" width="256"/>

----

13. Wenn alles funktioniert, sollte Ardublockly den Leerlauf mit Erfolg verifizieren.

  <img src="images/RunArdublockly/RunArdublockly_11.png" width="960"/>

----

14. Als Nächstes versuchen wir den leeren Code auf das Board hochzuladen. Drücke das Symbol, das **Sketch hochladen** (Upload Sketch....) in der Info anzeigt.

  <img src="images/RunArdublockly/RunArdublockly_12.png" width="256"/>

----

15. Als Ergebnis sollte der Arduino Terminal folgende Nachricht haben. Während des Hochladens sieht man die LEDs des Arduino blinken.

  <img src="images/RunArdublockly/RunArdublockly_13.png" width="960"/>

----

16. Wenn alles geklappt hat, ist nun alles klar zum Start >>

#### *Ready to go!*

---

#### [Zurück zum Index](#index)

### 2.2. Installationsprozedur für Android Tablet <a name="installationTablet"></a>


1. Installiere die folgende 3 Android Applikationen unter Nutzung von Google Play.

* [Pydroid3 - Python IDE](https://play.google.com/store/apps/details?id=ru.iiec.pydroid3)

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_1.png" width="512">


* [ArduinoDroid](https://play.google.com/store/apps/details?id=name.antonsmirnov.android.arduinodroid2)

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_2.png" width="512">


* [File Manager](https://play.google.com/store/apps/details?id=com.alphainventor.filemanager)

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_3.png" width="512">

----

2. Starte das Programm **File Manager**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_4.png" width="512">

----

3. Benutze den OTG Adapter USB A Buchse auf Micro USB B Stecker, um den USB Dongle mit der heruntergeladenen [ArduBlockly Software](https://gitlab.com/DigitalStages/ardublockly) und dem Tablet zu verbinden. Der USB Dongle sollte nun im File Manager sichtbar sein.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_5.png" width="960"/>

----

4. Halte den Finger solange auf dem ardublockly Symbol, bis sich das untere Menü öffnet. Tippe auf **Copy**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_6.png" width="960"/>

----

5. Gehe zurück und tippe auf **Main storage** (Hauptspeicher). Dann tippe auf **Paste**, um Ardublockly auf das Tablet zu kopieren.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_7.png" width="960"/>

----

6. Etwas Geduld, der Kopierprocess kann einige Minuten dauern.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_8.png" width="960"/>

----

7. Verlasse **File Manager** und starte **ArduinoDroid**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_9.png" width="512"/>

----

8. Jetzt wirst du die Libraries installieren, welche du für Ardublockly benötigst. Tippe dafür oben rechts auf das Symbol mit der Linie aus 3 Punkten. Dann klicke auf **Libraries**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_10.png" width="960"/>

----

9. In **Libraries** tippe auf **Add .ZIP library**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_11.png" width="960"/>

----

10. Suche das Ardublockly Verzeichnis und wähle das erste Zip Archiv unter **arduino-libraries**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_12.png" width="960"/>

----

11. Wenn alles geklappt hat, sollte eine Nachricht ähnlich wie unten erscheinen:

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_13.png" width="960"/>

----

12. Nachdem du alle Zielarchiv installiert hast, ist es nun Zeit, das Arduino Board mit **Board type** auszuwählen.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_14.png" width="960"/>

----

13. Der Arduino, welcher im Elegoo Komplettset genutzt wird, ist Arduino **Uno**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_15.png" width="960"/>

----

14. Kehre nun zurück in die Filemanager Applikation und gehe dort ins Verzeichnis **ardublockly**. Halte den Finger solange auf **start.py**, bis sich das Menü öffnet. Wähle dann **More >> Open with**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_16.png" width="960"/>

----

15. Öffne die Python Datei mit **Pydroid 3**. Wähle **ALWAYS**, sodass du nächstes Mal lediglich die Datei antippen brauchst.

<img src="images/RunArdublockly/RunArduBlocklyOnAndroid_17.png" width="960"/>

----

16. Das Programm zeigt die Python Datei in einem Text Editor. Du brauchst lediglich die gelbe runde Taste unten rechts anzutippen. Danach wird das Programm gestartet, der interne Webserver etabliert und der Webbrowser mit ArduBlockly geöffnet.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_18.png" width="960"/>

----

17. Lass uns gleich einmal testen, ob wir die einfachste Datei auf den Arduino laden können. Ziehe dazu unter **Function** (Funktionen) die Basisfunktion auf den Arbeitsplatz.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_19.png" width="960"/>

----

18. Da ArduBlockly momentan auf dem Tablet leider nicht automatisch den Code auf den Arduino hochladen kann, werden wir den Code als Arduino code abspeichern und in der ArduinoDroid direkt kompilieren und hochladen. Zum Speichern tippe auf das Symbol fürs Hauptmenü.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_20.png" width="960"/>

----

19. Danach tippe auf **Save Sketch** (Speichere Sketch).

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_22.png" width="960"/>

----

20. Wähle den Namen und das Verzeichnis aus. Dann tippe auf **Download** (Herunterladen).

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_23.png" width="960"/>

----

> JETZT IST ES AN DER ZEIT DEN USB DONGLE HERAUSZUZIEHEN. ANSTELLE DESSEN, STECKE DEN ARDUINO MIT DEM DAZUGEHÖRIGEN USB KABEL IN DEN ADAPTER AM TABLET.

----

21. Wechsle zu ArduinoDroid und tippe auf **Sketch >> Open from** (Sketch >> Öffne von).

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_24.png" width="960"/>

----


22. Tippe auf **Device** (Gerät). Es ist das Tablet selbst gemeint.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_25.png" width="960"/>

----

23. Suche die gespeicherte Arduino Datei mit der Endung *.ino*, wähle sie aus und tippe **Done** (Fertig).

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_26.png" width="960"/>

----

24. Du solltest nun den Code mit einer leeren ***setup()*** Funktion und einer leeren ***loop()*** Funktion. Gehe wieder ins Hauptmenü oben Rechts, diesmal auf **Actions** (Aktionen).

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_27.png" width="960"/>

----

25. Um den Code auf den Arduino hochzuladen, muss dieser erst kompiliert werden. Tippe also auf **Compile** (Kompiliere).

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_28.png" width="960"/>

----

26. Bei Erfolg wirst du die Nachricht ***Compilation finished*** sehen. Jetzt kannst du den Code auf den Arduino hochladen.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_29.png" width="960"/>

----

27. Dazu gehe erneut ins Hauptmenü und tippe auf **Actions** (Aktionen). Diesmal tippe auf **Upload** (Hochladen).

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_30.png" width="960"/>

----

28. Tippe auf **Upload over USB** (Übers USB hochladen).

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_31.png" width="960"/>

----

29. Wenn der Code erfolgreich hochgeladen wurde, sollte die Meldung **Sketch uploaded** (Skizze hochgeladen) erscheinen.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_32.png" width="960"/>

----

> Ich hoffe, dass wir haben in Zukunft eine direkte Verbindung von ArduBlockly zur Arduino IDE (ArduinoDroid) für Android OS. Dann wird das Hochladen vom Android Tablet so einfach wie beim Laptop/PC.

---

#### Ok. Alle Vorbereitungen sind abgeschlossen, die Ausrüstung ist startklar. Es kann losgehen ....
### ...Lass uns die Reise beginnen!

---

#### [Zurück zum Index](#index)


## 3. Übungen <a name="tutorials"></a>



### 3.1. Hallo mein Name ist Arduino <a name="let-me-introduce"></a>

>Hallo, mein Name ist Arduino. Ich bin ein Mikrokontroller! Darf ich mich vorstellen?

  <img src="images/Lesson1/Lesson1_Intro_10.png" width="960"/>

| *Bezeichnung*   | *Erklärung*  |
|---------------|---------------|
| **Atmega328 Microcontroller** | >> Das ist mein Gehirn. Hier wird alles gelenkt, geleitet und geschaltet.    |
| **Power Supply**   | >> Damit kannst du meinem Gehirn genug Energie zuführen (9 Volt Mhmm...Lecker). |
| **USB Plug**   |>> Auch die USB Verbindung hat Strom (5 Volt). Dies ist der Kanal zu deinem Computer oder Tablet, auf dem Du mir deine Aufgabe (das Programm) geben kannst.|
| **Reset Button**  |>> Der Druckknopf sagt mir das ich die Aufgabe von vorn beginnen soll.|
| **Analog Reference Pin (AREF)** |>> Mit diesem Pin kannst du eine maximale Energie (0 -  5 Volt) für meine analogen Eingänge festlegen. (Werden wir nicht benutzen)|
| **Digital Ground / Ground Pins**        |>> Das Erden schützt mich so, wie dich der Blitzableiter vom Haus.            |
| **Digital I/O**                         |>> Meine digitalen Ein- und Ausgänge >> das Symbol ~ bedeutet PWM. Pulsierende Verbindung (PWM), damit du auch die Geschwindigkeit eines Motors mit Digital (0 = aus, 1 = an) bestimmen kannst.|
| **Serial OUT (TX)**                     |>> Dieser Pin ist auch einer meiner digitalen I/O Pins (D 1). Damit kannst du mich mit einem anderen Gerät reden lassen. TX ist der Kommunikationseingang|
| **Serial IN (RX)**                      |>> Dieser Pin ist auch einer meiner digitalen I/O Pins (D 0). Damit kannst du mich mit einem anderen Gerät reden lassen. RX ist der Kommunikationsausgang|
| **In-Circuit-Serial Programmer**        |>> Damit kann ich als Erweiterung durch diese 2 Schnittstellen in ein Großprojekt eingesetzt werden (Werden wir nicht benötigen).|
| **Analog I/O**                          |>> Meine Analoge Ein- und Ausgänge sind nicht nur 0 (Strom aus) und 1 (Strom ein), sondern haben 1024 Stufen, ähnlich wie ein Dimmschalter.|
| **Voltage in Pin**                      |>> Hier kann ich Extraenergie zu Sensoren oder Motoren geben oder Extraenergie bekommen, falls die Energie über die 9V Batterie nicht reicht (Werden wir nicht benötigen).|
| **3.3 / 5 Volt Power Pin**              |>> Dies sind meine Stromausgänge, einmal mit 3.3 Volt und einmal mit 5 Volt.|
| **Reset Pin**              |>> Ist wie die Resettaste, wenn du einen Druckknopf an diesen Pin anschließt.|

---

#### [Zurück zum Index](#index)

### 3.2. Mein Blinker <a name="my-blinker"></a>

>Mein Blinker ist ein LED Licht auf meiner Platine. Finde es, es ist mit "L" bezeichnet!

  <img src="images/Lesson2/Lesson2_MyBlinker_1.png" width="600"/>

>Verbinde mich nun mit deinem Computer / Tablet!

---

Nachdem der Arduino via USB an den Computer angeschlossen wurde, sollten wir nun Ardublockly starten, und unter Settings sicher gehen, dass der Arduino unter COM Port eingestellt ist.


Dies ist der Code, das die LED zum Blinken bringt.

<img src="images/Lesson2/Lesson2_MyBlinker.png" width="960"/>

---

#### Was für Teile brauchst Du?

|**Anzahl**|*Bezeichung* | *Foto* |
|----------|-------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> |

#### Neue Komponenten

***Arduino Uno:***

* ist ein Mikrocontroller-Board basierend auf dem ATmega328P Chip.
* verfügt über 14 digitale Ein-/Ausgangspins (davon 6 als PWM-Ausgänge nutzbar), 6 analoge Eingänge, einen 16-MHz-Keramikresonator (CSTCE16M0V53-R0), einen USB-Anschluss, eine Strombuchse, einen ICSP-Header und eine Reset-Taste.
* enthält alles, was zur Unterstützung des Mikrocontrollers benötigt wird.
* schließst du einfach mit einem USB-Kabel an einen Computer an oder du betreibst es mit einem AC-to-DC-Adapter oder einer Batterie, um loszulegen.

---

#### Block für Block


1. Unter **Eingang/Ausgang** (Input/Output) finden wir den Block für die LED **Setze LED BUILTIN_1 zu** (set built-in BUILTIN_1 to). Klick/tippe diesen Block an und ziehe ihn auf den Arbeitsplatz.

  *Falls du das falsche Teil auf dem Arbeitsplatz gezogen hast, kannst du dies über das Menü ziehen. Damit wirst du es löschen.*

  <img src="images/Lesson2/Lesson2_MyBlinker_2.png" width="960"/><br>

2. **Wie lange soll die LED aufleuchten?** Unter **Zeit** finden wir den Block dafür. Klick/tippe den Block **Warte 1000 Millisekunden** (wait 1000 milliseconds) an und ziehe ihn auf den Arbeitsplatz.

  ***1000 Millisekunden sind ....?***

  <img src="images/Lesson2/Lesson2_MyBlinker_3.png" width="960"/><br>

3. Wiederhole die vorherigen 2 Schritte, wobei diesmal die Einstellung der LED **BUILTIN_1** auf ***LOW*** gestellt ist. Das bedeutet die LED ausgeschaltet bleibt ... für eine weitere Sekunde.

  <img src="images/Lesson2/Lesson2_MyBlinker_4.png" width="960"/><br>

4. Das ist schon alles. Nun müssen wir den Codeblock kompilieren. Gehe auf das Symbol mit dem Haken, dann klicke/tippe auf das Symbol, das **Verfiziere den Sketch** (Verify Sketch) anzeigt. Danach sollten wir im Arduino IDE Output  **Sketch erfolgreich verfiziert** (Successfully Verified Sketch) sehen.

  **Momentan ist unter einer ausschließlichen Nutzung des Android Tablets notwendig, den Code in ArduinoDroid zu kompilieren und hochzuladen. Benutze "Save Sketch", um den konvertierten Code im .ino Format zu speichern.**

  ***Dies trifft nicht für Computers und Laptops zu, bei welchen die Arduino Software automatisch das Kompilieren und Hochladen übernimmt. Daher kann es nun weitergehen.***

  <img src="images/Lesson2/Lesson2_MyBlinker_5.png" width="960"/><br>

5. Gib deinem Codeblock einen Namen.

  <img src="images/Lesson2/Lesson2_MyBlinker_6.png" width="960"/><br>

6. Speichere den Codeblock deines Projektes. Benutze **Speichern** (Save), um den Codeblock für ArduBlockly zu speichern.

  <img src="images/Lesson2/Lesson2_MyBlinker_7.png" width="960"/><br>

7. Zeit zum Hochladen. Klicke/tippe auf die große grüne Taste (oder links daneben), um den Code auf den Arduino hochzuladen. Der Hinweis wird **Lade Sketch zum Arduino** (Upload Sketch to Arduino) anzeigen.

  <img src="images/Lesson2/Lesson2_MyBlinker_8.png" width="960"/><br>

  Wenn das Hochladen erfolgreich war, sollte im Arduino IDE Output die Nachricht **Erfolgreich hochgeladen** (Successfully Uploaded Sketch) zu sehen sein.

>Naaaa....bin ich am Blinken??? Verändere doch mal die Zeitabstände. :-D

>Kannst du mich nen Morsecode blinken lassen?

---

### 3.3. Es werde Licht <a name="let-there-be-light"></a>

Je mehr Energie durch eine Leuchte fließt, so heller wird sie sein. Zu hohe Energie kann die LED jedoch zerstören. Daher werden wir Widerstände benutzen, um den Energiefluss einzustellen. Je höher der Widerstand, je weniger Energie wird fließen.

<img src="images/Lesson3/Lesson3_LED_1.png" width="960"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte**                   |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 1 | **Rote LED**                      |<img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/>    | 1 | **220 Ohm Widerstand**            |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>|
| 1 | **1k Ohm Widerstand**             |<img src="images/Parts/Part_Resistor_1KOhm.png" width="256"/> | 1 | **10k Ohm Widerstand**            |<img src="images/Parts/Part_Resistor_10KOhm.png" width="256"/>|
| 3 | **M-M Kabel**                     |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***Steckplatte***

* ermöglicht es Schaltungen zu testen, ohne dass gelötet werden muss.
* gibt es in zahlreichen Größen und Modellen.
* ist oft ein Plastikblock mit integrierten Metallstreifen, die die elektrische Verbindung zwischen den Kontaktstellen herstellen.

  <img src="images/Lesson3/Lesson3_LED_3.png" width="320"/><br>

* hat Stecklöcher gekennzeichnet als rote und blaue Linien, welche jeweils in Reihe miteinander verbunden (Rot/Blau) sind.
* hat Spalten von 5 Stecklöchern im oberen und unteren Hauptteil, welche ebenfalls miteinander verbunden sind (Grün).
* ist im Mittelteil (zwischen E und F) unterbrochen, sodass der obere und untere Teil unabhängig voneinander genutzt werden kann.


***LED oder Licht Emittierende Diode:***

* ist ein kleines Licht, welches sehr wenig Strom braucht und ewig hält. Jedoch kann sie auch kaputt gehen, wenn man zu viel Energie durchlaufen lässt.
* Der Strom wird die LED so weit aufheizen, bis sie durchbrennt. Um das zu verhindern, benutzen wir Widerstände. <br>
* Die LED hat einen positiven und einen negativen Anschluss. Der positive Anschluss ist länger als der negative.

***Widerstand:***

* ist kleines Element, das den Stromfluss zurückhält.
* Es gibt Widerstände mit verschiedenen Einheiten, welche als "Ohm" bezeichnet werden.
* Der griechische Buchstabe Ω Omega ist das Kurzsymbol. Ein K vor Ohm bedeutet Kilo oder 1000.
* Widerstände habe Farbringe, aus denen man die Ohmzahl errechnen kann. Es gibt Widerstände mit 4 oder 5 Farbringen.
* Die Widerstände haben keine Pole + oder -. Daher spielt es keine Rolle, welche Richtung der Widerstand benutzt wird.

  In dieser Übung werden wir 3 Widerstände nutzen. Du solltest Widerstände mit 5 Kreisbände in deinem Bausatz finden. Jede der Streifen im Bausatz haben Nummer auf den Papierbändern.

  ***Bitte entnehme nur ein Widerstand, damit kannst du später die losen Widerstände vergleichen und die Ohmzahl herausfinden.***

  220 Ohm Widerstand: <br>
  Rot|Rot|Schwarz|Schwarz|Braun

  1 kOhm Widerstand (1000 Ohm):  <br>
   Braun|Schwarz|Schwarz|Braun|Braun

  10 kOhm Widerstand (10000 Ohm): <br>
  Braun|Schwarz|Schwarz|Red|Braun


#### Schaltplan

  <img src="images/Lesson3/Lesson3_LED_2.png" width="960"/><br>


#### Bastelanweisung

***Info: Die folgenden Anweisung sind durch ein >> Zeichen verkürzt. Dies bedeutet hier:***<br>

***Verbinde Teil A mit jenem Teil B, oder Teil A >> Teil B!***

1. Kabel 1: Arduino GND (Erde) >> blaue Reihe der Steckplatte
2. Kabel 2: 5V Ausgang >> rote Reihe der Steckplatte
3. LED: Kurzes Ende der LED >> Blaue Reihe (Erde)
4. LED: Längeres Ende der LED >> Loch des Mittelteils
5. 220 Ohm Widerstand: In Reihe der "+" Reihe (Rot) >> Mittelteil
6. 1 kOhm Widerstand:  In Reihe der "+" Reihe (Rot) >> Mittelteil
7. 10 kOhm Widerstand: In Reihe der "+" Reihe (Rot) >> Mittelteil
8. Kabel 3: In Reihe der LED auf Mittelteil >> In Reihe des 220 Ohm Widerstand auf Mittelteil

#### *Stecke deinen Arduino in die USB Buchse deines Computers/Tablet*

>Und es werde Licht! Versuch doch mal die anderen Widerstände!*

>Oder wie wäre eine andere Farbe? (Vergesst nicht wo der längere Fuß hingehört)

---

#### [Zurück zum Index](#index)

### 3.4. Mehr Farben <a name="more-colors"></a>

Lass uns eine Lichtshow mit einer LED bauen. Dazu brauchen wir eine RGB LED.

<img src="images/Parts/Part_LED_RGB_5mm.jpg" width="512"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte**                   |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 1 | **RGB LED**                      |<img src="images/Parts/Part_LED_RGB_5mm.jpg" width="256"/>    | 3 | **220 Ohm Widerstand**            |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>|
| 4 | **M-M Kabel**                     |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***Die RGB LED:***

* sieht erst mal wie jede andere Diode aus. Doch in Wirklichkeit 3 Dioden eingebaut.
* bedeutet Rot, Grün, Blau. Für jede Farbe gibt ist eine LED eingebaut. Deshalb hat die RGB LED 4 Drahtbeine.
* hat ein Drahtbein für **+** jeder Farbe (Anode) und ein Bein **-** für die Erdung (Kathode).

  Auf dem folgenden Bild sind die verschiedenen Farben durch die Mischung der Lichtfarben Rot, Grün und Blau dargestellt.

  <img src="images/Lesson4/Lesson4_RGBLED_4.png" width="960"/><br>

* Die Lichtintensität hat eine Spannweite von 0 bis 255. Mit 0 sind dies 256 Einstellungen.
* Die Farben Rot, Grün und Blau werden erzeugt, indem die jeweilige LED voll aufgedreht wird, während die LEDs der anderen zwei Farben ausgeschalten sind. Also im Falle von Rot ist bekommt die rote LED den Wert 255, die grüne und blaue LED den Wert 0.

  ***Das kennst du bestimmt auch vom TV oder Monitor.***


#### Schaltplan

<img src="images/Lesson4/Lesson4_RGBLED_3.png" width="960"/><br>


#### Bastelanweisung

Um die RGB LED vor Schaden zu schützen, werden wir wieder einen Widerstand (220 Ohm) pro LED benutzen.

Jede der 3 LEDs bekommt einen Steckplatz am Arduino. Zusätzlich brauchen wir eine Erdung oder GND (Ground). Das Drahtbein dafür ist das längste Bein an der RGB LED. Daher kannst du auch sehen, welches Bein Rot, Grün und Blau ist.

Am Arduino kannst du bei den digitalen Steckplätzen ein Wellensymbol ~ bei einigen sehen. Dies zeigt an, dass der Anschluss über *PWM* verfügt.

Um die Helligkeit unserer LED einstellen zu können, benötigen wir die Technik der Pulsweitenmodulation.

Der digitale Steckplatz ohne *PWM* kann lediglich 0 oder 1 sein, also Strom aus oder Strom an. Damit wir 256 Stufen nutzen können, kontrolliert unser Arduino die elektrische Leistung des Anschlusses.

Der Arduino schaltet den Schalter in einem superschnellen Puls. 500-mal pro Sekunde, um genau zu sein. Jedoch kann unser Arduino bestimmen, wie lang dieser Schalter an bleibt:

* Bei der Einstellung 255 bleibt der Stromfluss die komplette Zeit zwischen den Pulsen erhalten.
* Bei der Einstellung 0 wird der Schalter bei jedem Puls sofort wieder ausgestellt.
* Die Einstellung 128 bedeutet exakt die Hälfte des Zeitabstandes zwischen 2 Pulsen.

Durch die PWM Technik kann der digitale Anschluss (~ Symbol) auch als analoger Steckplatz genutzt werden.

***Info: Die folgenden Anweisung sind durch ein >> Zeichen verkürzt. Dies bedeutet hier:***<br>

***Verbinde Teil A mit jenem Teil B, oder Teil A >> Teil B!***

1. RGB LED:  4 LED Drahtbeine >> nebeneinander im oberen Mittelteil.
2. Kabel 1:  Arduino GND (Erde) >> Mittelteilloch vor langem Drahtbein
3. 3 Widerstände: Vor jedem Fuss im oberen Mittelteil >> Längsposition im unteren Mittelteil
4. Kabel 2: Arduino ~6 >> Vor Widerstand links vom GND Kabel im unteren Mittelteil
5. Kabel 3: Arduino ~5 >> Vor Widerstand rechts vom GND Kabel im unteren Mittelteil
6. Kabel 4: Arduino ~3 >> Vor Widerstand rechts neben 3. Kabel im unteren Mittelteil

  **Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**


#### Block für Block

Dies ist der komplette Codeblock, welchen wir zusammenpuzzeln werden. Als Ergebnis wird die RGB LED Farbe sich von Rot über Grün zu Blau und dann zurück zu Rot verändern. Dazwischen kannst du die anderen Farben erkennen. Eine Reise durch die Regenbogenfarben.

<img src="images/Lesson4/Lesson4_RGBLED.png" width="960"/><br>
1. Als Erstes die 2 Basisfunktionen Vorlauf und Unendlichschleife. **Funktion >> Arduino Vorlauf, Arduino Unendlichschleife** (Functions >> Arduino run first, Arduino loop forever).

  <img src="images/Lesson4/Lesson4_RGBLED_5.png" width="960"/><br>

2. Nun melden wir jede der 3 LEDs beim Arduino an. **Eingang/Ausgang >> Setze digitale Pin#** (Input/Output >> set digital pin#).

  <img src="images/Lesson4/Lesson4_RGBLED_6.png" width="960"/><br>

3. Ziehe 3-mal dieses Teil in den Vorlauf. Verändere die Pinnummer für die rote LED zu ***6***, grüne LED zu ***5*** und blaue LED zu ***3***. Unser Arduino soll zu Beginn nur die rote LED mit **HIGH** anschalten, was soviel wit 1 oder Strom an bedeutet. Den Strom des grünen und blauen Steckplatzes werden wir durch die Auswahl von ***LOW*** ausstellen.

  **Was wäre, wenn ich alle 3 anlassen würde?**

  <img src="images/Lesson4/Lesson4_RGBLED_7.png" width="960"/><br>

4. Nun werden wir ein Element erzeugen, welches als Variable (veränderbarer Wert) die Einstellung der roten LED einstellt. **Variable >> setze Element zu** (Variables >> set item to).

  <img src="images/Lesson4/Lesson4_RGBLED_8.png" width="960"/><br>

5. Bevor wir den Namen ändern, lass uns die Einstellung als Nummer hinzufügen. **Mathe >> 0** (Math >> 0). Stecke den Block in die Variable **setze Element zu** (set item to).

  <img src="images/Lesson4/Lesson4_RGBLED_9.png" width="960"/><br>

6. Klicke/tippe auf **Element** und wähle **Umbenennen**, dann gib dieser Variable den Namen ***Rot***. Ändere die Nummer zu ***255***.

  <img src="images/Lesson4/Lesson4_RGBLED_10.png" width="960"/><br>

7. Wiederhole die Schritte von 4. bis 6. zweimal, jeweils für Grün und Blau. Benutze die rechte Maus/Padtaste (Laptop/PC) oder halte den Finger einige Sekunden auf dem Baustein (Tablet), um ein Popupfenster zu öffnen. Wähle **Dupliziere**.

  <img src="images/Lesson4/Lesson4_RGBLED_11.png" width="960"/><br>

8. Klicke auf **Rot**, um das Menü zu öffnen. Wähle **Neue Variable...** (New variable...), und ändere es einmal zu ***Grün*** und das andere Mal zu ***Blau***.

  <img src="images/Lesson4/Lesson4_RGBLED_12.png" width="960"/><br>

9. Ändere die Zahlen von **Grün** und **Blau** zu ***0***.

  <img src="images/Lesson4/Lesson4_RGBLED_13.png" width="960"/><br>

10. Ziehe erneut einen **Setze digitale Pin#** (set digital pin#) Block von **Eingang/Ausgang** (Input/Output), und ändere die Pinnummer zu ***6***. Klicke auf den **HIGH** Block und lösche diesen. Dann ziehe einen **Element** (item) Block von **Variable** (Variables) und setze diesen an die Stelle, wo **HIGH** vorher war. Klicke auf **Element** und wähle ***Rot***.

  <img src="images/Lesson4/Lesson4_RGBLED_15.png" width="960"/><br>

11. Klicke auf den gerade erstellten Block und kopiere diesen 2 mal. Wie vorher ändere die Pinnummer einmal zu ***5*** und das Element zu ***Grün*** und das andere Mal die Pinnummer zu ***3*** und das Element zu ***Blau***.

  <img src="images/Lesson4/Lesson4_RGBLED_16.png" width="960"/><br>

  **Bis jetzt leuchtet die LED lediglich in einer Farbe.**

  **Lass uns eine Zeit einsetzen, dann werden wir alle 7 Teile zweimal kopieren.**

12. Ziehe eine **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block von **Zeit** (Time), und stecke es an die unterste Stelle der Endlosschleife.

  <img src="images/Lesson4/Lesson4_RGBLED_17.png" width="960"/><br>

13. Jetzt kopiere alles in der Endlosschleife zweimal und setze es untereinander in der selben Reihenfolge wie die ersten Gruppe.
14. Dann ändere die Zahlen der **setze Element zu** (set item to) im ersten Abschnitt wie gehabt **Rot** zu ***255***, **Grün** zu ***0***, **Blau** zu ***0***. Im zweiten Abschnitt setze die Zahlen von **Rot** zu ***0***, **Grün** zu ***255***, **Blau** zu ***0***, und im dritten Abschnitt zu **Rot** zu ***0***, **Grün** zu ***0***, **Blau** zu ***255***.

  <img src="images/Lesson4/Lesson4_RGBLED_18.png" width="960"/><br>

15. Verifiziere deinen Code und lade es auf deinen Arduino.

  >Rot, Grün, Blau, Rot, Grün, Blau, Rot, Grün, Blau, Rot, Grün, Blau...

  **Versuch doch einmal ein paar andere Farben!**

  ***Seh dir noch einmal das Bild am Anfang an, da sind einige Beispiele.***

  **Aber wie kannst du verschiedene Farben ineinander fliessen lassen?**

16. Ziehe einen **zähle mit i von 1 zu 10 bei 1** Block von **Schleife** auf deinen Arbeitsplatz.

  **Mit dieser Schleife koennen wir Dinge automatisch verändern, bis eine gewisse Kondition erreicht ist.**

  <img src="images/Lesson4/Lesson4_RGBLED_19.png" width="960"/><br>

17. Ändere **zu** (to) zu ***255***, was bedeutet die Schleife zirkuliert 255 mal und addiert bei jeder Schleife eine 1. Dies werden wir nutzen um die Helligkeit der jeweiligen LED zu beeinflussen.

  <img src="images/Lesson4/Lesson4_RGBLED_20.png" width="960"/><br>

18. Ziehe zwei **Setze Element zu** (set item to) von **Variable** (Variables) auf deinen Arbeitsbereich.

  <img src="images/Lesson4/Lesson4_RGBLED_21.png" width="960"/><br>

19. Ziehe zwei **_+_** von **Mathe** (Math) in den Arbeitsbereich und verbinde diese mit den **Setze Element zu** (set item to) Blöcken in der Schleife.

  <img src="images/Lesson4/Lesson4_RGBLED_22.png" width="960"/><br>

20. Ziehe zwei **Element** (item) von **Variable** (Variables) auf deinen Arbeitsplatz. Verbinde diese jeweils mit der ersten Position in beiden **" "+" "**.

  <img src="images/Lesson4/Lesson4_RGBLED_23.png" width="960"/><br>

21. Verändere den Wert des ersten **Element** Block zu ***Rot*** und das **+** Zeichen zu ***-***. Der Wert des zweiten **Element** Block wird zu ***Grün***.

  <img src="images/Lesson4/Lesson4_RGBLED_24.png" width="960"/><br>

22. Ziehe nun zwei **0** aus **Mathe** (Math) jeweils in die zweite Position der **" "-/+" "** Blöcke.

  <img src="images/Lesson4/Lesson4_RGBLED_25.png" width="960"/><br>

23. Beide Nummern in den **0** Blöcken sollen **1** sein.

  **Die Schleife wird sich 255 mal wiederholen, wobei es mit jeder Schleife den Wert von *Rot* um Eins verringert und der Wert von *Grün* um Eins erhöht. Das Endresultat für *Rot* ist 1 und für *Grün* ist es 255.**

  <img src="images/Lesson4/Lesson4_RGBLED_26.png" width="960"/><br>

  **Um die Geschwindigkeit der Schleifen auszubremsen, verwenden wir eine Zeitpause von 10 Millisekunden bei jedem Zyklus.**

24. Ziehe einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block aus **Zeit** (Time) in die letzte Position von *zähle mit i von 1 zu 255 bei 1* (count with i from 1 to 255 by 1), und ändere den Wert **1000** zu ***10***.

  <img src="images/Lesson4/Lesson4_RGBLED_27.png" width="960"/><br>

25. Jetzt nehme den gesamten Block und setze diesen unterhalb der ersten Zeitpause **Warte 1000 Millisekunden** (wait 1000 milliseconds) der Endlosschleife.

  **Kompiliere und lade den Code auf deinen Arduino, um zu sehen was durch die Schleife geschieht.**

  <img src="images/Lesson4/Lesson4_RGBLED_28.png" width="960"/><br>

  **Jetzt wollen wir dies auch mit den beiden anderen LEDs machen. Also erst die grüne LED abdunkeln, während die blaue LED aufhellt, dann die blaue LED abdunkeln, während die rote LED aufhellt.**

26. Klicke dazu auf den *zähle mit i von 1 zu 255 bei 1* (count with i from 1 to 255 by 1) Block und duplizieren diesen zweimal. Danach verändere in der ersten Kopie den Wert von **Element** von **Rot** zu ***Grün***, und **Grün** zu ***Blau***. In der zweiten Kopie ändert sich der Wert vom **Element** Block von **Rot** zu ***Blau***, und **Grün** zu ***Rot***.

  <img src="images/Lesson4/Lesson4_RGBLED_29.png" width="960"/><br>

27. Nun füge die beide Blockgruppen *zähle mit i von 1 zu 255 bei 1* (count with i from 1 to 255 by 1) jeweils unter dem zweiten und dritten **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block der Endlosschleife.
28. Gib dem Project einen Namen, kompiliere und lade den Code auf deinen Arduino.

  <img src="images/Lesson4/Lesson4_RGBLED_30.png" width="960"/><br>

29. Speichere dein Project ab, damit du deinen eigenen Anstellungen nicht verlierst.

  **Experimentiere mit Farben, die du magst!**

  ***Wenn zum Beispiel eine der Farben lediglich nur 128 anstelle 255 in der Schleife erreichen muss, dann verändere die Zahl 1 in der zweiten Position der "Farbe +/- 1" zu 0.5.***

  >Das Leben ist so bunt, wie man es sich ausmalt. :-D


---

#### [Zurück zum Index](#index)

### 3.5. Knopf Spass <a name="button-fun"></a>

In dieser Übung wirst du mit dem Arduino, einem Druckknopf und einer LED Leuchte lernen, wie du Digitale Eingänge nutzt. Wir werden das Licht anschalten, welches nach einer Weile selbst wieder ausgeht.

>Ich bin der Hauslichtmeister!

<img src="images/Lesson5/Lesson5_DigitaleEingaenge_1.png" width="960"/>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte**                   |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 1 | **Rote LED**                      | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/>    | 1 | **220 Ohm Widerstand**            |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>|
| 1 | **10k Ohm Widerstand**            |<img src="images/Parts/Part_Resistor_10KOhm.png" width="256"/>| 3 | **Drucktaster**                     |<img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/> |
| 3 | **M-M Kabel**                     |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponente

***Der Drucktaster/Druckknopf:***

* ist ein Schalter, der beim Drücken die Kontakte verbindet, damit der elektrische Strom durchfliessen kann.
* Die beiden Stifte auf jeder Seite sind jeweils eine Verbindung.


#### Schaltplan

<img src="images/Lesson5/Lesson5_DigitaleEingaenge_2.png" width="512"/><br>

#### Bastelanweisung

1. RGB LED:  2 LED Drahtbeine >> Nebeneinander im unteren Mittelteil
2. Druckknopf:  2 x 2 Pin >> Im Zentrum vom Mittelteil mit 2 Pins oben und 2 Pins unten
3. 220 Ohm Widerstand: In Reihe der "-" Reihe (Blau) >> Unterer Mittelteil unterhalb des kurzen Beins der LED
4. 10 kOhm Widerstand: In Reihe der "-" Reihe (Blau) >> Unterer Mittelteil unterhalb des rechtem Pin des Druckknopfes.
5. Kabel 1: GND (Erde) >> In Reihe der "-" Reihe (Blau)
6. Kabel 2: 5V >> In Reihe der "+" Reihe (Rot)
7. Kabel 3: 13 >> Unterer Mittelteil unterhalb des lagen Beins der LED
8. Kabel 4: In Reihe der "+" Reihe (Rot) >> Unterer Mittelteil unterhalb des linken Pin des Druckknopfes.
9. Kabel 5: 2 >> Oberer Mittelteil oberhalb des rechten Pin des Druckknopfes.

  **Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson5/Lesson5_DigitaleEingaenge.png" width="960"/><br>

1. Als Ersten ziehe den Standardblock **Arduino Vorlauf:, Arduino Endlosschleife:** (Arduino run first:, Arduino loop forever:) aus **Funktion** (Functions) auf deinen Arbeitsbereich.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_3.png" width="960"/><br>

  **Als Nächstes werden wir die LED vorbereiten.**

2. Ziehe **Setze digitale Pin#** (set digital pin#) aus **Eingang/Ausgang** (Input/Output) in **Arduino Vorlauf:** (Arduino run first:). Ändere die Pinnummer zu ***13*** und **HIGH** zu **LOW**.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_4.png" width="960"/><br>

  **LOW bedeutet das LED Licht ist vorerst aus.**

  **Nun wollen wir das Drücken des Schalters als Variable bewahren.**

3. Ziehe einen **Setze Element zu** (set item to) Block unterhalb des **Setze digitale Pin#** (set digital pin#) hinein.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_5.png" width="960"/><br>

4. Ändere **Element** (item) zu **Schalter** (switch) durch das Wählen von **Bennene Variable um...** (Rename variable...).

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_6.png" width="960"/><br>

5. Ziehe den Block **Lese digitale Pin#** (read digital pin#) und verbinde diesen mit der **Setze Schalter zu** (set switch to).

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_7.png" width="960"/><br>

6. Ändere die Pinnummer zu ***2***. Dann kopiere den **Setze Schalter zu Lese digital Pin# 2** (set switch to read digital pin#2) Block und füge diesen in die erste Position von **Arduion Endlosschleife:** (Arduino loop forever:)

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_8.png" width="960"/><br>

  **Wenn wir den Knopf drücken, soll das Licht angehen.**

  ***Dafür nutzen wir einen neuen Block. Diese Methode wird oft im Programmieren genutzt und wird als "if else" Statement bezeichnet.***

  ***Dies bedeutet: Wenn etwas geschieht, dann mache das Folgende!***

7. Ziehe einen **wenn Tue** (if do) Block von **Logik** (Logic) unterhalb **Setze Schalter zu Lese digital Pin# 2** (set switch to read digital pin#2) in die Endlosschleife.

  **Wir benötigen einen weiteren Block, welcher eine Vergleich durchführt.**

8. Ziehe **" "=" "** von **Logik** (Logic) hinein und verbinde den Block zu **wenn** des **wenn Tue** Blocks.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_9.png" width="960"/><br>

  **Wir wollen erfahren, ob der Druckknopf gedruckt ist. Also brauchen wir die Variable mit dem Druckstatus des Knopfes.**

9. Hole dir einen **Element** (item) Block von **Variable** (variables) und stecke diesen in die erste Position von **" "=""**. Klicke auf **Element** (item) und wähle den Namen **Schalter** (switch).

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_10.png" width="960"/><br>

  **Zum Vergleich wollen wir wissen, ob der Strom durch den Schalter fliesst, also der Knopf gedrückt ist.**

10. Benutze dazu den Block **HIGH** von **Eingang/Ausgang** (Input/Output) als zweite Position von **Schalter = " "**.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_11.png" width="960"/><br>

  **Wenn der Schalter gedrückt ist, soll das Licht an und aus gehen.**

11. Dupliziere nun den **Setze digitale Pin# 13** (set digital pin# 13) Block zweimal, und setze beide in die **Tue** (do) Spalte.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_12.png" width="960"/><br>

12. Ändere **LOW** des ersten **Setze digitale Pin# 13** Blocks zu **HIGH**.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_13.png" width="960"/><br>

  **Jetzt muss natürlich noch eine Pause zwischen dem Ein- und Ausschalten gesetzt werden, damit das Licht beim Drücken des Knopfes nicht gleich wieder ausgeht.**

13. Ziehe aus **Zeit** (Time) einen Block **Warte 1000 Millisekunden** (wait 1000 milliseconds) heraus, und platziere diesen zwischen die beiden **Setze digitale Pin# 13** (set digital pin# 13) Blöcke.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_14.png" width="960"/><br>

  **Nun, eine Sekunde ist dann doch zu wenig, um in das Nachbarzimmer zu kommen.**

  **Lass uns also anstelle dessen 8 Sekunden nehmen.**

14. Ändere den Wert **1000** in **Warte 1000 Millisekunden** (wait 1000 milliseconds) zu ***8000***.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_15.png" width="960"/><br>

15. Gib deinem Projekt einen Namen und speichere den Blockcode.
16. Kompiliere und lade den Code auf deinen Arduino.

>Lichtlein, Lichtlein brenne...bis ich am Schlafen bin!

---

#### [Zurück zum Index](#index)

### 3.6. Beat Meister <a name="let-beat-a-rhythm"></a>

In diesem Beispiel werden wir ein Metronom bauen, welches die eingegebenen Schläge pro Minute als Ton wiedergibt.


<img src="images/Lesson6/Lesson6_AktiverBuzzer_1.png" width="960"/>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Aktiver Buzzer**                   |<img src="images/Parts/Part_Electronic_Buzzer.jpg" width="256"/>   |
| 2 | **W-M Kabel**                      | <img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/>    |  |  |  |

#### Neue Komponenten

***Der aktive Buzzer:***

* ist ein elektronischer Summer.
* wird mit Gleichstrom betrieben wird.
* wird häufig in Telefonen, Elektrospielzeug, Alarmsystemen, Druckern, Computern und vielen mehr benutzt.
* hat eine eingebaute Signalquelle und braucht deshalb nur Strom, um einen Ton zu geben.

***Das W-M Kabel:***

* hat auf einer Seite einen Stift und auf der anderen eine Buchse.
* kommt im Bausatz als Breitband mit mehreren dieser Kabel seite-an-seite.

***W-M Kabel >> Ziehe ein Paar, also zwei Kabel gemeinsam, vom Breitband ab.***

#### Bastelanweisung

1. W-M Kabel 1:  12 >> Buzzer "+"
2. W-M Kabel 2:  GND >> Buzzer "-"

#### Block für Block

<img src="images/Lesson6/Lesson6_AktiverBuzzer.png" width="960"/><br>

1. Als Ersten ziehe den Standardblock **Arduino Vorlauf:, Arduino Endlosschleife:** (Arduino run first:, Arduino loop forever:) aus **Funktion** (Functions) auf deinen Arbeitsbereich.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_2.png" width="960"/><br>

2. Ziehe **Setze digitale Pin#** (set digital pin#) aus **Eingang/Ausgang** (Input/Output) in **Arduino Vorlauf:** (Arduino run first:). Ändere die Pinnummer zu ***12*** und **HIGH** zu **LOW**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_3.png" width="960"/><br>

3. Ziehe einen **Setze Element zu** (set item to) Block unterhalb des **Setze digitale Pin#** (set digital pin#) hinein
4. Ändere **Element** (item) zu **BPM** durch das Wählen von **Bennene Variable um...** (Rename variable...).

  **In moderner Musik nutzt man BPM, was Beats Per Minute oder Schläge pro Minute bedeutet.**

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_4.png" width="960"/><br>

5. Setze nun einen **0** Block von **Mathe** (Mathe) an den **Setze BPM zu** (set BPM to) Block.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_5.png" width="960"/><br>

6. Ändere den Wert von **0** zu ***120***.
7. Klicke mit der rechten Maustaste oder halte deinen Finger auf den **Setze BPM zu** (set BPM to) Block mit ***120***, dann wähle **Dupliziere**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_6.png" width="960"/><br>

8. Klicke auf **BPM** der Kopie und wähle **Neue Variable...** (New variable...).
9. Klick erneut auf **i** und wähle diesmal **Benenne Variable um...** (Rename variable...) und gebe ***MilliPB*** ein.

  **Dies bedeutet Milliseconds Per Beat, oder Millisekunden Pro Schlag**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_7.png" width="960"/><br>

10. Ziehe nun den Block **120** vom **Setze MilliPB zu** (set MilliPB to) Block weg, und ersetze dies mit einem **" "+" "** Block von **Mathe** (Math).

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_8.png" width="960"/><br>

11. Setze nun den Block **120** in die erste Position von **" "+" "**.
12. Dann verändere den Wert von **120** zu **60000**.

  **1000 Millisekunden sind 1 Sekunde plus 60 Sekunden macht 60000 Millisekunden.**

  **Als nächstes nehmen wird die Schläge Pro Minute und teilen Sie durch die 60000 Millisekunde, damit wir die Millisekunden pro Schlag erhalten.**

13. Ziehe einen **Element** (item) Block aus **Variable** (Variables) und setze es in die zweite position vom **60000 + " "** Block.
14. Dann klicke auf das **+** Symbol und wähle ***/*** aus.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_9.png" width="960"/><br>

15. Positioniere den **Setze MilliPB zu 60000** (set MilliPB to 60000) in die untereste Stelle von **Arduino Vorlauf:** (Arduino run first).

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_10.png" width="960"/><br>

  **Nun ist es Zeit einen neuen Block zu lernen. Auch dieser ist eine Schleife. Es wiederholt den Inhalt der Schleife die angegebene Anzahl**

  **Wir benutzen diese Schleife um einen Buzzton zu erzeugen.**

16. Ziehe den Block **Wiederhole 10 mal** (repeat 10 times) aus **Schleife** (Loops), und platziere es in **Arduino Endlosschleife:** (Arudino loop forever:).

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_11.png" width="960"/><br>

17. Mache **10** zu ***50*** loops.
18. Dann wähle **Setze digitale Pin# 12** (set digital pin# 12) und dupliziere diesen Block zweimal.
19. Positioniere beide Duplikate in die **Wiederhole 50 mal** (repeat 50 times) Schleife, wobei der Wert des ersten Duplikats von **LOW** auf **HIGH** geändert werden muss.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_12.png" width="960"/><br>

20. Ziehe einen Block **Warte 1000 Millisekunden** (wait 1000 milliseconds) aus **Zeit** (Time), und ändere den Wert von **1000** auf ***2***.
21. Dann erzeuge 2 Kopien von diesem Block und positioniere ein Block jeweils unter  **Setze digitale Pin# 12 HIGH** (set digital pin# 12 HIGH) und  **Setze digitale Pin# 12 LOW** (set digital pin# 12 LOW).

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_13.png" width="960"/><br>

22. Den dritten **Warte 2 Millisekunden** (wait 2 milliseconds) positioniere unterhalb der **Wiederhole 50 mal** (repeat 50 times) Schleife in **Arduino Endlosschleife:**.

  **Die Wiederholschleife geht 50 Zyklen mit jeweils 2 Millisekunden an und 2 Millisekunden aus**

  **Wieviel Millisekunden sind dies als Gesamtzahl?**

23. Zieh nun die **2** aus dem **Warte 2 Millisekunden** (wait 2 milliseconds) Block unterhalb der Wiederholschleife.

  **Wir wollen Buzzer nur zu jedem Takt kurz spielen lassen. Deshalb muss er in der Zwischenzeit ausgeschalten bleiben.**

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_14.png" width="960"/><br>

24. Nehmen wir also einen Rechnungsblock **" " + " "** aus **Mathe** (Math) und setzen es anstelle der **2** Millisekunden ein.
25. Das Symbol von **" " + " "** muss zu ***-*** geändert werden, da wir die Spielzeit des Buzzers vom den Millisekunden zwischen den Schlägen abziehen wollen.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_15.png" width="960"/><br>

  **Die Spielzeit in Millisekunden ist 200 (50 Zyklen * 2 Millisekunden an * 2 Millisekunden aus).**

26. Ändere den Wert **2** zu ***200*** und setze diesen Block in die zweite Stelle vom **" " - " "** Block ein.
27. Dann ziehe einmal **Element** (item) von **Variable** (Variables) und setze es in die erste Position des **" " - 200** Blocks.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_16.png" width="960"/><br>

28. Klicke auf **Element** in **Element - 200** und wähle **MilliPB**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_17.png" width="960"/><br>

29. Speichere dein Projekt.
30. Verifiziere und lade es zu deinem Arduino.

#### *Versuche verschiedene Schläge pro Minute!*

***Verifizieren und hochladen nicht vergessen :-)***

---

#### [Zurück zum Index](#index)

### 3.7. Mini Piano <a name="mini-piano"></a>

>Ich spiel ein kleines Lied für dich!

In diesem Beispiel werden wir aus 5 Druckknöpfen und einem passiven Summer ein kleines Piano bauen, bei welchen du verschiedene Noten einstellen und spielen kannst.

<img src="images/Lesson7/Lesson7_PassiverBuzzer_1.png" width="960"/>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte**                   |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 1 | **Passiver Buzzer**                      | <img src="images/Parts/Part_PassiveBuzzer.jpg" width="256"/>    | 5 | **Druckknopf**            |<img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/>|
| 13 | **M-M Kabel**            |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/>| | | |


#### Neue Komponenten

***Passiver Buzzer:***

* generiert Audiosignale durch einen Pulse (PWM), welcher die Luft zum Vibrieren bring.
* vibriert in der Hertzfrequenz welche den jeweiligen Ton erzeugt.
* darf den Analogen Pin nicht nutzen, da dieser auf 500Hz festgelegt ist.


#### Schaltplan

<img src="images/Lesson7/Lesson7_PassiverBuzzer_2.png" width="960"/><br>

#### Bastelanweisung

1. Passiver Buzzer:  **+** und **-** Pin >> Horizontal im oberen Mittelteil
2. 5 Druckknöpfe nebeneinander (3 Stecklöcher Abstand): Über Trennlinie zwischen oberen und unteren Mittelteil
2. M-M Kabel 1: Oberer Mittelteil über Buzzer **+** >> Arduino Pin 4
3. M-M Kabel 2: Oberer Mittelteil über Buzzer **-** >> Obere blaue Reihe der Steckplatte
4. M-M Kabel 3: Obere blaue Reihe der Steckplatte >> Arduino GND
5. M-M Kabel 4: Oberhalb des linken Pins der Taste 1 im oberen Mittelteil >> Obere blaue Reihe der Steckplatte
6. M-M Kabel 5: Oberhalb des linken Pins der Taste 2 im oberen Mittelteil >> Obere blaue Reihe der Steckplatte
7. M-M Kabel 6: Oberhalb des linken Pins der Taste 3 im oberen Mittelteil >> Obere blaue Reihe der Steckplatte
8. M-M Kabel 7: Oberhalb des linken Pins der Taste 4 im oberen Mittelteil >> Obere blaue Reihe der Steckplatte
9. M-M Kabel 8: Oberhalb des linken Pins der Taste 6 im oberen Mittelteil >> Obere blaue Reihe der Steckplatte
10. M-M Kabel 9: Oberhalb des rechten Pins der Taste 1 im oberen Mittelteil >> Arduino Pin 7
11. M-M Kabel 10: Oberhalb des rechten Pins der Taste 2 im oberen Mittelteil >> Arduino Pin 8
12. M-M Kabel 11: Oberhalb des rechten Pins der Taste 3 im oberen Mittelteil >> Arduino Pin ~9
13. M-M Kabel 12: Oberhalb des rechten Pins der Taste 4 im oberen Mittelteil >> Arduino Pin ~11
14. M-M Kabel 13: Oberhalb des rechten Pins der Taste 5 im oberen Mittelteil >> Arduino Pin 12

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**


#### Block für Block

<img src="images/Lesson7/Lesson7_PassiverBuzzer.png" width="960"/><br>

1. Als Ersten ziehe den Standardblock **Arduino Vorlauf:, Arduino Endlosschleife:** (Arduino run first:, Arduino loop forever:) aus **Funktion** (Functions) auf deinen Arbeitsbereich.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_3.png" width="960"/><br>

  **Als ersten werden wir die Noten, die wir spielen wollen, als Variablen darstellen.**

2. Ziehe einen **Setze Element zu** (set item to) Block in die Vorlaufspalte.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_4.png" width="960"/><br>

3. Als Nächstes benötigen wir eine Zahl **0** von **Mathe** (Math) als Wert für unsere Variable.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_5.png" width="960"/><br>

4. Benenne die Variable **Element** (item) zu ***Note1***.

  ***Wie vorher, klicke auf "Element" (item) und wähle "Benenne Variable um..." (Rename variable...).***

5. Ändere den Zahlenwert zu ***262***.

  **262 ist in der Musik die Frequenz für das mittlere C.**

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_6.png" width="960"/><br>

  **Wir wollen nun für die weiteren 4 Tasten Noten bestimmen.**

6. Kopiere hierfür **Setze Note1 zu 262** viermal.

  ***Wie vorher, klicke mit der rechten Maustaste auf den Block oder halte den Finger für 2 Sekunde darauf, und wähle "Dupliziere" (Duplicate).***

7. Klicke dann auf **Note1** jeder Kopie und presse **Neue Variable...** (New variable...).

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_7.png" width="960"/><br>

8. Klicke erneut, diesmal auf **i**, **j**, **k**, **m**. Wähle **Benenne Variable um...** (Rename variable...) und ändere die Variablen nacheinander zu ***Note2***, ***Note3***, ***Note4*** und ***Note5***.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_8.png" width="960"/><br>

  **Die folgende Tabelle zeigt die musikalischen Noten und ihre Frequenzen**

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_9_NoteFreqList.png" width="960"/><br>

  **Wir werden ersteinmal die Noten der Oktave 4 verwenden. C, D, E, G, A**

9. Ändere die Werte der vier neuen Noten zu ***294***, ***329***, ***392*** und ***440***.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_9.png" width="960"/><br>

  **Nun werden wir die eigentlichen Tasten zuweisen. Zuerst als Varialben, sodass der Arduino die Info erhält, sobald du einen der Tasten drückst.**

10. Ziehe einen **Setze Element zu** (set item to) Block von **Variable** (Variables) in den Spalt der Endlosschleife.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_10.png" width="960"/><br>

  **Jetzt brauchen wir den Block der den digitalen Eingang liest, an den der Knopf angeschlossen ist.**

11. Verbinde einen **Lese digital mit PULL_UP Modus Pin# 0** (read digital with PULL_UP mode pin# 0) Block von **Eingang/Ausgang** (Input/Output) mit dem neuen Variablenblock in der Endlosschleife.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_11.png" width="960"/><br>

12. Bennene **Element** (item) zu ***Taste1*** um und ändere den Wert der Pinnummer **0** zu ***7***.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_12.png" width="960"/><br>

13. Dupliziere nun diesen Variablen Block viermal und setze diese untereinander in der Endlosschleife ein.
14. Wie bei den Noten, addiere neue Variablen und benenne diese zu ***Taste2***, ***Taste3***, ***Taste4*** und ***Taste5*** um.

  **Danach musst du die Tasten den jeweiligen Pinnummern zuweisen.**

15. Ändere die Pinnummern der vier neuen Tasten zu ***8***, ***9***, ***11*** und ***12***.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_13.png" width="960"/><br>

  **Jetzt werden wir wieder den Statementblock nutzen, um den Ton zu spielen, wenn die Taste gedrückt ist.**

  ***Wir werden mehr Konditionen hinzufügen, damit wir alle Tasten gleichzeitig abrufen können. Die Erweiterung werden zu folgender Logik führen: Wenn...andernfalls wenn...andernfalls wenn...sonst.***

16. Ziehe den Block **Wenn Tue** (if do) von **Logik** (Logic) unter die Tasten in der Endlosschleife.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_14.png" width="960"/><br>

17. Drücke nun auf das kleine Zahnradsymbol, und zieh viermal **andernfalls** (else if) von Links nach Rechts unterhalb des **wenn** (if) Bocks.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_15.png" width="960"/><br>

  **Der *Wenn Tue* Block in der Endlosschleife addiert neue Spalten.***

18. Ziehe als Abschluss noch einmal **sonst** (else) von Links nach Rechts unterhalb des letzten **andernfalls** (else if).
19. Schliesse das Fenster, indem du erneut auf das Zahnradsymbol klickst.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_16.png" width="960"/><br>

  **Wie im vorherigen Beispiel mit dem aktiven Buzzer, vergleichen wir den Status der Taste um zu erfahren, ob der Strom durchs drücken fliesst.**

20. Aus **Logik** (Logic) ziehe den Block **" " = " "** und verbinde diesen mit **Wenn** (if) des **Wenn Tue** (if do) Blocks.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_17.png" width="960"/><br>

21. Nutze einen **Element** (item) Block aus **Variable** (Variables) als erste Position von **" " = " "**.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_18.png" width="960"/><br>

22. Als zweite Position von **Element = " "** ziehe einen **HIGH** Block aus **Eingang/Ausgang** (Input/Output) hinein.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_19.png" width="960"/><br>

23. Ändere **Element = HIGH** zu ***Taste1 = LOW***.
24. Kopiere nun viermal den **Taste1 = LOW** Block.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_20.png" width="960"/><br>

25. Schliesse die neuen vier Kopien jeweils an eine **andernfalls** Verbindung.
26. Dann ändere die Variablennamen zu den Taste 2 - 5.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_21.png" width="960"/><br>

  **Nun wollen wir den passenden Ton spielen, wenn die jeweilige Taste gedrückt wird.**

27. Gehe zu **Audio**, und ziehe **Setze Ton zu pin# 0 auf Frequenz 220** (Set tone on pin# 0 at freqency 220) in die erste **Tue** (do) Spalte.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_22.png" width="960"/><br>

  **Die Frequenz ist als Standard auf 220 eingestellt. Wir wollen jedoch jeder Taste unser Note zuweisen, welche wir bereits durch Variablen gesetzt haben.**

28. Hole dir einen **Element** (item) Block aus **Variable** (Variables) und lege diesen Block auf die Stelle von **220**.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_23.png" width="960"/><br>

29. Ändere die Pinnummer **pin#** von **0** zu **4**.

  **Da ist der Summer angeschlossen.**

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_24.png" width="960"/><br>

30. Kopiere nun **Setze Ton zu pin# 4 auf Frequenz Note1** viermal, und füge diese jeweils in eine der **Tue** (do) Verbindungen.
31. Wechsle **Note1** jeweils zu ***Note2***, ***Note3***, ***Note4*** und ***Note5***.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_25.png" width="960"/><br>

  **Wenn du es jetzt ausprobierst, würde der Ton permanent spielen, nachdem du die Taste gedrückt hast.**

  **Deshalb haben wir "sonst". Wenn du nämlich keine Taste pressed, soll auch kein Ton gespielt werden.**

32. Ziehe den Block **Stoppe Ton von Pin# 0** (Turn off tone on pin# 0) in den letzten freien Spalt **sonst** (else).
33. Auch hier muss die Pinnummer von **0** zu **4** geändert werden.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_26.png" width="960"/><br>

34. Benenne dein Pianoprojekt und speichere es ab.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_27.png" width="960"/><br>

35. Kompiliere das Projekt und lade es auf deinen Arduino.

#### *Verändere die Frequenzen. Wenn du zu Zweit oder zu Dritt spielst, macht das mehr Spass verschiedene Noten zu spielen.*

***Mit Blöcken aus Mathematik und Zeit kannst du mit jeder Note sogar eine Sequenz spielen.***

---

#### [Zurück zum Index](#index)

### 3.8. Sam sagt 'Beweg deinen Arm!' <a name="sam-says-move-your-arm"></a>

Nehme mal an du hast einen Roboterarm, der sich bewegen soll, wenn du deinen Arm bewegst. In einer bestimmten Neigung wird sich beginnen der Motor bewegen.

>Tanz den Roboter!

<img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_1.png" width="960"/>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Neigungssensor**                   |<img src="images/Parts/Part_TiltSensor.jpg" width="256"/>   |
| 1 | **Servomotor SG90**                      | <img src="images/Parts/Part_ServoMotor.jpg" width="256"/>    | 3 | **M-M Kabel**            |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/>|
| 2 | **W-M Kabel**            |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/>|


#### Neue Komponenten

***Neigungssensor:***

* stellt eine momentane Ausrichtung oder Neigung fest.
* besteht aus einem Hohlraum mit einem beweglichen leitfähigen Material.
* hat auf einer Seite des Hohlraums elektrische Pole.
* verbindet die Pole durch die Masse in einem bestimmten Winkel oder Neigung.
* kann grobe Neigungen und Erschütterungen wahrnehmen.

***Servomotor:***

* sind Motoren, die sich um 180 Grad drehen lassen.
* werden durch das Senden von elektronischen Impulsen gesteuert.
* bestimmen die Position durch die empfangenen Impulse.

#### Schaltplan

<img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_2.png" width="960"/><br>

#### Bastelanweisung

1. W-M Kabel 1:  *+* Pol Neigungssensor >> Arduino Pin 2
2. W-M Kabel 2:  *-* Pol Neigungssensor >> Arduino GND (Erde)
3. M-M Kabel 1:  Data Pin Servermotor >> Arduino Pin ~9
4. M-M Kabel 2:  *+* Pol Servomotor >> Arduino 5 V
5. M-M Kabel 3:  *-* Pol Servomotor >> Arduino GND (Erde)

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor.png" width="960"/><br>

1. Ziehe den Basisblock mit Vorlauf und Endlosschleife auf deinen Arbeitsbereich.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_3.png" width="960"/><br>

  **Lass uns den Servormotor einrichten und einen Ausgangsposition geben.**

2. Ziehe dazu einen **Setze SERVO zum Pin 0 zu 90 Winkelgrad** (set SERVO from pin 0 to 90 Degrees) Block in den Vorlaufabschnitt des Basisblocks.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_4.png" width="960"/><br>

3. Lege die Pinnummer auf ***9*** fest.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_5.png" width="960"/><br>

  **Wie mit den Druckknopf, wollen wir das Schalten durch den Neigungssensor als Variable setzen.**

4. Gehe zu **Variable** (Variables) und ziehe einen **Setze Element zu** (set item to) Block in die Endlosschleife.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_6.png" width="960"/><br>

5. Dann ziehe einen **Lese digital Pin# 0** (read digital pin# 0) Block von **Eingang/Ausgang** (Input/Output) und verbinde diesen Block mit **Setze Element zu** (set item to).

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_7.png" width="960"/><br>

6. Benenne **Element** (item) in ***Neigung*** um und wähle die Pinnummer ***2***.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_8.png" width="960"/><br>

  **Wie im vorhergehenden Beispiel werden wir eine Statementmethode nutzen, um den Motor anzuschalten, sobald der Neigungssensor den Strom fliessen lässt.**

7. Hole dir einen **wenn Tue** (if do) Block von **Logik** (Logic) und füge diesen unterhalb der **Neigung** variable in die Endlosschleife.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_9.png" width="960"/><br>

  **Wir benötigen eine zweite Spalte "sonst", da wir den Motor wieder zurückfahren wollen, wenn der Schalter aus ist.**

8. Dazu klick wieder auf das Zahnradsymbol, und ziehe einen **sonst** (else) Block von Links nach Rechts und verbinde ihn mit **wenn** (if).

  ***Klicke erneut auf das Zahnradsymbol, um das Fenster wieder zu schliessen.***

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_10.png" width="960"/><br>

  **Wieder wollen wir den Status des Schalters checken.**

9. Deshalb ziehe einen **" " = " "** Block aus **Logik** (Logic) und verbinde ihn mit der **Wenn** (if) Verbindung.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_11.png" width="960"/><br>

10. Als nächstes setze einen **Element** (item) Block von **Variable** (Variables) in die erste Position von **" " = " "**.
11. Wähle für **Element** im Menü ***Neigung***.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_12.png" width="960"/><br>

12. Und wieder gehen wir holen wir uns in **Eingang/Ausgang** (Input/Output) den Block **HIGH** für die zweite Position von **Neigung = ""**.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_13.png" width="960"/><br>

  **Wenn der Neigungsensor sich einschaltet, soll sich der Motor um 90 Grad drehen. Ansonsten soll er wieder in die Ausgangsposition zurückbewegen.**

13. Dupliziere den **Setze SERVO zum Pin 9 zu 90 Winkelgrad** (set SERVO from pin 9 to 90 Degrees) Block zweimal, und setze jeweils einen Block in den **Tue** (do) und **sonst** (else) Spalte.
14. Ändere **90** des Blocks in **Tue** (do) zu ***180***.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_14.png" width="960"/><br>

  **Zum Abschluss soll die Schleife eine halbe Sekunde warten, bis der Status vom Neigungswinkel neu abgerufen wird. Damit kann man kleine Erschütterungen überbrücken.**

15. Setze einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block unterhalb des **Setze SERVO zum Pin 9 zu 180 Winkelgrad** in **Tue** ein.
16. Stelle die Zeit von **1000** auf ***500*** Millisekunden um.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_15.png" width="960"/><br>

17. Benenne dein Projekt und speichere den Blockcode ab.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_16.png" width="960"/><br>

18. Kompiliere den Code und lade ihn auf deinen Arduino.


>Tanz den Roboter!

---

#### [Zurück zum Index](#index)

### 3.9. Mehr spielt die Musik! <a name="lets-make-some-more-music"></a>

Diesmal wollen wir ein neues Musikinstrument bauen, das du mit den freien Händen oder mit einem Blatt Papier in der Luft spielst. Durch deine Bewegung veränderst du die Tonhöhe.

<img src="images/Lesson10/Lesson10_UltraschallSensor_1.png" width="960"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Passive Buzzer**                   |<img src="images/Parts/Part_PassiveBuzzer.jpg" width="256"/>   |
| 1 | **Ultraschallsensormodul**                      | <img src="images/Parts/Part_UltraSonicSensor.jpg" width="256"/>    | 6 | **W-M Kabel**            |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/>|


#### Neue Komponenten

***Ultraschallsensormodul:***

* kann Entfernungen von 2-400 cm mit einer Genauigkeit bis zu 3mm zu messen.
* beinhaltet eine Sender, Empfänger und einen Steuerschaltkreis.
* sendet 40 kHz Schallimpulse und prüft, ob ein Signal zurückkommt.
* misst den Abstand durch die Relation der Schallgeschwindigkeit und der halbierten Zeit, die das Signal braucht bis es wieder empfangen wird.


#### Schaltplan

<img src="images/Lesson10/Lesson10_UltraschallSensor_2_Schematic.png" width="960"/><br>

#### Bastelanweisung

1. W-M Kabel 1: *+* Pol Passive Buzzer >> Arduino Pin 4
2. W-M Kabel 2: *-* Pol Passive Buzzer >> Arduino GND (Erde)
3. W-M Kabel 3: VCC Pin Ultraschallsensormodul >> Arduino 5V
4. W-M Kabel 4: Trig Pin Ultraschallsensormodul >> Arduino Pin 12
5. W-M Kabel 5: Echo Pin Ultraschallsensormodul >> Arduino Pin ~11
6. W-M Kabel 6: GND Pin Ultraschallsensormodul >> Arduino GND (Erde)

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson10/Lesson10_UltraschallSensor.png" width="960"/><br>

1. Starte mit dem Basisblock mit Vorlauf und Endlosschleife aus **Funktion** (Functions).

  <img src="images/Lesson10/Lesson10_UltraschallSensor_3.png" width="960"/><br>

  **Mit dem folgenden Schritt werden wir den Ultraschallsensor einrichten.**

2. Ziehe einen **Setup HC-SR04 mit Echo Pin# 0 und Auslöser Pin# 0** (Set HC-SR04 with Echo Pin# 0 and Trigger Pin# 0) Block aus **Sensor** (Sensors) und platziere diesen in den Vorlaufteil des Basisblocks.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_4.png" width="960"/><br>

3. Setze die Echo Pinnummer zu ***11*** und die Auslöser Pinnummer zu ***12***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_5.png" width="960"/><br>

  **Als weiterer Wert wollen wir die Startfrequenz des Musikinstrument mit einer Variable festlegen.**

4. Hole dir einen **Setze Element zu** (set item to) Block von **Variable** (Variables) und lege diesen im Vorlauf des Basisblocks ab.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_6.png" width="960"/><br>

5. Nenne die Variable in ***StartFreq*** um.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_7.png" width="960"/><br>

6. Hole dir jetzt einen **0** Block aus **Mathe** (Math) und verbinde diesen mit **Setze StartFreq zu** (set StartFreq to).

  <img src="images/Lesson10/Lesson10_UltraschallSensor_8.png" width="960"/><br>

7. Definiere die Frequenz indem du Nummer ***400*** einträgst.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_9.png" width="960"/><br>

  **Nun wollen wir uns den gemessenen Abstand vom Ultraschallsensor als Variable aktualisieren.**

8. Ziehe dir einen neuen **Setze Element zu** (set item to) Block aus **Variable** (Variables) und setze diesen in die Endlosschleife ein.
9. Benenne **Element** in ***a*** (Abstand) um.
10. Gehe zu **Sensor** (Sensors) und hole dir **Lese HC-SR04 vom Echo Pin# 0** (Read HC-SR04 with Echo Pin# 0), um dies mit **Setze a zu** (set a to) zu verbinden.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_10.png" width="960"/><br>

11. Passe die Echo Pinnummer zu ***11*** an.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_11.png" width="960"/><br>

  **Nun wollen wir den gemessenen Abstand ins Verhältnis mit einem Frequenzbereich bringen.**

  **Das bedeutet, dass der Buzzer nicht den Abstand als Frequenz spielt, sondern eine höhere Nummer die im Vergleich zum Abstand steht.

12. Nehme dazu auch hier als Grundlage einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) und positioniere diesen in die Endlosschleife an zweiter Stelle.
13. Nenne diese Variable ***Note***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_12.png" width="960"/><br>

14. Um das Verhältnis zu erzeugen, gehe zu **Mathe** (Math) und verbinde einen **Relativiere** (Map) Block mit **Setze Note zu** (set Note to).

  <img src="images/Lesson10/Lesson10_UltraschallSensor_13.png" width="960"/><br>

  **Relativiere hat 5 Werte zu belegen. Der 1. Wert ist der Wert, der ins Verhältnis gebracht werden soll. Der 2. und 3. Wert beschreibt den Bereich in der sich der 1. Wert bewegen wird. Der 4. und 5. Wert ist der Bereich in welchem der 1. Wert relativiert werden soll.**

  ***Stell dir das so vor, wie eine Landkarte. Es gibt einen Massstab, in welche 1 cm auf der Mappe in Wirklichkeit ein 1 km ist. Die Mappe selber ist vielleicht von 0 - 50 cm. Relativ dazu ist die wirklich Landschaft 0-50 km. Die Position deines Fingers, welche auf die Mappe bei 20 cm tippt, ist relativ 20 km.***

  **Der erste Wert wird die Variable a sein.**

15. Nehme aus **Variable** (Variables) einen **Element** (item) Block und setze diesen in die erste Position von **Relativiere** (Map) ein.
16. Wähle vom Menü **a** als Variablenname.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_14.png" width="960"/><br>

  **Der 2. und 3. Wert soll der Abstand sein, den du zum Musizieren nutzt.**

17. Benutze zwei **0** Blocks von **Mathe** (Math) als 2. und 3. Position von **Relativiere** (Map).
18. Trage ***10*** als untersten Bereichswert und ***150*** als obersten Bereichswert ein.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_15.png" width="960"/><br>

  **Den 4. und 5. Wert wollen wir etwas flexibler gestalten, sodass du später die Frequenz einfacher ändern kannst.**

19. Erneut, hole dir einen **Element** (item) Block von **Variable** (Variables) und wähle den Namen ***StartFreq*** vom Variablenmenü.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_16.png" width="960"/><br>

20. Dupliziere **StartFreq** einmal und positioniere diesen Block in die 4. Position von **Relativiere** (Map).

  <img src="images/Lesson10/Lesson10_UltraschallSensor_17.png" width="960"/><br>

  **Damit sich der Bereich mit der Änderung der Startfrequenz verschiebt, werden wir eine Mathegleichung nutzen.**

  **Im Beispiel werden wir die Frequenz verdoppeln, damit wir mit unserem Instrument eine komplette Oktave als Bereich haben.**

21. Setze einen **" " + " "** Block von **Mathe** (Math) als 5. Wert von **Relativiere** (Map).
22. Dann nehme den losen **StartFreq** Block auf deinem Arbeitsbereich und füge diesen in die erste Position von **" " + " "**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_18.png" width="960"/><br>

23. Klicke auf das **+** Symbol und wähle anstelle dessen ***\****.
24. Dann nutze einen neuen **0** Block von **Mathe** (Math) als zweite Position von **StartFreq * " "***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_19.png" width="960"/><br>

25. Ändere den Wert von **0** auf **2**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_20.png" width="960"/><br>

  **Nun werden wir die Note als Zahl haben. Jetzt werden wir die Zahl in die Buzzer speissen, damit der den Ton von sich gibt.**

  **Ausserdem soll der Buzzer stoppen, wenn deinen Hände oder Papier nicht im Spielbereich sind.**

  ***Wie beim Druckknopf und Neigungssensor werden wir ein if/else Statement (wenn/dann) nutzen.***

26. Ziehe einen **wenn Tue** (if do) Block aus **Logik** (Logic) und plaziere diesen als untersten Block der Endlosschleife.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_21.png" width="960"/><br>

27. Presse das Zahnradsymbol, dann ziehe einen **andernfalls** (else if) und dann einen **sonst** (else) Block von Links nach Rechts.

  ***Schliesse das Fenster durch das erneute Drücken des Zahnradsymbols.***

  <img src="images/Lesson10/Lesson10_UltraschallSensor_23.png" width="960"/><br>

28. Verbinde wieder einen **" " = " "** Block von **Logik** (Logic) mit **wenn**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_24.png" width="960"/><br>

29. Ziehe einen **Element** (item) Block von **Variable** (Variables) und benutze diesen als erste Position von **" " = " "**.
30. Wähle ***a*** als Variablenname.
31. Dann ziehe einen **0** Block von **Mathe** (Math) für die zweite Position von **a = " "**, und trage die Nummer **10** ein.
32. Jetzt klick on das **=** Symbol und ändere es zu ***<***.

  ***Alles was unter also kleiner als 10 cm ist, soll keinen Ton erzeugen.***

  <img src="images/Lesson10/Lesson10_UltraschallSensor_25.png" width="960"/><br>

33. Dupliziere **a < 10**, und setze die Kopie in **andernfalls** ein.
34. Dann ändere das Symbol **<** zu ***>*** und den Wert von **10** zu ***150***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_26.png" width="960"/><br>

  **Um diesen Teil fertig zu stellen, brauchen wir noch den Block der den Ton abstellt.**

35. Von **Audio** hole dir einen **Stoppe Ton von Pin# 0** (Turn tone off on pin# 0) und setze ihn in den ersten **Tue** Spalt ein.
36. Definiere die Pinnumer des angeschlossenen Buzzer durch ***4***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_27.png" width="960"/><br>

37. Dupliziere **Stoppe Ton von Pin# 4** (Turn tone off on pin# 4) und platziere die Kopie in die zweite Spalte **Tue**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_28.png" width="960"/><br>

  **Jetzt lass uns den eigentlichen Tonmacher einsetzen.**

38. Ziehe einen **Setze Ton zu pin# 0 auf Frequenz 220** (Set ton on pin# 0 by frequency 220) in die **sonst** Spalte.
39. Verändere die Pinnummer zu ***4***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_29.png" width="960"/><br>

  **Jetzt holen wir uns die errechnete Frequenz durch die Variable Note.**

40. Ersetze **220** mit einem **Element** (item) Block aus **Variable** (Variables).

  <img src="images/Lesson10/Lesson10_UltraschallSensor_30.png" width="960"/><br>

41. Wähle für **Element** (item ) ***Note*** aus dem Variablenmenü.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_31.png" width="960"/><br>

  **Der Buzzer funktioniert besser, wenn man dem Messen des Abstandes etwas Zeit gibt, dafür den Ton etwas spielen lässt.**

42. Ziehe einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) aus **Zeit** (Time).
43. Reduziere die Zeit von **1000** auf **100**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_32.png" width="960"/><br>

44. Gib dem Musikinstrumentprojekt einen Namen und speichere es ab.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_33.png" width="960"/><br>

45. Kompiliere den Code und lade ihn auf deinen Arduino hoch.


#### *Versucht unterschiedliche Startfrequenzen, wie zum Beispiel die Hälfte oder ein Viertel Abstand von einander.*

---

#### [Zurück zum Index](#index)

### 3.10. Hört sich an wie ein Handy! <a name="sounds-like-a-phone"></a>

In der folgenden Übung werden wir 3 Tasten der Membrantastatur mit 3 verschiedenen Tönen ausstaffieren.

<img src="images/Lesson11/Lesson11_MembraneKeypad_1.png" width="960"/><br>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Passive Buzzer**                   |<img src="images/Parts/Part_PassiveBuzzer.jpg" width="256"/>   |
| 1 | **4 x 4 Membran Tastatur**                      | <img src="images/Parts/Part_4x4MembraneKeypad.jpg" width="256"/>    | 2 | **W-M Kabel**            |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/>|
| 8 | **M-M Kabel**                     |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***4 x 4 Membrantastatur:***

* ist eine eine Folientastatur, welche in Telefonen, Faxgeräten, Mikrowellen, Öfen, Türschlössern, usw. genutzt werden.
* hat 16 Tasten (0-9, A-D und * und #), aber nur 8 Anschlüsse. Vergleichbar mit einer Tabelle mit Zeilen und Spalten kommt die Tastatur mit der Hälfte der Anschlüsse aus.

#### Schaltplan

<img src="images/Lesson11/Lesson11_MembraneKeypad_2.png" width="960"/><br>

#### Bastelanweisung

1. W-M Kabel 1: *+* Pol Passiver Buzzer >> Arduino Pin 12
2. W-M Kabel 2: *-* Pol Passiver Buzzer >> Arduino GND (Erde)
3. M-M Kabel 1: Membrantastatur Pin 1 >> Arduino Pin ~9
4. M-M Kabel 2: Membrantastatur Pin 2 >> Arduino Pin 8
5. M-M Kabel 3: Membrantastatur Pin 3 >> Arduino Pin 7
6. M-M Kabel 4: Membrantastatur Pin 4 >> Arduino Pin ~6
7. M-M Kabel 5: Membrantastatur Pin 5 >> Arduino Pin ~5
8. M-M Kabel 6: Membrantastatur Pin 6 >> Arduino Pin 4
9. M-M Kabel 7: Membrantastatur Pin 7 >> Arduino Pin ~3
10. M-M Kabel 8: Membrantastatur Pin 8 >> Arduino Pin 2

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson11/Lesson11_MembraneKeypad.png" width="960"/><br>

1. Wie immer starte mit der Basisfunktion.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_3.png" width="960"/><br>

  **Die Membrantastatur benötigt 8 Eingänge, als 8 Pin Zuweisungen, um zu funktionieren.**

2. Ziehe einen **Setup 4x4 Memrane Keypad** Block von **Eingang/Ausgang** (Input/Output) in die Vorlaufschleife.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_4.png" width="960"/><br>

3. Klicke auf die jeweiligen Reihenzuweisungen (row) und weise ***6**, ***7***, ***8*** und ***9*** zu.
4. Dann weise ***2***, ***3***, ***4*** und ***5*** auf die Spalten (col) zu.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_5.png" width="960"/><br>

5. Ziehe nun einen **Setze Element zu " " als Character** (set item to " " as Character) von **Variable** (Variables) in die Endlosschleife.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_6.png" width="960"/><br>

6. Benenne **Element** in ***Key*** und **Character** to ***Text***.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_7.png" width="960"/><br>

7. Ziehe einen **Erhalte Tastendruck vom mkpad4x4 auf Reihe 1 pin#** (Receive key from mkpad4x4 on row 1 pin#) von **Eingang/Ausgang** (Input/Output) in " " von **Setze Key zu " " als Text** (set Key to " " as Text).

  <img src="images/Lesson11/Lesson11_MembraneKeypad_8.png" width="960"/><br>

8. Ändere die Pinnummer von *Erhalte Tastendruck vom mkpad4x4 auf Reihe 1 pin#** (Receive key from mkpad4x4 on row 1 pin#) zu ***6***.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_9.png" width="960"/><br>

  **Jetzt nutzen wir wieder eine Statementfunktion, um den jeweiligen Tasten beim Drücken eine spezifische Aufgabe zuzuweisen.**

9. Ziehe dazu einen **Wenn Tue** (if do) Block aus **Logik** (Logic) in die Endlosschleife.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_10.png" width="960"/><br>

  **Wir werden in unserem Beispiel lediglich 3 Tasten mit Aufgaben belegen.**

  ***Du kannst weitere Tasten selbst hinzufügen, addiere nur mehr der Spalten hinzu!***

10. Klicke auf das Zahnradsymbol der **Wenn Tue** (if do) Funktion, und ziehe zwei **anderenfalls** und einen **sonst** Block nach Rechts unterhalb von **wenn**.

  ***Schliesse das Menü in du erneut auf das Zahnradsymbol klickst!***

  <img src="images/Lesson11/Lesson11_MembraneKeypad_11.png" width="960"/><br>

11. Ziehe aus **Logik** (Logic) einen **" " = " "** Block und verbinde diesen mit **wenn**.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_12.png" width="960"/><br>

12. Nun ziehe einen **Element** (item) Block von **Variable** (Variables) in die erste Position von **" " = " "**.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_13.png" width="960"/><br>

  **Wir wollen die gepresste Taste der Membrantastatur mit der Zahl oder dem Zeichen als Text vergleichen.**

13. Ziehe aus **Text** einen **" "** Block und setze ihn als zweite Position von **Element = " "** (item = " ").

  <img src="images/Lesson11/Lesson11_MembraneKeypad_14.png" width="960"/><br>

14. Ändere **Element** (item) zu ***Key***, und fülle eine 1 in **" "**.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_15.png" width="960"/><br>

  **Wie bei einem Handy, wollen wir nun einen Ton erklingen lassen, wenn die Taste 1 auf der Membrantastatur gedrückt wird.**

15. Ziehe dafür den **Setze Ton zu pin# 0 auf Frequenz 220** (Set ton on pin# 0 to frequency 220) aus **Audio** in den ersten **Tue** Spalt.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_16.png" width="960"/><br>

  **Der Ton soll ausgeschalten bleiben, wenn keine Taste der Membrantastatur gedrückt ist.**

16. Ziehe einen **Stoppe Ton von Pin # 0** (Turn off tone on pin# 0) aus **Audio** und setze diesen in die letzte Spalte **sonst** (else).
17. Ändere beide Pinnummern zu ***12***.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_17.png" width="960"/><br>

  **Der Ton soll eine kurze Zeit ertönen, auch wenn du die Taste bereits nicht mehr drückst.**

18. Ziehe einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) aus **Zeit** (Time) unterhalb des **Setze Ton zu pin# 12 auf Frequenz 220** (Set ton on pin# 12 to frequency 220) Blocks.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_18.png" width="960"/><br>

19. Verkleinere den Wert **1000** zu ***100***.
20. Nun kopiere alles unter **Wenn** (if) und **Tue** (do) zweimal und fühle es in die jeweilige **anderenfalls** und dessen **Tue** ein.
21. Anstelle der **1** im Text der **anderenfalls** Verbindung trage ***2*** und ***3*** ein.
22. Trage die Frequenz ***680*** für **2** und ***880*** für **3** in **Setze Ton zu pin# 12 auf Frequenz 220** (Set ton on pin# 12 to frequency 220) ein.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_19.png" width="960"/><br>

23. Benenne dein Projekt, kompiliere es und lade es auf deinen Arduino.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_20.png" width="960"/><br>

  #### *Füge weitere Zahlentasten zur "Wenn Tue" (if do) Statementfunktion, und mache eine kleine Melodie daraus!*


---

#### [Zurück zum Index](#index)

### 3.11. Wie heiss ist es? <a name="how-hot-is-it"></a>

In dieser Übung wirst du ein Modul kennenlernen, mit dem du die Lufttemperatur und Luftfeuchtigkeit messen kannst.

Hier werden wir lediglich die Temperaturmessung nutzen.

>Rot hauchen und Blau pusten!

<img src="images/Lesson12/Lesson12_TemperatureSensor_1.png" width="960"/><br>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte**                   |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 1 | **RGB LED**                      | <img src="images/Parts/Part_LED_RGB_5mm.jpg" width="256"/>    | 3 | **220 Ohm Widerstand**            |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>|
| 1 | **DHT11 Temperatur- und Luftfeuchtigkeitssensor**            |<img src="images/Parts/Part_DHT11_TemperatureHumiditySensor.jpg" width="256"/>| 7 | **M-M Kabel**                     |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***DHT11 Temperatur- und Luftfeuchtigkeitssensor:***

* ist ein Verbundsensor, der ein digitales Signal mit den Werten für Temperatur und Luftfeuchtigkeit ausgibt.
* hat ein NTC Temperaturmessgerät, das an ein 8-bit Mikrokontroller angeschlossen ist.
* wird in Heizungen, Lüftungen, Klimatechnik, Luftentfeuchter, Prüf- und Inspektionsausrüstung, Automobilbau, Wetterstationen, Haushaltsgeräten, Feuchtigkeitsregulatoren und medizinische Geräten benutzt.


#### Schaltplan

<img src="images/Lesson12/Lesson12_TemperatureSensor_2.png" width="960"/><br>

#### Bastelanweisung

1. RGB LED:  4 LED Drahtbeine >> nebeneinander im unterem Mittelteil.
2. M-M Kabel 1:  Mittelteilloch vor langem Drahtbein >> Arduino GND (Erde)
3. 3 Widerstände: Vor jedem Fuss im unteren Mittelteil >> Längsposition im oberen Mittelteil
4. M-M Kabel 2: Vor Widerstand rechts vom GND Kabel im oberen Mittelteil >> Arduino ~6
5. M-M Kabel 3: Vor Widerstand links vom GND Kabel im oberen Mittelteil >> Arduino ~5
6. M-M Kabel 4: Vor Widerstand links neben 3. Kabel im oberen Mittelteil >> Arduino ~3
7. DHT11 Modul: Nebeneinander im unterem Mittelteil.
8. M-M Kabel 5: Oberhalb Data Pin im unteren Mittelteil >> Arduino Pin 2
9. M-M Kabel 6: Oberhalb VCC Pin (Mitte) im unteren Mittelteil >> Arduino 5V
10. M-M Kabel 7: Oberhalb GND Pin im unteren Mittelteil >> Arduino GND

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson12/Lesson12_TemperatureSensor.png" width="960"/><br>

**Diesmal beginnen wir mit dem Sensorblock. Setupblocks können auch vor die Basisfunktion initiiert werden.**

1. Hole dir einen **Setup DHT11 Sensor mit Pin# 0** (Setup DHT11 Sensor with Pin# 0) von **Sensor** (Sensors) auf deinen Arbeitsbereich.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_3.png" width="960"/><br>

2. Setze die Pinnummer auf ***2***.
3. Dann hole dir die Basisfunktion mit Vorlauf- und Endlosschleife von **Funktion** (Functions).

  <img src="images/Lesson12/Lesson12_TemperatureSensor_4.png" width="960"/><br>

  **Lass uns die drei Dioden der RGB LED initiieren.**

4. Ziehe dir dazu einen **Setze digitale Pin# 0 zu HIGH** (Set digital pin# 0 to HIGH) aus **Eingang/Ausgang** (Input/Output) in den Vorlaufspalt.
5. Wähle die Pinnummer ***3***.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_5.png" width="960"/><br>

6. Dupliziere diesen Block zweimal und setze diese untereinander in die Vorlaufschleife der Basisfunktion.
7. Ändere die Pinnummern zu ***5*** und ***6***, während der initiierte Status ***LOW*** in diesen beiden Fällen werden.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_6.png" width="960"/><br>

  **Wir wollen die durch die RGB LED erzeugte Farbe in jedem Loop anpassen, daher werden wir einen analogen Wert zu jeder LED senden.**

8. Ziehe einen Block **Setze analog Pin# 0 zu** (Set analog pin# 0 zu) aus **Eingang/Ausgang** (Input/Output) auf deinen Arbeitsbereich.
9. Wechsle die Pinnummer zu ***3***.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_7.png" width="960"/><br>

10. Nun ziehe **Element** (item) aus **Variable** (Variables) und setze es in die Verbindung von **Setze analog Pin# 3 zu** (Set analog pin# 3 zu).
11. Nenne **Element** (item) in ***Blau*** um.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_8.png" width="960"/><br>

12. Wiederhole die Schritte 8. - 11. zweimal. Benutze einmal Pinnummer ***5*** mit Variable ***Grün*** und das zweite Mal Pinnummer ***6*** mit Variable **Rot**.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_9.png" width="960"/><br>

  **Nun wollen wir die Temperatur messen und dem entsprechend die Farbe der RGB LED anpassen.**

  ***Kalt >> Blau und Warm >> Rot***

13. Von **Sensor** (Sensors) hole dir einen **Lese Temperatur mit DHT11 von Pin# 0** (Read Temperature with DHT11 from Pin# 0) und ziehe diesen auf deinen Arbeitsbereich.
14. Verändere die Pinummer zu ***2***.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_10.png" width="960"/><br>

  **Wir werden die Messung als Variable festhalten.**

15. Nehme einen **Setze Element zu " " als Character** (set item to " " as Character) Block aus **Variable** (Variables) auf deinen Arbeitsbereich.
16. Ziehe **Lese Temperatur mit DHT11 von Pin# 2** (Read Temperature with DHT11 from Pin# 2) als **" "** von **Setze Element zu " " als Character** (set item to " " as Character).

  <img src="images/Lesson12/Lesson12_TemperatureSensor_12.png" width="960"/><br>

17. Ändere nun den **als** Wert zu ***Dezimal*** in **Setze Element zu Lese Temperatur mit DHT11 von Pin# 2 als Character** (set item to Read Temperature with DHT11 from Pin# 2 as Character).

  <img src="images/Lesson12/Lesson12_TemperatureSensor_13.png" width="960"/><br>

  **Nun folgt die eigentlich Anpassung der gemessen Temperatur an die Lichtintensität der jeweiligen Farb-LED.**

18. Ziehe einen **Setze Element zu** (set item to) Block auf deinen Arbeitsplatz.
19. Nenne die Variable ***Rot***.
20. Dann dupliziere diesen Block zweimal, erzeuge neue Variablen und benenne diese ***Grün*** und ***Blau***.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_14.png" width="960"/><br>

  **Da sich die Farbe zwischen Blau und Rot bewegen soll, wird die grüne LED immer aus bleiben. Daher kann der analoge Wert statisch 0 sein.**

21. Ziehe einen **0** Block von **Mathe** (Math) und verbinde diesen zu **Setze Grün zu** (set Grün to).

  <img src="images/Lesson12/Lesson12_TemperatureSensor_15.png" width="960"/><br>

22. Hole dir jeweils einen **Relativiere " " vonUnten 0 vonOben 100 bisUnten 0 bisOben 1000** (Map " " fromLow 0 fromHigh 100 toLow 0 toHigh 1000) Block für die verbleibenden 2 Farben und verbinde diese jeweils mit **Setze Rot zu** (set Rot to) und **Setze Blau zu** (set Blau to).

  <img src="images/Lesson12/Lesson12_TemperatureSensor_16.png" width="960"/><br>

  **Die Temperatur zwischen 20 und 30 Grad Celsius soll die Helligkeit zwischen 0 (Aus) und 255 (Am Hellsten) bestimmen.**

23. Setze die Werte der Variable **Rot** zu ***vonUnten: 20 vonOben 30 bisUnten 0 bisOben 255*** und der Variable **Blau** zu ***vonUnten: 20 vonOben 30 bisUnten 255 bisOben 0***.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_17.png" width="960"/><br>

24. Ziehe einen **Element** (item) Block von **Variable** (Variables) in den Relativierungswert von **Rot** und ändere diese zu ***Temperatur***.
25. Wiederhole 24. für **Setze Blau zu...*** (set Blau to).
26. Ziehe den gesamten Block der 3 **Setze....** (set...) Blöcke an die untereste Stelle der Endlosschleife.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_18.png" width="960"/><br>

27. Ziehe zum Abschluss noch einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block in die Endlosschleife.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_19.png" width="960"/><br>

28. Kompiliere den Code und lade diesen auf den Arduino.
29. Benenne und speichere dein Projekt ab.


#### *Hauche warmen Atem oder puste am Sensor! Wie schauts mit anderen Farben aus?*

---

#### [Zurück zum Index](#index)

### 3.12. Links, Rechts, Vor und Zurück <a name="left-right-back-forth"></a>

Dieses Beispiel stellt den Joystick vor, der auch bei Spielekonsolen genutzt wird. Anstelle eines Spielecharakters werden wir die Bewegung durch LEDs darstellen.


<img src="images/Lesson13/Lesson13_AnalogJoystick_1.png" width="960"/><br>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Steckplatte** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Rote LED** | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/> | 1 | **Grüne LED** | <img src="images/Parts/Part_LED_Green_5mm.jpg" width="256"/> |
| 1 | **Gelbe LED** | <img src="images/Parts/Part_LED_Yellow_5mm.jpg" width="256"/> | 1 | **Blaue LED** | <img src="images/Parts/Part_LED_Blue_5mm.jpg" width="256"/> |
| 4 | **220 Ohm Widerstand** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>| 1 | **Joystick Modul** |<img src="images/Parts/Part_Analog_Joystick.jpg" width="256"/> |
| 5 | **W-M Kabel** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> | 9 | **M-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***Joystick Modul:***

* hat 5 Pins, jeweils für Stromversorgung, Erde, X Achse, Y Achse und Druckknopf.
* ist analog und erlaubt eine präzise horizonatle und vertikale Steuerung.
* ein integrierter Druckschalter ermöglicht zusätzliche Funktionalität.

#### Schaltplan

<img src="images/Lesson13/Lesson13_AnalogJoystick_2.png" width="960"/><br>

#### Bastelanweisung

1. W-M Kabel 1: Joystick GND Pin >> Arduino Pin GND
2. W-M Kabel 2: Joystick 5V Pin >> Arduino Pin 5V
3. W-M Kabel 3: Joystick VRx Pin >> Arduino Pin A0
4. W-M Kabel 4: Joystick VRy Pin >> Arduino Pin A1
5. W-M Kabel 5: Joystick SW Pin >> Arduino Pin 2
6. Gelbe LED: Linke Seite mit langen Bein (+) im oberen Mittelteil und kurzen Bein im unteren Teil
7. Blaue LED: Rechte Seite mit langen Bein (+) im oberen Mittelteil und kurzen Bein im unteren Teil
8. Grüne LED: Mitte oberer Mittelteil in selber Reihe kurzes Bein links und langes Bein rechts
9. Rote LED: Mitte unterer Mittelteil in selber Reihe kurzes Bein links und langes Bein rechts
10. Widerstand 220 Ohm: Jeweils oberhalb des rechten Beins der LED >> Selbe Zeile 4 - 8 Stecklöcher nach rechts
11. M-M Kabel 1: Obere blaue Reihe der Steckplatte >> Arduino GND (Erde)
12. M-M Kabel 2: Unterhalb des Beins der gelben LED im unteren Mittelteil >> Obere blaue Reihe der Steckplatte
13. M-M Kabel 3: Unterhalb des linken Beins der roten LED im unteren Mittelteil >> Obere blaue Reihe der Steckplatte
14. M-M Kabel 4: Unterhalb des Beins der blauen LED im unteren Mittelteil >> Obere blaue Reihe der Steckplatte
15. M-M Kabel 5: Oberhalb des linken Beins der grünen LED im oberen Mittelteil >> Obere blaue Reihe der Steckplatte
16. M-M Kabel 6: Oberhalb des freien Beins des Widerstands der gelben LED im oberen Mittelteil >> Arduino Pin 7
17. M-M Kabel 7: Oberhalb des freien Beins des Widerstands der blauen LED im oberen Mittelteil >> Arduino Pin 8
18. M-M Kabel 8: Oberhalb des freien Beins des Widerstands der roten LED im unteren Mittelteil >> Arduino Pin 12
19. M-M Kabel 9: Oberhalb des freien Beins des Widerstands der grünen LED im oberen Mittelteil >> Arduino Pin 13


**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson13/Lesson13_AnalogJoystick.png" width="960"/><br>

1. Ziehe zu Beginn die Basisfunktion mit Vorlauf und Endlosschleife von **Funktion** (Functions) auf deinen Arbeitsbereich.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_3.png" width="960"/><br>

  **Wir werden 4 LEDs für die Richtungen nutzen. Lass uns diese als Nächstes initiieren.**

2. Ziehe dazu 4 mal einen **Setze digitale Pin# 0 zu HIGH** (Set digital pin# 0 to HIGH) Block von **Eingang/Ausgang** (Input/Output) in die Vorlaufschleife.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_4.png" width="960"/><br>

3. Setze die Pinnummern auf ***7***, ***8***, ***12*** und ***13***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_5.png" width="960"/><br>

  **Der Joystick ist analog und funktioniert so ähnlich wie 2 Potentiometer, welche die mittlere Position als Ausgangspunkt hat. Also vorwärts und rückwärts ist ein Potentiometer, links und rechts ist ein Potentiometer.**

4. Ziehe einen Block **Setze Element zu " " als Character** (set item to " " as Character) von **Variable** (Variables) auf deinen Arbeitsbereich.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_6.png" width="960"/><br>

5. Nenne **Element** in ***FB*** (oder VZ wie Vorwärts/Rückwärts) um.
6. Dann fülle **" "** mit einem **Lese analog Pin# A0** (Read analog pin#) Block aus **Eingang/Ausgang** (Input/Output).

  <img src="images/Lesson13/Lesson13_AnalogJoystick_7.png" width="960"/><br>

7. Wähle den Typ ***Nummer*** (Number) anstelle von **Character** und ziehe den Block in die Endlosschleife der Basisfunktion.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_8.png" width="960"/><br>

8. Dupliziere den Block, erzeuge eine neue Variable mit **Neue Variable...*** (New variable...) und nenne diese **LR** (Links/Rechts).
9. Verändere die Pinnummer zu ***A1***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_9.png" width="960"/><br>

  **Nun werden wir die dir bekannte Statementfunktion "wenn Tue" einsetzen. Wir benötigen vier Spalten für jede Richtung, eine für den Knopfdruck und eine Spalte für Sonstiges.**

10. Hole dir einen **wenn Tue** (if do) Block aus **Logik** (Logic) und setze es in die Endlosschleife.
11. Presse nun das Zahnradsymbol, ziehe und verbinde 4 **anderenfalls** und 1 **sonst** Block auf die rechte Seite mit **wenn**.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_10.png" width="960"/><br>

12. Dupliziere jeden **Setze digitale Pin#...** (Set digital pin#...) Block in der Vorlaufschleife 3 mal.
13. Setze die erste Kopie vom Block mit Pinnummer **7** in die erste **Tue** (do) Spalte, die erste Kopie vom Block mit Pinnummer **8** in die zweite **Tue** (do) Spalte, die erste Kopie vom Block mit Pinnummer **12** in die dritte **Tue** (do) Spalte und die erste Kopie vom Block mit Pinnummer **13** in die vierte **Tue** (do) Spalte.
14. Setze alle zweiten Kopien in die fünfte **Tue** Spalte.
15. Setze alle dritten Kopien in die **sonst** Spalte und wechsle alle **HIGH** Werte zu ***LOW***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_11.png" width="960"/><br>

  **Die vier Richtungen haben zwei Potentiometer, welche jeweils eine Datenspanne von 0 - 1023 (1024 gesamt) haben. Die Ruheposition liegt also bei Beiden bei 511. Alles über 511 ist eine Richtung und darunter ist die andere Richtung.**

  **Wir werden eine Spannweite festlegen.**

16. Ziehe einen **" " und " "** (" " and " ") Block aus **Logik** (Logic) und verbinde es mit **wenn**.
17. Dann ziehe zwei **" " + " "** Blöcke aus **Logik** (Logic) und setze diese in die erste und zweite Position von **" " und " "** (" " and " ").

  <img src="images/Lesson13/Lesson13_AnalogJoystick_13.png" width="960"/><br>

18. Hole dir zwei **0** Blöcke aus **Mathe** (Math) in deine Szene und setze diese jeweils in die zweite Position von **" " + " "**.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_14.png" width="960"/><br>

19. Nun ändere das erste **+** Symbol zu ***>*** und das zweite **+** Symbol zu ***<=***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_16.png" width="960"/><br>

20. Anstelle **0** des **" " > 0** Blockes benutze ***530***.

  ***Die Zahl ist höher als die Mittelposition, da gerade ein analoger Eingang der Wert etwas schwanken kann. Um einen Wackelkontakteffekt zu vermeiden ist die Zahl höher oder niedriger angesetzt.***

21. Ersetze **0** des **" " <= 0** Blockes mit ***1023***.
22. Ziehe für die erste Position von **" " > 530** und **" " <= 1023** einen **Element** (item) Block aus **Variable** (Variables).

  <img src="images/Lesson13/Lesson13_AnalogJoystick_17.png" width="960"/><br>

23. Wähle beide Male ***LR*** als Variablenname.
24. Dupliziere den **LR > 530 und LR <= 1023** Block und setze die Kopie in den erste **anderenfalls** Anschluss.

<img src="images/Lesson13/Lesson13_AnalogJoystick_19.png" width="960"/><br>

25. Modifziere die Symbole und Zahlen des Blocks zu ***LR < 500 und LR >= 0***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_21.png" width="960"/><br>

26. Mache jeweils von **LR > 530 und LR <= 1023** und **LR < 500 und LR >= 0** eine Kopie und verbinde diese in der selben Reihenfolge mit der zweiten und dritten **anderenfalls** Anknüpfungspunkt.
27. Verändere nun alle 4 **LR** der 2 Kopien zu ***FB***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_22.png" width="960"/><br>

  **Damit sollten die LEDs aufleuchten, wenn wir den Joystickknopf bewegen. Doch beim Drücken sollen alle LEDs aufleuchten.**

28. Ziehe einen neuen **" " = " "** Block aus der Kategorie **Logik** (Logic) und verknüpfe es mit der vierten **anderenfalls** Verbindung.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_23.png" width="960"/><br>

29. Als erste Position des **" " = " "** nutzen wir den **Lese digital mit PULL_UP Modus pin# 0** (Read digital with PULL_UP mode pin# 0) Block aus **Eingang/Ausgang** (Input/Output).
30. Lege die Pinnummer ***2*** fest.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_24.png" width="960"/><br>

31. Als zweite Position von **Lese digital mit PULL_UP Modus pin# 2 = " "** (Read digital with PULL_UP mode pin# 2 = " ") benutze eine **HIGH** Block von **Eingang/Ausgang** (Input/Output).
32. Ändere den Wert von **HIGH** zu ***LOW***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_25.png" width="960"/><br>

33. Gib deinem Projekt einen Namen und speichere dein Sketch.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_26.png" width="960"/><br>

34. Kompiliere den Code und lade ihn auf deinen Arduino.

#### *Game on!!!*

---

#### [Zurück zum Index](#index)

### 3.13. Die Starlight Show <a name="star-light-show"></a>

In diesem Projekt wirst du lernen wie mit einer Fernbedienung eine Minianzeige bedient, welche deine eigenen kleinen Grafiken darstellt.

>Gib mir ein Gesicht und lass mich Lächeln!

Wir werden lediglich 3 Grafiken in der Übung mit 3 Tasten der Fernbedienung verknüpfen. Du kannst es dann wie immer erweitern.


<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_1.png" width="960"/><br>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Prototyp Erweiterungsmodul**                   |<img src="images/Parts/Part_PrototypeExpansionModule.jpg" width="256"/>   |
| 1 | **InfraRot Modul**                      | <img src="images/Parts/Part_IRModule.jpg" width="256"/>    | 1 | **Fernbedienung**            |<img src="images/Parts/Part_IRRemoteControl.jpg" width="256"/>|
| 1 | **MAX7219 Module**            |<img src="images/Parts/Part_8x8DotMatrixMAX7219.jpg" width="256"/>| 6 | **W-M Kabel**                     |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> |
| 3 | **M-M Kabel**                     |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***Prototyp Erweiterungsmodul:***

* ist ein aufsteckbares Prototype Board für den Arduino Uno, das mit einer Ministeckplatte kommt.
* hat alle Arduino Pins + jeweils 3 extra GND (Erde) und 5V Steckplätze zur Verfügung.

***InfraRot Modul mit Fernbedienung:***

* sind kleine Mikrochips mit eine InfraRot empfangende Fotozelle.
* oft genutzt in TVs und DVD Spielern.
* hat einen Sender, welcher als Fernbedienung bekannt ist und auf einer Frequenz von 38kHz sendet.
* empfängt InfraRot Muster von der Fernbedienung, die wie ein Morsecode die Nachricht erkennt und zum Beispiel den TV an- oder ausschaltet.

***MAX7219 8x8 LED Matrix:***

* ist ein serieller Displaytreiber, der eine bis zu 8 Segmenten Anzeige, eine Balkendiagramm oder 64 einzelne LEDs steuern kann.
* ist mit den bereits erwähnten 64 LEDs als eine 8x8 Matrix angeordnet, die individuell angesteuert werden können.


#### Schaltplan

<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_2.png" width="960"/><br>

#### Bastelanweisung

1. Prototyp Erweiterungsmodul: Auf passende Pin Stecklöcher gesteckt
2. InfraRot Modul:  Aufgesteckt in Reihe im unteren Bereich der Ministeckplatte mit Diode nach Aussen zeigend
3. 3 Widerstände: Vor jedem Fuss im oberen Mittelteil >> Längsposition im unteren Mittelteil
4. M-M Kabel 1: IR Modul GND >> Arduino GND (Erweiterungsmodul)
5. M-M Kabel 2: IR Modul VCC >> Arduino 5V (Erweiterungsmodul)
6. M-M Kabel 3: IR Modul DATA >> Arduino 13 (Erweiterungsmodul)
7. W-M Kabel 1: MAX7219 VCC >> Arduino 5V (Erweiterungsmodul)
8. W-M Kabel 2: MAX7219 GND >> Arduino GND (Erweiterungsmodul)
9. W-M Kabel 3: MAX7219 DIN >> Arduino 12 (Erweiterungsmodul)
10. W-M Kabel 4: MAX7219 CS >> Arduino 11 (Erweiterungsmodul)
11. W-M Kabel 5: MAX7219 CLK >> Arduino 10 (Erweiterungsmodul)

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix.png" width="960"/><br>

1. Natürlich auch hier beginnen wird mit der Basisfunktion.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_3.png" width="960"/><br>

2. Ziehe einen **Setup IR Fernbedienung mit Pin# 0** (Setup IR Remote Control with Pin# 0) von **Eingang/Ausgang** (Input/Output) in die Vorlaufschleife.
3. Ändere die Pinnummer zu ***13***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_4.png" width="960"/><br>

  **Jetzt werden wir die gedrückte Taste der Fernbedienung als Variable festhalten.**

4. Nehme einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) und füge ihn in die Endlosschleife ein.
5. Benenne die Variable **Element** in ***Button*** (Schalter) um.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_5.png" width="960"/><br>

6. Hole dir den **Lese Wert von IR Fernbedienung Pin# 0** (Read value from IR Remote Control on Pin# 0) Block aus **Eingang/Ausgang** (Input/Output) und verbinde diesen mit **Setze Button zu** (set Button to).
7. Ändere die Pinnummer zu ***13***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_7.png" width="960"/><br>

8. Ziehe nun einen **wenn Tue** (if do) Block aus **Logik** (Logic) und addiere es zu der Endlosschleife.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_8.png" width="960"/><br>

9. Benutze wieder das Zahnradsymbol, um zwei **anderenfalls** (else if) Blöcke hinzuzufügen.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_9.png" width="960"/><br>

  **Wir wollen vergleichen, ob die gedrückte Taste einer bestimmten Nummer entspricht.**

10. In **Logik** (Logic) findest du den **" " = " "** Block, den dafür nutzen kannst. Verknüpfe ihn mit der **wenn** Verbindung.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_10.png" width="960"/><br>

11. Als Nächstes fülle die erste Position von **" " = " "** mit einem **Element** (item) Block aus **Variable** (Variables).
12. Wähle für **Element** (item) ***Button***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_11.png" width="960"/><br>

13. Hole dir einen **0** Block aus **Mathe** (Math) und ziehe es in die zweite Position von **Button = " "**
14. Ändere den Wert zu ***1***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_12.png" width="960"/><br>

  **Die MAZ7219 Anzeige hat eigene Blöcke, welche die Grundeinstellung und die Darstellung sowie das Leeren der Anzeige regelt.**

15. Ziehe aus **Bildschirm** (Displays) den Block **Setup MAX7219 auf DIN Pin...** (Setup MAX7219 on DIN Pin...) und setze ihn in der Vorlaufschleife ab.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_13.png" width="960"/><br>

16. Verändere die Pinnummern zu ***DIN: 12***, ***CS: 11*** und ***CLK: 10***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_14.png" width="960"/><br>

17. Ziehe einen Block **Element Erzeuge Character MAX7219 DIN Pin# 0** (item Set Character MAX7219 DIN Pin# 0) von **Bildschirm** (Displays) in die erste **Tue** (do) Spalte.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_15.png" width="960"/><br>

18. Benenne **Element** (item) in ***Heart*** (Herz) um und wähle die Pinnummer ***12***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_16.png" width="960"/><br>

19. Erzeuge 2 Kopien vom Block der mit **wenn** (if) verbunden ist und verknüpfe einen jeweils mit einer **anderenfalls** Verbindung.
20. Ändere die Nummern zu ***2*** und ***3***.

  ***Damit werden wir unsere Zeichen darstellen, wenn man 1 2 oder 3 auf der Fernbedienung drückt.***

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_17.png" width="960"/><br>

  **Das Zeichen soll ein paar Sekunden angezeigt werden. Danach soll die Anzeige sich löschen.**

21. Ziehe einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block aus **Zeit** (Time) und platziere den Block unterhalb **Heart Erzeuge Character...** (Heart Set Character...).
22. Erhöhe den Zeitwert auf ***3000*** (3 Sekunden).

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_18.png" width="960"/><br>

23. Um die Anzeige zu leeren, ziehe einen **Lösche Anzeige MAX7219 DIN Pin# 0** (Clear Display MAX7219 DIN Pin# 0) Block unterhalb der **wenn Tue** (if do) Funktion in die Endlosschleife.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_19.png" width="960"/><br>

24. Wähle die Pinnummer ***12***.
25. Mache jeweils 2 Kopien von **Heart Erzeuge Character...** (Heart Set Character...) und dem Zeitblock und setze diese in die zweite und dritte Spalter hinter **Tue** (do).

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_20.png" width="960"/><br>

26. Klicke auf **Heart** und wähle erst **Neue Variable...** (New Variable...), dann **Benenne Variable um...** um dem zweiten Zeichen den Namen ***Smile*** und dem dritten Zeichen den Namen ***A*** zu geben.

27. Erstelle nun die Zeichen für alle 3 Blöcke anhand des Bildes.

  ***Beim Klicken auf die Felder erzeugst du kleine Häckchen. Jedes Häckchen wird eine LED auf deiner Anzeige aufleuchten lassen.***

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_21.png" width="960"/><br>

28. Benenne dein Projekt und speichere es ab.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_22.png" width="960"/><br>

29. Kompiliere den Code und lade es auf deinen Arduino.

**Drücke Nummer 1, 2 und 3 nacheinander.**

>Ahhhhhh...jetzt hab ich ein Lächeln!

#### *Addiere doch eigene Bildchen zum Code!*

***Das untere Foto zeigt die Zahlenbelegung für deine Fernbedienung:***

<img src="images/Lesson14/Part_IRRemoteControl.jpg" width="512"/><br>

---

#### [Zurück zum Index](#index)

### 3.14. Animierte Animatoren <a name="get-animated"></a>

Als Erweiterung gibt es hier noch 3 weitere Beispiele, welche dir weitere Möglichkeiten eröffnen.

Doch hier gebe ich dir nur ein paar Hinweise und die Bilder.

#### *Versuche diese Beispiele selbst herauszufinden.*

Benutze das vorhergegende Projekt als Vorlage.

**Vergesse nicht sofort einen anderen Namen zu wählen, wenn du das Vorlageprojekt öffnest, damit du dieses nicht verlierst!**

#### An / Aus

* In diesem Beispiel benötigst du ein weitern **andernfalls** Block in der **wenn Tue** Funktion.
* Addressiere es zur Nummer **10**, was die Taste **AUS** auf der Fernbedienung representiert.
* Lösche die Zeitblöcke aus allen **Tue** Bereichen.
* Ziehe die **Lösche Anzeige...** (Clear Display...) in die neue Spalte der **wenn Tue** (if do) Funktion.

<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_23_OnOff.png" width="960"/><br>

#### Kreisanimation

* In diesem Beispiel addieren wir eine neue Konditionsfunktion die zur booleschen Logik gehört.
* In Verbindung mit einer **Wenn Tue** Funktion wird zum Beispiel etwas nur dann gemacht, wenn es als **wahr** gekennzeichnet ist.
* Die Animation wird nur dann abgespielt, wenn eine Variable auf **Wahr** geschaltet wurde.
* Erzeuge also als erstes eine Variable mit dem Namen **Animation**
* Dann verbinde die Variable mit einem **Wahr** (true) Block aus **Logik** (Logic).
* Die Ausgangsposition ist **Falsch**.
* Wenn du das Vorlageprojekt nutzt, ziehe die Zeichen- und Zeitblöcke aus der **wenn Tue** auf den Arbeitsplatz daneben.
* Verringere die **andernfalls** Spalten auf lediglich eine Zusatzspalte.
* Wähle eine Tastennummer ***1*** und die zweite ***10*** zum An- und Ausschalten der Animation.
* Addiere jeweils eine Kopie von **Setze Animation zu Falsch** (set Animation do False) zu **Tue**.
* Unter Tastenummer **1** ändere **Falsch** zu ***Wahr***.
* Unter Tastennummer **10** ziehe den **Lösche Anzeige...** (Clear Display...) Block hinzu.
* Addiere einen neuen **wenn Tue** (if do) in die Endlosschleife.
* Erzeuge unter **wenn** einen Block **Animation = Wahr**.
* Ziehe alle drei Zeichen- und Zeitblöcke in den **Tue** (do) Abschnitt.
* Ändere die Namen zu ***Kreis 1***, ***Kreis2*** und ***Kreis3***.
* Folge der Häckchenbesetzung dem Bild unten oder versuch etwas anderes.
* Die Zeit muss auf ***90*** Millisekunden reduziert werden, damit eine Animation daraus wird.

***Die Zeit bestimmt die Geschwindigkeit der Animation.***

**Vergess nicht diese Beispiel mit eigenem Namen abzuspeichern.**

<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_23_Animation.png" width="960"/><br>

#### Space Invaders

* Nutze das vorhergehende Beispiel als Vorlage.
* Lösche ein Zeichenblock und einen Zeitblock.
* Erhöhe die Zeit auf **200** Millisekunden.
* Dann zeichne die bekannten zwei Posen der Spielfigur (siehe Bild unten).

<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_23_SpaceInvaders.png" width="960"/><br>



---

#### [Zurück zum Index](#index)

### 3.15. Alles dreht sich <a name="round-and-round-it-goes"></a>

Dieses Projekt ist, das du nutzen kannst ein kleines Fahrzeug zu steuern.
Das Gyroskop kennst du bestimmt schon von deinem Handy.
Es kann die Rotation und die Geschwindigkeit der Drehung wahrnehmen.
Das Projekt hat:

* zwei grüne LEDs um die Beschleunigung darzustellen.
* zwei rote LEDs um das Bremsen zu zeigen.
* einen Steppermotor zum Lenken.

>Lass uns losfahren!

<img src="images/Lesson15/Lesson15_Gyroscope_1.png" width="960"/><br>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Prototyp Erweiterungsmodul**                   |<img src="images/Parts/Part_PrototypeExpansionModule.jpg" width="256"/> |
| 2 | **Rote LED** | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/> | 2 | **Grüne LED** | <img src="images/Parts/Part_LED_Green_5mm.jpg" width="256"/> |
| 4 | **220 Ohm Widerstand** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>| 1 | **GY-521 Gyroskop** |<img src="images/Parts/Part_GY-521.jpg" width="256"/> |
| 1 | **Servomotor** |<img src="images/Parts/Part_ServoMotor.jpg" width="256"/> | 7 | **M-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |
| 4 | **W-M Kabel** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> |

#### Neue Komponenten

***GY-521 Modul:***

* beinhaltet einen MEMS Beschleunigungssensor und ein MEMS Gyroskop in einem einzigen Chip.
* kann eine 16-bit analog zu digital Umwandlung je Kanal dürchführt. Daher kann das Modul die X-, Y und Z-Werte zeitgleich messen und ausgeben.
* wird in Smartphones, Wearables, Spielecontrollern und vielen weiteren Geräten benutzt.
* ermöglicht es, an einem Objekt befestigt, die Ausrichtung des Objektes im 3-dimensionalen Raum zu bestimmen.
* kann auch zur Erkennung von Bewegungen genutzt werden.

#### Schaltplan

<img src="images/Lesson15/Lesson15_Gyroscope_2.png" width="960"/><br>

#### Bastelanweisung

1. Rote LED 1: Im unteren Mittelteil in 5. Reihe mit kurzes Bein links und langes Bein rechts 2 Stecklöcher dazwischen
2. Rote LED 2: Im unteren Mittelteil in 2. Reihe mit kurzes Bein links und langes Bein rechts 3 Stecklöcher dazwischen
3. Grüne LED 1: Im oberen Mittelteil in 2. Reihe mit kurzes Bein links und langes Bein rechts 2 Stecklöcher dazwischen
4. Grüne LED 2: Im oberen Mittelteil in 5. Reihe mit kurzes Bein links und langes Bein rechts 3 Stecklöcher dazwischen
5. 4 Widerstände: In 3. und 4. Reihe des oberen und unteren Bereich über oder unter des langen Bein der LED >> 4 Steckplätze in Reihe nach rechts versetzt
6. M-M Kabel 1: Oberhalb des rechten Beins des Widerstands in 3. Reihe (Grüne LED) >> Arduino Pin 8
7. M-M Kabel 2: Oberhalb des rechten Beins der Widerstands in 4. Reihe (Grüne LED) >> Arduino Pin 7
8. M-M Kabel 3: Oberhalb des rechten Beins der Widerstands in 3. Reihe (Rote LED) >> Arduino Pin 12
9. M-M Kabel 4: Oberhalb des rechten Beins der Widerstands in 4. Reihe (Rote LED) >> Arduino Pin 13
10. M-M Kabel 5: Servermotor GND >> Arduino GND
11. M-M Kabel 6: Servermotor VCC >> Arduino 5V
12. M-M Kabel 7: Servermotor DATA >> Arduino Pin 9
13. W-M Kabel 1: GY-521 VCC >> Arduino 3V Pin
14. W-M Kabel 2: GY-521 GND >> Arduino GND
15. W-M Kabel 3: GY-521 SCL >> Arduino Pin Analog 5 (A5)
16. W-M Kabel 4: GY-521 SDA >> Arduino Pin Analog 4 (A4)

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson15/Lesson15_Gyroscope.png" width="960"/><br>

1. Und wie immer beginnen wir mit der Basisfunktion.

  <img src="images/Lesson15/Lesson15_Gyroscope_3.png" width="960"/><br>

  **Als Erstes kümmeren wir uns um das Gyroskop und die Steuerung.**

2. Ziehe von **Sensor** (Sensors) einen **Setup GY-521 Gyroskop mit A4|A5** (Setup GY-521 Gyroskop with A4|A5) auf deinen Arbeitsbereich.

  ***Dieser Setupblock kann wie einige andere in erster Position ausserhalb der Vorlaufschleife positionert werden. Der Code wird trotzdem funktionieren.***

  <img src="images/Lesson15/Lesson15_Gyroscope_4.png" width="960"/><br>

  **Wir wollen die 3 Achsen einer drei-dimensionalen Drehung wieder als Variablen speichern.**

3. Ziehe den **GY-521 Messe Winkelgrad von** (GY-521 Measure angles on) aus **Sensor** (Sensors) in die Endlosschleife.

  <img src="images/Lesson15/Lesson15_Gyroscope_5.png" width="960"/><br>

4. Nutze für jede Achse eine Variable durch **Neue Variable...** (New Variable...) und **Benenne Variable um...** (Rename Variable...) mit den Namen ***AngleX***, ***AngleY*** und ***AngleZ***.

  **Setup den Servomotor.**

5. Ziehe einen **Setze SERVO zum Pin 0 zu 90 Winkelgrad** (Set SERVO from Pin 0 to 90 Degree) von **Motor** (Motors).
6. Ändere die Pinnummer zu ***9***.

  <img src="images/Lesson15/Lesson15_Gyroscope_7.png" width="960"/><br>

7. Ziehe viermal einen **Setze digital Pin# 0 zu HIGH** (Set digital Pin# 0 to HIGH) von **Eingang/Ausgang** (Input/Output) in die Vorlaufschleife.
8. Setze die Pinnummern auf ***7***, ***8***, ***12*** und ***13***.

  <img src="images/Lesson15/Lesson15_Gyroscope_8.png" width="960"/><br>

  **Nun werden wird die "wenn Tue" Funktion mit 5 Spalten einrichten. 1 - Gasgeben, 2 - Vollgas geben, 3 - Bremsen, 4 - Vollbremsung, 5 - Weder Gasgeben noch Bremsen.**

9. Ziehe eine **wenn Tue** (if do) Funktion in die Endlosschleife.

  <img src="images/Lesson15/Lesson15_Gyroscope_10.png" width="960"/><br>

10. Benutze das Zahnradsymbol, um 4 **anderenfalls** (else if) Blöcke dem **wenn** (if) hinzuzufügen.

  **Wir wollen wieder einen Bereich mit minimum und maximum festlegen**

11. Verbinde einen **" " und " "** (" " and " ") Block mit **wenn**.

  <img src="images/Lesson15/Lesson15_Gyroscope_11.png" width="960"/><br>

12. Benutze als erste Position von **" " und " "** (" " and " ") einen **" " = " "** Block.

  <img src="images/Lesson15/Lesson15_Gyroscope_12.png" width="960"/><br>

13. Das Symbol **=** muss zu **>** werden.

  <img src="images/Lesson15/Lesson15_Gyroscope_13.png" width="960"/><br>

14. Die erste Position des **" " > " "** Block wird ein **Element** (item) Block aus **Variable** (Variables).
15. Wähle für **Element** die Option ***AngleX***.

  <img src="images/Lesson15/Lesson15_Gyroscope_14.png" width="960"/><br>

16. Die zweite Position von **AngleX > " "** ist ein **0** Block aus **Mathe** (Math). Die Zahl wird ***10***.
17. Kopiere **AngleX > 10** für die zweite Position von **AngleX > 10 und " "** (AngleX > 10 and " ").
18. Ändere die Werte zu ***AngleX < 30***.

  <img src="images/Lesson15/Lesson15_Gyroscope_16.png" width="960"/><br>

19. Mache 4 Kopien von **AngleX > 10 und AngleX < 30** (AngleX > 10 and AngleX < 30) und füge diese an jede der **anderenfalls** Verbindung.
20. Die erste Kopie wird ***AngleX >= 30 und AngleX <= 180***.
21. Die zweite Kopie wird ***AngleX < -10 und AngleX > -30***.
22. Die dritte Kopie wird ***AngleX <= -30 und AngleX >= -180***.
23. Die vierte Kopie wird ***AngleX < 10 und AngleX > -10***.

  <img src="images/Lesson15/Lesson15_Gyroscope_17.png" width="960"/><br>

24. Erzeuge 5 Kopien von jeder der 4 **Setze digital Pin# ...** (Set digital Pin# ...) aus der Vorlaufschleife, und füge diese in jeweils eine der **Tue** Spalten.

  ***Sei dir sicher, dass die Reihenfolge der Pins in jeder Spalte 7, 8, 12 und 13 bleibt.***

25. Die Werte in der ersten **Tue** (do) Spalte werden ***HIGH***, ***LOW***, ***LOW***, ***LOW***.
26. Die Werte in der zweiten **Tue** (do) Spalte werden ***HIGH***, ***HIGH***, ***LOW***, ***LOW***.
27. Die Werte in der dritten **Tue** (do) Spalte werden ***LOW***, ***LOW***, ***HIGH***, ***LOW***.
28. Die Werte in der vierten **Tue** (do) Spalte werden ***LOW***, ***LOW***, ***HIGH***, ***HIGH***.
29. Die Werte in der fünften **Tue** (do) Spalte werden ***LOW***, ***LOW***, ***LOW***, ***LOW***.

  <img src="images/Lesson15/Lesson15_Gyroscope_18.png" width="960"/><br>

  **Nun werden wir die Motorlenkung adressieren.**

30. Ziehe dafür einen neuen **wenn Tue** (do) Block von **Logik** (Logic) in die letzte Position der Endlosschleife.
31. Drücke auf das Zahnradsymbol und addiere einen **anderenfalls** (else if) und einen **sonst** (else) Block zum **wenn** auf der rechten Seite.

  ***Schliesse das Menü durch das erneute Klicken auf das Zahnradsymbol.***

32. Erstelle zwei Kopien des **AngleX < 10 und AngleX > -10** Blocks und setze diese jeweils in die neue **wenn** und **anderenfalls** Verbindung.
33. Ändere die erste Kopie unter **wenn** zu **AngleZ > 10 und AngleZ < 180**.
34. Ändere die zweite Kopie unter **anderenfalls** zu **AngleZ < -10 und AngleZ > -180**.

  <img src="images/Lesson15/Lesson15_Gyroscope_19.png" width="960"/><br>

35. Kopiere den **Setze SERVO zum Pin 9 zu 90 Winkelgrad** (Set SERVO from Pin 9 to 90 Degree) Block aus dem Vorlauf 3 mal.
36. Setze jeweils eine Kopie in die beiden **Tue** Spalte und eine Kopie in die **sonst** Spalte.

  <img src="images/Lesson15/Lesson15_Gyroscope_20.png" width="960"/><br>

  **Die Nullstellung des Motors unter "sonst" bleibt 90 Grad.**

  **Die Drehungen nach Links und Rechst müssen jedoch in Relation gebracht werden, da beim Autofahren das Lenken ja auch nicht direkt die Räder im selben Winkel dreht, die du am Lenkrad drehst.**

37. Ziehe einen **Relativiere " " vonUnten 0 vonOben 100 bisUnten 0 bisOben 1000** (Map " " fromLow 0 fromHigh 100 toLow 0 to toHigh 1000) Block aus **Mathe** (Math) in die Winkelgrad **90** des **Setze SERVO zum Pin 9 zu 90 Winkelgrad** (Set SERVO from Pin 9 to 90 Degree) Block.
38. Hole dir einen **Element** (item) Block aus **Variable** (Variables) und füge diesen in die erste Position von **Relativiere " " vonUnten 0 vonOben 100 bisUnten 0 bisOben 1000** (Map " " fromLow 0 fromHigh 100 toLow 0 to toHigh 1000).

  <img src="images/Lesson15/Lesson15_Gyroscope_21.png" width="960"/><br>

39. Ändere die Werte des Blocks zu ***Relativiere AngleZ vonUnten 10 vonOben 90 bisUnten 90 bisOben 180***.
40. Kopiere diesen Block und füge die Kopie als Wert für den Winkelgrad **90** der zweiten **Tue** Spalte.
41. Ändere dessen Werte zu ***Relativiere AngleZ vonUnten -10 vonOben -90 bisUnten 90 bisOben 0***.

  <img src="images/Lesson15/Lesson15_Gyroscope_22.png" width="960"/><br>

42. Benenne dein Projekt und speichere es.

  <img src="images/Lesson15/Lesson15_Gyroscope_23.png" width="960"/><br>

43. Kompiliere deinen Code und lade das Projekt auf deinen Arduino.


#### *Aber wo ist der Sound beim Gasgeben? Hier kann man mit dem passiven Buzzer noch was machen!*

---

#### [Zurück zum Index](#index)

### 3.16. Rot, Gelb, Grün....und los! <a name="red,yellow,green...and-go"></a>

Im Strassenverkehr gibt es Ampeln, die sich nur dann Umschalten, wenn ein Auto an der Kreuzung steht.

**Lass uns so eine Ampel bauen!**

<img src="images/Lesson16/Lesson16_Motiondetector_1.png" width="960"/><br>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Prototyp Erweiterungsmodul**                   |<img src="images/Parts/Part_PrototypeExpansionModule.jpg" width="256"/>   |
| 1 | **Rote LED**                      | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/>    | 1 | **Gelbe LED**            |<img src="images/Parts/Part_LED_Yellow_5mm.jpg" width="256"/>|
| 1 | **Grüne LED**                      | <img src="images/Parts/Part_LED_Green_5mm.jpg" width="256"/>    | 3 | **220 Ohm Widerstand**            |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>|
| 1 | **HC-SR501 PIR Bewegungssensor**   |<img src="images/Parts/Part_HC-SR501.jpg" width="256"/>| 3 | **W-M Kabel**   |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> |
| 5 | **M-M Kabel**   |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***HC-SR501 PIR Bewegungssensor:***

* selbst hat zwei Sensorpunkte, wobei jeder der beiden Punkte aus einem speziellen Material besteht, das auf Infrarotlicht reagiert.
* besitzt ein Linse, die hier benutzt wird, um soviel Lichtstrahlen einzufangen wie möglich.
* vergleicht die Menge des empfangenen Infrarotlicht das von der Umgebung abgestrahlt wird.
* registriert eine Veränderung der Menge, wenn ein warmes Objekt, wie zum Beispiel auch ein Mensch oder Tier die Infrarotlichtmenge in der Umgebung ändert.
* schaltet bei einer Erhöhung ein, und bei einer Verringerung aus.
* erkennt Bewegungen in einem Winkel von 110°.
* benötigt ca. 1 Minute nach dem Anschalten zum Initialisieren. Erst nach dieser Minute sollten die folgenden Einstellungen vorgenommen werden.
* hat 2 Drehregler um den Zeitabstand zwischen 5 Sekunden und 5 Minuten und die Sensibilität von 3 bis 7 Meter zu variieren.
* hat 1 Jumpschalter, um festzulegen, ob der Sensor einmal oder jedes Mal ausgelöst wird. Dies schaltet auch den Zeitabstand ein und aus.

#### Schaltplan

<img src="images/Lesson16/Lesson16_Motiondetector_2.png" width="960"/><br>

#### Bastelanweisung

1. Rote LED: Im oberen Mittelteil in 4. Reihe mit kurzes Bein links und langes Bein
2. Gelbe LED: Im unteren Mittelteil in 1. Reihe mit kurzes Bein links und langes Bein rechts
3. Grüne LED: Im unteren Mittelteil in 4. Reihe mit kurzes Bein links und langes Bein rechts 2 Stecklöcher dazwischen
4. 3 Widerstände 220 Ohm: Oberhalb oder unterhalb des langen Beins jeder LED >> Zweites Bein des Widerstands 4 Stecklöcher rechts
5. M-M Kabel 1: Oberhalb der linken Beine der LEDs im oberen Bereich >> Arduino GND
6. M-M Kabel 2: Unterhalb des linken Beins der LED im unteren Bereich >> Arduino GND
7. M-M Kabel 3: Oberhalb des rechten Beins vom Widerstand der roten LED im oberen Bereich >> Arduino Pin 2
8. M-M Kabel 4: Oberhalb des rechten Beins vom Widerstand der gelben LED im unteren Bereich >> Arduino Pin 4
9. M-M Kabel 5: Oberhalb des rechten Beins vom Widerstand der grünen LED im unteren Bereich >> Arduino Pin 8
10. W-M Kabel 1: HC-SR501 VCC >> Arduino 5V Pin
11. W-M Kabel 2: HC-SR501 DATA >> Arduino Pin 7
12. W-M Kabel 3: HC-SR501 GND >> Arduino GND

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson16/Lesson16_Motiondetector.png" width="960"/><br>

1. Wir starten mit der Basisfunktion.

  <img src="images/Lesson16/Lesson16_Motiondetector_3.png" width="960"/><br>

  **Wir werden eine rote, eine gelbe und eine grüne LED für die Ampellichter nutzen.**

2. Ziehe 3 **Setze digital Pin# 0 zu HIGH** (Set digital pin# 0 to HIGH) Blöcke aus **Eingang/Ausgang** (Input/Output) in die Vorlaufschleife.

  <img src="images/Lesson16/Lesson16_Motiondetector_4.png" width="960"/><br>

  **Die rote LED wird beim Einschalten brennen, daher wird die LED in der Vorlaufschleife auf HIGH eingestellt, während die anderen Beiden auf LOW gestellt werden.**

3. Wähle die Pinnummer ***2***, ***4***, ***8*** und deren Status ***HIGH***, ***LOW***, ***LOW***.

  <img src="images/Lesson16/Lesson16_Motiondetector_5.png" width="960"/><br>

  **Wir werden einen boolesche Operation nutzen, welche mit wahr und falsch arbeiten. Wenn eine Bewegung registriert wird, schaltet der Wert von falsch auf wahr.**

4. Hole einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) in die Vorlaufschleife.
5. Benenne die Variable in ***Switch*** (Schalter) um.
6. Ziehe dann **Wahr** (true) aus **Logik** (Logic) und verbinde es zu **Setze Switch zu** (set Switch to).

  <img src="images/Lesson16/Lesson16_Motiondetector_6.png" width="960"/><br>

7. Ändere den Wert von **Wahr** zu ***Falsch***.
8. Dupliziere diesen Block und ziehe ihn in die Endlosschleife.

  <img src="images/Lesson16/Lesson16_Motiondetector_7.png" width="960"/><br>

  **Wir werden eine "wenn Tue" Funktion nutzen, um Falsch zu Wahr zu ändern, wenn ein Signal vom Bewegungsmelder kommt.**

9. Füge einen **wenn Tue** (if do) Block von **Logik** (Logic) in die Endlosschleife über **Setze Switch zu Falsch** (set Switch to false) ein.
10. Als Nächstes verbinde einen **" " = " "** Block von **Logik** (Logic) mit dem **wenn** Steckplatz.

  <img src="images/Lesson16/Lesson16_Motiondetector_8.png" width="960"/><br>

11. Als erste Position des **" " = " "** Block nutze einen **Lese digital Pin# 0** (Read digital Pin# 0) Block aus **Eingang/Ausgang** (Input/Output).
12. Wähle die Pinnummer ***7***.
13. Als zweite Position von **Lese digital Pin# 7 = " "** (Read digital Pin# 7 = " ") brauchst du einen **HIGH** Block aus **Eingang/Ausgang** (Input/Output).

  <img src="images/Lesson16/Lesson16_Motiondetector_9.png" width="960"/><br>

14. Kopiere erneut **Setze Switch zu Falsch** (set Switch to false) und ziehe es in die **Tue** Spalte.
15. Ändere nun den Wert **Falsch** (false) zu ***Wahr*** (true).

  <img src="images/Lesson16/Lesson16_Motiondetector_10.png" width="960"/><br>

16. Füge einen **wenn Tue** (if do) Block von **Logik** (Logic) in die Endlosschleife über **Setze Switch zu Falsch** (set Switch to false) und unterhalb des ersten **wenn Tue** Blocks ein.
17. Als Nächstes verbinde einen **" " = " "** Block von **Logik** (Logic) mit dem **wenn** Steckplatz.
18. Als erste Position des **" " = " "** Block nutze einen **Element** (item) Block aus **Variable** (Variables).
19. Wähle den Variablennamen ***Switch***.
20. Als zweite Position von **Switch = " "** (Switch = " ") brauchst du einen **Wahr** (true) Block aus **Logik** (Logic).

  <img src="images/Lesson16/Lesson16_Motiondetector_11.png" width="960"/><br>

  **Jetzt werden wir die Ampellichtsequenz erstellen.**

  ***Wir beginnen mit der gelben LED, da die rote LED bereits leuchten wird.***

21. Ziehe einen **Setze digital Pin# 0 zu HIGH** (Set digital pin# 0 to HIGH) Block in die **Tue** Spalte des **wenn Switch = Wahr Tue** (if Switch = true) Blocks.
22. Wähle die Pinnummer ***4***.
23. Dann ziehe einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block von **Zeit** (Time) darunter.

  <img src="images/Lesson16/Lesson16_Motiondetector_12.png" width="960"/><br>

24. Trage als Zeit ***3000*** ein.

  <img src="images/Lesson16/Lesson16_Motiondetector_13.png" width="960"/><br>

  **Nach Rot, Gelb-Rot, kommt Grün.**

25. Kopiere alle 3 **Setze digital pin#...** (set digital pin#...) Blöcke und plaziere diese in der selben Reihenfolge unterhalb des Zeitblocks.
26. Ändere den Wert von Pin 2 zu ***LOW***, von Pin 4 zu ***LOW*** und von Pin 8 zu ***HIGH***.
27. Kopiere den Zeitblock **Warte 3000 Millisekunden** (wait 3000 milliseconds) und ziehe die Kopie unterhalb der **Setze digital Pin# 8 zu HIGH** (Set digital pin# 8 to HIGH).
28. Verlängere die Zeit auf ***5000***.

  <img src="images/Lesson16/Lesson16_Motiondetector_14.png" width="960"/><br>

  **Nach der Grünphase schaltet die Ampel zurück auf Gelb.**

29. Kopiere den **Setze digital pin#...** (set digital pin#...) Block von Pinnummer 4 und 8. Setze diese nacheinander unterhalb des Zeitblocks mit 5000 Millisekunden.
30. Ändere den Wert von Pin 4 zu ***HIGH*** und von Pin 8 zu ***LOW***.
31. Dupliziere den Zeitblock mit 3000 Millisekunden und plaziere ihn darunter.

  <img src="images/Lesson16/Lesson16_Motiondetector_15.png" width="960"/><br>

  **Nach 3 Sekunden schaltet dann das Gelb auf Rot.**

32. Kopiere den **Setze digital pin#...** (set digital pin#...) Block von Pinnummer 2 und 4. Setze diese nacheinander unterhalb des Zeitblocks mit 3000 Millisekunden.
33. Ändere den Wert von Pin 2 zu ***HIGH*** und von Pin 4 zu ***LOW***.

  <img src="images/Lesson16/Lesson16_Motiondetector_16.png" width="960"/><br>

  **Damit ist die Sequenz beendet. Es bleibt Rot, bis eine Bewegung erneut registriert wird.**

34. Benenne dein Projekt und speichere es ab.

  <img src="images/Lesson16/Lesson16_Motiondetector_17.png" width="960"/><br>

35. Kompiliere den Code und lade ihn auf deinen Arduino.

---

#### [Zurück zum Index](#index)

### 3.17. Wie spät ist es? <a name="what-time-is-it"></a>

<img src="images/Lesson17/Lesson17_Clock_1.png" width="960"/><br>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte**                   |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 1 | **DS3231 RTC Modul**   | <img src="images/Parts/Part_DS3231_RTC_Modul.jpg" width="256"/>    | 1 | **LCD1602 Modul**     |<img src="images/Parts/Part_LCD1602_Modul.jpg" width="256"/>|
| 1 | **Potentiometer 10 kOhm**            |<img src="images/Parts/Part_Potentiometer10kohm.jpg" width="256"/>| 20 | **M-M Kabel**                    |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |


#### Neue Komponenten

***DS3231 Real Time Clock Modul:***

* ist ein einfacher Chip, der die Zeit anzeigen kann.
* hat eine kleine integrierte Batterie, sodass die Uhr weiterhin funktioniert, auch wenn sonst keine Energie durch den Arduino zugeführt wird.

***LCD 16 x 2 Display***

* ist eine alphanumerische Flüssigkeitkristallanzeige.
* hat eine LED-Hintergrundbeleuchtung.
* kann zwei Reihen mit jeweils 16 Zeichen darstellen.

***Potentiometer 10 kOhm***

* ist ein analoger Drehregler mit einem Wertebereich von 0 - 1024.
* wird als Beispiel als Dimmschalter genutzt um die Lichtstärke zu variieren.
* wird in vielen elektrischen Geräten verwandt.

#### Schaltplan

<img src="images/Lesson17/Lesson17_Clock_2.png" width="960"/><br>


#### Bastelanweisung

1. LCD 16x2 Display: Im oberen oder unteren Mittelteil in Reihe (Rechts)
2. DS3231 RTC: Im oberen oder unteren Mittelteil in Reihe (Links)
3. Potentiometer: Mit zwei Pins im oberen Mittelteil über Mittelbrücke mit einem Pin im unteren Mittelteil (Mitte)
4. M-M Kabel 1: Obere blaue Reihe der Steckplatte  >> Arduino GND
5. M-M Kabel 2: Obere rote Reihe der Steckplatte >> Arduino 5V
6. M-M Kabel 3: Oberhalb linken Pin (GND) des Potentiometer oberer Mittelteil >> Obere blaue Reihe (-) der Steckplatte
7. M-M Kabel 4: Oberhalb rechten Pin (VCC) des Potentiometer oberer Mittelteil >> Obere rote Reihe (+) der Steckplatte
8. M-M Kabel 5: Unterhalb Pin des Potentiometer im unteren Mittelteil >> Oberhalb 3. Pin der LCD 16x2
9. M-M Kabel 6: Oberhalb 1. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
10. M-M Kabel 7: Oberhalb 2. Pin der LCD 16x2 >> Obere rote Reihe (+) der Steckplatte
11. M-M Kabel 8: Oberhalb 4. Pin der LCD 16x2 >> Arduino Pin 7
12. M-M Kabel 9: Oberhalb 4. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
13. M-M Kabel 10: Oberhalb 5. Pin der LCD 16x2 >> Arduino Pin 8
14. M-M Kabel 11: Oberhalb 11. Pin der LCD 16x2 >> Arduino Pin ~9
15. M-M Kabel 12: Oberhalb 12. Pin der LCD 16x2 >> Arduino Pin ~10
16. M-M Kabel 13: Oberhalb 13. Pin der LCD 16x2 >> Arduino Pin ~11
17. M-M Kabel 14: Oberhalb 14. Pin der LCD 16x2 >> Arduino Pin 12
18. M-M Kabel 15: Oberhalb 15. Pin der LCD 16x2 >> Obere rote Reihe (+) der Steckplatte
19. M-M Kabel 16: Oberhalb 16. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
20. M-M Kabel 17: Oberhalb DS3231 RTC GND >> Obere rote Reihe der Steckplatte
21. M-M Kabel 18: Oberhalb DS3231 RTC VCC >> Obere blaue Reihe der Steckplatte
22. M-M Kabel 19: Oberhalb DS3231 RTC SDA >> Arduino Pin Analog 4 (A4)
23. M-M Kabel 20: Oberhalb DS3231 RTC SDL >> Arduino Pin Analog 5 (A5)

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson17/Lesson17_Clock.png" width="960"/><br>

1. Starte mit der Basisfunktion.

  **initiiere das Echtzeituhr und die Anzeige.**

2. Ziehe **Setup Echtzeituhr auf Pin# A4 und A5** (Setup Real-Time Clock on pin# A4 and A5) von **Zeit** (Time) in die Vorlaufschleife.

  <img src="images/Lesson17/Lesson17_Clock_3.png" width="960"/><br>

3. Dann hole einen **Setup LCD 16x2 auf digitale Pins** (Setup LCD 16x2 auf digital pins) von **Bildschirm** (Displays) in die Vorlaufschleife.
4. Ändere die Pinnummern zu ***DB4: 9, DB5: 10, DB6: 11, DB7: 12***.

  <img src="images/Lesson17/Lesson17_Clock_4.png" width="960"/><br>

  **Wenn du die Echtzeituhr das erste Mal nutzt, oder das Datum und Uhrzeit später neu einstellen willst, benötigen wir den Block dafür.**

5. Hole dir den Block **Stelle Uhr vom RTC manuell ein** (Set clock of RTC manually) von **Zeit** (Time) in die Vorlaufschleife.

  <img src="images/Lesson17/Lesson17_Clock_5.png" width="960"/><br>

  **Speichere die Daten der Uhr als Wert in Varialben.**

6. Ziehe einen **Lese Datum und Zeit vom RTC Modul** (Read Date and Time from RTC Module) Block von **Zeit** (Time) in die Endlosschleife.

  <img src="images/Lesson17/Lesson17_Clock_6.png" width="960"/><br>

7. Wähle nacheinander die **Element** Variablen, klick auf **Neue Variable...** (New variable...). Nenne diese passend zur jeweiligen Datenpunkt. Hier: ***Year*** (Jahr), ***Month*** (Monat), ***Day*** (Tag), ***Hour*** (Stunde), ***Minute*** (Minute), ***Second*** (Sekunde).

  <img src="images/Lesson17/Lesson17_Clock_7.png" width="960"/><br>

  **Jetzt soll das Datum und Zeit zur Anzeige geschickt werden.**

8. Ziehe ein **LCD 16x2 >> Schreibe zur Anzeige** (LCD 16x2 >> Write to Display) Block von **Bildschirm** (Displays) in die Endlosschleife.
9. Trenne die beiden **" "** Blöcke von **LCD 16x2 >> Schreibe zur Anzeige** (LCD 16x2 >> Write to Display).

  ***Behalte sie auf deinem Arbeitsbereich. Wir werden diese noch benötigen.***

  <img src="images/Lesson17/Lesson17_Clock_9.png" width="960"/><br>

  **Wir haben 2 Zeilen in unserer Anzeige. Eine Zeile soll das Datum anzeigen und eine Zeile die Zeit.**

  **Da wir mehr als eine Variable haben, werden wir diese zu einem Text zusammensetzen.**

10. Hole dir zwei **Erzeuge Text mit** (create text with) Blöcke von **Text** und verbinde diese zu **1. Zeile** (1st Line) und **2. Zeile** (2nd Line).

  <img src="images/Lesson17/Lesson17_Clock_10.png" width="960"/><br>

  **Auch zwei Verbindungen reichen noch nicht.**

11. Presse auf das jeweilige Zahnradsymbol und ziehe 3 weitere **Element** Blöcke von links nach rechts.

  ***Es sollten nun 5 Verbindungen pro Linie sein.***

  <img src="images/Lesson17/Lesson17_Clock_11.png" width="960"/><br>

12. Klicke nun jeweils auf **Year** (Jahr), **Month** (Monat), **Day** (Tag) und verknüpfe diese nacheinander mit der 1., 3. und 5. Verbindung in **Erzeuge Text mit** von **1. Zeile**.
13. Wiederholde dies mit **Hour** (Stunde), **Minute** (Minute), **Second** (Sekunde) und verknüpfe diese nacheinander mit der 1., 3. und 5. Verbindung in **Erzeuge Text mit** von **2. Zeile**.

  <img src="images/Lesson17/Lesson17_Clock_12.png" width="960"/><br>

  ***Jetzt kannst du die " " Blöcke nutzen, welche du auf die Ablage gezogen hattest.***

14. Kopiere oder hole dir **" "** Blöcke von **Text**, damit insgesamt die 4 übrigen Verbindungen von **Erzeuge Text mit** der **1. Zeile** und **2. Zeile** gefüllt sind.
15. Fülle ***/*** als Text für die Abstände in Datum, also **1. Zeile**, und ***:*** als Text für die Abstände in Zeit, also **2. Zeile**.

  <img src="images/Lesson17/Lesson17_Clock_13.png" width="960"/><br>

  **Jetzt soll die Anzeige mit jeder Schleife gelöscht, damit diese neu beschrieben werden kann.**

16. Ziehe einen **Lösche LCD 16x2 Anzeige** (Clear LCD 16x2 Display) Block aus **Bildschirm** (Displays) in den Mittelteil der Endlosschleife.

  <img src="images/Lesson17/Lesson17_Clock_14.png" width="960"/><br>

17. Trage nun das Datum und Uhrzeit in die Felder des **Stelle Uhr vom RTC manuell ein** (Set clock of RTC manually) Blocks.

  ***Setze die Zeit ca. eine Minute in die Zukunft, damit du noch Zeit hast den Code zu verifizieren. Rechne auch mit einigen Sekunden nachdem du das Hochladen zum Arduino ausgelöst hast.***

  <img src="images/Lesson17/Lesson17_Clock_15.png" width="960"/><br>

18. Verifiziere dein Code, warte bis die Zeit ein paar Sekunden an die Voreinstellung herankommt, dann lade es auf deinen Arduino.

  <img src="images/Lesson17/Lesson17_Clock_16.png" width="960"/><br>

19. Wenn die Zeit korrekt ist, klicke auf das Häckchen im **Stelle Uhr vom RTC manuell ein** (Set clock of RTC manually) Block.

  **Damit kannst du nun weiterhin an deinem Code, der Darstellung und weiteren Projekten arbeiten.**

  **Solang die kleine Battery im Modul genug Energie hat, wird die Uhr weiterticken, auch wenn du den Arduino davon entfernst.**

  <img src="images/Lesson17/Lesson17_Clock_17.png" width="960"/><br>

  #### *Hast du etwas bei der Anzeige bemerkt?*

  *Wenn die Zahl der Minuten und Sekunden unterhalb der 10 sind, also einstellig sind, wird diese auch nur so dargestellt.*

  **Lass uns anhand einer neuen Funktion eine 0 vor die Stunden, Minuten und Sekunden setzen, wenn diese unter 10 fallen.**

20. Trenne die Variablen **Hour** (Stunde), **Minute** (Minute), **Second** (Sekunde) von **Erzeuge Text mit** (Create Text with) der **2. Zeile** und lege sie auf dem Arbeitsplatz ab.

  ***Du kannst die Variable Minute und Sekunde löschen!***

21. Ziehe den Block **Test Wenn Wahr Wenn Falsch** (Test if true if false) aus **Logik** (Logic) in die vorherigen Position von **Hour** (Stunde).

  <img src="images/Lesson17/Lesson17_Clock_18.png" width="960"/><br>

  **Wenn es wahr ist, dass die Zahl unter 10 ist, dann füge eine 0 vor die Zahl. Wenn es falsch ist, nehme die Zahl, wie sie kommt.**

22. Wir benötigen den **" " = " "** Block aus **Logik** (Logic) als Vergleichfunktion in der **Test** Verbindung.

23. Ändere das **=** Symbol zu ***<*** und ziehe **Hour** (Stunde) vom Arbeitsplatz in die erste Position von **" " < " "**.

  <img src="images/Lesson17/Lesson17_Clock_19.png" width="960"/><br>

24. Als zweite Position von **Hour < " "** brauchen wir ein **0** Block von **Mathe** (Math).
25. Ändere den Wert zu ***10***.

  <img src="images/Lesson17/Lesson17_Clock_20.png" width="960"/><br>

  **Wenn wahr, wollen wir einen Text aus einer 0 und der jeweiligen Zahl erzeugen.**

26. Ziehe **Erzeuge Text mit** (Create Text with) aus **Text** und verbinde es mit **Wenn Wahr** (if true).

  <img src="images/Lesson17/Lesson17_Clock_21.png" width="960"/><br>

  **Die Zahl 0 wollen wir bereits als Text zur Verfügung stellen.**

27. Die 1. Position von **Erzeuge Text mit** (Create Text with) wird ein **" "** Block aus **Text**.
28. Schreibe eine ***0*** hinein.

  <img src="images/Lesson17/Lesson17_Clock_22.png" width="960"/><br>

29. Mache nun 2 Kopien des Variablenblocks **Hour** und setze diese jeweils in die 2. Position von **Erzeuge Text mit** (Create Text with) der **Wenn Wahr** (if true) und in die **Wenn Falsch** Verbindung.

  <img src="images/Lesson17/Lesson17_Clock_23.png" width="960"/><br>

30. Jetzt klicke auf den **Test Wenn Wahr Wenn Falsch** (Test if true if false), dupliziere diesen zweimal und füge diese jeweils in die Platzhalter wo Minute und Sekunde vorher platziert war.
31. Ändere nun die Variablen von **Hour** zu ***Minute*** oder ***Seconds***, je nach Position.

  <img src="images/Lesson17/Lesson17_Clock_24.png" width="960"/><br>

32. Gib deinem Projekt einen Namen und speichere es ab.

  ***Check nochmal, ob das Häckchen in der manuellen Uhreneinstellung nicht sichtbar ist.***

33. Kompiliere den Code und lade ihn auf deinen Arduino.

---

#### [Zurück zum Index](#index)

### 3.18. Wie tief ist es? <a name="how-deep-is-it"></a>

Für dieses Projekt ist es ratsam ein kleines Gefäss mit Wasser bereitstellt, den wir wollen den Stand messen.

Den Wasserstandsmelder kann man nutzen um zu Verhindern, dass etwas überläuft oder die Wasserversorgung von Pflanzen immer genug Wasser im Tank haben.

<img src="images/Lesson18/Lesson18_Waterlevelsensor_1.png" width="960"/><br>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte**                   |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 1 | **Wasserstandsmodul**   | <img src="images/Parts/Part_WaterLevelModul.jpg" width="256"/>    | 1 | **LCD1602 Modul**     |<img src="images/Parts/Part_LCD1602_Modul.jpg" width="256"/>|
| 1 | **Potentiometer 10 kOhm**            |<img src="images/Parts/Part_Potentiometer10kohm.jpg" width="256"/>| 3 | **W-M Kabel**                    |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |
| 19 | **M-M Kabel**                    |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***Wasserstandsmodul***

* wurde entwickelt, um zum Beispiel zum Erkennen von Regen, dem Wasserstand oder auslaufender Flüssigkeiten zu erkennen.
* besteht aus einem elektronischer Anschluss, einem 1 MΩ Widerstand und mehreren blanken Leitern bzw. Antennen.
* funktioniert indem der Widerstand mit den Leitungen und die Erdung mit Leitungen verbunden sind. Sobald Wasser auf den Sensor kommt, werden diese kurzgeschlossen und der Strom fliesst.
* kann mit einem digitalen Pin als Schalter dienen, während es mit einem analgen Pin zusätzlich Wasserstand oder Wassermenge bestimmen kann.

#### Schaltplan

<img src="images/Lesson18/Lesson18_Waterlevelsensor_2.png" width="960"/><br>

#### Bastelanweisung

1. LCD 16x2 Display: Im oberen oder unteren Mittelteil in Reihe (Rechts)
2. Potentiometer: Mit zwei Pins im oberen Mittelteil über Mittelbrücke mit einem Pin im unteren Mittelteil (Mitte)
3. M-M Kabel 1: Obere blaue Reihe der Steckplatte  >> Arduino GND
4. M-M Kabel 2: Obere rote Reihe der Steckplatte >> Arduino 5V
5. M-M Kabel 3: Oberhalb linken Pin (GND) des Potentiometer oberer Mittelteil >> Obere blaue Reihe (-) der Steckplatte
6. M-M Kabel 4: Oberhalb rechten Pin (VCC) des Potentiometer oberer Mittelteil >> Obere rote Reihe (+) der Steckplatte
7. M-M Kabel 5: Unterhalb Pin des Potentiometer im unteren Mittelteil >> Oberhalb 3. Pin der LCD 16x2
8. M-M Kabel 6: Oberhalb 1. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
9. M-M Kabel 7: Oberhalb 2. Pin der LCD 16x2 >> Obere rote Reihe (+) der Steckplatte
10. M-M Kabel 8: Oberhalb 4. Pin der LCD 16x2 >> Arduino Pin 7
11. M-M Kabel 9: Oberhalb 4. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
12. M-M Kabel 10: Oberhalb 5. Pin der LCD 16x2 >> Arduino Pin 8
13. M-M Kabel 11: Oberhalb 11. Pin der LCD 16x2 >> Arduino Pin ~9
14. M-M Kabel 12: Oberhalb 12. Pin der LCD 16x2 >> Arduino Pin ~10
15. M-M Kabel 13: Oberhalb 13. Pin der LCD 16x2 >> Arduino Pin ~11
16. M-M Kabel 14: Oberhalb 14. Pin der LCD 16x2 >> Arduino Pin 12
17. M-M Kabel 15: Oberhalb 15. Pin der LCD 16x2 >> Obere rote Reihe (+) der Steckplatte
18. M-M Kabel 16: Oberhalb 16. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
19. M-M Kabel 17: Oberhalb Wasserstandsmodul GND Kabel >> Obere rote Reihe der Steckplatte
20. M-M Kabel 18: Oberhalb Wasserstandsmodul VCC Kabel >> Obere blaue Reihe der Steckplatte
21. M-M Kabel 19: Oberhalb Wasserstandsmodul S Kabel >> Arduino Pin Analog 3 (A3)
22. W-M Kabel 1: Wasserstandsmodul GND Pin >> Oberes Mittelteil der Steckplatte
23. W-M Kabel 2: Wasserstandsmodul VCC Pin >> Oberes Mittelteil der Steckplatte
24. W-M Kabel 3: Wasserstandsmodul S Pin >> Oberes Mittelteil der Steckplatte

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**


#### Block für Block

<img src="images/Lesson18/Lesson18_Waterlevelsensor.png" width="960"/><br>

  **Nachdem du die Basisfunktion auf den Arbeitsbereich gezogen hast, geht es mit der Anzeige weiter.**

1. Ziehe den Setupblock **Setup LCD 16x2 auf digitalen Pins** (Set LCD 16x2 on digital pins) in die Vorlaufschleife.
2. Benutze die Pinnummern nacheinander ***9***, ***10***, ***11***, ***12***.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_3.png" width="960"/><br>

  **Die Wasserstandsanzeige ist sehr genau. Du brauchst lediglich die Veränderung in gewissen Abständen, die Anzeige wechselt nicht zu oft.**

  **Daher wirst du den derzeitigen mit einem Wert vergleichen, welchen du zuvor als Variable festgehalten hast.**

3. Setze die erste Variable durch einen **Setze Element zu** (set item to) Block von **Variable** (Variables) in Verbindung mit einem **0** Block von **Mathe** (Math), welche verbunden in der Vorlaufschleife landen.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_4.png" width="960"/><br>

4. Ändere den Variablennamen zu ***Value*** (Wert).

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_5.png" width="960"/><br>

5. Dupliziere den Block **Setze Value zu 0** (set Value to 0), lege ihn in der Vorlaufschleife ab.
6. Wähle **Neue Variable...** (New Variable...) und ändere den Variablennamen zu ***OldValue*** (Alter Wert).
7. Dupliziere den Block **Setze Value zu 0** (set Value to 0) erneut und lege ihn in der Endlosschleife ab.
8. Ersetze die **0** dieses Block mit **Lese analog Pin# A0** (Read analog pin# A0) aus **Eingang/Ausgang** (Input/Output).

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_6.png" width="960"/><br>

9. Ändere die Pinnummer von **Lese analog Pin# A0** (Read analog pin# A0) zu ***A3***.
10. Hole dir einen **Wenn Tue** (if do) Block von **Logik** (Logic) in die Endlosschleife.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_7.png" width="960"/><br>

11. Verbinde **Wenn** (if) mit einem **" " and " "** Block aus **Logik** (Logic).
12. Wechsle **und** (and) zu ***oder*** (or).

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_8.png" width="960"/><br>

13. Als erste Position von **" " or " "** nimm einen **" " = " "** Block aus **Logik** (Logic).

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_9.png" width="960"/><br>

14. Verändere das **=** zu ***>***.
15. Füge einen **Element** Block von **Variable** (Variables) als erste Position von **" " > " "** ein.
16. Wähle für **Element** den Variablennamen ***Value***.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_10.png" width="960"/><br>

17. Als zweite Position von **Value > " "** brauchst du einen **" " + " "** Block von **Mathe** (Math).

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_11.png" width="960"/><br>

18. Dupliziere den **Value** Block und ziehe diesen in die erste Position von **" " + " "**.
19. Wechsle den Variablennamen zu ***OldValue***.
20. Für die zweite Position in **OldValue + " "** benötigst du einen **0** Block aus **Mathe** (Math).
21. Erhöhe die Zahl auf ***2***.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_12.png" width="960"/><br>

22. Dupliziere nun den kompletten Block **Value > OldValue + 2** und füge diesen in die zweite Position von **Value > OldValue + 2 or " "** ein.
23. Ändere nun dessen Werte von **>** zu ***<*** und **+** zu ***-***.

  ***Durch das rechte Mausklick oder Fingerhalten auf "Value > OldValue + 2 or Value < OldValue - 2", kannst du im Menü die Option "Externe Eingänge" auswählen, um diesen Block vertikal darzustellen.***

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_13.png" width="960"/><br>

  **Wenn der neue Wert um 2 sinkt oder ansteigt, soll die Anzeige gelöscht und alte Wert mit dem neuen ausgetauscht werden.**

24. Ziehe den Block **Lösche LCD 16x2 Anzeige** (Clear LCD 16x2 Display) von **Bildschirm** (Displays) in die **Tue** Spalte.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_14.png" width="960"/><br>

25. Hole dir einen **Setze Element zu** (set item to) und einen **Element** (item) Block aus **Variable** (Variables) auf den Arbeitsbereich.
26. Ändere den Variablenname **Element** zu ***Value*** und verbinde ihn mit **Setze Element zu** (set item to).
27. Wähle **OldValue** für **Element** des **Setze Element zu** (set item to) Blocks und ziehe den gesamten Block unterhalb der **Lösche LCD 16x2 Anzeige** (Clear LCD 16x2 Display) in der **Tue** Spalte.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_15.png" width="960"/><br>

  **Auch hier geben wir der Anzeige ein wenig Zeit um Flickern zu verhindern.**

28. Ziehe einen **Warte 100 Mikrosekunden** (wait 100 microseconds) von **Zeit** (Time) in die Endlosschleife als unterste Position.
29. Reduziere die Zeit auf ***10***.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_16.png" width="960"/><br>

  **Nun brauch nur noch die Anzeige den Wert anzeigen.**

30. Positioniere einen Block **LCD 16x2 >> Schreibe zur Anzeige** (LCD 16x2 >> Write to Display) unterhalb des Zeitblocks in die Endlosschleife.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_17.png" width="960"/><br>

31. Schreibe in den Textblock der ersten Zeile ***WaterLevel*** (Wasserstand), und lösche den Textblock der zweiten Zeile.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_18.png" width="960"/><br>

32. Benutze einen **Element** Block aus **Variable** (Variables) als Text für die zweite Zeile des **LCD 16x2 >> Schreibe zur Anzeige** (LCD 16x2 >> Write to Display) Blocks.
33. Wähle für **Element** den Variablenname ***OldValue***.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_19.png" width="960"/><br>

34. Benenne dein Projekt und speichere es.
35. Verifiziere den Code und lade ihn auf deinen Arduino.

  ***Halte nun den Sensor ins Wasser***

>Ich sinke!!!

>Wer hat von meinem Becherchen gedrunken?!

---

#### [Zurück zum Index](#index)

### 3.19. Wie ein Schreikopf <a name="lets-scream"></a>

In diesem Projekt wollen wir ein kleines Mikrofon nutzen, um deine Stimme mit einem grafischen Pegel darzustellen.

<img src="images/Lesson19/Lesson19_MicrophoneSensor_1.png" width="960"/><br>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Steckplatte** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Soundsensormodul** | <img src="images/Parts/Part_SoundSensor.jpg" width="256"/> | 1 | **LCD1602 Modul** |<img src="images/Parts/Part_LCD1602_Modul.jpg" width="256"/>|
| 1 | **Potentiometer 10 kOhm** |<img src="images/Parts/Part_Potentiometer10kohm.jpg" width="256"/>| 4 | **W-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |
| 20 | **M-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***Soundsensormodul***

* ist ein Wandler, der Schallwellen in elektrische Signale verwandelt.
* hat als Mikrofon die umgekehrte Aufgabe eines Lautsprechers.
* ist ein Kondensatormikrofon welches in Smartphones und Laptop genutzt werden.

Ein Kondesatormikrofon hat 2 kleine Platten in einem kleinen Abstand eingebaut.
Eine Platte ist fixiert und wird mit Energie geladen. Die zweite Platte ist flexible und wird durch die Schallwellen in Bewegung versetzt.
Wenn diese Platten sich berühren, ändert sich die Kapazität, welche dann je nach der Art des Sounds und Lautstärke variiert.

#### Schaltplan

<img src="images/Lesson19/Lesson19_MicrophoneSensor_2.png" width="960"/><br>

#### Bastelanweisung

1. LCD 16x2 Display: Im oberen oder unteren Mittelteil in Reihe (Rechts)
2. Potentiometer: Mit zwei Pins im oberen Mittelteil über Mittelbrücke mit einem Pin im unteren Mittelteil (Mitte)
3. M-M Kabel 1: Obere blaue Reihe der Steckplatte  >> Arduino GND
4. M-M Kabel 2: Obere rote Reihe der Steckplatte >> Arduino 5V
5. M-M Kabel 3: Oberhalb linken Pin (GND) des Potentiometer oberer Mittelteil >> Obere blaue Reihe (-) der Steckplatte
6. M-M Kabel 4: Oberhalb rechten Pin (VCC) des Potentiometer oberer Mittelteil >> Obere rote Reihe (+) der Steckplatte
7. M-M Kabel 5: Unterhalb Pin des Potentiometer im unteren Mittelteil >> Oberhalb 3. Pin der LCD 16x2
8. M-M Kabel 6: Oberhalb 1. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
9. M-M Kabel 7: Oberhalb 2. Pin der LCD 16x2 >> Obere rote Reihe (+) der Steckplatte
10. M-M Kabel 8: Oberhalb 4. Pin der LCD 16x2 >> Arduino Pin 7
11. M-M Kabel 9: Oberhalb 4. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
12. M-M Kabel 10: Oberhalb 5. Pin der LCD 16x2 >> Arduino Pin 8
13. M-M Kabel 11: Oberhalb 11. Pin der LCD 16x2 >> Arduino Pin ~9
14. M-M Kabel 12: Oberhalb 12. Pin der LCD 16x2 >> Arduino Pin ~10
15. M-M Kabel 13: Oberhalb 13. Pin der LCD 16x2 >> Arduino Pin ~11
16. M-M Kabel 14: Oberhalb 14. Pin der LCD 16x2 >> Arduino Pin 12
17. M-M Kabel 15: Oberhalb 15. Pin der LCD 16x2 >> Obere rote Reihe (+) der Steckplatte
18. M-M Kabel 16: Oberhalb 16. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
19. W-M Kabel 1: Soundsensormodul GND Pin >> Oberes Mittelteil der Steckplatte
20. W-M Kabel 2: Soundsensormodul VCC Pin >> Oberes Mittelteil der Steckplatte
21. W-M Kabel 3: Soundsensormodul D0 Pin >> Oberes Mittelteil der Steckplatte
22. W-M Kabel 4: Soundsensormodul A0 Pin >> Oberes Mittelteil der Steckplatte
23. M-M Kabel 17: Oberhalb Soundsensormodul GND Kabel >> Obere rote Reihe der Steckplatte
24. M-M Kabel 18: Oberhalb Soundsensormodul VCC Kabel >> Obere blaue Reihe der Steckplatte
25. M-M Kabel 19: Oberhalb Soundsensormodul D0 Kabel >> Arduino Pin ~3
26. M-M Kabel 20: Oberhalb Soundsensormodul A0 Kabel >> Arduino Pin Analog 0 (A0)

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson19/Lesson19_MicrophoneSensor.png" width="960"/><br>

1. Nachdem du die Basisfunktion auf dem Arbeitsbereich abgelegt hast, ziehe einen **Setup LCD 16x2 auf digitalen Pins** (Setup LCD 16x2 on digital pins) Block aus **Bildschirm** (Displays) in die Vorlaufschleife.
2. Ändere die Pinnummern zu ***9***, ***10***, ***11*** und ***12***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_3.png" width="960"/><br>

3. Hole dir einen **Lösche LCD 16x2 Anzeige** (Clear LCD 16x2 Anzeige) Block aus **Bildschirm** (Displays) in die Endlosschleife.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_4.png" width="960"/><br>

  **Wenn dein Arduino ein Audiosignal erhält, soll die eingebaute LED aufleuchten.**

4. Ziehe einen **Setze built-in LED BUILTIN_1 zu HIGH** (Set built-in LED BUILTIN_1 to HIGH) Block von **Eingang/Ausgang** (Input/Output) in die Vorlaufschleife.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_5.png" width="960"/><br>

  **Die Anzeige wird sich je nach Lautstärkesignal verändern. Das Signal hat eine Reichweite von 0 - 1024. Jede Stufe der Lautstärkerepresentation besteht aus 2 Zeichen. Die gesamte Zeile ist 16 Zeichen.**

  **Daher werden die 1024 in 8 Teile aufgeteilt, also 64 pro Stufe.**

5. Platziere dazu einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) in die Vorlaufschleife.
6. Nenne die Variable ***BarValue*** (Stufenwert).

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_6.png" width="960"/><br>

7. Verknüpfe einen **0** Block aus **Mathe** (Math) mit dem **Setze BarValue zu** (set BarValue to).
8. Erhöhe den **0** Wert auf ***64***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_7.png" width="960"/><br>

  **Nun wirst du den Stufenwerttext erzeugen, dass später in der Anzeige sichtbar ist.**

9. Ziehe einen weiteren **Setze Element zu** (set item to) Block aus **Variable** (Variables) in die Vorlaufschleife.
10. Gib diesen Block den Namen **BarText** (Stufenwerttext).
11. Dann ziehe ein **" "** Block aus **Text** und verbinde diesen mit **Setze BarText zu** (set BarText to).
12. Trage ***>>*** als Text ein.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_8.png" width="960"/><br>

  **Jetzt wirst du den analogen Wert des Mikrofons als Variable deklarieren.**

13. Ziehe erneut einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) diesmal in die Endlosschleife.
14. Nenne die Variable **AnalogValue** (Analoger Wert).

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_9.png" width="960"/><br>

15. Verbinde diesen Block mit **Lese analog Pin# A0** (Read analog pin# A0) aus **Eingang/Ausgang** (Input/Output).

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_10.png" width="960"/><br>

16. Setze unterhalb einen weiteren Variablenblock mit dem Namen ***DigitalValue*** ein, und verbinde diesen mit einem **Lese digital Pin# 0** (Read digital pin# 0) Block aus **Eingang/Ausgang** (Input/Output).
17. Wähle die digitale Pinnummer ***3***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_11.png" width="960"/><br>

  **Nutze eine "Wenn Tue" Funktion, um die eingebaute LED aufleuchten zu lassen, wenn das Audiosignal registriert wird.**

18. Ziehe einen **Wenn Tue** (if do) Block aus **Logik** (Logic) in die Endlosschleife.
19. Klicke auf das Zahnradsymbol und addiere den **sonst** Block zur Funktion.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_12.png" width="960"/><br>

20. Verbinde einen **" " = " "** Block aus **Logik** (Logic) mit **wenn**.
21. Dann benutze einen **Element** Block aus **Variable** (Variables) als erste Position von **" " = " "**.
22. Wähle **DigitalValue** als me.
23. Als zweite Position von **DigitalValue = " "** brauchst du einen **HIGH** Block aus **Eingang/Ausgang** (Input/Output).

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_13.png" width="960"/><br>

24. Mache zwei Kopien von **Setze built-in LED BUILTIN_1 zu HIGH** (Set built-in LED BUILTIN_1 to HIGH) und füge diese jeweils in den Spalt von **Tue** und **sonst** ein.
25. Verändere den Wert des Blocks in **Tue** zu ***LOW***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_14.png" width="960"/><br>

  **Um den Stufenwerttext dem hereinkommenden Audiosignal anzupassen, brauchst du eine "Wenn tue" Funktion.**

26. Ziehe einen **Wenn Tue** (if do) Block aus **Logik** (Logic) in die Endlosschleife.
27. Klicke auf das Zahnradsymbol und füge 6 **anderenfalls** Blöcke hinzu, damit du insgesamt 8 Spalten hast.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_15.png" width="960"/><br>

  **Nun wirst du wie in vorherigen Beispielen einen Wertebereich festlegen, indem eine gewisse Aufgabe absolviert wird.**

  ***In diesem Beispiel soll ein bestimmter Stufenwerttext in einem bestimmten Lautstärkebereich dargestellte werden.***

28. Als Basis nutze einen **" " und " "** (" " and " ") Block aus **Logik** (Logic) und verbinde diesen mit **wenn**.
29. Setze für die erste Position einen **" " = " "** Block aus **Logik** (Logic) ein.
30. Ändere das **=** Symbol zu ***>=***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_16.png" width="960"/><br>

31. Als erste Position von **" " >= " "** benutze ein **Element** Block aus **Variable** (Variables).
32. Der me dafür ist **AnalogValue**.
33. Für die zweite Position benötigst du einen **0** Block aus **Mathe** (Math).

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_17.png" width="960"/><br>

34. Duplizier den **AnalogVaule >= 0** Block und nutze diesen in der zweiten Position von **AnalogValue >= 0 und " "** daneben.
35. Ändere das Symbol **>=** dort zu ***<=***.
36. Ziehe den **0** Block von **AnalogValue <= 0** auf den Arbeitsbereich und setze anstelle dessen einen **" " + " "** Block aus **Mathe** (Math) dafür ein.
37. Ändere das Symbol von **+** zu **x**.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_18.png" width="960"/><br>

38. Verwende erste Position von **" " x " "** einen **Element** (item) Block von **Variable** (Variables) mit dem Variablennamen ***BarValue***.
39. Ziehe den Matheblock von deinem Arbeitsbereich in die zweite Position von **BarValue x " "**.
40. Verändere den Wert auf ***2***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_19.png" width="960"/><br>

41. Dupliziere den kompletten Block in Verbindung mit **wenn** (if) und platziere diese Kopie in die erste **anderenfalls** (else if) Verbindung.
42. Mache einen Kopie von **BarValue x 2** und setze diese in die zweite osition der ersten Position des gesamten **anderenfalls** (else if) Verbindungsblock anstelle von **0**.

  ***Den "0" Block kannst du löschen.***

43. Ändere den Wert **2** des **BarValue x 2** Block in der zweiten Position des gesamten **anderenfalls** (else if) Verbindungsblock zu ***4***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_20.png" width="960"/><br>

44. Erzeuge jetzt 6 Duplikate des Block verbunden zum ersten **anderenfalls** (else if) Schlitz.
45. Setze jeweils eine Kopie in die verbleibenden **anderenfalls** (else if) Schlitz.
46. Jetzt erhöhe die beiden Werte **2** und **4** in jedem folgenden **andernfalls** (else if) um ***2***.

  ***1. Kopie >> 1. Wert 1: 4, Wert 2: 6***<br>
  ***2. Kopie >> 1. Wert 1: 6, Wert 2: 8***<br>
  ***3. Kopie >> 1. Wert 1: 8, Wert 2: 10***<br>
  ***4. Kopie >> 1. Wert 1: 10, Wert 2: 12***<br>
  ***5. Kopie >> 1. Wert 1: 12, Wert 2: 14***<br>
  ***6. Kopie >> 1. Wert 1: 14, Wert 2: 16***

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_21.png" width="960"/><br>

47. Erzeuge 8 Kopien von **Setze BarText zu >>** aus der Vorlaufschleife und setze jeweils eine Kopie in eine **Tue** Spalte.
48. Der Text bleibt **>>** in der ersten **Tue** Spalte, während in jedem folgender **Tue** (do) Spalte zum vorhergehendem Text einmal ***>>*** hinzufügt wird.

  ***1. BarText: >>***<br>
  ***2. BarText: >>>>***<br>
  ***3. BarText: >>>>>>***<br>
  ***4. BarText: >>>>>>>>***<br>
  ***5. BarText: >>>>>>>>>>***<br>
  ***6. BarText: >>>>>>>>>>>>***<br>
  ***7. BarText: >>>>>>>>>>>>>>***<br>
  ***8. BarText: >>>>>>>>>>>>>>>>***<br>

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_22.png" width="960"/><br>

  **Jetzt wirst hast du den Stufenwerttext, der das empfangene Signal vom Mikrofon darstellen kann. Sende es nun zur Anzeige.**

49. Zieh einen **LCD 16x2 >> Schreibe zur Anzeige** (LCD 16x2 >> Write to display) Block von **Bildschirm** (Displays) in die unterste Position der Endlosschleife.
50. Nutze zwei **Element** (item) Blöcke aus **Variable** (Variables) anstelle der **" "** Textblöcke.

  ***Die Textblöcke kannst du löschen.***

51. Wähle für beide Variablennamen ***BarText***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_23.png" width="960"/><br>

  **Zum Schluss soll die Anzeige etwas Zeit bleiben, um den Text jeweils darzustellen.**

52. Benutze einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block aus **Zeit** (Time) für die letzte Position der Endlosschleife.
53. Ändere den Wert auf ***100***.
54. Benenne dein Projekt und speichere es ab.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_24.png" width="960"/><br>

55. Kompiliere den Code und lade es auf dein Arduino.

#### *Laut sein erlaubt! Wie gut funktioniert es?*


---

#### [Zurück zum Index](#index)

### 3.20. 3 mal das Gleiche bitte <a name="3-times-the-same"></a>

In unserem zweiten Temperaturbeispiel wollen wird die Temperatur korrekt darstellen. Nicht nur das! Du wirst dir Gleichungen erstellen, mit dem du Grad Celsius zu Fahrenheit und davon zu Kelvin umrechnen kannst.

Mit dem Druckknopf kannst du dann durch die 3 Temperaturdarstellungen durchschalten.

<img src="images/Lesson20/Lesson20_Thermometer_1.png" width="960"/><br>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte**                   |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 1 | **Thermistor**                      | <img src="images/Parts/Part_10kThermistor.jpg" width="256"/>    | 1 | **Potentiometer 10 kOhm**            |<img src="images/Parts/Part_Potentiometer10kohm.jpg" width="256"/>|
| 1 | **10k Ohm Widerstand**            |<img src="images/Parts/Part_Resistor_10KOhm.png" width="256"/>| 1 | **Drucktaster**                     |<img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/> |
| 1 | **LCD1602 Modul**     |<img src="images/Parts/Part_LCD1602_Modul.jpg" width="256"/>| 20 | **M-M Kabel**                     |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***Thermistor:***

* ist ein thermischer Widerstand, der den Strom der Energie durch die Temperatur beeinflusst.
* besitzt eine Verstärkung bei welcher 1 Grad Celsius den Widerstand um mehr als 100 Ohm verändert.

Es gibt zwei Formen von Thermistoren. Einer heisst NTC (Negativer Temperaturkoeffizient) und einer PTC
(Positiver Temperaturkoeffizient).

NTC-Thermistoren werden normalerweise zur Temperaturmessung genutzt.
PTCs werden hingegen oft als wiederverwendbare Sicherungen benutzt, da mit steigendem Energiefluss auch
die Temperatur steigt. Wenn es zu heiss wird unterbricht die Sicherung den Energiefluss.

#### Schaltplan

<img src="images/Lesson20/Lesson20_Thermometer_2.png" width="960"/><br>

#### Bastelanweisung

1. LCD 16x2 Display: Im oberen oder unteren Mittelteil in Reihe (Rechts)
2. Potentiometer: Mit zwei Pins im oberen Mittelteil über Mittelbrücke mit einem Pin im unteren Mittelteil (Mitte)
3. Thermistor: Im oberen Mittelteil in 4. Reihe (Links)
4. Druckknopf: Über den Trennlinie mit 2 Pins im oberen Mittelteil und 2 Pins im unteren Mittelteil (Links Aussen)
5. Widerstand 10 kOhm: Oberhalb des linken Thermistorpins in 2. Reihe >> Obere blaue Reihe (-) der Steckplatte
6. M-M Kabel 1: Obere blaue Reihe der Steckplatte  >> Arduino GND
7. M-M Kabel 2: Obere rote Reihe der Steckplatte >> Arduino 5V
8. M-M Kabel 3: Oberhalb linken Pin (GND) des Potentiometer oberer Mittelteil >> Obere blaue Reihe (-) der Steckplatte
9. M-M Kabel 4: Oberhalb rechten Pin (VCC) des Potentiometer oberer Mittelteil >> Obere rote Reihe (+) der Steckplatte
10. M-M Kabel 5: Unterhalb Pin des Potentiometer im unteren Mittelteil >> Oberhalb 3. Pin der LCD 16x2
11. M-M Kabel 6: Oberhalb 1. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
12. M-M Kabel 7: Oberhalb 2. Pin der LCD 16x2 >> Obere rote Reihe (+) der Steckplatte
13. M-M Kabel 8: Oberhalb 4. Pin der LCD 16x2 >> Arduino Pin 7
14. M-M Kabel 9: Oberhalb 4. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
15. M-M Kabel 10: Oberhalb 5. Pin der LCD 16x2 >> Arduino Pin 8
16. M-M Kabel 11: Oberhalb 11. Pin der LCD 16x2 >> Arduino Pin ~9
17. M-M Kabel 12: Oberhalb 12. Pin der LCD 16x2 >> Arduino Pin ~10
18. M-M Kabel 13: Oberhalb 13. Pin der LCD 16x2 >> Arduino Pin ~11
19. M-M Kabel 14: Oberhalb 14. Pin der LCD 16x2 >> Arduino Pin 12
20. M-M Kabel 15: Oberhalb 15. Pin der LCD 16x2 >> Obere rote Reihe (+) der Steckplatte
21. M-M Kabel 16: Oberhalb 16. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
22. M-M Kabel 17: Oberhalb des linken Druckknopf Pin >> Obere blaue Reihe der Steckplatte
23. M-M Kabel 18: Oberhalb des rechten Druckknopf Pin >> Arduino 2
24. M-M Kabel 19: Oberhalb des linken Thermistor Pins in 3. Reihe unterhalb des Widerstands >> Arduino Pin Analog 0 (A0)
25. M-M Kabel 20: Oberhalb des rechten Thermistor Pins >> Obere rote Reihe (+) der Steckplatte

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson20/Lesson20_Thermometer.png" width="960"/><br>

  **Lass uns sogleich nach der Bereitstellung der Basisfunktion mit den Umrechnungsgleichungen beginnen. Als ersten Schritte wirst du den Wert des Thermistors als Variable initiieren, welche der Gundlagenwert für Formeln ist.**

1. Starte mit einem **Setze Element zu " " als Character** (Set item to " " to Character) von **Variable** (Variables) in die Endlosschleife.
2. Für die **" "** Verbindung nutze **Lese analog Pin# A0** (Read analog pin# A0) von **Eingang/Ausgang** (Input/Output).
3. Gib **Element** den Variablennamen ***Temperature*** (Temperatur).
4. Ändere **Character** zu ***Dezimal*** (Decimal).

  <img src="images/Lesson20/Lesson20_Thermometer_3.png" width="960"/><br>

  **Nun wirst du zwei Gleichungen erstellen, welche die Temperaturdaten zu Kelvin umrechnen.**

5. Ziehe **Setze Element zu " " als Character** (Set item to " " to Character) von **Variable** (Variables) in die Endlosschleife.
6. Für die **" "** Verbindung nutze **Wurzel** (square root) aus **Mathe** (Math).

  <img src="images/Lesson20/Lesson20_Thermometer_4.png" width="960"/><br>

7. Setze **Wurzel** (square root) zu ***ln***.
8. Gib **Element** den Variablennamen ***Kelvin***.
9. Ändere **Character** zu ***Doppelt*** (Double).

  <img src="images/Lesson20/Lesson20_Thermometer_5.png" width="960"/><br>

10. Verbinde nun einen **" " + " "** Block aus **Mathe** (Math) mit **ln**.
11. Wähle für das **+** Symbol ***x***.
12. Für die erste Position von **" " x " "** benutze ein **0** Block aus **Mathe** (Math).
13. Der **0** Wert erhöhst du auf ***10000***.
14. Ziehe einen weiteren Block **" " + " "** als zweite Position von **10000 x " "**.
15. Ändere das **+** Symbol zu ***-***.
16. Die zweite Position dieses **" " - " "** Blockes wird ein **0** Block aus **Mathe** (Math) mit ***1*** als Wert.
17. Als erste Position von **" " - 1** hole dir erneut einen **" " + " "** Block in die Szene.
18. Wähle für das **+** Symbol ***/***.
19. Die erste Position wiederum von **" " / " "** wird ein **0** Block mit ***1024*** als Wert.
20. Die zweite Position von **1024 / " "** ist ein **Element** Block aus **Variable** (Variables).
21. Ändere **Element** zu ***Temperature*** (Temperatur).

  <img src="images/Lesson20/Lesson20_Thermometer_6.png" width="960"/><br>

  **Dies war der erste Teil der Formel um Kelvin zu errechnen. Lass uns mit dem zweiten Teil fortfahren.**

22. Ziehe eine **Setze Element zu " " als Character** (Set item to " " to Character) Block von **Variable** (Variables) in die Endlosschleife unterhalb dem vorherigen Formelblock.
23. Gib **Element** den Variablennamen ***Kelvin***.
24. Ändere **Character** zu ***Doppelt*** (Double).

  ***Fahre nun direkt auf deinem Arbeitsbereich fort. Wir werden diesen Block später in den gerade erzeugten Block einsetzen.***

25. Ziehe einen **" " + " "** Block aus **Mathe** (Math) auf deinen Arbeitsbereich unterhalb der Basisfunktion.
26. Verändere das **+** Symbol zu ***x***.
27. Als zweite Position nutze ein **Element** (item) Block aus **Variable** (Variables) mit dem Variablennamen ***Kelvin***.
28. Die erste Position wird ein **0** Block aus **Math** (Mathe) mit einem Wert von ***0.0000000876741***.

  ***Wenn du diese Nummer eingibst wird sich die Darstellung der Zahl in 8.76741e-8 verändern. Die 8 am Ende bedeutet, dass das Komma 8 Stellen nach vorn rückt.***

  <img src="images/Lesson20/Lesson20_Thermometer_7.png" width="960"/><br>

29. Benutze diesen Block nun als erste Position eines neuen **" " + " "** Blocks, den du auch auf den Arbeitsbereich ziehst.
30. Auch hier muss das Symbol ***x*** werden.
31. Und auch hier ist die zweite Position ein **Element** (item) Block aus **Variable** (Variables) mit dem Variablennamen ***Kelvin***.
32. Dieser **8.76741e-8 x Kelvin x Kelvin** Block wird wiederum die zweite Position einen neuen **" " + " "** Blocks.
33. Die erste Position ist ein Zahlblock **0** mit dem Wert ***0.000234125***.

  <img src="images/Lesson20/Lesson20_Thermometer_8.png" width="960"/><br>

34. Dieser gesamte Block wird nun die zweite Position eines **" " + " "** Blocks, welchen du aus **Mathe** (Math) holst.
35. Die erste Position ist wieder ein Zahlblock **0** mit dem Wert ***0.001129148***.
36. Dieser Riesenblock wird jetzt zur zweiten Position letzten **" " + " "** Block für diesen Teil der Kelvinformel, den du von **Mathe** (Math) holst.
37. Das Symbol dieses Blocks muss ***/*** werden.
38. Die erste Position ist ein **0** Block mit dem Wert ***1***.

  <img src="images/Lesson20/Lesson20_Thermometer_9.png" width="960"/><br>

39. Ziehe nun alles in den **Setze Kelvin zu " " als Doppelt** (set Kelvin to " " as Double) Block.

  <img src="images/Lesson20/Lesson20_Thermometer_10.png" width="960"/><br>

  **Mit Kelvin kannst du nun die Temperatur in Grad Celsius errechnen.**

40. Fahre mit einem **Setze Element zu " " als Character** (Set item to " " to Character) Block von **Variable** (Variables) in der Endlosschleife unterhalb dem vorherigen Formelblock fort.
41. Gib **Element** den Variablennamen ***Celsius***.
42. Ändere **Character** zu ***Dezimal*** (Decimal).
43. Ziehe einen **" " + " "** Block in die **" "** Lücke.
44. Das Symbol wird zu ***-***.
45. Die erste Position von **" " - " "** wird ein **Element** Block mit dem Variablenname ***Kelvin***.
46. Die zweite Position wird ein Zahlblock **0** mit dem Wert ***273.15***.

  <img src="images/Lesson20/Lesson20_Thermometer_11.png" width="960"/><br>

  **Die dritte Formel errechnet Fahrenheit und wird sich aus Grad Celsius erschliessen.**

47. Ziehe einen **Setze Element zu " " als Character** (Set item to " " to Character) Block von **Variable** (Variables) in die Endlosschleife unterhalb des "Celsius" Blocks.
48. Gib **Element** den Variablennamen ***Fahrenheit***.
49. Ändere **Character** zu ***Dezimal*** (Decimal).
50. Ziehe einen **" " + " "** Block in die **" "** Lücke.
51. Die zweite Position wird zu einer Zahl ***32*** durch einen Zahlenblock von **Mathe** (Math).
52. Die erste Position ist ein weiterer **" " + " "** Block, dessen Symbol jedoch in ***/*** geändert wird.
53. Die zweite Position dieses Blocks wird auch hier ein Zahlenblock mit ***5*** als Wert.
54. Und auch dieser Block erhält einen **" " + " "** Block als erste Position.
55. Das Symbol in diesem Fall wird ***x***.
56. Die zweite Position wird wie bei den vorherigen Blöcken eine Zahl mit dem Wert ***9***.
57. Die erste Position ist nun ein **Element** Block aus **Variable** (Variables) mit dem Variablenname ***Celsius***.

  <img src="images/Lesson20/Lesson20_Thermometer_12.png" width="960"/><br>

  **Das ist alles mit den Formeln. Jetzt wirst du die Temperatur an die Anzeige senden.**

58. Setze nun unter dem letzeten Formelblock einen **LCD 16x2 >> Schreibe zur Anzeige** Block aus **Bildschirm** (Displays) ein.
59. Trage in die erste Zeile **Temperatur ist** (Temperature is)
60. Ziehe den Textblock der zweiten Zeile aus der Verbindung, lege diesen auf deinem Arbeitsbereich daneben und trage **Celsius** ein.

  ***Füge ein Leerzeichen vor das C von Celsius, damit Platz zwischen der Nummer und Celsius bleibt.***

61. Zieh einen **Erzeuge Text mit** Block aus **Text** in die Verbindung der zweiten Zeile.
62. Verwende hierum wieder einen **Element** (item) Block mit dem Variablennamen ***Celsius*** für die erste Verbindung.
63. Als zweite Position benutze den Textblock, den du auf deinen Arbeitsbereich abgelegt hattest.

  **Wir wollen auch bei dieser Anzeige etwas Zeit einsetzen, da die Temperatur sich nicht so rasant ändern wird. Anschliessen löschst du die Anzeige für die nächste Ablesung.**

64. Ziehe einen **Warte 1000 Millisekunden** Zeitblock in die unterste Position der Endlosschleife und ändere den Zeitwert auf ***500***.
65. Anschliessend plaziere darunter einen **Lösche LCD 16x2 Anzeige** (Clear LCD 16x2 Display) Block aus **Bildschirm** (Displays).

  **Bevor wir mit dem Druckknopf und die verschiedenen Temperaturanzeigen fortfahren....Probier es doch erstmal aus!**

  ***Kompiliere den Code und lade ihn auf deinen Arduino.***

  <img src="images/Lesson20/Lesson20_Thermometer_13.png" width="960"/><br>

  #### *Funktioniert es?*

  **Okay, lass uns mit dem Druckknopf weitermachen.**

  **Da wir nur einen Knopf benutzen, soll die Temperatur bei jedem Knopfdruck in Celsius, Fahrenheit und Kelvin nacheinander dargestellt werden. Nach Kelvin soll wieder Celsius an die Reihe kommen.**

66. Ziehe einen **Setze Element zu** (set item to) Block von **Variable** (Variables) mit Variablennamen **BtnVal** (Knopfwert) in die Vorlaufschleife.
67. Verbinde diesen mit einem Zahlenblock **0**.

  **Dies ist der Status des Druckknopfs 0 = Einmal gedrückt, 1 = Zweimal gedrückt, 2 = Dreimal gedrückt.**

  <img src="images/Lesson20/Lesson20_Thermometer_14.png" width="960"/><br>

  **Jetzt musst du es arrangieren, dass die Statuszahl mit jedem Knopfdruck steigt und wenn dreimal gedrückt, wieder bei 0 beginnt.**

68. Ziehe einen **wenn Tue** Block aus **Logik** (Logic) in die oberste Postion der Endlosschleife.
69. Benutze das Zahnradsymbol um eine **anderenfalls** Spalte hinzuzufügen.
70. Verbinde einen **" " und " "** (" " and " ") Block aus **Logik** (Logic) mit **wenn**.
71. Füge jeweils einen **" " = " "** Block aus **Logik** (Logic) in die erste und zweite Position von **" " und " "** (" " and " ").
72. Ändere das Symbol **=** des **" " = " "** Blocks der zweiten Position zu ***\<***.
73. Die erste Position des ersten **" " = " "** Blocks ist ein **Lese digital mit PULL_UP Modus Pin#0** (Read digital with PULL_UP mode Pin#0) aus **Eingang/Ausgang** (Input/Output).
74. Wähle die Pinnummer ***2***.
75. Die zweite Position des ersten **" " = " "** Blocks wird ein **HIGH** Block auch aus **Eingang/Ausgang** (Input/Output).
76. Ändere den Wert zu ***LOW***.
77. Die erste Position des zweiten **" " = " "** Blocks ist ein **Element** Block aus **Vairable** (Variablen) mit dem Variablennamen ***BtnVal*** (Knopfwert).
78. Dies zweite Position des zweiten **" " = " "** Blocks ist ein Zahlenblock **0** mit einem geänderten Wert von **2**.
79. Dupliziere nun den gesamten Block **Lese digital mit PULL_UP Modus Pin# 2 = LOW und BtnVal < 0** und verbinde diesen mit **anderenfalls** (else if)
80. Der einzige Unterschied zwischen den zwei Blöcken wird das Zeichen **<**, welches im Abschnitt **anderenfalls** wieder ***=*** werden muss.

  <img src="images/Lesson20/Lesson20_Thermometer_15.png" width="960"/><br>

  **Wenn der Tastenwert unter 2 ist, soll dieser Wert nun um 1 addiert werden.**

  **Wenn der Tastenwert jedoch 2 ist, soll der Wert wieder 0 werden.**

81. Dupliziere den Block **Setze BtnVal zu 0** (set BtnVal to 0) aus der Vorlaufschleife 2 mal und füge die Kopie in die jeweilige **Tue** (do) Spalte von **wenn** (if) und **anderenfalls** (else if).
82. Ziehe den Zahlenblock **0** unter **wenn** (if) auf deinen Arbeitsbereich und ändere den Wert zu **1**.
83. Verbinde **Setze BtnVal zu** (set BtnVal to) in **wenn** (if) mit einem **" " + " "** Block aus **Mathe** (Math).
84. Als erste Position von **" " + " "** benutze einen **Element** (item) Block aus **Variable** (Variables) mit dem Variablenname ***BtnVal*** (Knopfwert).
85. Ziehe nun den Zahlenblock **1** von deinem Arbeitsbereich in die zweite Position von **BtnVal + " "**.

  <img src="images/Lesson20/Lesson20_Thermometer_16.png" width="960"/><br>

  **Zu letzt wollen wird nun die jeweiligen Temperaturen abhängig von dem Knopfwert darstellen.**

86. Ziehe den gesamten **LCD 16x2 >> Schreibe zur Anzeige** (LCD 16x2 >> Write to Display) von unterhalb der Formelblöcke aus der Endlosschleife auf deinen Arbeisbereich.

  ***Der Zeitblock und Anzeigelöschblock wird mit dem Block verknüpft bleiben. Das ist ok, du wirst gleich diese Blöcke zurückziehen.***

87. Platziere nun einen **wenn Tue** (if do) Block aus **Logik** (Logic) unterhalb der Formelblöcke in der Endlosschleife.
88. Nutze das Zahnradsymbol um 2 **anderenfalls** Blöcke hinzuzufügen.
89. Klicke nun auf den Zeitblock auf dem Arbeitsbereich und ziehe diesen zurück in die Endlosschleife unterhalb des neuen **wenn Tue** (if do) Blocks.

  ***Der Anzeigelöschblock soll und sollte mit dabei sein.***

90. Nun hole dir einen **" " = " "** Block aus **Logik** (Logic) und verbinde diesen mit **wenn** (if).
91. Die erste Position von **" " = " "** wird ein **Element** (item) Block mit Variablennamen ***BtnVal***.
92. Die zweite Position von **BtnVal = " "** wird ein Zahlenblock **0**.
93. Dupliziere den **BtnVal = 0** zweimal und setze diese jeweil in einen **anderenfalls** (else if) Schlitz ein.
94. Der Wert des Zahlenwerts ändert sich im ersten **anderenfalls** (else if) zu ***1*** und im zweiten **anderenfalls** (else if) zu ***2***.

  <img src="images/Lesson20/Lesson20_Thermometer_17.png" width="960"/><br>

  **Jetzt werden wir "Tue" belegen. Für jeden Knopfwert eine Temperaturanzeige.**

95. Ziehe den **LCD 16x2 >> Schreibe zur Anzeige** (LCD 16x2 >> Write to Display) von deinem Arbeitsbereich in die erste **Tue** (do) Spalte.
96. Dupliziere **LCD 16x2 >> Schreibe zur Anzeige** (LCD 16x2 >> Write to Display) 2 mal und füge diese jeweils in die anderen **Tue** (do) Saplten.
97. Ändere nun den Variablennamen und Test des zweiten **Tue** (do) Bereichs in ***Fahrenheit*** und des zweiten **Tue** (do) Bereichs in ***Kelvin***.

  <img src="images/Lesson20/Lesson20_Thermometer_18.png" width="960"/><br>

98. Geb deinem Projekt einen Namen und speichere ab.

  <img src="images/Lesson20/Lesson20_Thermometer_19.png" width="960"/><br>

99. Kompiliere den Code und lade es hoch auf deinen Arduino.

#### *Wie heiss ist es?*

***...Es ist so heiss, dass die Bauern den Hühnern Eis füttern, damit sie keine Spiegeleier legen!***

---

#### [Zurück zum Index](#index)

### 3.21. Du bist eingeladen? <a name="you-shall-pass"></a>

Dieses Beispiel zeigt dir, wie man ein Lese/Schreibgerät programmierst. Diese findest du möglicherweise in der Schule am Kopierer oder Türen. Es funktioniert über Radiowellen, sodass ein direkter Kontakt nicht notwendig ist.

Du wirst die LCD 16x2 Anzeige nutzen, um die Resultate anzuzeigen. Es kann auch ein Türschloss sein.

<img src="images/Lesson21/Lesson21_RFIDScanner_1.png" width="960"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte**                   |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 1 | **RC522 RFID-Modul**  | <img src="images/Parts/Part_RFID_RC522.jpg" width="256"/>    | 1 | **LCD1602 Modul**     |<img src="images/Parts/Part_LCD1602_Modul.jpg" width="256"/>|
| 1 | **Potentiometer 10 kOhm**  |<img src="images/Parts/Part_Potentiometer10kohm.jpg" width="256"/>| 7 | **W-M Kabel**            |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/>|
| 23 | **M-M Kabel**                     |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***RC522 RFID-Modul mit Karte und Dongle***

* ist ein hochintegriertes Lese-/Schreibgerät zur kontaktlosen Kommunikation bei 13,56 MHz.
* hat ein Sendeteil, das eine Antenne zur Kommunikation mit ISO/IEC 14443A bzw. MIFARE® Karten und Transpondern betreiben kann, ohne dass diese Karten eine externe Stromversorgung benötigen.
* hat ein Empfangsteil, das einen Schaltkreis zur Demodulierung und Entschlüsselung des Signals von ISO/IEC 14443A/MIFARE® kompatiblen Karten und Transpondern beinhaltet.
* unterstützt die kontaktlose Kommunikation mit Übertragrungsraten bis zu 848 kBit/s in beide Richtungen.


#### Schaltplan

<img src="images/Lesson21/Lesson21_RFIDScanner_2.png" width="960"/><br>

#### Bastelanweisung

1. LCD 16x2 Display: Im oberen oder unteren Mittelteil in Reihe (Rechts)
2. Potentiometer: Mit zwei Pins im oberen Mittelteil über Mittelbrücke mit einem Pin im unteren Mittelteil (Mitte)
3. M-M Kabel 1: Obere blaue Reihe der Steckplatte  >> Arduino GND
4. M-M Kabel 2: Obere rote Reihe der Steckplatte >> Arduino 5V
5. M-M Kabel 3: Oberhalb linken Pin (GND) des Potentiometer oberer Mittelteil >> Obere blaue Reihe (-) der Steckplatte
6. M-M Kabel 4: Oberhalb rechten Pin (VCC) des Potentiometer oberer Mittelteil >> Obere rote Reihe (+) der Steckplatte
7. M-M Kabel 5: Unterhalb Pin des Potentiometer im unteren Mittelteil >> Oberhalb 3. Pin der LCD 16x2
8. M-M Kabel 6: Oberhalb 1. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
9. M-M Kabel 7: Oberhalb 2. Pin der LCD 16x2 >> Obere rote Reihe (+) der Steckplatte
10. M-M Kabel 8: Oberhalb 4. Pin der LCD 16x2 >> Arduino Pin 7
11. M-M Kabel 9: Oberhalb 4. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
12. M-M Kabel 10: Oberhalb 5. Pin der LCD 16x2 >> Arduino Pin 8
13. M-M Kabel 11: Oberhalb 11. Pin der LCD 16x2 >> Arduino Pin ~9
14. M-M Kabel 12: Oberhalb 12. Pin der LCD 16x2 >> Arduino Pin ~10
15. M-M Kabel 13: Oberhalb 13. Pin der LCD 16x2 >> Arduino Pin ~11
16. M-M Kabel 14: Oberhalb 14. Pin der LCD 16x2 >> Arduino Pin 12
17. M-M Kabel 15: Oberhalb 15. Pin der LCD 16x2 >> Obere rote Reihe (+) der Steckplatte
18. M-M Kabel 16: Oberhalb 16. Pin der LCD 16x2 >> Obere blaue Reihe (-) der Steckplatte
19. W-M Kabel 1: RFID-Modul VCC Pin >> Oberes Mittelteil der Steckplatte
20. W-M Kabel 2: RFID-Modul RST Pin >> Oberes Mittelteil der Steckplatte
21. W-M Kabel 3: RFID-Modul GND Pin >> Oberes Mittelteil der Steckplatte
22. W-M Kabel 4: RFID-Modul MISO Pin >> Oberes Mittelteil der Steckplatte
23. W-M Kabel 5: RFID-Modul SOMI Pin >> Oberes Mittelteil der Steckplatte
24. W-M Kabel 6: RFID-Modul SCK Pin >> Oberes Mittelteil der Steckplatte
25. W-M Kabel 7: RFID-Modul SDA Pin >> Oberes Mittelteil der Steckplatte
23. M-M Kabel 17: Oberhalb RFID-Modul VCC Kabel >> Arduino 3.3V
24. M-M Kabel 18: Oberhalb RFID-Modul RST Kabel >> Arduino Pin ~9
25. M-M Kabel 19: Oberhalb RFID-Modul GND Kabel >> Obere blaue Reihe (-) der Steckplatte
26. M-M Kabel 20: Oberhalb RFID-Modul MISO Kabel >> Arduino Pin 12
27. M-M Kabel 21: Oberhalb RFID-Modul SOMI Kabel >> Arduino Pin ~11
28. M-M Kabel 22: Oberhalb RFID-Modul SCK Kabel >> Arduino Pin 13
29. M-M Kabel 23: Oberhalb RFID-Modul SDA Kabel >> Arduino Pin ~10

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson21/Lesson21_RFIDScanner.png" width="960"/><br>

Mit der Basisfunktion auf deinem Arbeitsbereich wirst du nun die Basisblöcke des Lese/Schreibgerät und der Anzeige einsetzen.

1. Ziehe einen **Setup RFID mit Pin# 9 und SS Pin# 10** (Setup RFID with Pin# 9 and SS Pin# 10) Block von **Eingang/Ausgang** (Input/Output) in die Vorlaufschleife.

  <img src="images/Lesson21/Lesson21_RFIDScanner_3.png" width="960"/><br>

2. Weiterhin hole einen **Setup LCD 16x2 auf digitalen Pins** (Setup LCD 16x2 on digital pins) aus **Bildschirm** (Displays) und plaziere diesen in die Vorlaufschleife.

  <img src="images/Lesson21/Lesson21_RFIDScanner_4.png" width="960"/><br>

3. Ändere die Pinnummern zu ***3***, ***5***, ***6***, und ***2***.

  <img src="images/Lesson21/Lesson21_RFIDScanner_5.png" width="960"/><br>

  **Jede Karte oder Dongle hat eine eigene Identifizierung. Diese musst du zuerst herausfinden. Der Code benutzt eine boolesche Funktion (Wahr/Falsch), um neue Identifizierung darzustellen oder Karten/Dongle auf die Identität kontrollieren.**

4. Benutze einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) mit einem **Wahr** (true) Block aus **Logik** (Logic) zusammen, welche in die Vorlaufschleife platziert werden.

  <img src="images/Lesson21/Lesson21_RFIDScanner_6.png" width="960"/><br>

5. Benenne **Element** (item) in ***NewID*** (NeueID) um.

  <img src="images/Lesson21/Lesson21_RFIDScanner_7.png" width="960"/><br>

  **Als erstes wirst du nun einen Begrüssungstext für die Anzeige erzeugen, welcher erscheint nachdem du den Arduino anschaltest.**

6. Ziehe dazu einen **LCD 16x2 >> Schreibe zur Anzeige** (LCD 16x2 >> Write to Display) von **Bildschirm** (Displays) in die Endlosschleife.

  ***Benutze deinen eigenen Text. Denk daran....16 Zeichen pro Linie!***

7. Füge ***>>>Welcome to<<<*** für die erste Linie und ***>DIGITAL STAGES<*** für die zweite Linie ein.

  <img src="images/Lesson21/Lesson21_RFIDScanner_8.png" width="960"/><br>

  **Nun wirst du eine "wenn Tue" Funktion nutzen, um einen Teil für das Lesen einer neuen Karte oder Dongle zu bauen. Dies ist der Teil, welcher ausgelöst wird, wenn NewID wahr ist.**

8. Benutze einen **wenn Tue** (if do) Block aus **Logik** (Logic) und platziere diesen unterhalb des Begrüssungstextblock.

9. Drücke auf das Zahnradsymbol und addiere einen **anderenfalls** Block zur Funktion.

  <img src="images/Lesson21/Lesson21_RFIDScanner_9.png" width="960"/><br>

10. Verbinde ein **" " + " "** Block mit **wenn** des Funktionsblocks.
11. Die erste Position wird ein **Element** Block aus **Variable** (Variables), welcher den Variablennamen ***NewID*** erhält.
12. Die zweite Position von **NewID = " "** wird ein **Wahr** Block aus **Logik** (Logic).
13. Dupliziere dien **NewID = Wahr** Block und verbinde diesen mit **anderenfalls**.
14. Ändere hier jedoch **Wahr** zu ***Falsch***.

  <img src="images/Lesson21/Lesson21_RFIDScanner_10.png" width="960"/><br>

  **Nun werden wir die eigentliche ID vom Gerät lesen.***

15. Ziehe einen **RFID RC522 >> Lese UID Element** (RFID RC522 >> Get UID item) Block aus **Eingang/Ausgang** (Input/Output) in die Spalte von **Tue** (do) unter **wenn** (if).
16. Benenne **Element** in ***GetID*** (LeseID) um.

  <img src="images/Lesson21/Lesson21_RFIDScanner_11.png" width="960"/><br>

  **Als Nächstes soll die Identität angezeigt werden.**

17. Unterhalb des Leseblocks mit **GetID**, positioniere einen **LCD 16x2 >> Schreibe zu Anzeige** (LCD 16x2 >> Write to Display) Block aus **Bildschirm** (Displays).

  <img src="images/Lesson21/Lesson21_RFIDScanner_12.png" width="960"/><br>

18. Schreibe in den Textblock der ersten Zeile ***New ID:*** (Neue ID:).
19. Lösche den zweiten Textblock und ersetze diesen mit einem **Element** Block aus **Variable** (Variables).
20. Wechsle den Variablenname zu ***GetID***.

  <img src="images/Lesson21/Lesson21_RFIDScanner_13.png" width="960"/><br>

  ***Versichere dich, dass NewID im Vorlauf auf Wahr gestellt ist.***

  ***Kompiliere nun den Code und lade es auf deinen Arduino.***

  <img src="images/Lesson21/Lesson21_RFIDScanner_14.png" width="960"/><br>

  #### *Zeit zum Testen!*

  **Nachdem der Begrüssungstext angezeigt wurde, halte den Dongle und die Karte über das Lesemodul.**

  ***Schreibe dir die Identität deiner Karte und Dongle auf!***

  **Ok, da du nun die Identität deiner Karte / Dongle kennst, kannst du nun dies abfragen.**

21. Dupliziere den **RFID RC522 >> Lese UID GetID** (RFID RC522 >> Get UID GetID) aus der **Tue** (do) Spalte von **wenn** (if) und füge die Kopie in die **Tue** Spalte von **anderenfalls**.
22. Darunter platziere einen neuen **wenn Tue** (if do) Block.

  **Hier kannst du später viele Identitäten hinzufügen. Addiere dafür einen "anderenfalls" Teil pro Identität hinzu.**

  **Du wirst jetzt lediglich eine Identität hinzufügen, damit du auch ein Negativbeispiel hast.**

  <img src="images/Lesson21/Lesson21_RFIDScanner_15.png" width="960"/><br>

23. Klicke auf das Zahnradsymbol und addiere den **sonst** (else) Block zum Funktionsblock.
24. Verbinde einen **" " = " "** Block aus **Logik** (Logic) mit **wenn** (if) dieses Blocks.
25. Die erste Position von **" " = " "** wird ein **Element** (item) Block aus **Variable** (Variables) mit dem Variablennamen ***GetID***.
26. Die zweite Position wird ein Textblock **" "** aus **Text** mit der Identität deiner Karte oder Dongle.

  <img src="images/Lesson21/Lesson21_RFIDScanner_16.png" width="960"/><br>

  **In der "Tue" Spalte von "wenn" oder "anderenfalls" kommt nun der Code der ausgelöst wird, wenn die bestimmte Identität erkannt ist.**

  **Das kann ein Türöffner, Licht, Sound oder eine bestimmter Text wie der Name des Kartenbesitzers sein.**

27. Der erste Block in der **Tue** (do) Spalte von **wenn** (if) wird der Block **Lösche LCD 16x2 Anzeige** (Clear LCD 16x2 Display) aus **Bildschirm** (Displays).
28. Danach ziehe einen Block **LCD 16x2 >> Schreibe zu Anzeige** (LCD 16x2 >> Write to Display) von **Bildschirm** (Displays) darunter.
29. Schreibe ***+++ID ACCEPTED++*** (ID akzeptiert) für die erste Linie und ***+Access Granted+*** (Zugang gewährt) für die zweite Linie ein.

  ***Es kann auch hier dein eigener Text sein. Bedenke die maximale Zeichenmenge von 16 pro Zeile.***

  <img src="images/Lesson21/Lesson21_RFIDScanner_17.png" width="960"/><br>

  **Gib eine Zeit, bevor die Anzeige wieder erlischt.**

30. Ziehe nun einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block aus **Zeit** (Time) unterhalb des **LCD 16x2 >> Schreibe zu Anzeige** (LCD 16x2 >> Write to Display) Blocks und erhöhe die Zeit auf ***3000*** Millisekunden.
31. Platziere nun einen **Lösche LCD 16x2 Anzeige** (Clear LCD 16x2 Display) Block aus **Bildschirm** (Displays) zum Abschluss darunter.

  <img src="images/Lesson21/Lesson21_RFIDScanner_18.png" width="960"/><br>

32. Dupliziere nun jeden einzelnen Block aus der **Tue** Spalte von **wenn** und füge diese nacheinander in die **Tue** Spalte von **sonst**.

  ***Dies kannst du auch mit jeder erweiterten "anderenfalls" Spalte in Zukunft machen.***

33. Ändere nun den Text der ersten Linie zu ***====WRONG ID====*** (Falsche ID) und den der zweiten Linie zu ***==Access Denied=*** (Zugang versagt).

  <img src="images/Lesson21/Lesson21_RFIDScanner_19.png" width="960"/><br>

34. Speichere dein Projekt nachdem du es benannt hast.
35. Versichere dich, dass der Wert von **NewID** in der Vorlaufstufe auf **Falsch** gestellt ist.
36. Kompiliere den Code und lade ihn hoch.

#### *Nehme deine Karte und halte sich in die Nähe des Lesegeräts. Dann versuche deinen Dongle*


---

#### [Zurück zum Index](#index)

### 3.22. Blink Blink Blink <a name="blink-blink-blink"></a>

Je grösser und komplizierter deine Projekte werden, um so mehr Pins brauchst du dafür. Nun, dein Arduino hat eine gewisse Zahl davon. Was nun, wenn du mehr Eingänge und/oder Ausgänge benötigst?

Hier kommt der Schieberegister ins Spiel. Dieser kann nämlich durch ein Zeitpulse verschiedene Anschlüsse verwalten, wobei dieser lediglich nur 3 Pins (plus Strom und Erde) benötigt. Nicht nur das, denn der Schieberegister kann in Reihe geschalten werden und damit noch wesentlich mehr Anschlüsse.


<img src="images/Lesson22/Lesson22_ShiftRegister_1.png" width="960"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 2 | **Rote LED** | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/> | 2 | **Gelbe LED** | <img src="images/Parts/Part_LED_Yellow_5mm.jpg" width="256"/> |
| 2 | **Grüne LED** | <img src="images/Parts/Part_LED_Green_5mm.jpg" width="256"/> | 2 | **Blaue LED** | <img src="images/Parts/Part_LED_Blue_5mm.jpg" width="256"/> |
| 8 | **220 Ohm Widerstand** | <img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>| 1 | **Schieberegister 74HC595N** | <img src="images/Parts/Part_74HC595N.jpg" width="256"/> |
| 18 | **M-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***Schieberegister 74HC595N***

* ist ein Chip, der acht Speicherstellen hat, die jeweils entweder eine 1 oder eine 0 speichern können.
* ändert die Werte über den „Data“- und den „Clock“-Anschluss des Chips.
* empfängt regelmäßig 8 Pulse vom Arduino über den Zeitpin (Clock) und überprüft jeden der Datenpins (Data), ob dieser "HIGH" oder "LOW" also 1 oder 0 ist.
* besitzt einen "Latch" Pin, der aktiviert wird sobald die Werte der Datenpin überprüft wurden, um diese im Register zu speichern.
* hat zusätzlich einen „Output Enable“ (OE)-Pin, durch den alle Ausgänge auf einmal an bzw ausgeschaltet werden können.


#### Schaltplan

<img src="images/Lesson22/Lesson22_ShiftRegister_2.png" width="960"/><br>

#### Bastelanweisung

1. 2 grüne, 2 rote, 2 gelbe und 2 blaue LEDs im Abstand von 2 Stecklöchern: Kurzes Bein in oberer blaue Reihe (-) der Steckplatte >> Langes Bein 1. Reihe oberes Mittelteil
2. Widerstand 220 Ohm: 4. Reihe der Spalte der jeweiligen LED im oberen Mittelteil >> 1. Reihe der jeweiligen selben Spalte im unteren Mittelteil
3. 74HC595N: Über Trennlinie des Mittelteils 8 Pins im oberen Mittelteil und 8 Pins im unteren Mittelteil mit Karbe auf Chip nach Links zeigend.
4. M-M Kabel 1: Obere blaue Reihe (-) der Steckplatte >> Arduino GND
5. M-M Kabel 2: Obere blaue Reihe (-) der Steckplatte >> Untere blaue Reihe (-) der Steckplatte
6. M-M Kabel 3: Untere rote Reihe (+) der Steckplatte >> Arduino 5V
7. M-M Kabel 4: Unterhalb Widerstand Steckloch in Spalte 1 im unteren Mittelteil >> Oberhalb Shiftregister Pin 2 im oberen Mittelteil
8. M-M Kabel 5: Unterhalb Widerstand Steckloch in Spalte 2 im unteren Mittelteil >> Unterhalb Shiftregister Pin 1 im unterer Mittelteil
9. M-M Kabel 6: Unterhalb Widerstand Steckloch in Spalte 3 im unteren Mittelteil >> Unterhalb Shiftregister Pin 2 im unterer Mittelteil
10. M-M Kabel 7: Unterhalb Widerstand Steckloch in Spalte 4 im unteren Mittelteil >> Unterhalb Shiftregister Pin 3 im unterer Mittelteil
11. M-M Kabel 8: Unterhalb Widerstand Steckloch in Spalte 5 im unteren Mittelteil >> Unterhalb Shiftregister Pin 4 im unterer Mittelteil
12. M-M Kabel 9: Unterhalb Widerstand Steckloch in Spalte 6 im unteren Mittelteil >> Unterhalb Shiftregister Pin 5 im unterer Mittelteil
13. M-M Kabel 10: Unterhalb Widerstand Steckloch in Spalte 7 im unteren Mittelteil >> Unterhalb Shiftregister Pin 6 im unterer Mittelteil
14. M-M Kabel 11: Unterhalb Widerstand Steckloch in Spalte 8 im unteren Mittelteil >> Unterhalb Shiftregister Pin 7 im unterer Mittelteil
15. M-M Kabel 12: Unterhalb Shiftregister Pin 8 im unterer Mittelteil >> Untere blaue Reihe (-) der Steckplatte
16. M-M Kabel 13: Oberhalb Shiftregister Pin 1 im oberen Mittelteil >> Untere rote Reihe (+) der Steckplatte
17. M-M Kabel 14: Oberhalb Shiftregister Pin 3 im oberen Mittelteil >> Arduino Pin 12
18. M-M Kabel 15: Oberhalb Shiftregister Pin 4 im oberen Mittelteil >> Obere blaue Reihe (-) der Steckplatte
19. M-M Kabel 16: Oberhalb Shiftregister Pin 5 im oberen Mittelteil >> Arduino Pin ~11
20. M-M Kabel 17: Oberhalb Shiftregister Pin 6 im oberen Mittelteil >> Arduino Pin ~9
21. M-M Kabel 18: Oberhalb Shiftregister Pin 7 im oberen Mittelteil >> Untere rote Reihe (+) der Steckplatte

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson22/Lesson22_ShiftRegister.png" width="960"/><br>

1. Nachdem du Basisfunktion auf den Arbeitsbereich gezogen hast.

  **Wenn du einen Reihe von Code immer wieder benutzen möchtest, kannst du eine Funktion nutzen die diese zu einen neuen Block verbindet.**

2. Ziehe dafür einen **zu Mache etwas** (to do something) Block aus **Funktion** (Functions) und platziere diesen oberhalb der Basisfunktion.
3. Gib dieser Funktion den Namen ***updateShiftRegister***.

  <img src="images/Lesson22/Lesson22_ShiftRegister_3.png" width="960"/><br>

4. Ziehe nun 4 **Setze digitale Pin# 0 zu HIGH** (set digital pin# to HIGH) aus **Eingang/Ausgang** (Input/Output) auf deinen Arbeitsbereich.
5. Platziere 2 in die **updateShiftRegister** Funktion und 2 in die Vorlaufschleife der Basisfunktion.
6. Ändere die Pinnummern der Blöcke in **updateShiftRegister** beide zu ***11***, während der **HIGH** Wert des ersten Blocks ***LOW*** wird.
7. Die Pinnummern der beiden Blöcke in der Vorlaufschleife werden jeweils zu ***9*** und ***12***.

  <img src="images/Lesson22/Lesson22_ShiftRegister_4.png" width="960"/><br>

  **Jetzt integrierst du die Funktionalität des Schieberegister.**

8. Platziere einen **shiftOut >> Daten# 0 Clk# 0 Ordnung MSBFIRST byteVal Element** (shiftOut >> Data# 0 Clk# 0 Order MSBFIRST byteVal item) aus **Verschieden** (Various) zwischen die beiden Blöcke in **updateShiftRegister**.

  <img src="images/Lesson22/Lesson22_ShiftRegister_5.png" width="960"/><br>

9. Ändere den Datenpin zu ***12***, den Clkpin ***9***, die Ordnung zu ***LSBFIRST*** und benenne die byteVal **Element** in ***LEDs*** um.

  <img src="images/Lesson22/Lesson22_ShiftRegister_6.png" width="960"/><br>

10. Ziehe einen **Setze Element zu " " as Characater** (set item to " " to Character) Block in die Vorlaufschleife.
11. Füge einen **0** Block aus **Mathe** (Math) in die **" "** Lücke.
12. Wähle für **Element** (item) die Variable ***LEDs*** und für **Character** das Format ***Byte***.
13. Dupliziere diesen Block und ziehe die Kopie in die Endlosschleife.

  <img src="images/Lesson22/Lesson22_ShiftRegister_7.png" width="960"/><br>

  **Nun wirst du deine eigens erstellte Funktion als Block einfügen.**

14. Platziere den Block **updateShiftRegister**, der jetzt in **Funktion** (Functions) zur Verfügung steht in die Endlosschleife.

  <img src="images/Lesson22/Lesson22_ShiftRegister_8.png" width="960"/><br>

15. Setze darunter einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block aus **Zeit** (Time) in die Endlosschleife und verringere die Zeit zu ***100*** Millisekunden.
16. Dupliziere diesen Zeitblock und lasse diesen erst einmal auf deinem Arbeitsbereich liegen.

  <img src="images/Lesson22/Lesson22_ShiftRegister_9.png" width="960"/><br>

  **Der Schieberegister benutzt ein Byte, welche aus 8 Bit besteht, um die 8 Anschlüsse zu organisieren.**

  **In diesem Beispiel hast du 8 LEDs die durch die byteVal Variable angesprochen werden können.**

  **Mit einer Schleifenfunktion werden wir nun durch die 8 Bits des Bytes gehen und jeweils die LED für 100 Millisekunden aufleuchten lassten.**

17. Ziehe nun die Schleifenfunktion **zähle mit i von 0 zu 10 bei 1** (count with i from 0 to 10 by 1) aus **Schleife** (Loops) in die Endlosschleife.
18. Ändere die **10** zu ***7***.

  <img src="images/Lesson22/Lesson22_ShiftRegister_10.png" width="960"/><br>

19. Dupliziere nun **updateShiftRegister** und füge es in die **Tue** (do) Spalte ein.
20. Positioniere den Zeitblock vom Arbeitsbereich darunter.

  <img src="images/Lesson22/Lesson22_ShiftRegister_11.png" width="960"/><br>

  **Nun fehlt nur noch die Zuweisung des Bits des byteVal LEDs.**

21. Platziere den Block **Setze Variable von Element zum Bit Element** (Set variable of item to bit item) aus **Verschieden** (Various) in die erste Position von **Tue** (do) der Schleifenfunktion.

  <img src="images/Lesson22/Lesson22_ShiftRegister_12.png" width="960"/><br>

22. Wähle ***LEDs*** als erste **Element** Variable und ***i*** als zweite.
23. Benenne dein Projekt und speichere ab.

  <img src="images/Lesson22/Lesson22_ShiftRegister_13.png" width="960"/><br>

24. Kompiliere den Code und lade das Projekt auf deinen Arduino.


  #### *Verändere doch mal die Zeit, die Ordnung (LSBFIRST) und die "bei" Nummer der Schleifenfunktion.*


---

#### [Zurück zum Index](#index)

### 3.23. Hell, Heller, Leuchtend hell <a name="brighter,lighter,light-brighter"></a>

In diesem Beispiel wirst du sehen, wie die Bits der Schieberegister direkt je nach dem analogen Sensors kontrolliert werden. Hierbei werden wir eine Fotozelle benutzen. Je direkter du Licht auf die Fotozelle strahlst, um so mehr LEDs werden aufleuchten.

<img src="images/Lesson23/Lesson23_PhotoCell_1.png" width="960"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 2 | **Rote LED** | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/> | 2 | **Gelbe LED** | <img src="images/Parts/Part_LED_Yellow_5mm.jpg" width="256"/> |
| 2 | **Grüne LED** | <img src="images/Parts/Part_LED_Green_5mm.jpg" width="256"/> | 2 | **Blaue LED** | <img src="images/Parts/Part_LED_Blue_5mm.jpg" width="256"/> |
| 8 | **220 Ohm Widerstand** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/> | 1 | **Schieberegister 74HC595N** |<img src="images/Parts/Part_74HC595N.jpg" width="256"/> |
| 1 | **1 kOhm Widerstand** |<img src="images/Parts/Part_Resistor_1KOhm.png" width="256"/> | 1 | **Fotowiderstand** |<img src="images/Parts/Part_Photocell.jpg" width="256"/> |
| 20 | **M-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***Fotowiderstand***

* ist ein Widerstand, der duch Licht beeinflusst wird.
* wird manchmal auch LDR genannt.
* hat in kompletter Dunkelheit einen Widerstand von 50 kOhm.
* hat im hellen Licht ca. einen Widerstand von 500 Ohm.
* kann wie ein Potentiometer über einen analogen Pin vom Arduino und einem 1 kOhm Widerstand als Zusatz verwendet werden.

#### Schaltplan

<img src="images/Lesson23/Lesson23_PhotoCell_2.png" width="960"/><br>

#### Bastelanweisung

1. 2 grüne, 2 rote, 2 gelbe und 2 blaue LEDs im Abstand von 2 Stecklöchern: Kurzes Bein in oberer blaue Reihe (-) der Steckplatte >> Langes Bein 1. Reihe oberes Mittelteil
2. Widerstand 220 Ohm: 4. Reihe der Spalte der jeweiligen LED im oberen Mittelteil >> 1. Reihe der jeweiligen selben Spalte im unteren Mittelteil
3. 74HC595N: Über Trennlinie des Mittelteils 8 Pins im oberen Mittelteil und 8 Pins im unteren Mittelteil mit Karbe auf Chip nach Links zeigend.
4. M-M Kabel 1: Obere blaue Reihe (-) der Steckplatte >> Arduino GND
5. M-M Kabel 2: Obere blaue Reihe (-) der Steckplatte >> Untere blaue Reihe (-) der Steckplatte
6. M-M Kabel 3: Untere rote Reihe (+) der Steckplatte >> Arduino 5V
7. M-M Kabel 4: Unterhalb Widerstand Steckloch in Spalte 1 im unteren Mittelteil >> Oberhalb Shiftregister Pin 2 im oberen Mittelteil
8. M-M Kabel 5: Unterhalb Widerstand Steckloch in Spalte 2 im unteren Mittelteil >> Unterhalb Shiftregister Pin 1 im unterer Mittelteil
9. M-M Kabel 6: Unterhalb Widerstand Steckloch in Spalte 3 im unteren Mittelteil >> Unterhalb Shiftregister Pin 2 im unterer Mittelteil
10. M-M Kabel 7: Unterhalb Widerstand Steckloch in Spalte 4 im unteren Mittelteil >> Unterhalb Shiftregister Pin 3 im unterer Mittelteil
11. M-M Kabel 8: Unterhalb Widerstand Steckloch in Spalte 5 im unteren Mittelteil >> Unterhalb Shiftregister Pin 4 im unterer Mittelteil
12. M-M Kabel 9: Unterhalb Widerstand Steckloch in Spalte 6 im unteren Mittelteil >> Unterhalb Shiftregister Pin 5 im unterer Mittelteil
13. M-M Kabel 10: Unterhalb Widerstand Steckloch in Spalte 7 im unteren Mittelteil >> Unterhalb Shiftregister Pin 6 im unterer Mittelteil
14. M-M Kabel 11: Unterhalb Widerstand Steckloch in Spalte 8 im unteren Mittelteil >> Unterhalb Shiftregister Pin 7 im unterer Mittelteil
15. M-M Kabel 12: Unterhalb Shiftregister Pin 8 im unterer Mittelteil >> Untere blaue Reihe (-) der Steckplatte
16. M-M Kabel 13: Oberhalb Shiftregister Pin 1 im oberen Mittelteil >> Untere rote Reihe (+) der Steckplatte
17. M-M Kabel 14: Oberhalb Shiftregister Pin 3 im oberen Mittelteil >> Arduino Pin 12
18. M-M Kabel 15: Oberhalb Shiftregister Pin 4 im oberen Mittelteil >> Obere blaue Reihe (-) der Steckplatte
19. M-M Kabel 16: Oberhalb Shiftregister Pin 5 im oberen Mittelteil >> Arduino Pin ~11
20. M-M Kabel 17: Oberhalb Shiftregister Pin 6 im oberen Mittelteil >> Arduino Pin ~9
21. M-M Kabel 18: Oberhalb Shiftregister Pin 7 im oberen Mittelteil >> Untere rote Reihe (+) der Steckplatte
22. Fotozelle: Über Trennlinie des Mittelteils rechts neben dem Shiftregister
23. 1 kOhm Widerstand: 1. Reihe der Fotozellenspalte >> Obere blaue Reihe (-) der Steckplatte
24. M-M Kabel 19: Unterhalb der Fotozelle in unteren Mittelteil >> Untere rote Reihe (+) der Steckplatte
25. M-M Kabel 20: Zwischen Widerstand und Fotozelle in der selben Spalte im oberen Mittelteil >> Arduino Analog Pin A0


**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson23/Lesson23_PhotoCell.png" width="960"/><br>

  **Diesmal beginnen wir mit dem Code des letzten Projekts.**

1. Öffne das Schieberegister-Projekt und lösche alles aus der Schleifenfunktion.
2. Dann lösche den Zeitblock in der Endlosschleife und die zwei **0 als Byte** (0 as Byte) Blöcke, welche mit **Setze LEDs zu** (set LEDs to) verbunden sind.

  <img src="images/Lesson23/Lesson23_PhotoCell_3.png" width="960"/><br>

3. Ziehe nun die Schleifenfunktion aus der Endlosschleife und platziere sie zurück oberhalb des **updateShiftRegister** Blocks.
4. Erzeuge ein Duplikat von **Setze LEDs zu** (set LEDs to) und platziere diese in 1. Reihe der Endlosschleife.
5. Klicke auf **LEDs** des Blocks und dann auf **Neue Variable...*** (New Variable...) und benenne die Variable zu ***ReadLight*** (LeseLicht).
6. Nun verbinde jeweils ein **0** Block aus **Mathe** (Math) mit dem **Setze LEDs zu** (set LEDs to) der Vorlauf- und Endlosschleife.
7. Lösche den Nummerblock **7** aus der Schleifenfunktion und wechsle es mit einem **Element** (item) Block aus **Variable** (Vairables).
8. Wechsle den Variablenname zu ***ReadLight***.

  <img src="images/Lesson23/Lesson23_PhotoCell_4.png" width="960"/><br>

9. Verbinde einen **" " + " "** Block von **Mathe** (Math) mit **Setze ReadLight zu** (set ReadLight to).
10. Ändere das **+** Symbol zu ***/***.
11. Für die erste Position von **" " / " "** benutze einen **Lese digital Pin# 0** (Read digital pin# 0) aus **Ausgang/Eingang** (Input/Output).
12. Die zweite Position ist ein **0** Block von **Mathe** (Math) mit einem Wert von ***57***.

  <img src="images/Lesson23/Lesson23_PhotoCell_5.png" width="960"/><br>

  **Der Lichtwert der Fotozelle soll die Anzahl der 8 LEDs bestimmen.**

  **Wenn der Lichtwert grösser als 512 ist (512 - 1024) sollen alle 8 LEDs brennen.**

13. Ziehe einen **wenn Tue** Block von **Logik** (Logic) und plaziere diesen zwischen **Setze ReadLight zu** (set ReadLight to) und **Setze LEDs zu** (set LEDs to).
14. Verbinde **wenn** mit einem **" " = " "** Block aus **Logik** (Logic).
15. Ändere das Symbol **=** zu ***>***.
16. Die erste Position von **" " > " "** wird ein **Element** (item) Block aus **Variable** (Variables) mit Variablennamen ***ReadLight***.
17. Die zweite Position wird ein Nummberblock **0** mit Wert ***8***.

  <img src="images/Lesson23/Lesson23_PhotoCell_6.png" width="960"/><br>

18. Ziehe einen **Setze Element zu** (set item to) Block in die **Tue** Spalte der **wenn ReadLight > 8** Funktion.
19. Wähle den Variablennamen ***ReadLight*** und verbinde einen Nummberblock **0** aus **Mathe** (Math) mit dem Wert ***8*** damit.

  <img src="images/Lesson23/Lesson23_PhotoCell_7.png" width="960"/><br>

20. Platziere in der **zähle mit i von 0 zu ReadLight by 1** Funktion einen **Setze Element zu** (set item to) Block mit dem Variablennamen **LEDs**.
21. Verbinde diesen Block mit einem **" " + " "** Block aus **Mathe** (Math).
22. Die erste Position ist ein **Element** Block aus **Variable** (Variables) mit Variablenname ***LEDs***.

  <img src="images/Lesson23/Lesson23_PhotoCell_8.png" width="960"/><br>

23. Die zweite Position von **LEDs + " "** wird ein **" " & ""** Block aus **Verschieden** (Various).
24. Ändere das Symbol **&** zu **<<**.
25. Wiederum die erste Postion hier wird ein Nummerblock **0** mit dem Wert ***1***, und die zweite ein **Element** Block mit ***i*** als Variablesnamen.

  <img src="images/Lesson23/Lesson23_PhotoCell_9.png" width="960"/><br>

26. Gib dem Projekt einen anderen Namen und speichere es ab.
27. Dann kompliere und lade es auf den Arduino hoch.

  #### *Benutze dein Flashlight von deinem Handy oder Tablet und leuchte auf die Fotozelle!*

---

#### [Zurück zum Index](#index)

### 3.24. 9.8.7.6.5.4.3.2.1.0...Start! <a name="countdown...9...0"></a>

In diesem Projekt wirst du lernen, wie du eine Einzifferanzeige aus 7 Segmenten (plus Punkt) benutzen kannst.

Wir werden einen Countdown von 9 bis 0 programmieren.


<img src="images/Lesson24/Lesson24_7SegmentDisplay_1.png" width="960"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Steckplatte**  |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 8 | **220 Ohm Widerstand** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>| 1 | **Schieberegister 74HC595N**                     |<img src="images/Parts/Part_74HC595N.jpg" width="256"/> |
| 1 | **1-Ziffer-Siebensegmentanzeige** |<img src="images/Parts/Part_7SegmentDisplay.jpg" width="256"/>| 28 | **M-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

**1-Ziffer-Siebensegmentanzeige**

* besteht aus 8 LEDs, welche 7 Segmente für eine Ziffer und einen Punkt genutzt werden.
* hat 10 Pins mit 2 Erde und jeweils 1 Pin für eine LED.
* ist eine Alternative zu den komplexeren Punktmatrixanzeigen.

Die Zahl 8 wird angezeigt, wenn alle Segmente mit Strom versorgt werden, und wenn Sie die Stromversorgung für 'g' trennen, wird die Zahl 0 angezeigt. In einer Sieben-Segment-Anzeige kann Strom (oder Spannung) an verschiedenen Pins angelegt werden gleichzeitig, so dass wir Kombinationen der numerischen Anzeige von 0 bis 9 bilden können. Da Siebensegmentanzeigen kein Alphabet wie X und Z bilden können, kann es nicht für Alphabet und nur zur Anzeige von dezimalen numerischen Größen verwendet werden. Siebensegmentanzeigen können jedoch die Buchstaben A, B, C, D, E und F bilden, sodass sie auch zur Darstellung von Hexadezimalziffern verwendet werden können.

<img src="images/Lesson25/Lesson25_7SegmentDisplay_SegCharacters.png" width="128"/><br>

#### Schaltplan

<img src="images/Lesson24/Lesson24_7SegmentDisplay_2.png" width="960"/><br>

#### Bastelanweisung

1. Siebensegmentanzeige: Über Trennlinie des Mittelteils mit oberen Pins im oberen Mittelteil links neben Widerständen >> Untere Pins in unteren Mittelteil
2. Widerstand 220 Ohm: Über Trennlinie des Mittelteils jeweils im Abstand von 2 Stecklöcher in einer Spalte mit einem Bein im oberen Mittelteil und einem Bein im unteren Mittelteil
3. 74HC595N: Über Trennlinie des Mittelteils 8 Pins im oberen Mittelteil und 8 Pins im unteren Mittelteil mit Karbe auf Chip nach Links zeigend.
4. M-M Kabel 1: Obere blaue Reihe (-) der Steckplatte >> Arduino GND
5. M-M Kabel 2: Obere blaue Reihe (-) der Steckplatte >> Untere blaue Reihe (-) der Steckplatte
6. M-M Kabel 3: Untere rote Reihe (+) der Steckplatte >> Arduino 5V
7. M-M Kabel 4: Unterhalb Widerstand Steckloch in Spalte 1 im unteren Mittelteil >> Oberhalb Shiftregister Pin 2 im oberen Mittelteil
8. M-M Kabel 5: Unterhalb Widerstand Steckloch in Spalte 2 im unteren Mittelteil >> Unterhalb Shiftregister Pin 1 im unterer Mittelteil
9. M-M Kabel 6: Unterhalb Widerstand Steckloch in Spalte 3 im unteren Mittelteil >> Unterhalb Shiftregister Pin 2 im unterer Mittelteil
10. M-M Kabel 7: Unterhalb Widerstand Steckloch in Spalte 4 im unteren Mittelteil >> Unterhalb Shiftregister Pin 3 im unterer Mittelteil
11. M-M Kabel 8: Unterhalb Widerstand Steckloch in Spalte 5 im unteren Mittelteil >> Unterhalb Shiftregister Pin 4 im unterer Mittelteil
12. M-M Kabel 9: Unterhalb Widerstand Steckloch in Spalte 6 im unteren Mittelteil >> Unterhalb Shiftregister Pin 5 im unterer Mittelteil
13. M-M Kabel 10: Unterhalb Widerstand Steckloch in Spalte 7 im unteren Mittelteil >> Unterhalb Shiftregister Pin 6 im unterer Mittelteil
14. M-M Kabel 11: Unterhalb Widerstand Steckloch in Spalte 8 im unteren Mittelteil >> Unterhalb Shiftregister Pin 7 im unterer Mittelteil
15. M-M Kabel 12: Unterhalb Shiftregister Pin 8 im unterer Mittelteil >> Untere blaue Reihe (-) der Steckplatte
16. M-M Kabel 13: Oberhalb Shiftregister Pin 1 im oberen Mittelteil >> Untere rote Reihe (+) der Steckplatte
17. M-M Kabel 14: Oberhalb Shiftregister Pin 3 im oberen Mittelteil >> Arduino Pin 2
18. M-M Kabel 15: Oberhalb Shiftregister Pin 4 im oberen Mittelteil >> Obere blaue Reihe (-) der Steckplatte
19. M-M Kabel 16: Oberhalb Shiftregister Pin 5 im oberen Mittelteil >> Arduino Pin ~3
20. M-M Kabel 17: Oberhalb Shiftregister Pin 6 im oberen Mittelteil >> Arduino Pin 4
21. M-M Kabel 18: Oberhalb Shiftregister Pin 7 im oberen Mittelteil >> Untere rote Reihe (+) der Steckplatte
22. M-M Kabel 19: Oberhalb Siebensegmentanzeige Pin 3 im oberen Mittelteil >> Obere blaue Reihe (-) der Steckplatte
23. M-M Kabel 20: Unterhabl Siebensegmentanzeige Pin 3 im unteren Mittelteil >> Untere blaue Reihe (-) der Steckplatte
24. M-M Kabel 21: Oberhalb Siebensegmentanzeige Pin 1 im oberen Mittelteil >> Oberhalb Widerstand Steckloch in Spalte 7 im oberen Mittelteil
25. M-M Kabel 22: Oberhalb Siebensegmentanzeige Pin 2 im oberen Mittelteil >> Oberhalb Widerstand Steckloch in Spalte 6 im oberen Mittelteil
26. M-M Kabel 23: Oberhalb Siebensegmentanzeige Pin 4 im oberen Mittelteil >> Oberhalb Widerstand Steckloch in Spalte 1 im oberen Mittelteil
27. M-M Kabel 24: Oberhalb Siebensegmentanzeige Pin 5 im oberen Mittelteil >> Oberhalb Widerstand Steckloch in Spalte 2 im oberen Mittelteil
28. M-M Kabel 25: Unterhalb Siebensegmentanzeige Pin 1 im unteren Mittelteil >> Oberhalb Widerstand Steckloch in Spalte 5 im oberen Mittelteil
29. M-M Kabel 26: Unterhalb Siebensegmentanzeige Pin 2 im unteren Mittelteil >> Oberhalb Widerstand Steckloch in Spalte 4 im oberen Mittelteil
30. M-M Kabel 27: Unterhalb Siebensegmentanzeige Pin 4 im unteren Mittelteil >> Oberhalb Widerstand Steckloch in Spalte 3 im oberen Mittelteil
31. M-M Kabel 28: Unterhalb Siebensegmentanzeige Pin 5 im unteren Mittelteil >> Oberhalb Widerstand Steckloch in Spalte 8 im oberen Mittelteil

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson24/Lesson24_7SegmentDisplay.png" width="960"/><br>

  **Da wir wieder den Schieberegister benutzen, kannst du eines der zwei vorhergehenden Projekte nutzen.**

  ***Denk daran, es ist immer besser als Erstes dem Projekt einen neuen Namen zu geben, wenn man andere Projekte als Vorlage nutzt.***

1. Lösche komplett alles aus der Endlosschleife.
2. In der Vorlaufschleife verbleiben die beiden **Setze digtal pin#** Blöcke.
3. Ziehe die Variable **LEDs** aus **shiftOut >> ...** der **updateShiftRegister** Funktion auf den Arbeitsbereich.
4. Benenne **LEDs** in ***Digit*** um.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_3.png" width="960"/><br>

  **Die Anzeige braucht eine Initiierung.**

5. Ziehe einen **Setup 7 Segment Anzeige mit 74HC595** (Setup 7 Segment Display) Block in die erste Position der Vorlaufschleife.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_4.png" width="960"/><br>

6. Für den leeren Platz von **shiftOut >> ...** in **updateShiftRegister** benutzen wir den Block **7 Segmente Anzeige >> Schreibe Ziffer** (7 Segment Display >> Write Digit) aus **Bildschirm** (Displays).

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_5.png" width="960"/><br>

7. Fülle nun den leeren Platz von **7 Segmente Anzeige >> Schreibe Ziffer** (7 Segment Display >> Write Digit) mit dem Variablenblock **Digit** vom Arbeitsbereich.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_6.png" width="960"/><br>

8. Ziehe einen **zähle mit i von 0 zu 10 bei 1** (count with i from 0 to 10 by 1) Block aus **Schleife** (Loops) in die Endlosschleife.
9. Vertausche den **10** Block mit dem **0** Block, oder gebe die Zahlen per Tastatur ein.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_7.png" width="960"/><br>

10. Füge nun 2 **Warte 1000 Millisekunden** (wait 1000 milliseconds) Blöcke aus **Zeit** (Time) jeweils einen in **Tue** (do) der Schleifenfunktion und einen darunter als letzten Block in der Endlosschleife.
11. Erhöhe die Zeit des Zeitblocks in der letzten Reihe der Endlosschleife zu ***3000***.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_8.png" width="960"/><br>

12. Ziehe einen **updateShiftRegister** Block aus **Funktion** (Funkctions) in der **Tue** (do) Spalte der Schleifenfunktion.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_9.png" width="960"/><br>

  **Jetzt musst du noch die Digit bzw. die Nummer setzen, die angezeigt werden soll.**

13. Positioniere einen **Setze Element zu** (set item to) Block von **Variable** (Variables) in die erste Reihe der **Tue** Spalte in der Schleifenfunktion.
14. Ändere den Variablenname zu ***Digit***.
15. Verbinde diesen Block mit einem **" " + " "** Block aus **Mathe** (Math).
16. Wechsle das **+** Symbol zu ***-***.
17. Die erste Position wird ein **Element** (item) Block mit dem Variablennamen **i**.
18. Die zweite Position ist ein **0** Zahlenblock mit Wert ***1***.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_10.png" width="960"/><br>

19. Speichere das Projekt, nachdem es benannte hast (im Falle du es nicht am Anfang bereits gemacht).

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_11.png" width="960"/><br>

20. Kompiliere den Code und lade ihn auf deinen Arduino.

#### *Lebe lang und in Frieden! Start in 9.......*


---

#### [Zurück zum Index](#index)

### 3.25. Und 1, 2, 3, 4 <a name="1..2..3..4"></a>

Wir hatten zwar schon einen Metronom, doch wollen wir nun eine visuelle Version bauen, welche dann auch mit Sound angereichert werden kann.

Jedenfalls kannst du im Takt bleiben, auch wenn du das Metronom nicht hören kannst.

Mit den zwei Druckknöpfen kannst du die BPM (Schläge pro Minute) einstellen.

<img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_1.png" width="960"/><br>

Die Anzeige mit den 7 Segmenten und dem Punkt haben eine bestimmte Zuordnung, welche mit Buchstaben definiert sind. Jedes Segment ist eine LED, auch wenn du mehr als eine Anzeige nutzt, wie in diesem Fall.

Die nebeneinander angebrachten 7 Segmentanzeigen werden durch Zeitpulse kontrolliert.

Weil wir für diese Anzeige keinen Schieberegister benutzen, werden wir die Anzeige LED Pins direkt an den Arduino leiten.

<img src="images/Lesson25/Lesson25_7SegmentDisplay_SegCharacters.png" width="128"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Steckplatte**  |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 2 | **Druckknopf**  | <img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/> | 1 | **4-Ziffer-Siebensegmentanzeige** |<img src="images/Parts/Part_4Digit7SegmentDisplay.jpg" width="256"/>|
| 19 | **M-M Kabel**  |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***4-Ziffer-Siebensegmentanzeige:***

* ist eine Kombination von 4 x 1 Ziffer 7-Segment-Anzeigen mit 4 extra Pins, um die jeweilige Ziffer ein- und auszuschalten.
* wird für Digitale Uhren, Radiowecker, Rechner, Armbanduhren, Tachometer, Kilometerzähler für Kraftfahrzeuge, Radiofrequenzanzeigen, usw. verwendet.

#### Schaltplan

<img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_2.png" width="960"/><br>

#### Bastelanweisung

1. 4 x Siebensegmentanzeige: Über Trennlinie des Mittelteils mit oberen Pins im oberen Mittelteil links neben Widerständen >> Untere Pins in unteren Mittelteil
2. Druckknopf: Über Trennlinie des Mittelteils jeweils im Abstand von 2 Stecklöcher
3. M-M Kabel 1: Obere blaue Reihe (-) der Steckplatte >> Arduino GND
4. M-M Kabel 2: Unterhalb Siebensegmentanzeige Pin 1 im unteren Mittelteil >> Arduino Analog Pin A0
5. M-M Kabel 3: Unterhalb Siebensegmentanzeige Pin 2 im unteren Mittelteil >> Arduino Pin ~5
6. M-M Kabel 4: Unterhalb Siebensegmentanzeige Pin 3 im unteren Mittelteil >> Arduino Pin 1
7. M-M Kabel 5: Unterhalb Siebensegmentanzeige Pin 4 im unteren Mittelteil >> Arduino Pin 4
8. M-M Kabel 6: Unterhalb Siebensegmentanzeige Pin 5 im unteren Mittelteil >> Arduino Pin 8
9. M-M Kabel 7: Unterhalb Siebensegmentanzeige Pin 6 im unteren Mittelteil >> Arduino Pin 11
10. M-M Kabel 8: Oberhalb Siebensegmentanzeige Pin 1 im oberen Mittelteil >> Arduino Pin ~6
11. M-M Kabel 9: Oberhalb Siebensegmentanzeige Pin 2 im oberen Mittelteil >> Arduino Pin 2
12. M-M Kabel 10: Oberhalb Siebensegmentanzeige Pin 3 im oberen Mittelteil >> Arduino Pin 7
13. M-M Kabel 11: Oberhalb Siebensegmentanzeige Pin 4 im oberen Mittelteil >> Arduino Pin ~9
14. M-M Kabel 12: Oberhalb Siebensegmentanzeige Pin 5 im oberen Mittelteil >> Arduino Pin ~10
15. M-M Kabel 13: Oberhalb Siebensegmentanzeige Pin 6 im oberen Mittelteil >> Arduino Pin ~3
16. M-M Kabel 14: Oberhalb Druckknopf 1 linker Pin 1 im oberen Mittelteil >> Arduino Pin 12
17. M-M Kabel 15: Oberhalb Druckknopf 2 linker Pin 1 im oberen Mittelteil >> Arduino Pin 13
18. M-M Kabel 16: Oberhalb Druckknopf 1 rechter Pin 1 im oberen Mittelteil >> Obere blaue Reihe (-) der Steckplatte
19. M-M Kabel 17: Oberhalb Druckknopf 2 rechter Pin 1 im oberen Mittelteil >> Obere blaue Reihe (-) der Steckplatte


**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay.png" width="960"/><br>

  **Nachdem du Basisfunktion auf den Arbeitsbereich gezogen hast, wirst du die Anzeige einstellen. Hier sind alle einzelnen Pins anzugeben.**

1. Ziehe einen **Setup 4 x 7 Segment Anzeige zur Pin#** (Setup 4 x 7 Display to pin#) aus **Bildschirm** (Displays) in die Vorlaufschleife.
2. Ändere die Pinnummern zu ***A:2, B:3, C:4, D:5, E:A0, F:7, G:8, DP:1, D1:6, D2:9, D3:10, D4:11***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_3.png" width="960"/><br>

3. Positioniere 2 **Setze Element zu** Blöcke von **Variable** (Variables) in die Vorlaufschleife.
4. Gib den Variablen die Namen ***BPM*** und ***ShowBPM***.
5. Verbinde **BPM** mit einem Zahlenblock **0** aus **Mathe** (Math) mit dem Wert ***120***.
6. Verbinde **ShowBPM** mit **Wahr** (true) aus **Logik** (Logic) und ändere den Wert zu ***Falsch*** (false).

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_4.png" width="960"/><br>

7. Ziehe 2 weitere **Setze Element zu** Blöcke von **Variable** (Variables) diesmal in die Endlosschleife.
8. Gib den Variablen die Namen ***valDwn*** (Wert runter) und ***valUp*** (Wert hoch).
9. Verbinde beide Blöcke mit einem **Lese digital mit PULL_UP Modus Pin# 0** (Read digital with PULL_UP mode Pin# 0) aus **Eingang/Ausgang** (Input/Output).
10. Setze die Pinnummer für **valDwn** zu ***12*** und für **valUp** zu ***13***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_5.png" width="960"/><br>

  **Wenn du einen der Druckknöpfe drückst, soll die Anzeige die "Schläge pro Minute" automatisch anzeigen.**

11. Positioniere einen **wenn Tue** (if do) Block aus **Logik** (Logic) in die Endlosschleife.
12. Verbinde **wenn** (if) mit einem **" " und " "** Block aus **Logik** (Logic).
13. Wechsle **und** (and) zu ***oder*** (or).

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_6.png" width="960"/><br>

14. Die erste Position bekommt einen **" " = " "** Block aus **Logik** (Logic) mit einem **Element** (item) Block aus **Variable** (Variables) als erste Position und einem **HIGH** Block aus **Eingang/Ausgang** (Input/Output) als zweite Position.
15. **Element** wird zu ***valDwn*** und **HIGH** wird zu ***LOW***.
16. Dupliziere **valDwn = Low** und füge die Kopie in die zweite Position von **valDwn = LOW or " "**.
17. Ändere den Variablennamen der Kopie zu ***valUp***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_7.png" width="960"/><br>

18. Mache eine Kopie von **Setze ShowBPM zu Falsch** (set ShowBPM to false) und ziehe den Block in den **Tue** (do) Spalt von **wenn Tue** in der Endlosschleife.
19. Wechsle **Falsch** (false) zu ***Wahr*** (true).

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_8.png" width="960"/><br>

20. Ziehe **wenn Tue** (if do) Block aus **Logik** (Logic) in die Endlosschleife.
21. Verbinde einen **" " = " "** Block aus **Logik** (Logic) mit **wenn** (if).
22. Als erste Position von **" " = " "** füge einen **Element** Block ein und wähle ***ShowBPM*** als Variablennamen.
23. Für die zweite Position von **ShowBPM = " "** setze einen **Wahr** Block aus **Logik** (Logic) ein.
24. Erzeuge eine komplette Kopie des **wenn Tue** Blocks und platziere diesen unterhalb des Originalen.
25. Ändere hier den Wert von **Wahr** zu ***Falsch***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_9.png" width="960"/><br>

  **Wenn die "Schläge pro Minute" gezeigt wird, soll man die Druckknöpfe nutzen können, um die Zahl zu erhöhen oder zu verringern. Nach 3 Sekunden wird die Anzeige automatisch wieder auf den Metronomzähler zurückschalten.**

26. Füge einen **wenn Tue** (if do) Block in die **Tue** Spalte der **wenn ShowBPM = Wahr** (if ShowBPM = Wahr) Funktion.
27. Nutze das Zahnradsymbol um 2 **anderenfalls** hinzuzufügen.
28. Hole dir einen Kopie des Blocks **valDwn = LOW** aus der oberen **wenn Tue** (if do) Funktion und verbinde diese mit **wenn** der neu erstellten **wenn Tue** (if do) Funktion.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_10.png" width="960"/><br>

29. Fahre mit einer Kopie von **valUp = LOW** aus der oberen **wenn Tue** (if do) Funktion und verbinde diese mit dem ersten **anderenfalls** Schlitz.
30. Verbinde für den zweiten **anderenfalls** Abschnitt ebenfalls einen **" " = " "** Block aus **Logik** (Logic).
31. Ändere das **=** Symbol zu ***>***.
32. Als erste Position benötigst du auch hier einen **Element** (item) aus **Variable** (Variables).
33. Gib der Variable den Namen ***curTime*** (momentane Zeit).
34. Die zweite Position wird ein Zahlenblock **0** mit dem Wert ***3000***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_11.png" width="960"/><br>

  **Nun kommt der Teil bei welchen die Zahl addiert oder subtrahiert wird. Dabei werden die 3 Sekunden Wartezeit immer wieder auf 0 gestellt.**

35. Ziehe einen **Setze Element zu** (set item to) in den **Tue** (do) Spalt von **wenn valDwn = LOW** (if valDwn = LOW) und wähle den Variablenname ***BPM***.
36. Verknüpfe einen **" " + " "** Block aus **Mathe** (Math) mit **Setze BPM zu** (set BPM to).
37. Als erste Position ziehe dir **Element** (item) Block aus **Variable** (Variables) hinein und wähle auch hier ***BPM***.
38. Ändere das Symbol von **+** zu ***-***.
39. Dann setze einen Zahlenblock **0** mit dem Wert ***1*** in die zweite Position.
40. Platziere unter **Setze BPM zu BPM - 1** (set BPM to BPM - 1) einen Zeitblock **Warte 1000 Millisekunden** (wait 1000 milliseconds) mit dem Wert ***200***.
41. Darunter kommt erneut ein **Setze Element zu** (set item to) aus **Variable** (Variables) und gebe der Variable den Namen ***startTime***.
42. Verbinde diesen Block mit **Momentane Zeit vergeben (Millisekunden)** (current elapsed Time milliseconds) aus **Zeit** (Time).

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_12.png" width="960"/><br>

43. Mache jeweils ein Duplikat der 3 Blockkombinationen des **Tue** (do) Spaltes von **wenn valDwn = LOW** (if valDwn = LOW) und setze dies in der selben Reihenfolge in die darunterliegende **anderenfalls** (else if) Spalte.
44. Hier brauchst du lediglich das **-** Symbol wieder auf ***+*** verändern.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_13.png" width="960"/><br>

45. Kopiere den Block **Setze showBPM = Wahr** (set showBPM = true) aus der obersten **wenn Tue** (if do) Funktion in der Endlosschleife und setze diesen in die **Tue** (do) Spalte des zweiten **anderenfalls** Abschnitts **anderenfalls cutTime > 3000** (else if curTime > 3000).
46. Dann ändere den **Wahr** Wert zu ***Falsch***.

  **Unterhalb der eingebetten "wenn Tue" (if do) Funktion brauchen wir nun noch zwei Blockkombinationen, welche dadurch in der "wenn ShowBPM = Wahr" (if ShowBPM = true) an unterster Stelle landen.**

47. Ziehe einen **4 x 7 Segment Anzeige >> Schreibe/Ziffer " "** (4 x 7 Segment Display >> Write Digit/Char " ") Block aus **Bildschirm** (Displays) unterhalb der gesamten **wenn valDwn = LOW ...** Funktion.
48. Setze in die Lücke einen **Element** (item) Block und wähle ***BPM*** als Variablennamen.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_14.png" width="960"/><br>

  **Nun noch die momentane Zeit ausrechnen, um die 3 Sekunden zum automatischen Ausschalten zu erhalten.**

49. Setze als unteresten Blockeintrag der **wenn ShowBPM = Wahr** (if ShowBPM = true) eine Kopie von **Setze startTime zu Momentane Zeit vergeben (Millisekunden)** ein.
50. Ändere den Variablennamen **startTime** zu ***curTime***.
51. Trenne den Block **Momentane Zeit vergeben (Millisekunden)** (current elapsed Time milliseconds) von **Setze curTime zu** (set curTime to) und lege den Block auf deinen Arbeitsbereich ab.
52. Verbinde **Setze curTime zu** (set curTime to) mit einem **" " + " "** Block aus **Mathe** (Math).
53. Ändere das **+** Symbol zu ***-***.
54. Als erste Position von **" " - " "** hole dir den **Momentane Zeit vergeben (Millisekunden)** (current elapsed Time milliseconds) Block vom Arbeitsbereich zurück.
55. Als zweite Position benutze einen **Element** (item) Block aus **Variable** (Variables) und wähle ***startTime*** als Namen.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_15.png" width="960"/><br>

  **Nun kommt der Teil dran, der die Zahlen 1, 2, 3 und 4 im Rhythmus der "Schläge pro Minute" darstellt.**  

  **Dies findet alles in der Funktion "ShowBPM = Falsch" statt.**

56. Ziehe einen **Setze Element zu** (set item to) Block von **Variable** (Variables) in die **Tue** (do) der **ShowBPM = Falsch** Funktion.
57. Benenne den Variablennamen in ***Beat*** um.
58. Dann verbinde diesen Block mit einem **" " + " "** Block aus **Mathe** (Math) und ändere das **+** Symbol zu ***x***.
59. In der zweiten Position von **Setze Beat zu " " + " "** brauchst du einen Zahlenblock **0** aus **Mathe** (Math) mit dem Wert ***100***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_16.png" width="960"/><br>

60. Als erste Position ist ein erneuter **" " + " "** Block aus **Mathe** (Math) notwendig.
61. Das Symbol muss hierbei zu **/** geändert werden.
62. Die erste Position von **" " / " "** wird ein Zahlenblock **0** aus **Mathe** (Math) mit ***600*** als Wert.
63. Die zweite Position von **600 / " "** wird diesmal ein **Element** (item) Block aus **Variable** (Variables) mit dem Variablennamen ***BPM***.
64. Ziehe darunter einen **Element D1 4 x 7 Segment Anzeige >> Erzeuge Char** (item D1 4 x 7 Segment Display >> Create Char) Block aus **Bildschirm** (Displays).
65. Benenne **Element** in ***StepOne*** um und klicke auf die Häckchenfelder, sodass eine Eins dargestellt werden wird.

  ***Nimm dir dafür die grafische Darstellung der 7 Segmentanzeige am Anfang dieses Projektes zur Hilfe!***

  **Danach soll die Nummer im Takt dargestellt werden.**

66. Hole dir einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block aus **Zeit** (Time) und platziere diesen unter den Anzeigeblock.
67. Lösche den **1000** Block und ersetze diesen mit einem **Element** (item) Block, welcher den Nahmen ***Beat*** erhält.

  **Nachdem die Anzeige die Nummer Takt angezeigt hat, soll diese gelöscht werden.**

68. Ziehe einen **4 x 7 Segment Anzeige >> Lösche Anzeige** (4 x 7 Segment Display >> Clear Display) Block aus **Bildschirm** (Displays) unter den Zeitblock.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_17.png" width="960"/><br>

  **Momentan würde nur die 1 angezeigt werden.**

69. Wiederhole 64. - 68. dreimal mit folgenden Variablennamen und Pinnummern:
  * ***StepTwo D2*** <br>
  * ***StepThree D3*** <br>
  * ***StepFour D4*** <br>


70. Benutze die Häckchenfelder um die jeweilige Nummer 2, 3, und 4 darzustellen.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_18.png" width="960"/><br>

71. Benenne dein Projekt und speichere das Projekt ab.
72. Kompiliere den Code und lade ihn auf deinen Arduino.

  #### *Und jetzt: Immer im Takt bleiben!*


---

#### [Zurück zum Index](#index)

### 3.26. Lass uns etwas Wind machen <a name="lets-make-some-wind"></a>

In diesem Beispiel wirst du einen DC Motor + Rotor mit 2 Druckknöpfen kontrollieren. Die Einstellung der Druckknöpfe wird die Richtung des Motors und dessen Geschwindigkeit ändern.

<img src="images/Lesson26/Lesson26_DCMotor_1.png" width="960"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Steckplatte** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Breadboard Stromversorgungsmodul** | <img src="images/Parts/Part_BreadBoardPowerSupply.jpg" width="256"/> | 1 | **L293D IC Motorkontroller** |<img src="images/Parts/Part_MotorcontrollerL293D.jpg" width="256"/> |
| 2 | **Drucktaster** |<img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/> | 1 | **3-6V DC Motor mit Lüfterblättern** |<img src="images/Parts/Part_DCMotorWithBlades.jpg" width="256"/> |
| 8 | **M-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***Breadboard Stromversorgungsmodul***

* wird benötigt, wenn man zum Beispiel einen Motor mit Energie versorgen muss.
* dient zum Schutz des Arduino.
* kann die obere und untere Ausgangspannung über Jumper (Steckschalter) bestimmen. Mögliche Spannung sind 3.3 Volt oder 5 Volt.
* hat einen Schalter, um die Stromversorgung ein- und auszuschalten.

***Stelle sicher, dass du das Stromversorgungsmodul richtig auf der Steckplatte plazierst. Beachte dabei die *+* und *-* Zeichen, um die richtige Position zu erkennen.***

***L293D IC Motorkontroller***

* ist ein vielseitiger Chip.
* kann zwei Motoren voneinander unabhängig steuern.
* ist ein 4-Kanal Hochstromtreiber, welcher bidirektionale Ströme von bis zu 1000 mA bei Spannungen von 4,5V bis 36V generieren kann.
* kann Relays, Magnetspulen, DC- und bipolare Schrittmotoren mit hohem Strom- bzw Spannungsbedarf betreiben.
* hat verschieden Pins die in Erdepins, Stromflussschalterpins, 3.3 und 5 Volt Eingängen und Richtungspins ausgestattet.
* kann durch die Nutzung von PWM Pulsen des Arduino die Geschwindigkeit ändern.

  <img src="images/Lesson26/N295L_MotorController.png" width="512"/><br>

***3-6V DC Motor mit Lüfterblättern***

* ist ein Gleichstrommotor und damit der häufigst genutze Motor.
* hat zwei Leitungen, eine positive und eine negative.

Wenn Sie diese beiden Kabel direkt an eine Batterie anschließen, dreht sich der Motor. Wenn Sie die Leitungen vertauschen, dreht der Motor in die entgegengesetzte Richtung.

#### ***Warnung − Betreibe diesen Motor nicht direkt über Pins der Arduino-Platine an! Dies kann die Platine beschädigen!***

#### Schaltplan

<img src="images/Lesson26/Lesson26_DCMotor_2.png" width="960"/><br>

#### Bastelanweisung

1. Stromversorgungsmodul: Jeweils die **+** und **-** Pins mit den blauen (+) und roten (-) Reihen verbinden.
2. L293D: Über Trennlinie des Mittelteils 8 Pins im oberen Mittelteil und 8 Pins im unteren Mittelteil mit Karbe auf Chip nach Links zeigend
3. Druckknopf: Über Trennlinie des Mittelteils jeweils im Abstand von 2 Stecklöcher
4. M-M Kabel 1: Untere blaue Reihe (-) der Steckplatte >> Arduino GND
5. M-M Kabel 2: Oberhalb Druckknopf 1 linker Pin 1 im oberen Mittelteil >> Arduino Pin 7
6. M-M Kabel 3: Oberhalb Druckknopf 2 linker Pin 1 im oberen Mittelteil >> Arduino Pin 8
7. M-M Kabel 4: Oberhalb Druckknopf 1 rechter Pin 1 im oberen Mittelteil >> Untere blaue Reihe (-) der Steckplatte
8. M-M Kabel 5: Oberhalb Druckknopf 2 rechter Pin 1 im oberen Mittelteil >> Untere blaue Reihe (-) der Steckplatte
9. M-M Kabel 6: Unterhalb L293D Pin 1 im unteren Mittelteil >> Arduino Pin ~5
10. M-M Kabel 7: Unterhalb L293D Pin 2 im unteren Mittelteil >> Arduino Pin 4
11. M-M Kabel 8: Unterhalb L293D Pin 4 im unteren Mittelteil >> Untere blaue Reihe (-) der Steckplatte
12. M-M Kabel 9: Unterhalb L293D Pin 7 im unteren Mittelteil >> Arduino Pin ~3
13. M-M Kabel 8: Unterhalb L293D Pin 8 im unteren Mittelteil >> Untere rote Reihe (+) der Steckplatte
14. DC Motor rotes Kabel **+**: Unterhalb L293D Pin 3 im unteren Mittelteil
15. DC Motor schwarzes Kabel **-**: Unterhalb L293D Pin 6 im unteren Mittelteil


**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson26/Lesson26_DCMotor.png" width="960"/><br>

1. Platziere einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) in die Vorlaufschleife.
2. Benenne **Element** in ***Speed*** um.
3. Dann verbinde **Setze Speed zu** (set Speed to) mit einem **0** Zahlenblock aus **Mathe** (Math) und ändere den Wert zu ***255***.

  <img src="images/Lesson26/Lesson26_DCMotor_3.png" width="960"/><br>

  **Wir haben 2 Pins vom Motorkontroller, um die Drehrichtung zu bestimmen.**

4. Ziehe 2 **Setze digitale Pin# 0 zu HIGH** (set digital pin# 0 to) Blöcke aus **Eingang/Ausgang** (Input/Output) in die Vorlaufschleife.
5. Der erste Block erhält die Pinnummer ***3***.
6. Der zweite Block erhält die Pinnummer ***4*** und einen Status ***LOW***.

  <img src="images/Lesson26/Lesson26_DCMotor_4.png" width="960"/><br>

  **Nutze einen booleschen Operator, um festzuhalten ob sich die Drehrichtung ändern soll.**

7. Zieh einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) in die Vorlaufschleife und benenne ihn in ***revDir*** um.
8. Addiere zum **Setze revDir zu** (set revDir to) einen **Wahr** Block aus **Logik** (Logic) und setze den Wert auf ***Falsch***.

  <img src="images/Lesson26/Lesson26_DCMotor_5.png" width="960"/><br>

  **Benutze eine Variable mit Nummern, um die momentane Geschwindigkeitsstufe zu halten.**

9. Platziere einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) in die Vorlaufschleife und benenne ihn in ***SpeedLevel*** um.
10. Addiere zum **Setze SpeedLevel zu** (set SpeedLevel to) einen **0** Block aus **Mathe** (Math) und setze den Wert auf ***2***.

  <img src="images/Lesson26/Lesson26_DCMotor_6.png" width="960"/><br>

  **Du brauchst zwei Variable, welche die ganze Zeit auf die Druckknöpfe hört, falls einer gedrückt wird.**

11. Zieh einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) diesmal in die Endlosschleife und benenne ihn in ***Btn1*** um.
12. Addiere zum **Setze Btn1 zu** (set Btn1 to) einen **Lese digital mit PULL_UP Modus Pin# 0** (Read digital with PULL_UP mode Pin# 0) Block aus **Eingang/Ausgang** (Input/Output) und setze die Pinnummer auf ***7***.
13. Nun kopiere die gesamte Blockkombination **Setze Btn1 zu ...** (set Btn1 to ...) und platziere das Duplikat darunter.
14. Gib der Kopie einen neue Variable mit dem Namen ***Btn2*** und die Pinnummer ***8***.

  <img src="images/Lesson26/Lesson26_DCMotor_7.png" width="960"/><br>

  **Jetzt wirst du die Drehgeschwindigkeit anhand der Geschwindigkeitsstufe errechnen.**

15. Kopiere den Block **Setze Speed zu 255** (set Speed to 255) aus der Vorlaufschleife und ziehe das Duplikat in die Endlosschleife.
16. Trenne nun den **255** Block und verringere die Zahl auf ***150***.
17. Verbinde nun **Setze Speed zu** (set Speed to) mit einem **" " + " "** Block aus **Mathe** (Math) und nutze den Block **150** in der ersten Position.

  <img src="images/Lesson26/Lesson26_DCMotor_8.png" width="960"/><br>

18. Die zweite Position von **150 + " "** braucht einen **" " + " "** Block aus **Mathe** (Math) mit dem Symbol ***x***.
19. Wiederum hier besteht die erste Position aus einem **Element** (item) Block aus **Variable** (Variables) mit dem Namen **SpeedLevel**.
20. Die zweite Position ist einen Zahlenblock mit dem Wert ***50***.

  <img src="images/Lesson26/Lesson26_DCMotor_9.png" width="960"/><br>

  **Um die Geschwindigkeit über den Motorkontroller zu steuern, musst du die Geschwindigkeit über einen analogen Pin senden.**

21. Ziehe einen **Setze analog Pin# 0 zu** (Set analog pin# 0 to) Block aus **Eingang/Ausgang** (Input/Output) in die Endlosschleife.
22. Die Pinnummer muss ***5*** sein.
23. Verbinde einen **Element** (item) Block aus **Variable** (Variables) mit dem Block und setze den Variablenname ***Speed*** ein.

  <img src="images/Lesson26/Lesson26_DCMotor_10.png" width="960"/><br>

  **Wenn sich die Drehrichtung ändern soll, brauchen wir die Funktion, die auf Wahr und Falsch des booleschen Operators reagiert.**

24. Nutze hierfür einen **wenn Tue** (if do) Block aus **Logik** (Logic), den du in der Endlosschleife positionierst.
25. Benutze das Zahnradsymbol um einen **anderenfalls** (else if) Teil hinzuzufügen.
26. Dann setze einen **" " = " "** Block aus **Logik** (Logic) hinter **wenn** (if) ein.
27. Als erste Position wähle einen **Element** (item) Block aus **Variable** (Variables) mit dem Variablennamen ***revDir***.
28. In der zweiten Position von **revDir = " "** brauchst du einen **Wahr** (true) Block aus **Logik** (Logic).
29. Ändere den Wert zu ***Falsch*** (false).

  <img src="images/Lesson26/Lesson26_DCMotor_11.png" width="960"/><br>

30. Mache nun eine Kopie von **revDir = Falsch** (revDir = false) und verbinde diese mit **anderenfalls** (else if).
31. Ändere den Wert **Falsch** zurück zu ***Wahr***.

  <img src="images/Lesson26/Lesson26_DCMotor_12.png" width="960"/><br>

32. Kopiere beide **Setze digitale Pin#...** (set digital pin#...) Blöcke aus der Vorlaufschleife und setze sie in die **Tue** Spalte von **wenn revDir = Falsch**.

  <img src="images/Lesson26/Lesson26_DCMotor_13.png" width="960"/><br>

33. Kopiere noch einmal beide **Setze digitale Pin#...** (set digital pin#...) Blöcke aus der Vorlaufschleife und setze sie in die **Tue** Spalte von **anderenfalls revDir = Wahr**.
34. Diesmal ändere **LOW** zu ***HIGH*** und **HIGH** zu ***LOW***.

  <img src="images/Lesson26/Lesson26_DCMotor_14.png" width="960"/><br>

  **Als Nächstes wirst du den Code erstellen, der jedesmal ausgelöst wird, wenn du einen der Druckknöpfe drückst.**

35. Ziehe dazu einen neuen **wenn Tue** (if do) Block aus **Logik** (Logic) in die Endlosschleife.
36. Auch hier nutze das Zahnradsymbol, um einen **anderenfalls** (else if) Teil hinzuzufügen.
37. Ziehe nun erneut einen **" " = " "** Block aus **Logik** (Logic) und verbinde ihn zu **wenn** (if).
38. Die erste Position wird ein **Element** (item) Block aus **Variable** (Variables) mit dem Variablennamen ***Btn1***.
39. Die zweite Position wird ein **HIGH** Block aus **Eingang/Ausgang** (Input/Output).
40. Der Wert von **HIGH** muss sich zu ***LOW*** ändern.
41. Erzeuge ein Duplikat von **Btn1 = LOW** und verbinde die Kopie mit **anderenfalls** (else if).
42. Ändere den Variablennamen der Kopie zu ***Btn2***.

  <img src="images/Lesson26/Lesson26_DCMotor_15.png" width="960"/><br>

43. Erzeuge eine Kopie von **Setze revDir zu Falsch** (set revDir to false) aus der Vorlaufschleife und ziehe diese in die **Tue** Spalte von **wenn Btn1 = LOW** (if Btn1 = LOW).
44. Nutze nun den **Ändere Element bei 1** (change item by 1) Block aus **Mathe** (Math) mit dem Variablennamen ***SpeedLevel*** in **wenn Btn1 = LOW**.

  <img src="images/Lesson26/Lesson26_DCMotor_16.png" width="960"/><br>

  **Bei jedem Knopfdruck wird sich eine 1 addieren. Doch du brauchst nur 0, 1 und 2 als Geschwindigkeitsstufe. Also brauchen wird hier eine Methode um auf 0 zurückzugehn, wenn es über 2 geht.**

45. Ziehe daher einen weiteren **wenn Tue** (if do) Block aus **Logik** (Logic) in die **Tue** (do) Spalte von **wenn Btn1 = LOW** (if Btn1 = LOW).
46. Setze einen **" " = " "** Block von **Logik** (Logic) als **wenn** (if) Kondition ein.
47. Das Symbol **=** ändert sich zu ***>***.
48. Die erste Position ist ein **Element** (item) Block aus **Variable** (Variables) mit dem Variablennamen **SpeedLevel** (Geschwindigkeitsstufe).
49. Die zweite Position ist ein Nummernblock ***2***.
50. Für den **Tue** Teil kopiere den **Setze SpeedLevel zu 2** (set SpeedLevel to 2) Block aus der Vorlaufschleife hinein.
51. Dann reduziere die Nummer **2** auf **0**.

  <img src="images/Lesson26/Lesson26_DCMotor_17.png" width="960"/><br>

  **Nach jedem Knopfdruck soll das Programm kurz warten, bis es weitergeht, damit auch ein Doppeldruck als Einzeldruck gilt.**

52. Ziehe einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) unterhalb der **wenn SpeedLevel > 2** (if SpeedLevel > 2).
53. Ändere den Wert auf **200**.

  <img src="images/Lesson26/Lesson26_DCMotor_18.png" width="960"/><br>

  **Jetzt ist als bei Knopf 1 klar, nun braucht der 2. Knopf die selben Block mit der Ausnahme des booleschen Wertes.**

54. Kopiere alle Blöcken der **Tue** Spalte unter **wenn Btn1 = LOW** und füge diese in der selben Reihenfolge in die **Tue** Spalte unter **anderenfalls Btn2 = LOW**.
55. Der einzige Wert, welcher sich ändert ist **Falsch** zu ***Wahr***, um die Drehrichtung zu ändern.

  <img src="images/Lesson26/Lesson26_DCMotor_19.png" width="960"/><br>

56. Benenne dein Projekt und speichere es ab.

  <img src="images/Lesson26/Lesson26_DCMotor_20.png" width="960"/><br>

57. Kompiliere den Code und lade das Projekt auf deinen Arduino.

  ***Versichere dich, das der Stecker des Breadboard Stromversorgungsmodul in der Steckdose ist und der Schalter daran gedrückt ist.***


---

#### [Zurück zum Index](#index)

### 3.27. Tik Tak Tok <a name="tic-tac-toc"></a>

Stell dir vor, du möchtest mit deinem Arduino eine Grünanlage mit Wasserzufuhr, Türöffner, Fensteröffner, Belüftung, Heizung usw steuern. Viele der Geräte sollen automatisch ein- und ausgeschalten werden, auch jene, welche wesentlich höherer Engergie bedarf.

Hier kommt das Relais ins Spiel. Du kannst mit deinem Arduino diesen elektrischen Schalter bedienen, in diesem Falle einen 6V Motor.

*Mit anderen Relaisschalter kannst du sogar Geräte mit 220 Volt steuern. Dies solltest du jedoch nur mit Absprache einer erfahrenen Person in Betracht ziehen.*


<img src="images/Lesson27/Lesson27_Relay_1.png" width="960"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Steckplatte** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Breadboard Stromversorgungsmodul** | <img src="images/Parts/Part_BreadBoardPowerSupply.jpg" width="256"/> | 1 | **L293D IC Motorkontroller** |<img src="images/Parts/Part_MotorcontrollerL293D.jpg" width="256"/> |
| 1 | **5V Relais** |<img src="images/Parts/Part_5VRelais.jpg" width="256"/> | 1 | **3-6V DC Motor mit Lüfterblättern** |<img src="images/Parts/Part_DCMotorWithBlades.jpg" width="256"/> |
| 4 | **W-M Kabel** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> | 6 | **M-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***5V Relais:***

* ist ein durch Elektrizität ausgelöster Schalter.
* benutzen oft einen Elektromagneten, der bei angelegter Spannung mechanisch den Schaltkreis schließt.
* benutzt man, wenn man den Schaltkreis mit einem schwächeren Steuerstromkreis schalten will oder aber, wenn man mit einem Steuersignal viele Schaltkreise auf einmal schalten will. Der Steuerstromkreis und der zu steuernde Stromkreis sind dabei elektrisch vollkommen voneinander isoliert.


#### Schaltplan

<img src="images/Lesson27/Lesson27_Relay_2.png" width="960"/><br>

#### Bastelanweisung

1. Stromversorgungsmodul: Jeweils die **+** und **-** Pins mit den blauen (+) und roten (-) Reihen verbinden.
2. L293D: Über Trennlinie des Mittelteils 8 Pins im oberen Mittelteil und 8 Pins im unteren Mittelteil mit Karbe auf Chip nach Links zeigend
3. M-M Kabel 1: Untere blaue Reihe (-) der Steckplatte >> Arduino GND
4. M-M Kabel 2: Unterhalb L293D Pin 1 im unteren Mittelteil >> Arduino Pin ~5
5. M-M Kabel 3: Unterhalb L293D Pin 2 im unteren Mittelteil >> Arduino Pin 4
6. M-M Kabel 4: Unterhalb L293D Pin 4 im unteren Mittelteil >> Untere blaue Reihe (-) der Steckplatte
7. M-M Kabel 5: Unterhalb L293D Pin 7 im unteren Mittelteil >> Arduino Pin ~3
8. M-M Kabel 6: Unterhalb L293D Pin 8 im unteren Mittelteil >> Untere rote Reihe (+) der Steckplatte
9. W-M Kabel 1: Untere Mittelposition vom 5V Relais >> Untere rote Reihe (+) der Steckplatte
10. W-M Kabel 2: Untere Position Links vom 5V Relais >> Unterhalb L293D Pin 3 im unteren Mittelteil
11. W-M Kabel 3: Untere Position Rechts vom 5V Relais >> Unterhalb L293D Pin 6 im unteren Mittelteil
12. W-M Kabel 4: Obere Position Rechts vom 5V Relais >> Im oberen Mittelteil
13. DC Motor rotes Kabel **+**: Unterhalb W-M Kabel 4 auf oberen Mittelteil
14. DC Motor schwarzes Kabel **-**: Obere blaue Reihe (-) der Steckplatte

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson27/Lesson27_Relay.png" width="960"/><br>

1. Wie immer, ziehe die Basisfunktion aus **Funktion** (Functions) auf deinen Arbeitsbereich.
2. Dann ziehe 3 **Setze digitale Pin# 0 zu HIGH** (set digital pin# 0 to HIGH) aus **Eingang/Ausgang** (Input/Output) in die Vorlaufschleife.
3. Wähle die folgenden Pinnummer und Status: ***5 >> HIGH***, ***3 >> HIGH*** und ***4 >> LOW***.

  <img src="images/Lesson27/Lesson27_Relay_3.png" width="960"/><br>

4. Dupliziere **Setze digital Pin# 5 zu HIGH** (set digital pin# 5 zu HIGH) und ziehe die Kopie in die Endlosschleife.
5. Hole dir einen **Wiederhole 10 mal Tue** (repeat 10 times do) Block aus **Schleife** (Loops) in die Endlosschleife.
6. Ändere die Nummer zu ***5***.
7. Dupliziere nun **Setze digital Pin# 3 zu HIGH** (set digital pin# 3 zu HIGH) und **Setze digital Pin# 4 zu LOW** (set digital pin# 4 zu LOW).
8. Ziehe die beiden Kopien in die **Tue** (do) Spalte von **Wiederhole 5 mal** (repeat 5 times).
9. Positioniere unter die Kopien einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) Block aus **Zeit** (Time) und verringere den Wert auf ***500***.
10. Dupliziere nun diese 3 Blöcke erneut und platziere diese direkt unterhalb des originalen Zeitblocks.
11. Dann ändere den Status von Pinnummer **3** zu ***LOW*** und von Pinnummer **4** zu ***HIGH***.

  <img src="images/Lesson27/Lesson27_Relay_5.png" width="960"/><br>

12. Erzeuge eine Kopie von **Setze digitale Pin# 5 zu HIGH** (set digital pin# 5 to HIGH) und ziehe diese unterhalb der **Wiederhole 5 mal** (repeat 5 times) Schleife in der Endlosschleife.
13. Ändere den Status zu ***LOW***.
14. Dann füge wiederum darunter einen weiteren Zeitblock **Warte 1000 Millisekunden** (wait 1000 milliseconds) ein und erhöhe den Wert auf ***3000***.

  <img src="images/Lesson27/Lesson27_Relay_6.png" width="960"/><br>

15. Dupliziere nun alle Blöcke der Endlosschleife und setze diese in der selben Reihenfolge als zweiten Teil unter den originalen Teil.

  ***Ändere nun zum Beispiel die Wartezeiten in der Wiederholungsschleife zu 750.***

  <img src="images/Lesson27/Lesson27_Relay_7.png" width="960"/><br>

16. Gib dem Projekt einen Namen, speichere, kompiliere und lade es auf deinen Arduino.

  #### *Mit der Magie des Relaisschalter kannst du nun dem Fan einen Rhythmus geben!*

  ***Versuche es mit anderen Wiederholungszahlen und Wartezeiten.***

---

#### [Zurück zum Index](#index)

### 3.28. Brumm! <a name="brummm"></a>

In diesem Projekt wirst du einen Motor nutzen, welcher zum Beispiel für ein Achse in einem 3D Drucker genutzt wird.

Du wirst den Motor durch den Rotationsdekodierer mit Druckknopf steuern, wobei die Drehrichtung des Rotationsdekodierer die Drehrichtung des Motors bestimmt. Je mehr die in eine Richtung drehst, desto mehr dreht sich der Motor in diese Richtung.

Mit dem Drücken des Knopfes dreht sich der Motor auf die Ausgangsposition zurück.


<img src="images/Lesson28/Lesson28_StepperMRotaryPot_1.png" width="960"/><br>


#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Steckplatte** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Breadboard Stromversorgungsmodul** | <img src="images/Parts/Part_BreadBoardPowerSupply.jpg" width="256"/> | 1 | **ULN2003 Schrittmotortreibermodul** |<img src="images/Parts/Part_ULN2003_StepperModule.jpg" width="256"/>|
| 1 | **Schrittmotor** |<img src="images/Parts/Part_StepperMotor.jpg" width="256"/>| 1 | **Drehgebermodul** |<img src="images/Parts/Part_RotaryEncoder.jpg" width="256"/> |
| 11 | **F-M Kabel** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> | 1 | **M-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***ULN2003 Schrittmotortreibermodul:***

* Der einfachste Weg, um einen unipolaren Schrittmotor mit einem Arduino
anzusteuern ist eine Verbindung im Transistor Array des ULN2003A zu überbrücken.
Der ULN2003A-Chip enthält sieben Darlington Transistoren und hat zusätzlich
sieben weitere TIP120 Transistoren an Board. Er kann bis zu 500 mA pro Kanal
ausgeben und hat einen internen Spannungsabfall von etwa 1V im eingeschalteten
Zustand. Er enthält außerdem interne Klemmdioden, um Spannungsspitzen zu
209 / 223
verlagern, wenn induktive Lasten betrieben werden. Um den Schrittmotor zu
kontrollieren, muss in einer speziellen Frequenz jede der Spulen mit Spannung
versorgt werden.



***Schrittmotor:***

* ist ein elektromechanisches Gerät, das elektrische Pulse in diskrete mechanische Bewegungen umwandelt.
* rotiert in einer Weise, die stark abhängig vom Eingangssignal bzw den Pulsen vom Arduino.
* Sequenz der Pulse bestimmt direkt die Richtung der Rotation, während die Länge der Rotation von der Anzahl und Frequenz der Pulse abhängt.


***Drehgebermodul:***

* ist ein Sensor für den Drehwinkel.
* wird auch als Rotationsdekodierer genannt.
* ist im Gegensatz zum Potentiomenter digital.
* hat einen Druckknopf.

#### Schaltplan

<img src="images/Lesson28/Lesson28_StepperMRotaryPot_2.png" width="960"/><br>

#### Bastelanweisung

1. Schrittmotor: Verbinde Motorkabelstecker mit ULN2003 Buchse
2. ULN2003: Über Trennlinie des Mittelteils 8 Pins im oberen Mittelteil und 8 Pins im unteren Mittelteil mit Karbe auf Chip nach Links zeigend
3. M-M Kabel 1: Untere blaue Reihe (-) der Steckplatte >> Arduino GND
4. W-M Kabel 1: ULN2003 Pin In1 >> Arduino Pin ~11
5. W-M Kabel 2: ULN2003 Pin In2 >> Arduino Pin ~10
6. W-M Kabel 3: ULN2003 Pin In3 >> Arduino Pin ~9
7. W-M Kabel 4: ULN2003 Pin In4 >> Arduino Pin 8
8. W-M Kabel 5: ULN2003 Pin **-** >> Untere blaue Reihe (-) der Steckplatte
9. W-M Kabel 6: ULN2003 Pin **+** >> Untere rote Reihe (+) der Steckplatte
10. W-M Kabel 7: Drehgeber Pin GND >> Arduino GND
11. W-M Kabel 8: Drehgeber Pin 5V >> Arduino 5V
12. W-M Kabel 9: Drehgeber Pin SW >> Arduino Pin 4
13. W-M Kabel 10: Drehgeber Pin DT >> Arduino Pin ~3
14. W-M Kabel 11: Drehgeber Pin CLK >> Arduino Pin 2

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson28/Lesson28_StepperMRotaryPot.png" width="960"/><br>

  **Du wirst sogleich mit einem eigenem Modul oder eigenen Funktion beginnen.**

1. Nachdem du die Basisfunktion auf den Arbeitsbereich gezogen hast, geht es mit einer eigenen Funktion weiter.

  **ISR (Interrupt Service Routine) ist ein Hardware Unterbrecher, welcher permanent auf die Veränderung eines Pins. In diesen Fall ist es Pinnummer 2, an welchem der Rotationsdekodierer verbunden ist.**


2. Ziehe einen **zu Mache etwas** (to do something) Block aus **Funktion** (Funktions) oberhalb der Basisfunktion auf deinen Arbeitsbereich.
3. Nenne die Funktion ***isr***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_3.png" width="960"/><br>

4. In erster Reihe in der Funktion platzierst du einen **Warte 100 Millisekunden** (wait 100 milliseconds) aus **Zeit** (Time) mit dem Wert ***4***.
5. Dann ziehe einen **wenn Tut** (if do) Block von **Logik** (Logic) darunter und benutze das Zahnradsymbol um **sonst** (else) hinzuzufügen.
6. Verbinde einen **Lese digitale Pin# 0** (Read digital pin# 0) aus **Eingang/Ausgang** (Input/Output) mit **wenn**.
7. Ändere die Pinnummer zu ***2***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_4.png" width="960"/><br>

8. Addiere zum **Tue** (do) Spalt einen **Setze Element zu** (set item to) Block aus **Variable** (Variables).
9. Benenne die Variable ***RotDir*** und verbinde diese mit einer Kopie von **Lese digitale Pin# 2** (Read digital pin# 2).
10. Wähle die Pinnummer ***3***.
11. Erzeuge ein Duplikat von **Setze RotDir zu Lese digitale Pin# 3** (set RotDir to Read digital pin# 3) und setze die Kopie in die **sonst** (else) Spalte.
12. Füge dort zwischen **Setze RotDir zu** (set RotDir to) und **Lese digitale Pin# 3** (Read digtal pin# 3) einen **Nicht** (not) Block aus **Logik** (Logic).

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_5.png" width="960"/><br>

13. Positioniere nun unterhalb der **wenn Lese digitale Pin# 2** (if Read digital pin# 2) Funktion eine weitere **Setze Element zu** (set item to) Variable ein und gib dieser den Namen ***TurnDetected*** (Drehung erkannt).
14. Verbinde diese mit einem **Wahr** (true) Block aus **Logik** (Logic)

  **Fahre nun mit der Vorlaufschleife fort.**

15. Ziehe einen **Binde an Unterbrecher# 0** (Attache on Interrupt# 0) aus **Verschieden** (Various) in die Vorlaufschleife.
16. Ändere den Modus zu ***FALLING***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_6.png" width="960"/><br>

17. Ziehe nun deine Funktion **isr** aus **Funktion** (Functions) in den Funktionsspalt von **Binde an Unterbrecher# 0** (Attache on Interrupt# 0).
18. Unter diese Funktion ziehe einen **Setup Steppermotor MeinStepper** (Setup stepper motor MyStepper) aus **Motor** (Motors) in die Vorlaufschleife.
19. Nachdem du die Nummer der Pins auf ***4*** gestellt hast, gebe folgende Pinnummern ein: ***Pin1#: 8  Pin2#: 10  Pin3#: 9  Pin4#: 11***.
20. Die Schritte pro Umdrehung werden ***700*** und die Geschwindigkeit wird ***32***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_7.png" width="960"/><br>

21. Kopiere **Setze TurnDetected zu Wahr** (set TurnDetected to true) aus der **isr** Funktion und füge das Duplikat in die Vorlaufschleife ein.
22. Dann ändere den Wert zu **Falsch** (false).
23. Mache nun von dieser Blockkombination ein Duplikat und positioniere es direkt darunter in die Vorlaufschlaufe.
24. Der Variablenname wird zu ***RotDir*** und der Wert wird wiederum **Wahr**.
25. Der letze Eintrag in die Vorlaufschleife wird ein erneuter Variablenblock **Setze Element zu** (set item to) aus **Variable** (Variables).
26. Benenne die Variable zu ***RotPos*** und verbinde diese mit einem Zahlenblock **0** aus **Mathe** (Mathe).

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_8.png" width="960"/><br>

  **Als erstes wirst du die Resetfunktion der Drehbewegung in die Endlosschleife einfügen. Wenn du den Knopf des Rotationsdekodierer drückst, dreht sich der Motor automatisch zur Ausgangsposition zurück.**

27. Beginne mit einem **wenn Tue** (if do) Block aus **Logik** (Logic).
28. Verbinde **wenn** (if) erst mit einem **Nicht** (not) Block aus **Logik** (Logic), dann mit einem **Lese digital mit PULL_UP Modus Pin# 0** (Read digital with PULL_UP mode Pin# 0).
29. Die Pinnummer wird ***4***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_9.png" width="960"/><br>

30. Ziehe einen weiteren **wenn Tue** (if do) Block aus **Logik** (Logic) in die **Tue** (do) Spalte der **wenn not Lese digital mit PULL_UP Modus Pin# 4** (if not Read digital with PULL_UP mode Pin# 4).
31. Nutze nun das Zahnradsymbol, um in der neuen Funktion **sonst** (else) hinzuzufügen.
32. Verbinde **wenn** (if) mit einem **" " = " "** Block aus **Logik** (Logic).
33. Die erste Stelle füllst du mit einem **Element** (item) aus **Variable** (Variables), das den Namen ***RotPos*** erhält.
34. Die zweite Stelle wird ein Zahlenblock **0** aus **Mathe** (Math).

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_10.png" width="960"/><br>

  **In dieser Funktion wirst du nur die "sonst" Spalte nutzen. Die "Tue" Spalte bleibt leer, also nichts wird im Falle geschehen, wenn die Rotationsposition 0 ist.**

35. Erzeuge eine Kopie von **Setze RotPos zu 0** (set RotPos to 0) aus der Vorlaufschleife und platziere das Duplikat in **sonst** (else) der **wenn RotPos = 0** (if RotPos = 0) Funktion.
36. Platziere darüber einen **Bewege Motor MeinStepper " " Schritte** (move stepper MyStepper " " steps) in die **sonst** Spalte.
37. Ziehe den Zahlenblock **10** auf deinen Arbeitsbereich und ersetze ihn mit einem **" " + " "** Block aus **Mathe** (Math).
38. Ändere das Symbol **+** zu ***x***.
39. Benutze nun den Zahlenblock **10** in der zweiten Position von **" " x " "** und verändere den Wert zu ***-1***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_11.png" width="960"/><br>

40. Die erste Position von **" " x -1** startet mit einem weiteren **" " + " "** Block aus **Mathe** (Math).
41. Ändere auch hier das Symbol **+** zu ***x***.
42. Die erste Position ist ein **Element** (item) Block mit dem Variablennamen ***RotPos***.
43. Die zweite Position ist ein Zahlenblock **0** mit Wert ***50*** als Wert.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_12.png" width="960"/><br>

  **Sobald der Interrupter ein Signal registriert und der boolesche Wert sich dadurch auf "Wahr" umstellt, soll die Rotationsposition mit der momentane Position aufgefrischt werden.**

44. Ziehe einen **wenn Tue** (if do) Block aus **Logik** (Logic) unterhalb der vorhergehenden **wenn Nicht Lese digital mit PULL_UP Modus Pin# 4** (if not Read digital with PULL_UP mode Pin# 4) Funktion.
45. Verbinde einen **" " = " "** Block aus **Logik** (Logic) mit **wenn** (if) der neuen **wenn Tue** Funktion.
46. Benutze einen **Element** (item) Block aus **Variable** (Variables) als erste Position von **wenn " " = " "** und wähle ***TurnDetected*** als Variablenname.
47. Die zweite Position wird ein **Wahr** (true) Block aus **Logik** (Logic).
48. Für die **Tue** (do) Spalte ziehe einen **Setze Element zu** (set item to) Block verbunden mit einem Block **Element** (item) aus **Variable** (Variables) hinein.
49. Erzeuge einen Variablenname ***PrevPos*** für **Setze Element zu** (set item to) und wähle den Variablenname ***RotPos*** für den verbunden Block.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_13.png" width="960"/><br>

50. Darunter ziehe eine weitere **wenn Tue** (if do) Funktion aus **Logik** (Logic) in die **wenn TurnDetected = Wahr** (if TurnDetected = true) Funktion und drücke das Zahnradsymbol um den **sonst** Block zur Funktion hinzuzufügen.
51. Verbinde einen **" " = " "** Block aus **Logik** (Logic) mit **wenn**.
52. Die erste Position wird ein Elementblock mit gewählten Variablennamen ***RotDir***, während die zweite ein **Wahr** (true) Block sein soll.
53. Setze in die **Tue** Spalte eine Kopie von **Setze RotPos zu 0** (set RotPos to 0) aus der Vorlaufschleife ein.
54. Trenne den Nummerblock, lege diesen auf dem Arbeitsbereich ab und erhöhe die Nummber auf ***1***.
55. Dann verbinde einen **" " + " "** Block aus **Mathe** (Math) mit ***RotPos*** als Variablenblock in erster und der Nummerblock **1** als zweite Position.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_14.png" width="960"/><br>

56. Erzeuge eine Kopie von **Setze RotPos zu RotPos + 1** (set RotPos to Rotpos + 1) und ziehe sie in die **sonst** Spalte und ändere das Symbol **+** zu ***-***.
57. Anschliessend positioniere einen **Setze Element zu** (set item to) Block unterhalb der **wenn RotDir = Wahr** (if RotDir = true) Funktion.
58. Verbinde einen **Wahr** Block aus **Logik** (Logic) und ändere den Wert zu ***Falsch***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_15.png" width="960"/><br>

  **Addiere nun zwei weitere "wenn Tue" Funktionen um die eigentliche Motorbewegung auszuführen.**

59. Positioniere einen neuen **wenn Tue** (if do) Block aus **Logik** (Logic) in die unterste Reihe der Endlosschleife.
60. Ziehe einen **" " = " "** Block aus **Logik** (Logic) und verbinde diesen mit **wenn** (if).
61. Die zweite Position wird ein Elementblock mit Variablenname ***RotPos***.
62. Die erste Position benötigt einen **" " + " "** Block mit einem Nummerblock in zweiter Position beides aus **Mathe** (Math).
63. Ändere die Nummer zu ***1*** und setze einen Elementblock mit Variablennamen ***PrevPos***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_16.png" width="960"/><br>

64. Ziehe einen **Bewege Motor MeinStepper 10 Schritte** (move stepper MyStepper 10 steps) in die **Tue** (do) Spalte.
65. Ändere den Wert des Nummerblock zu ***50***.
66. Jetzt erzeuge eine komplette Kopie der **wenn PrevPos + 1 = RotPos** (if PrevPos + 1 = RotPos) und platziere diese direkt darunter.
67. Ändere das **+** Symbol zu ***-*** und den Wert des Nummerblocks von **50** zu ***-50***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_17.png" width="960"/><br>

68. Gib dem Projekt einen Namen und speichere es ab.
69. Kompiliere den Code und lade es auf den Arduino hoch.

#### *Drehe den Rotationsdekodierer in beide Richtungen, dann drücke den Drehregler.*

***Du kannst die Schritte ändern, um die Dauer der Bewegung des Schrittmotors zu beeinflussen.***

---

#### [Zurück zum Index](#index)

### 3.29. Zu guter Letzt <a name="last-but-not-least"></a>

In dem letzten Beispiel wirst du den Schrittmotor mit der Fernbedienung bedienen.

<img src="images/Lesson29/Lesson29_StepperMIRRemote_1.png" width="960"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Kabel** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Steckplatte** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Breadboard Stromversorgungsmodul** | <img src="images/Parts/Part_BreadBoardPowerSupply.jpg" width="256"/> | 1 | **ULN2003 Schrittmotortreibermodul** |<img src="images/Parts/Part_ULN2003_StepperModule.jpg" width="256"/>|
| 1 | **Schrittmotor**  |<img src="images/Parts/Part_StepperMotor.jpg" width="256"/>| 1 | **IR-Empfängermodul** |<img src="images/Parts/Part_IRModule.jpg" width="256"/> |
| 1 | **IR-Fernbedienung** | <img src="images/Parts/Part_IRRemoteControl.jpg" width="256"/> | 6 | **F-M Kabel** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> |
| 4 | **M-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

***Keine***

#### Schaltplan

<img src="images/Lesson29/Lesson29_StepperMIRRemote_2.png" width="960"/><br>

#### Bastelanweisung

1. Schrittmotor: Verbinde Motorkabelstecker mit ULN2003 Buchse
2. ULN2003: Über Trennlinie des Mittelteils 8 Pins im oberen Mittelteil und 8 Pins im unteren Mittelteil mit Karbe auf Chip nach Links zeigend
3. M-M Kabel 1: Untere blaue Reihe (-) der Steckplatte >> Arduino GND
4. W-M Kabel 1: ULN2003 Pin In1 >> Arduino Pin ~11
5. W-M Kabel 2: ULN2003 Pin In2 >> Arduino Pin ~10
6. W-M Kabel 3: ULN2003 Pin In3 >> Arduino Pin ~9
7. W-M Kabel 4: ULN2003 Pin In4 >> Arduino Pin 8
8. W-M Kabel 5: ULN2003 Pin **-** >> Untere blaue Reihe (-) der Steckplatte
9. W-M Kabel 6: ULN2003 Pin **+** >> Untere rote Reihe (+) der Steckplatte
10. W-M Kabel 7: IR Modul Pin GND >> Untere blaue Reihe (-) der Steckplatte
11. W-M Kabel 8: IR Modul Pin 5V >> Untere rote Reihe (+) der Steckplatte
12. W-M Kabel 9: IR Modul Pin DATA >> Arduino Pin 12

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

<img src="images/Lesson29/Lesson29_StepperMIRRemote.png" width="960"/><br>

  **Du wirst die Taste 13, 14 und 15 programmieren um den Schrittmotor zu bedienen.**

  <img src="images/Lesson29/Part_IRRemoteControl.jpg" width="512"/><br>

1. Nachdem du die Basisfunktion auf den Arbeitsbereich abgelegt hast.
2. Ziehe einen **Setup Steppermotor MeinStepper Steppermotor** (Setup stepper motor MyStepper stepper motor) in die Vorlaufschleife.
3. Stelle die Anzahl der Pins auf ***4***, die Pinnummern auf ***8***, ***10***, ***9*** und ***11***.
4. Die Schritte pro Umdrehung ***900*** und die Geschwindigkeit zu ***32***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_3.png" width="960"/><br>

5. Platziere einen **Setze Element zu** (set item to) Block mit Variablenname ***RotPos*** verbunden mit einem Zahlenblock **0**.
6. Ziehe einen **Setup IR Fernbedienung mit Pin# 0** (Setup IR Remote Control with Pin# 0) Block von **Eingang/Ausgang** (Input/Output) in die Vorlaufschleife.
7. Ändere die Pinnummer zu ***12***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_4.png" width="960"/><br>

  **Der erste Teil der Endlosschleife wird, wie beim Beispiel davor, den Motor in die Ausgangsposition bringen. Diesmal jedoch mit dem Tastendruck der Fernbedienung.**

8. Ziehe einen **wenn Tue** (if do) Funktionsblock aus **Logik** (Logic) in die Endlosschleife und verbinde einen **" " = " "** Block und verbinde diesen mit **wenn** (if).
9. Die erste Position wird ein **Lese Wert von IR Remote Control Pin# 0** (Read Value from IR Remote Control on Pin# 0) Block aus **Eingang/Ausgang** (Input/Output) mit Pinnummer ***12***.
10. Die zweite Position wird ein Zahlenblock mit dem Wert ***14***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_5.png" width="960"/><br>

11. Setze eine weitere Funktion **wenn Tue** (if do) von **Logik** (Logic) in die **Tue** (do) Spalte.
12. Nutze das Zahnradsymbol, um den **sonst** Teil hinzuzufügen.
13. Verbinde einen **" " = " "** Block zu **wenn** (if) und benutze eine Elementblock mit Variablenname ***RotPos*** in erster Position.
14. Für die zweite Position nutze einen Zahlenblock **0**.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_6.png" width="960"/><br>

  **Auch hier bleibt die "Tue" Spalte leer. Fahre mit der "sonst" Spalte fort.**

15. Ziehe einen **Bewege Motor MeinStepper 10 Schritte** (move stepper MyStepper 10 steps) Block aus **Motor** (Motors) in die **sonst** Spalte.
16. Trenne den Nummberblock **10** und lege diesen auf dem Arbeitsbereich ab.
17. Setze anstelle dessen einen **" " + " "** Block aus **Mathe** (Math) ein und ändere das Symbol von **+** zu ***x***.
18. Die zweite Position ist ein Nummerblock mit ***-1*** als Wert.
19. Die erste Position benötigt einen weiteren **" " + " "** Block mit dem **+** Symbol geändert zu ***x***.
20. Ziehe für die zweite Position den Nummerblock **10** vom Arbeitsbereich mit erhöhten Wert ***100*** hinein.
21. Als erste Position von **" " x 100** nutze ein Elementblock mit dem Variablennamen ***RotPos***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_7.png" width="960"/><br>

22. Erzeuge eine Duplikat des **Setze RotPos zu 0** (set RotPos to 0) aus der Vorlaufschleife und positioniere die Kopie in die **sonst** (else) Spalte der **wenn RotPos = 0** (if RotPos = 0) Funktion.
23. Nun ziehe einen **Setze Element zu** (set item to) verbunden einem **Element** Block in die Endlosschleife.
24. Ändere das erste Element zu ***PrevPos*** and das zweite Element ***RotPos***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_8.png" width="960"/><br>

  **Jetzt wirst du die beiden anderen Tasten programmieren.**

25. Ziehe einen **wenn Tue** Funktionsblock aus **Logik** (Logic) in die Endlosschleife und benutze das Zahnradsymbol um einen **anderenfalls** Block hinzuzufügen.
26. Verbinde ein **" " = " "** Block mit **wenn** (if) und benutze **Lese Wert von IR Fernbedienung Pin# 0** (Read Value from IR Remote Control on Pin# 0) in erster Position.
27. Wähle die Pinnummer ***12*** und setze einen Zahlenblock mit Wert ***15*** als zweite Position von **wenn Lese Wert von IR Fernbedienung Pin# 12 = " "** (if Read Value from IR Remote Control on Pin# 12 = " ").

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_9.png" width="960"/><br>

28. Erzeuge eine Kopie von **wenn Lese Wert von IR Fernbedienung Pin# 12 = 15** (if Read Value from IR Remote Control on Pin# 12 = 15) und verbinde das Duplikat mit **anderenfalls** darunter.
29. Ändere den Wert **15** zu ***13***.
30. Ziehe einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) in die **Tue** (do) Spalte von **wenn Lese Wert von IR Fernbedienung Pin# 12 = 15** (if Read Value from IR Remote Control on Pin# 12 = 15).
31. Gib dem neuen Block den Variablenname ***RotPos*** und verbinde den Block mit einem **" " + " "** Block aus **Mathe** (Math).
32. Die erste Position wird ein Elementblock mit Variablennamen ***RotPos***.
33. Die zweite Position wird ein Zahlenblock mit dem Wert ***1***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_10.png" width="960"/><br>

34. Erzeuge eine Kopie von **Setze RotPos zu Rotpos + 1** (set RotPos to RotPos + 1) und setze das Duplikat in die **Tue** (do) Spalte von **anderenfalls Lese Wert von IR Fernbedienung Pin# 12 = 13** (else if Read Value from IR Remote Control on Pin# 12 = 13).
35. Ändere das **+** Symbol zu ***-***.
36. Ziehe eine weitere **wenn Tue** (if do) Funktion in die Endloschleife.
37. Verbinde einen **" " = " "** Block aus **Logik** (Logic) mit **wenn** (if) des neuen Funktionsblock.
38. Ändere das **=** Symbol zu **<**.
39. Die erste und zweite Position werden zu Variablenblock **Element** mit den Variablennamen 1. ***PrevPos*** und 2. ***RotPos***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_11.png" width="960"/><br>

40. Nehme einen **Bewege Motor MeinStepper 10 Schritte** (move stepper  MyStepper 10 steps) Block von **Motor** (Motors) und setze diesen in die **Tue** Spalte von **wenn PrevPos < RotPos** (if PrevPos < RotPos).
41. Ziehe den Zahlenblock **10** aus **Bewege Motor MeinStepper 10 Schritte** (move stepper  MyStepper 10 steps) auf den Arbeitsbereich und ersetze ihn mit einem **" " + " "** Block aus **Mathe** (Math).
42. Ändere das **+** Symbol zu ***x*** und ziehe den **10** Zahlenblock zurück in die zweite Position.
43. Erhöhe den Wert **10** zu ***100***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_12.png" width="960"/><br>

44. Die erste Position von **Bewege Motor MeinStepper " " x 100 Schritte** (move stepper  MyStepper " " x 100 steps) wird ein **" " + " "** Block aus **Mathe** (Math).
45. Beide Position werden zu Variablenblöcke **Element** mit den Variablennamen 1. ***RotPos*** und 2. ***PrevPos***.
46. Füge einen **Warte 1000 Millisekunden** (wait 1000 milliseconds) in die **Tue** (do) Spalte von **wenn PrevPos < RotPos** (if PrevPos < RotPos) und ändere den Zeitwert zu ***100***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_13.png" width="960"/><br>

47. Erzeuge eine komplette Kopie der **wenn PrevPos < RotPos** (if PrevPos < RotPos) Funktion und positioniere sie direkt unter dem Original.
48. Wähle anstelle des **<** Symbols ***>***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_14.png" width="960"/><br>

49. Speichere dein Projekt, nachdem du es bennant hast.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_15.png" width="960"/><br>

50. Kompiliere es und lade es auf deinen Arduino.

***Verbinde die beiden unteren "wenn Tue" Funktionen unter Nutzung von " " oder " "***.

***Erweitere die Steuerung mit mehr Tasten und unterschiedlicher Dauer der Drehbewegung.***

---

#### [Zurück zum Index](#index)

## 4. Roboterprojekt "Ardubot"<a name="robotproject"></a>

Dieses Projekt ist eine Beispielprojekt in welchem das vorherig erlangte Wissen eine komplexere praktische Anwendung findet.

Wie alle vorhergehenden Beispiele ist auch dieses Projekt dafür da dich an das Basiswissen heranzubringen. Wie du mit Sicherheit schon bemerkt hast, öffnet jedes Beispiel ein Türchen zu eigenen Projekten.

Ob du nun Beispiele miteinander mischst oder komplett neue Projekte aus deinen eigenen Ideen entwickelst, du wirst lernen, dass du beim Programmieren des Arduino verschiedene Wege nehmen kannst, um an das selbe Ziel zu gelangen.

Der kleine Roboter, den du nun bauen und programmieren wirst ist lediglich ein weiteres Beispiel. Der Unterschied ist jedoch, das dieser Roboter ein fantastische Basis bietet, deine eigenen Code auszuprobieren. Du wirst es direkt durch das Verhalten des Roboters erleben können.

<img src="images/RobotBuild/RobotBuild.png" width="600"/>

----

### Robotprojekt BOM (Bill of Materials) oder Materialliste

Die folgende List zeigt dir, welche Teile aus deinem Elegoo Bausatz **The Most Complete Starter Kit** für das *ArduBot* Projekt verfügbar sind:

| *Teil vom Elegoo Bausatz* | *Anzahl* | *Bild* | *Teil vom Elegoo Bausatz* | *Anzahl* | *Bild* |
|---------------------------|----------|--------|-----------------------------------|----------|--------|
| **Arduino Board Uno R3** | 1 | <img src="images/Parts/Part_ArduinoUnoR3.jpg" width="128"/> | **Arduino Prototyp Erweiterungsmodul** | 1 | <img src="images/Parts/Part_PrototypeExpansionModule.jpg" width="128"/> |
| **Servomotor** | 1 | <img src="images/Parts/Part_ServoMotor.jpg" width="128"/> | **Ultraschall Modul** | 1 | <img src="images/Parts/Part_UltraSonicSensor.jpg" width="128"/> |
| **Druckknopf** | 1 | <img src="images/Parts/Part_PushButton_4Pin.jpg" width="128"/> | **Passiver Buzzer** | 1 | <img src="images/Parts/Part_PassiveBuzzer.jpg" width="128"/> |
| **9 Volt Batterie** | 1 | <img src="images/Parts/Part_9VBattery.jpg" width="128"/> | **Breadboard Verbindungskabel** | 18 | <img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="128"/> |
| **Verbindungskabel Dupont MxM** | 10 | <img src="images/Parts/Part_Cable_Dupont_MM.jpg" width="128"/> |  | | |


Um den Roboter bauen zu können, bedarf es zusätzlich Teile, welche ich unter dem Namen **Ardubot Bausatz** aufführe. Diesen Bausatz kannst du selbst zusammensetzen (siehe Anleitung unten), oder bei mir bestellen.


| *Teil vom Ardubot Bausatz* | *Anzahl* | *Bild* | *Teil vom Ardubot Bausatz* | *Anzahl* | *Bild* |
|--------------------------|-------|------|-------------------------|-------|-----|
| **Fahrgestellteile** | 1     | <img src="images/RobotBuild/RobotBuild_CNC_2.jpg" width="128"/> | **DC Motor mit Rad** | 2 | <img src="images/Parts/Part_DCMotorWheel.jpg" width="128"/> |
| **9V Batteriebox mit Schalter und Stecker** | 1 | <img src="images/Parts/Part_9VBatterycase4ArduinoDCPlug.jpg" width="128"/> | **InfraRot Sensor Modul** | 2 | <img src="images/Parts/Part_IRSensorModule.jpg" width="128"/> |
| **Gummiband** | 1 | <img src="images/Parts/Part_Rubberband.jpg" width="128"/> | **Rollerrad** | 1 | <img src="images/Parts/Part_RollerWheel.jpg" width="128"/> |
| **Arudino Schraube M3** | 4 | <img src="images/Parts/Part_ArduinoScrew_M3_5m.jpg" width="128"/>  | **PCB Mount Hexagonal 6 x 6 mm** | 8 | <img src="images/Parts/Part_PCBMount_6mm.jpg" width="128"/> |
| **PCB Mount Hexagonal 30 x 6 mm** | 4 | <img src="images/Parts/Part_PCBMount_30mm.jpg" width="128"/> | **Rollerradschraube M3 5mm** | 4 | <img src="images/Parts/Part_Screw_M3_5mm.jpg" width="128"/> |
| **DC Motor Schraube M3 30mm** | 4 | <img src="images/Parts/Part_DCScrew_M3_30mm.jpg" width="128"/> | **PCB Mount Schraube M3 8mm** | 4 | <img src="images/Parts/Part_PCBMount_Screw_M3_8mm.jpg" width="128"/> |
| **Mutter M3** | 16 | <img src="images/Parts/Part_Nut_M3.jpg" width="128"/> |


Als Alternative zum Ardubot Bausatz kann das 2WD Smart Car Chassis Set genutzt werden. Hier die notwendigen Teile:

| *Teil* | *Anzahl* | *Bild* | *Teil* | *Anzahl* | *Bild* |
|------|-------|------| -----|-------|------|
| **Smart Car Fahrgestell mit 2 Motoren** | 1 | <img src="images/Parts/Part_2WDSmartCarChassis.png" width="128"/> | **Verbindungskabel Dupont FxF** | 2 | <img src="images/Parts/Part_Cable_Dupont_MM.jpg" width="128"/> |
| **Rubberband** | 1 | <img src="images/Parts/Part_Rubberband.jpg" width="128"/> | **Arduino Schraube M3 5 mm** | 4 | <img src="images/Parts/Part_ArduinoScrew_M3_5m.jpg" width="128"/> |
| **9V Batteriebox mit Schalter und Stecker** | 1 | <img src="images/Parts/Part_9VBatterycase4ArduinoDCPlug.jpg" width="128"/> | **InfraRot Sensor Modul** | 2 | <img src="images/Parts/Part_IRSensorModule.jpg" width="128"/>  |

---

#### [Zurück zum Index](#index)

### 4.1. Vorbereitung <a name="rp-preparation"></a>

Ok, als erstes musst du je nach Set Vorbereitungen treffen.

#### *Ardubot Bausatz - Nur wenn du es selbst erstellen möchtest*

Falls du über eine CNC Maschine (auch mit 3-D Drucker möglich), kannst du die Basisplatten selbst erstellen. Du kannst im Falle eines 3D Druckers die STL Dateien nutzen.

Die folgende Darstellung veranschaulicht die Nutzung des GCodes mit einer CNC Fräse.

| 1. Lade die Daten für die Ardubot Elemente herunter. | **2. Der GCode ist für eine MDF-platte 3 mm x 200 mm x 300 mm gedacht.** |
|-----------------------------|------------------------------|
| ***[D.I.Y CNC Designs](/utilities/ArduBot_CNCParts.zip)*** | <img src="images/Parts/Part_MDFBoard_20x30cm.jpg" width="512"/> |
| **3. Nachdem die Teile gefrässt sind, löse, säubere und begradige die Einzelelemente.** | **4. Bemale die Elemente mit einer oder mehreren Farben nach Belieben.** |
| <img src="images/RobotBuild/RobotBuild_CNC_1.jpg" width="600"/> | <img src="images/RobotBuild/RobotBuild_CNC_2.jpg" width="600"/> |

---

#### *2WD Smart Car Chassis*

Wenn du diesen Bausatz gekauft hast wirst du Motoren ohne angebrachte Kabel erhalten. Die Kabel müssen daher an die Motoren gelötet werden.

| 1. Halbiere die beiden Verbindungskabel FxF. | 2. Löte die Kabel mit eine Farbe für "+" und eine Farbe für "-".|
|---------------------------------------------|-----------------------|
| <img src="images/RobotBuild/RobotBuild_Part1_1.jpg" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_2.jpg" width="600"/> |

---

#### [Zurück zum Index](#index)

### 4.2. Zusammenbau Part 1<a name="rp-assemblyP1"></a>

Nachdem alle Vorbereitungen getroffen sind, kannst du den ersten Teil des Roboters zusammenbauen. Dies ist das Fahrgestell selbst und alle Bestandteile, welche direkt mit dem Fahrgestell verbunden sind.

#### *Ardubot Bausatz*

| 1. Schiebe ein T-Teil mit 2 Löchern in den Schlitz und halte das zweite T-Teil in der Nutte im die untere Platte. | 2. Setze einen Motor mit Rad dazwischen ein und verschraube diesen mit beiden T-Teilen (2 x DC Motor Schraube M3 30mm + 2 x Mutter M3)|
|-------------------------------|--------------------------------|
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_1.png" width="512"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_2.png" width="600"/> |
| **3. Schiebe ein T-Teil mit 2 Löchern in den Schlitz und halte das zweite T-Teil in der Nute auf der anderen Seite.** | **4. Setze den zweiten Motor mit Rad dazwischen ein und verschraube diesen mit beiden T-Teilen (2 x DC Motor Schraube M3 30mm + 2 x Mutter M3).** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_3.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_4.png" width="600"/>|
| **5. Verschraube nun das Rollerrad mit 4 x PCB Mount Hexagonal 6 x 6 mm und 4 x Rollerradschraube M3 5mm.** | **6. Füge die 2 InfraRot-Sensormodule in die vorderen breiten Schlitze und klemme diese vorsichtig mit den T-Teilen ein.** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_5.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_6.png" width="600"/>|
| **7. Nehme nun die obere runde Platte und schiebe die beiden T-Teile mit Nute in die vorderen Schlitze mit der Nute nach hinten zeigend.** | **8. Setze den Servomotor mit Kabel nach von zeigend ein und schiebe das U-Teil vorsichtig in die Nuten.** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_7.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_8.png" width="600"/>|
| **9. Verschraube das U-Teil mit dem Servomotor** | **10. Schiebe beide Smiley-Teile und schiebe sie auf das breite T-Teil mit einem Loch.** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_9.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_10.png" width="600"/>|
| **11. Setze das Ultraschall-Modul mit Pins nach oben in die Löcher der Smiley-Teile. Setze vorsichtig das gesamte Teil auf die Welle des Servomotors** | **12. Schraube nun den Arduino mit 4 x Arudino Schrauben M3, 4 x PCB Mounts Hexagonal 6 x 6 mm und 4 x Muttern M3 auf die obere Platte. Dann nutze den Gummi um Batteriebox (inklusive der 9V Batterie) mit der oberen Platte zu verbinden.** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_11.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_12.png" width="600"/> |
| **13. Schraube nun mit 4 x PCB Mount Schrauben M3 8mm, 4 x PCB Mount Hexagonal 30 x 6 mm und 4 x Muttern M3 die obere Plattform mit der unteren zusammen.** | **14. Führe die Kabel durch die hinteren Löcher der oberen Platform und verbinde das Batterieboxkabel mit dem Arduino.** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_13.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_CNC_3.jpg" width="600"/> |


#### *2WD Smart Car Chassis*


| 1. Schiebe ein T-Teil mit 2 Löchern in den Schlitz und halte das zweite T-Teil in der Nutte im die untere Platte. | 2. Setze einen Motor mit Rad dazwischen ein und verschraube diesen mit beiden T-Teilen (2 x DC Motor Schraube M3 30mm + 2 x Mutter M3) |
|--------------------------|-------------------------------------|
| <img src="images/RobotBuild/RobotBuild_Part1_3.jpg" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_4.jpg" width="600"/> |
| **3. Wiederhole dies mit dem zweiten Motor, dann stecke die Räder auf die Motorachsen.** | **4. Verschraube nun die Rollerräder mit 8 x PCB Mount Hexagonal 6 x 6 mm und 8 x Rollerradschraube M3 5mm.**    |
| <img src="images/RobotBuild/RobotBuild_Part1_5.jpg" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_6.jpg" width="600"/> |
| **5. Führe die Motorkabel durch die hinteren Schlitze der oberen Plattform.**   | **6. Schraube nun den Arduino mit 3 x Arudino Schrauben M3 und 3 x Muttern M3 auf die obere Platte**     |
| <img src="images/RobotBuild/RobotBuild_Part1_7.png" width="600"/>     | <img src="images/RobotBuild/RobotBuild_Part1_8.jpg" width="600"/> |
| **7. Nutze den Gummi um Batteriebox (inklusive der 9V Batterie) mit der oberen Platte zu verbinden.**                       | **8. Verbinde das Batterieboxkabel mit dem Arduino.** |
| <img src="images/RobotBuild/RobotBuild_Part1_9.jpg" width="600"/> |   <img src="images/RobotBuild/RobotBuild_Part1_10.jpg" width="600"/> |

---

#### [Zurück zum Index](#index)

### 4.3. Zusammenbau Part 2<a name="rp-assemblyP2"></a>

In diesem Teil wirst dem kleinen Roboter die ersten Fahrstunden erteilen. Du wirst den Arduino so programmieren, dass der Roboter einige Sekunden in einer Richtung fährt, anhält, sich 180 Grad dreht, zurückfährt, sich am Ausgangspunkt wieder 180 Grad dreht und dann die Schleife erneut durchläuft.

<img src="images/RobotBuild/RobotBuild_Part2_DCMotors_1.jpg" width="512"/><br>

#### Was für neue Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Prototyp Erweiterungsmodul (APEM)** |<img src="images/Parts/Part_PrototypeExpansionModule.jpg" width="256"/> | 2 | **DC Motor mit Rad**  | <img src="images/Parts/Part_DCMotorWheel.jpg" width="256"/>   |
| 1 | **Druckknopf** | <img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/> | 1 | **Motorkontroller L293D** |<img src="images/Parts/Part_MotorcontrollerL293D.jpg" width="256"/>|
| 18 | **M-M Kabel** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Schaltplan

<img src="images/RobotBuild/RobotBuild_Part2_DCMotors_2.jpg" width="960"/><br>

**Chip Pin Verteilung:**

<img src="images/RobotBuild/RobotBuild_Part2_DCMotors_2_Chip.jpg" width="512"/><br>

#### Bastelanweisung

1. Arduino Prototyp Erweiterungsmodul: Stecke das Modul auf den Arduino
2. Motorkontroller L293D: Stecke die Chip in die vordersten Löcher über den Mittelteil des Erweiterungsmodul mit der Einkerbung nach hinten zeigend
3. Druckknopf: Stecke diesen in die hinteren Löcher über den Mittelteil des APEM
4. M-M Kabel 1: Oberer Teil Spalte 1 Reihe 4 >> APEM GND Erweiterungsmodul
5. M-M Kabel 2: Oberer Teil Spalte 3 Reihe 4 >> APEM Pin 11
6. M-M Kabel 3: Oberer Teil Spalte 10 Reihe 3 >> Unterer Teil Spalte 10 Reihe 3
7. M-M Kabel 4: Oberer Teil Spalte 10 Reihe 2 >> Oberer Teil Spalte 17 Reihe 2
8. M-M Kabel 5: Oberer Teil Spalte 13 Reihe 3 >> Oberer Teil Spalte 14 Reihe 3
9. M-M Kabel 6: Unterer Teil Spalte 13 Reihe 3 >> Unterer Teil Spalte 14 Reihe 3
10. M-M Kabel 7: Oberer Teil Spalte 14 Reihe 4 >> Unterer Teil Spalte 14 Reihe 2
11. M-M Kabel 8: Oberer Teil Spalte 11 Reihe 3 >> APEM Pin 5
12. M-M Kabel 9: Oberer Teil Spalte 16 Reihe 3 >> APEM Pin 6
13. M-M Kabel 10: Unterer Teil Spalte 10 Reihe 4 >> APEM 5V
14. M-M Kabel 11: Unterer Teil Spalte 11 Reihe 3 >> APEM Pin 9
15. M-M Kabel 12: Unterer Teil Spalte 13 Reihe 5 >> APEM GND
16. M-M Kabel 13: Unterer Teil Spalte 16 Reihe 3 >> APEM Pin 10
17. M-M Kabel 14: Unterer Teil Spalte 17 Reihe 3 >> APEM 5V
18. M-M Kabel 15: DC Motor Links Pol 1 >> Oberer Teil Spalte 12 Reihe 3
19. M-M Kabel 16: DC Motor Links Pol 2 >> Oberer Teil Spalte 15 Reihe 3
20. M-M Kabel 17: DC Motor Rechts Pol 1 >> Unterer Teil Spalte 15 Reihe 3
21. M-M Kabel 18: DC Motor Rechts Pol 2 >> Oberer Teil Spalte 12 Reihe 3


**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**


#### Block für Block

<img src="images/RobotBuild/RobotBuild_Part2_DCMotors.jpg" width="960"/><br>

  **Lass uns mit den Motorpins beginnen. 2 Pins für jede Richtung pro Motor.**

1. Nachdem du die Basisfunktion hereingezogen hast, ziehe 4 **Setze digitale Pin# 0 zu HIGH** (Set digital pin# 0 to HIGH) Blöcke aus **Eingang/Ausgang** (Input/Output) in die Vorlaufschleife.
2. Wähle die Pinnummern ***5***, ***6***, ***9*** und ***10****.
3. Dann ändere alle **HIGH** Werte zu ***LOW***.

  **Alle Motoren und Richtungen sind ausgeschalten.**

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_3.jpg" width="960"/><br>

  **Nun werden wird den Druckknopf einsetzen, welcher die Funktion Start und Stop übernimmt.**

4. Ziehe einen **Setze Element zu** (Set item to) Block aus **Variable** (Variables) in die Endlosschleife.
5. Benenne **Element** zu ***StartBtn***.
6. Verbinde **Setze StartBtn zu** (Set StartBtn to) mit einem **Lese digital mit PULL_UP Modus Pin# 0** (Read digital with PULL_UP mode Pin# 0) Block aus **Eingang/Ausgang** (Input/Output).
7. Ändere die Pinnummer zu ***11***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_4.jpg" width="960"/><br>

  **Für jede Bewegung Vorwärts, Rückwärts, Links und Rechts werden wir nun eine Blockfunktion erstellen.**

8. Hole dir einen **zu Mache etwas** (to do something) Block aus **Funktion** (Functions) auf deinen Arbeitsbereich.
9. Nenne die Funktion ***Forward*** (Vorwärts).

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_5.jpg" width="960"/><br>

10. Dupliziere alle 4 **Setze digitale Pin#...** (Set digital pin#...) Blöcke aus der Vorlaufschleife und setze sie in die **zu Forward** (to Forward) Funktion.
11. Ändere den Wert von Pinnummer **5** und **10** zu ***HIGH***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_6.jpg" width="960"/><br>

12. Erzeuge 4 Duplikate der kompletten **zu Forward** (to Forward) Funktion und positioniere dies untereinander auf deinen Arbeitsbereich.
13. Nenne die erste Kopie ***Backward*** (Rückwärts) und setze die Werte von Pin# 5 zu ***LOW***, Pin# 6 zu ***HIGH***, Pin# 9 zu ***HIGH*** und Pin# 10 zu ***LOW***.
14. Nenne die erste Kopie ***Stop*** und setze die Werte von Pin# 5 zu ***LOW***, Pin# 6 zu ***LOW***, Pin# 9 zu ***LOW*** und Pin# 10 zu ***LOW***.
15. Nenne die erste Kopie ***Right*** (Rechts) und setze die Werte von Pin# 5 zu ***HIGH***, Pin# 6 zu ***LOW***, Pin# 9 zu ***HIGH*** und Pin# 10 zu ***LOW***.
16. Nenne die erste Kopie ***Left*** (Links) und setze die Werte von Pin# 5 zu ***LOW***, Pin# 6 zu ***HIGH***, Pin# 9 zu ***LOW*** und Pin# 10 zu ***HIGH***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_7.jpg" width="960"/><br>

  **Nachdem du den Startknopf gedrückt hast, soll der Roboter ein gewisses Fahrprotokoll ablaufen und dann warten, bis du wieder den Knopf drückst, um den Prozess zu wiederholen.**

17. Nutze dazu einen Schleifenblock **Wiederhole während Tue** (repeat while do) aus **Schleife** (Loops), welchen du in die Endlosschleife ziehst.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_8.jpg" width="960"/><br>

18. Verbinde den Schleifenblock mit **" " = " "** aus **Logik** (Logic).

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_9.jpg" width="960"/><br>

19. Als erste Position setze einen **Element** (item) Variablenblock mit Variablennamen ***StartBtn*** ein.
20. Die zweite Position von **StartBtn = " "** wird ein **HIGH** Block aus **Eingang/Ausgang** (Input/Output).
21. Ändere den Wert zu ***LOW***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_10.jpg" width="960"/><br>

  **Lass uns nun das Fahrprotokoll integrieren.**

22. Ziehe aus **Funktion** (Functions) einen **Forward** Block in die **Wiederhole während StartBtn = LOW** (repeat while StartBtn = LOW) Schleife.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_11.jpg" width="960"/><br>

  **Nun die Fahrtdauer für vorwärts.**

23. Unterhalb **Forward** kommt ein **Warte 1000 Millisekunden** (wait 1000 Millisekunden) Block aus **Zeit** (Time) und ändere die Zeit zu ***2000***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_12.jpg" width="960"/><br>

24. Wiederhole Schritte 22. und 23. mit folgenden Funktionen und Zeitwerten:

  **Stop**, ***500***, **Right**, ***700***, **Stop**, ***500***, **Forward**, ***2000***, **Stop**, ***500***, **Left**, ***1400***, **Stop**, ***500***, **Backward**, ***2000***, **Stop**, ***500***, **Right**, ***700***, **Stop**, ***500***, **Backward**, ***2000***, **Stop**, ***500***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_13.jpg" width="960"/><br>

  **Nach Ablauf den Fahrtprotokolls willst du die Schleife beenden, damit die Schleife erst dann wieder abläuft, nachdem du den Startknopf erneut gedrückt hast.**

25. Der letzte Block in der **Wiederhole während StartBtn = LOW** (repeat while StartBtn = LOW) Schleife wird ein **Beende Schleife** (break out of loop) Block aus **Schleife** (Loops).

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_14.jpg" width="960"/><br>

26. Benenne dein Projekt und speichere es ab.

  ***Verbinde deinen ArduBot Arduino via USB zu deinen Tablet/PC.***

27. Verifiziere und lade es auf deinen Arduino.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_15.jpg" width="960"/><br>

  ***Entferne das USB Kabel. Versichere dich, das die Batteriebox auf "An" gestellt ist, sodass dein Arduino Energie bekommt.***

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_16.jpg" width="960"/><br>

***Setze den Roboter auf den Boden und positioniere ihn oder sie in die Richtung in der er/sie sich bewegen soll. Dann drücke den Druckknopf.***

**Falls sich dein ArduBot zu viel oder zu wenig nach links oder rechts dreht, schalte den Strom aus, verbinde ihn/sie zu deinem Computer und ändere den Zeitwert "700" der Linksbewegung, Rechtsbewegung oder Beides.**

***Lade den Code erneut auf deinen Arduino und teste die Bewegung. Wiederhole die Prozedur bis du mit der Drehung zufrieden bist.***

#### *Schreibe deine eigenen Fahrtprotokolle!*


---

#### [Zurück zum Index](#index)

### 4.4. Zusammenbau Part 3<a name="rp-assemblyP3"></a>

Nun wird der Roboter lernen auf dem Boden nach dunklen Streifen zu suchen. Das linke und rechte Infrarot-Sensormodul wird dann den Roboter helfen, diesem Pfad zu folgen.

> Ist es Schicksal oder vorgezeichnet? Ich folge dem Pfad, manchmal breche is aus, dann folge ich auch wieder meinen Pfad wiederfinde!


<img src="images/RobotBuild/RobotBuild_Part3_IRSensors_1.jpg" width="960"/><br>

#### Hinweis:

* ***Im Falle des ArduBot sind die Infrarot-Modul bereits eingesetzt.***
* ***Benutze anderenfalls 4 Drähte um die Löcher in den Modulen und die kleinen Schlitze im vorderen Teil der unteren Plattform zu verbinden (siehe Foto oben).***
* ***Klebe schwarzes Klebeband in Form eines T auf weisses Papier. Dies wirst du nutzen, um die InfraRot-Sensoren zu kalibrieren (siehe Foto unten).***
* ***Finde oder bastel eine kleine Box oder Zylinder mit dem du den Roboter erhöhen kannst, sodass die Räder ca. 1 cm über den Boden schweben. Wir benötigen auch dies für die Kalibration der Sensoren.***

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_TCalib.jpg" width="512"/><br>

#### Was für neue Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 2 | **InfraRot Sensormodul** |<img src="images/Parts/Part_IRSensorModule.jpg" width="256"/> | 6 | **W-M Kabel**  |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/>   |

#### Schaltplan

<img src="images/RobotBuild/RobotBuild_Part3_IRSensors_2.jpg" width="960"/><br>

#### Bastelanweisung

1. W-M Kabel 1: Linker InfraRot-Sensor OUT >> APEM Pin A1
5. W-M Kabel 2: Linker InfraRot-Sensor GND >> APEM GND
6. W-M Kabel 3: Linker InfraRot-Sensor VCC >> APEM 5V
7. W-M Kabel 4: Rechter InfraRot-Sensor OUT >> APEM Pin A0
8. W-M Kabel 5: Rechter InfraRot-Sensor GND >> APEM GND
9. W-M Kabel 6: Rechter InfraRot-Sensor VCC >> APEM 5V

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**


#### Block für Block

<img src="images/RobotBuild/RobotBuild_Part3_IRSensors.jpg" width="960"/><br>

  **Beginne mit dem vorhergehenden Projekt.**

1. Lösche die komplette **Wiederhole während** Schleife.
2. Gib dem Projekt einen neuen Namen (Roboterbau_Teil_3 oder so).

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_3.jpg" width="960"/><br>

  **Da du diesmal keinen vorbereiteten Fahrkurs einprogrammierst, wirst du den Startknopf mit Hilfe von booleschen Operatoren nutzen.**

3. Ziehe einen **Setze Element zu** (set item to) Block von **Variable** (Variables) in die Vorlaufschleife.
4. Benenne die Variable **Element** (item) als ***On/Off*** (An/Aus).
5. Verbinde diesen Block nun mit einem **Wahr** (true) Block aus **Logik** (Logic) und ändere den Wert zu **Falsch** (false).

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_4.jpg" width="960"/><br>

  **Nun wirst du die Relation von Knopfdruck zur Kondition An/Aus erzeugen.**

6. Benutze einen **wenn Tue** (if do) Block aus **Logik** (Logic) in der Endloschleife.
7. Drücke das Zahnradsymbol und ziehe einen **anderenfalls** (else if) Block unter den **wenn** Block.
8. Verbinde einen **" " und " "** (" " and " ") Block aus **Logik** (Logic) mit **wenn**.
9. In die erste Position setze ein **" " = " "** Block aus **Logik** (Logic) ein.
10. Wiederum die erste Position ist ein **Element** (item) Block mit Variablennamen ***StartBtn***.
11. Für die zweite Position von **StartBtn = " "** benutze einen **HIGH** Block aus **Eingang/Ausgang** (Input/Output) und ändere den Wert zu ***LOW***.
12. Kopiere den Block **StartBtn = LOW** und setze das Duplikat in die zweite Position von **StartBtn = LOW und " "** ein.
13. Wähle dort den neuen Variablennamen ***OnOff***, lösche den **LOW** Block und ersetze diesen mit **Wahr** (true) aus **Logik** (Logic).
14. Ändere den Wert von **Wahr** (true) zu ***Falsch*** (false).
15. Erzeuge nun eine Kopie des Blocks **StartBtn = LOW und OnOff = Falsch** (StartBtn = LOW and OnOff = false) und verbinde diesen mit **anderenfalls** (else if).
16. Ändere nun den Wert **Falsch** (false) zu ***Wahr*** (true).
17. Dupliziere nun den Block **Setze OnOff zu Falsch** aus der Vorlaufschleife und setze jeweils eine Kopie in die **Tue** (do) Spalten.
18. Verändere den Wert **Falsch** (false) von **Setze OnOff zu Falsch** in der **Tue** Spalte von **wenn** (if) zu ***Wahr***.

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_5.jpg" width="960"/><br>

  **Als Nächstes wirst du die Daten der InfraRot-Sensor als Variablen festhalten.**

19. Setze 2 **Setze Element zu** (set item to) Blöcke aus **Variable** (Variables) in die ersten Reihen der Endlosschleife ein und nenne die Variablen ***LeftIR*** und ***RightIR***.
20. Dann verbinde beide Blöcke mit jeweils einem **Lese digital Pin# 0** (Read digital pin# 0) Block aus **Eingang/Ausgang** (Input/Output).
21. Gib **LeftIR** die Pinnummer ***A1*** und **RightIR** die Pinnummer ***A0***.

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_6.jpg" width="960"/><br>

  **Wenn ArduBot mit dem Druckknopf angestellt ist, soll er einer schwarzen Linie folgen.**

22. Ziehe einen Block **wenn Tue** (if do) Block aus **Logik** (Logic) oberhalb der existierenden **wenn Tue** (if do) Funktion in die Endlosschleife.
23. Verbinde zu **wenn** (if) eine Kopie von **OnOff = Wahr** (OnOff = true) aus der darunterliegenden **andernfalls** (else if) Verbindung.
24. In die **Tue** (do) Spalte von **wenn OnOff = Wahr** (if OnOf = true) ziehe ein weiteren **wenn Tue** (if do) Block aus **Logik** (Logic).
25. Drücke das Zahnradsymbol und addiere 3 **anderenfalls** (else if) Blöcke zu **wenn** (if).

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_7.jpg" width="960"/><br>

26. Verbinde nun eine Blockkombination **LeftIR = 0 und RightIR = 0** mit **wenn** (if) in der **Tue** (do) Spalte von **wenn OnOff = Wahr** (if OnOff = Wahr).
27. Verbinde die 3 **anderenfalls** Abschnitte mit **LeftIR = 1 und RightIR = 0**, **LeftIR = 0 und RightIR = 1** und **LeftIR = 1 und RightIR = 1**.
28. Fülle die **Tue** Spalten nacheinander mit jeweils einen der folgenden Funktionen aus **Funktion** (Functions): **Forward**, **Left**, **Right** und **Stop**.

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_8.jpg" width="960"/><br>

29. Speichere das Projekt und verifiziere es.
30. Verbinde das USB Kabel und lade es auf deinen Ardubot Arduino.

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_9.jpg" width="960"/><br>

***Setze den Roboter auf die Box oder Zylinder, welche/r auf dem weissen Papier mit dem T steht. Nachdem du den Strom angestellt hast und du den Druckknopf gedrückt hast, drehe langsam mit einem Schraubendreher am weissen Drehregler des jeweiligen Sensor bis die Räder sich gerade so in dieser Position drehen.***

**Wenn alles mit der Konfiguration geklappt hat, sollte dein ArduBot stoppen, sobald der obere Querstreifen des T Symbols erreicht ist.**

***Nimm dir nun ein grosses Blatt Papier und male darauf mit einem schwarzen Markerstift eine Bahn, der dein Roboter folgen soll. Du kannst auch schwarzes Klebeband nutzen.***


#### *Auf dem Teppich und mit der Linie bleiben, ArduBot!*

---

#### [Zurück zum Index](#index)

### 4.5. Zusammenbau Part 4<a name="rp-assemblyP4"></a>

In diesem Teil verbinden wird den Servomotor und den Ultraschallsensor mit dem Arduino. Der Programmteil wird vorerst nur den Ultraschallsensor nutzen. Dardurch wird der Roboter stehen bleiben, sobald ein Hindernis in Fahrtrichtung auftaucht.

> Kopf und Hals auf dem Rumpf zum Herz Trumpf. Jetzt kann ich sehen!

<img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_1.png" width="640"/><br>

#### Was für neue Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Ultraschallsensor** |<img src="images/Parts/Part_UltraSonicSensor.jpg" width="256"/> | 1 | **Servomotor**  |<img src="images/Parts/Part_ServoMotor.jpg" width="256"/>   |
| 4 | **W-M Kabel**  | <img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> | 5 | **M-M Kabel**  |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

***Hinweis: Im Falle des ArduBot ist der Servomotor und das Ultraschall-Modul bereits eingesetzt.***

#### Schaltplan

<img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_2.jpg" width="960"/><br>

#### Bastelanweisung

1. M-M Kabel 1: Servomotor OUT >> APEM Pin 8
2. M-M Kabel 2: Servomotor VCC >> Oberer Teil Spalte 5 Reihe 3
3. M-M Kabel 3: Servomotor GND >> Oberer Teil Spalte 6 Reihe 3
4. M-M Kabel 1: Ultraschall-Sensor GND >> Oberer Teil Spalte 6 Reihe 2
5. W-M Kabel 2: Ultraschall-Sensor ECHO >> APEM Pin 13
6. W-M Kabel 3: Ultraschall-Sensor TRIG >> APEM Pin 12
7. W-M Kabel 4: Ultraschall-Sensor VCC >> Oberer Teil Spalte 5 Reihe 2
8. M-M Kabel 4: Oberer Teil Spalte 5 Reihe 1 >> APEM 5V
9. M-M Kabel 5: Oberer Teil Spalte 6 Reihe 4 >> APEM GND

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**


#### Block für Block

<img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_3.jpg" width="960"/><br>

  **Du startest mit dem letzten Projekt.**

1. Importiere das letzte Projekt und gib es einen neuen Namen.

  **Initiiere den Ultraschallsensor.**

2. Ziehe einen **Setup HC-SR04 mit Echo Pin# 0 und Auslöser Pin# 0** (Setup HC-SR04 with Echo Pin# 0 and Trigger Pin# 0) aus **Sensor** (Sensors) in die oberste Zeile der Vorlaufschleife.
3. Ändere die Echo Pin# zu ***13*** und die Auslöser Pin# ***12***.

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_4.jpg" width="960"/><br>

4. In der ersten Reihe der Endlosschleife füge einen **Setze Element zu** (set item to) aus **Variable** (Variables) mit dem Variablennamen ***Distance*** ein.
5. Verbinde diesen Variablenblock mit **Lese HC-SR04 von Echo Pin# 0** (Read HC-SR04 from Echo Pin# 0) aus **Sensor** (Sensors).

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_5.jpg" width="960"/><br>

6. Ändere die Echo Pin# zu ***13***.

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_6.jpg" width="960"/><br>

  **Nun soll ArduBot in der Vorwärtsbewegung stoppen und rückwärts fahren, solang sich ein Hindernis innerhalb von 20 cm befindet.**

7. Ziehe einen **wenn Tue** (if do) Block aus **Logik** (Logic) unterhalb der **Forward** Funktion in der **Tue** (do) Spalte von **wenn LeftIR = 0 und RightIR = 0** (if LeftIR = 0 and RightIR = 0).
8. Benutze das Zahnradsymbol und füge den **sonst** (else) Abschnitt hinzu.
9. Dann kopiere die Blockkombination **LeftIR = 0 und RightIR = 0** (LeftIR = 0 and RightIR = 0) und verbinde das Duplikat mit **wenn** (do) unterhalb der **Forward** Funktion.
10. Ändere die Blockkombination zu ***Distance <= 20 und Distance > 0*** und ziehe einen **Stop** Block aus **Funktion** (Functions) in die **Tue** Spalte darunter.

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_7.jpg" width="960"/><br>

11. Nun ziehe den **Forward** Block oberhalb von **wenn Distance <= 20 und Distance > 0** (wenn Distance <= 20 and Distance > 0) in die **sonst** Spalte.

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_8.jpg" width="960"/><br>

12. Speichere das Projekt und verifiziere deinen Code.
13. Verbinde deinen ArduBot mit USB zum Computer und lade den Code hoch.

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_9.jpg" width="960"/><br>

  ***Schalte deinen ArduBot an und stelle ihn auf die Linienbahn. Dann drücke den Startknopf.***

  ***Halte nun deine Hand vor die Augen (Ultraschallsensor) deines Roboters.***


#### *Abstand halten, ArbuBot!*

---

#### [Zurück zum Index](#index)

### 4.6. Zusammenbau Part 5<a name="rp-assemblyP5"></a>

Dieser Abschnitt konzentriert sich auf den Servomotor. Damit kann der Roboter einen grosseren Abschnitt nach Hindernissen abtasten. Der Roboter wird sich in die Richtung drehen, in der es keine Hindernisse oder diese weiter entfernt sind.

> Jetzt kann ich meinen Kopf drehen, und mich umschauen! Dann geh ich da hin, wo ich Platz hab!

<img src="images/RobotBuild/RobotBuild_Part5_ServoMotor.jpg" width="640"/><br>

#### Was für neue Teile brauchst Du?

Keine neuen Teile sind benötigt, da du nun den Servomotor programmierst, der bereits eingebaut und verkabelt ist.

#### Block für Block

<img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_3.jpg" width="960"/><br>

  **Hier führst du das vorherige Projekt weiter. Falls ArduBot ein Hindernis vor sich hat, soll er/sie stoppen, sich umschauen und entscheiden in welche Richtung auszuweichen.**

1. Starte mit vorherigen Projekt und ändere den Namen.
2. Ziehe den Block **Setze SERVO mit Pin# 0 zu 90 Winkelgrad** (Set SERVO with Pin# 0 to 90 Degrees) aus **Motor** (Motors) in die erste Reihe der Vorlaufschleife.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_4.jpg" width="960"/><br>

3. Wähle die Pinnummer ***8***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_5.jpg" width="960"/><br>

4. Ziehe 2 **Setze Element zu** (set item to) aus **Variable** (Variables) in die letzte Reihe der Vorlaufschleife.
5. Gib den Variablen die Namen ***rightDistance*** und ***leftDistance***.
6. Verbinde beide Blöcke mit einem Nummerblock **0** aus **Mathe** (Math).

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_6.jpg" width="960"/><br>

7. Lege einen Block **zu Mache etwas** (to do something) aus **Funktion** (Functions) auf deinen Arbeitsbereich.
8. Benenne die Funktion ***lookRight***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_7.jpg" width="960"/><br>

9. Erzeuge 2 Duplikate von **Setze SERVO mit Pin# 8 zu 90 Winkelgrad** (Set SERVO with Pin# 8 to 90 Degrees) aus der Vorlaufschleife und lege sie in der Funktion **lookRight**.
10. Ändere den Wert **90** des Zahlenblocks der oberen Kopie zu ***45***.
11. Ziehe 2 **Warte 1000 Millisekunden** (wait 1000 milliseconds) Blocks aus **Zeit** (Time) zwischen die beiden **Setze SERVO zum Pin 8...** (Set SERVO from Pin 8...) Blöcke.
12. Verringere den Zeitwert des ersten Blocks zu ***500*** und des zweiten Block zu ***100***.
13. Zwischen den beiden Zeitblöcken setze einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) mit dem Namen ***rightDistance*** ein.
14. Verbinde diesen Block mit **Lese HC-SR04 vom Echo Pin# 0** (Read HC-SR04 from Echo Pin# 0) aus **Sensor** (Sensors).
15. Wähle die Pinnummer ***13***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_8.jpg" width="960"/><br>

16. Dupliziere die komplette **lookRight** Funktion und nenne diese ***lookLeft***.
17. Nun ändere den Zahlenblock **45** zu ***135***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_9.jpg" width="960"/><br>

18. Ziehe jeweils einen **lookRight** und **lookLeft** Block aus **Funktion** (Functions) und platziere dies untereinander in die letzte Reihe der Vorlaufschleife.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_11.jpg" width="960"/><br>

  **Wenn der Roboter auf ein Hindernis vor sich trifft soll er/sie anhalten, sich ummschauen.**

19. Setze nun folgende Funktionen und Zeitblöcke unterhalb der Funktion **Stop** im eingebetteten **wenn Distance <= 20 und Distance > 0** Logikblock: ***300***, **Backward**, ***400***, **Stop**, ***300***, **lookRight**, ***300***, **lookLeft**, ***300****.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_12.jpg" width="960"/><br>

  **Im Vergleich zu beiden Distanzen entscheidet ArduBot in welche Richtung es weitergeht.**

20. Positioniere unterhalb des letzten Zeitblock mit Wert **300** einen **wenn Tue** (if do) Block aus **Logik** (Logic) und benutze das Zahnradsymbol, um einen **anderenfalls** (else if) Block hinzuzufügen.
21. Verbinde **wenn** (if) mit einem **" " = " "** Block aus **Logik** (Logic) und wähle das Symbol ***>=*** anstelle **=**.
22. Beide Positionen werden **Element** Blöcke aus **Variable** (Variables) mit dem Namen in der ersten Position ***leftDistance*** und der zweiten Position ***rightDistance***.
23. Dupliziere **leftDistance >= rightDistance** und verbinde die Kopie mit **anderenfalls** (else if).
24. Ändere hier das Symbol zu ***>***, die erste Variable **leftDistance** zu ***rightDistance*** und die zweite Variable **rightDistance** zu ***leftDistance***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_13.jpg" width="960"/><br>

25. Addiere die folgenden Funktionen und Zeitblöcke in die **Tue** (do) Spalte von **wenn leftDistance >= rightDistance**: **Left**, ***500***, **Stop**, ***500***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_14.jpg" width="960"/><br>

26. Danach addiere die folgenden Funktionen und Zeitblöcke in die **Tue** (do) Spalte von **wenn rightDistance > leftDistance**: **Right**, ***500***, **Stop**, ***500***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_15.jpg" width="960"/><br>

27. Speichere das Projekt, verfiziere es und lade es auf deinen Arduino hoch.


***Du kannst durch Veränderung der Werte in den Zahlenblöcken das Verhalten deines ArduBots beeinflussen.***

#### *Jetzt kannst du deinen Roboter auf die Reise durch dein Zimmer schicken.*

---

#### [Zurück zum Index](#index)

### 4.7. Zusammenbau Part 6<a name="rp-assemblyP6"></a>

Im abschliessenden Teil, wollen wir dem Roboter noch eine Stimme geben. Der Roboter meldet sich, wenn etwas sehen kann.

> Dideldidu....ich seh etwas, siehst es auch du!?

<img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_1.jpg" width="640"/><br>

#### Was für Teile brauchst Du?

| *Anzahl* | *Bezeichung* | *Foto*  |*Anzahl*  | *Bezeichung*  | *Foto* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Passiver Buzzer** |<img src="images/Parts/Part_PassiveBuzzer.jpg" width="256"/> | 2 | **M-M Kabel**  | <img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/>  |

#### Schaltplan

<img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_2.jpg" width="960"/><br>

#### Bastelanweisung

1. Passiver Buzzer: Stecke den Buzzer **+** in Spalte 4 Reihe 5 und **-** in Spalte 7 Reihe 5 des unteren Teils.
2. M-M Kabel 1: Unterer Teil Spalte 4 Reihe 2 >> APEM 5V
3. M-M Kabel 2: Unterer Teil Spalte 7 Reihe 2 >> Oberer Teil Spalte 6 Reihe 5

**Vergleiche deine Version mit dem Schaltplanfoto, bevor wir weitermachen.**

#### Block für Block

1. Fahre mit dem letzten Projekt fort, wobei es immer besser ist dem Projekt einen neuen Namen zu geben, um das Bisherige zu bewahren.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_3.jpg" width="960"/><br>

  **Beginne mit einer Variable, welche die Startfrequenz des Tones hält.**

2. Ziehe einen **Setze Element zu** (set item to) Block aus **Variable** (Variables) in die unterste Reihe der Vorlaufschleife.
3. Benenne **Element** in ***RootNote*** um und verbinde den Block mit einem Zahlenblock aus **Mathe** (Math).
4. Erhöhe die Nummer auf ***440***.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_4.jpg" width="960"/><br>

  **ArduBot soll Laute von sich lassen, wenn ein Hindernis zwischen 20 und 50 cm liegt.**

5. Füge einen **wenn Tue** (if do) Block aus **Logik** (Logic) in die erste Reihe der **Tue** (do) Spalte von **wenn On/Off = Wahr** (if On/Off = true).
6. Benutze das Zahnradsymbol, um **sonst** (else) hinzuzufügen.
7. Verbinde nun **wenn** (do) mit einem **" " und " "** Block aus **Logik** (Logic).
8. Füge in die erste Position einen **" " + " "** Block aus **Mathe** (Math).
9. Ändere das Symbol **+** zu ***<***.
10. Die erste Position von **" " < " "** wird ein **Element** Block mit Variablennamen **Distance**.
11. Die zweite Position wird ein Zahlenblock **0** aus **Mathe** (Math) mit dem erhöhten Wert ***50***.
12. Erzeuge nun eine Kopie von **Distance < 50** und füge dies in die zweite Position von **wenn Distance < 50 und " "** (if Distance < 50 and " ") ein.
13. Das Symbol **<** wird ***>*** und der Nummerwert ändert sich zu ***20***.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_5.jpg" width="960"/><br>

  **Nun den Teil, welcher die eigentlichen Töne erzeugt.**

14. Addiere zur **Tue** (do) Spalte von **wenn Distance < 50 und Distance > 20** (if Distance < 50 and Distance > 20) einen **Setze Ton zu pin# 0 auf Frequenz 220** (Set tone on pin# 0 to 220) Block aus **Audio** und wähle die Pinnumer ***4***.
15. In die **Tue** (do) Spalte von **sonst** (else if) hingegen kommt ein **Stoppe Ton von Pin# 0** aus **Audio** mit Pinnummer ***4***.
16. Ziehe einen Variablenblock **Element** auf deinen Arbeitsbereich und wähle den Variablennamen ***RootNote***.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_6.jpg" width="960"/><br>

  **Wenn du lediglich die Basisnote spielst, würde sich dein ArduBot sehr monotone und leblos anhören. Integriere daher etwas Zufall hinein.**

17. Ziehe 2 **" " + " "** Blöcke aus **Mathe** (Math) auf deinen Arbeitsbereich.
18. Ändere das Symbol des ersten Block zu ***x*** und ziehe den zweiten Block in die zweite Position dies ersten Blocks.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_8.jpg" width="960"/><br>

19. Die erste Position des zweiten Block **" " + " "** wird ein **Zufallsnummer zwischen 1 und 100** (random integer from 1 to 100) aus **Mathe** (Math), wobei die Nummer **100** zu ***4*** wird.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_9.jpg" width="960"/><br>

20. Die zweite Position des zweiten Block **Zufallsnummer zwischen 1 und 4 + " "** (random integer from 1 to 4 + " ") wird ein **Zufallsbruch** (random fraction) Block aus **Mathe** (Math).
21. Ziehe nun den Variablenblock **RootNote** von deinem Arbeitsbereich in die erste Position des ersten Block **" " x Zufallsnummer zwischen 1 und 4 + Zufallsbruch** (" " x random integer from 1 to 4 + random fraction).
22. Ersetze letztendlich die Nummer **220** des Blocks **Setze Ton zu pin# 4 auf Frequenz 220** (Set tone on pin# 4 to 220)** mit der kompletten Blockkombination **RootNote x Zufallsnummer zwischen 1 und 4 + Zufallsbruch** (RootNote x random integer from 1 to 4 + random fraction).

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_10.jpg" width="960"/><br>

23. Speichere die letzte Version des ArduBot-Projektes, kompiliere es und lade es auf deinen Arduino.


  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_11.jpg" width="960"/><br>

### *Geschafft! :-D*

---

<img src="images/RobotBuild/RobotBuild_FrontSideTopView.png" width="960"/><br>


### [*ArduBot Video*](images/RobotBuild/BuildARobot.mp4)

#### *Viel Spass mit deinem ArduBot!*

Von hier aus kannst du deine eigenen Ideen integrieren. Ich geb dir noch ein paar Anregungen mit auf den Weg.

Zum Beispiel:

* das Umfahren des Hindernisses das auf dem Pfad liegt und das anschliessende Wiederfinden des Pfades.
* verschiedene Melodien für unterschiedliche Situationen.
* LEDs zum Blinken.
* die LED Matrix, die dann Gesichter macht.
* ...

---
## ENDE

### Ich hoffe, dir hat dieser Kurs gefallen. Viel Spaß ein "Maker" zu sein :-)

***Vielen Dank, dass du dir die Zeit genommen hast, und wenn du Feedback und/oder Vorschläge hast, werde ich mich darüber sehr freuen!***

#### contact@jensmeisner.net
