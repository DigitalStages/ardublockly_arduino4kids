# ArduBlockly_Arduino4Kids

This is the tutorial for the course ["ArduBlockly - Arduino for Kids"](https://gitlab.com/DigitalStages/ardublockly_arduino4kids)

# Index <a name="index"></a>

1. [Preparation](#preparation)

2. [Installation](#installation)

    2.1. [Installation procedure for Windows, Mac, Linux PC](#installationPC)

    2.2. [Installation procedure for Android Tablet](#installationTablet)

3. [Tutorials](#tutorials)

    3.1. [Hello My Name Is Arduino](#let-me-introduce)

    3.2. [My Blinker](#my-blinker)

    3.3. [Let There Be Light](#let-there-be-light)

    3.4. [More Colors](#more-colors)

    3.5. [Button Fun](#button-fun)

    3.6. [Beat Master](#let-beat-a-rhythm)

    3.7. [Mini Piano](#mini-piano)

    3.8. [Sam Says 'Move Your Arm!'](#sam-says-move-your-arm)

    3.9. [Let Us Make Some More Music!](#lets-make-some-more-music)

    3.10. [Sounds Like A Phone!](#sounds-like-a-phone)

    3.11. [How Hot Is It?](#how-hot-is-it)

    3.12. [Left, Right, Back And Forth](#left-right-back-forth)

    3.13. [The Starlight Show](#star-light-show)

    3.14. [Animated Animators](#get-animated)

    3.15. [Round And Round It Goes](#round-and-round-it-goes)

    3.16. [Red, Yellow, Green....Go!](#red,yellow,green...and-go)

    3.17. [What Time Is It?](#what-time-is-it)

    3.18. [How Deep Is It?](#how-deep-is-it)

    3.19. [Let Us Scream](#lets-scream)

    3.20. [Three Times The Same?](#3-times-the-same)

    3.21. [You Shall Pass](#you-shall-pass)

    3.22. [Blink Blink Blink](#blink-blink-blink)

    3.23. [Brighter, Lighter, Light-Brighter](#brighter,lighter,light-brighter)

    3.24. [9.8.7.6.5.4.3.2.1.0...Take Of!](#countdown...9...0)

    3.25. [And 1, 2, 3, 4](#1..2..3..4)

    3.26. [Let Us Make Some Wind](#lets-make-some-wind)

    3.27. [Tik Tak Tok](#tic-tac-toc)

    3.28. [Brumm!](#brummm)

    3.29. [Last But Not Least](#last-but-not-least)


4. [ArduBot Project](#robotproject)

    4.1. [Preparation](#rp-preparation)

    4.2. [Assembly: Part 1](#rp-assemblyP1)

    4.3. [Assembly: Part 2](#rp-assemblyP2)

    4.4. [Assembly: Part 3](#rp-assemblyP3)

    4.5. [Assembly: Part 4](#rp-assemblyP4)

    4.6. [Assembly: Part 5](#rp-assemblyP5)

    4.7. [Assembly: Part 6](#rp-assemblyP6)


---

#### [Back To Index](#index)

## 1. Preparation <a name="preparation"></a>

* To participate in the course, you need access to a PC or Android tablet and an Elegoo "The Most Complete Starter Kit" Arduino Project Kit. If you want to use a tablet, you also need a mini USB <> USB adapter.
* If you bring your own laptop or tablet, you should have the software installed so that we can start immediately.


### 1.1. ELEGOO "The Most Complete Starter Kit" Arduino Project
<br>

<img src="images/ElegooTheMostCompleteStarterKit.jpg" width="960"/>

#### Product information

The Ultimate Starter Kit for Arduino Elegoo is ideal for children, adolescents and adults. The extensive kit is supplied with a lot of accessories. 63 different components and over 200 parts are included. The latest UN R3 development board is also included. Your project is so easy to implement, because even the LCD1602 module requires no soldering. The course includes over 30 lessons including programs and explanations.

<br>

### 1.2. Hardware for Android Tablets

To connect the Arduino to the tablet, you need an OTG adapter USB A jack on Micro USB B plug.

<img src="images/Parts/OTG_USB_Adapter.jpg" width="512"/>

<br>

### 1.3. Software for PC, Mac und Linux Computer
* [ArduBlockly Software](https://gitlab.com/DigitalStages/ardublockly)
* [Arduino Software](https://www.arduino.cc/en/Main/Software_) >> So that you can upload the code to the Arduino
* [Python](https://www.python.org/downloads/) >> So that you can use Ardublockly in the web browser

<br>

### 1.4. Software for Android Tablets

* [ArduBlockly Software](https://gitlab.com/DigitalStages/ardublockly)
* [ArduinoDroid Software](https://play.google.com/store/apps/details?id=name.antonsmirnov.android.arduinodroid2) >> So that you can upload the code to the Arduino
* [Pydroid3 - Python IDE](https://play.google.com/store/apps/details?id=ru.iiec.pydroid3) >> So that you can use Ardublockly in the web browser
* [File Manager](https://play.google.com/store/apps/details?id=com.alphainventor.filemanager) >> So that we can select and copy files

<br>


---

#### [Back To Index](#index)

## 2. Installation <a name="installationPC"></a>

This section aids you through the installation process, so it is easier for you to run Ardublockly and use your Arduino with your laptop / PC or tablet. This seems a bit complicated at first, so we will go step-by-step through the procedure from installation to configuration.

### 2.1. Installation procedure for Windows, Mac, Linux PC <a name="installation"></a>

1. Install the Arduino software with the installation file you downloaded from the top link for your computer system.

2. Start the program and choose in the menu ***Sketch >> Include Library >> Add .ZIP Library***

  <img src="images/RunArdublockly/RunArdublockly_ImportLibraries_1.png" width="960">

----

3. Search the Ardublockly directory and select the first zip file under **Arduino libraries** and press OK.

  <img src="images/RunArdublockly/RunArdublockly_ImportLibraries_2.png" width="960"/>

----

4. If everything worked, there should be a message as follows:

  <img src="images/RunArdublockly/RunArdublockly_ImportLibraries_3.png" width="640"/>

----

5. Repeat this with all ZIP files in the Libraries directory. These are necessary for the modules and sensors from the Arduino Kit to work.
6. Close the Arduino program and install Python with the installation file that you downloaded for your operating system.

#### Run ArduBlockly

  **Windows:**
  ***If you installed Python3, you will be able to just double-click start.py to run ArduBlockly.***

1. Go to the **ardublockly** directory with your file manager of your computer.
2. Open a terminal window and enter ***python3*** (do not forget space).
3. Then drag the **start.py** file into the terminal window.

  <img src="images/RunArdublockly/RunArdublockly_1.png" width="960"/>

----

4. The command line should look as similar as pictorial example.
>!The Arduino Board should be started before the program is started via USB to the computer!

  Press then Enter

  <img src="images/RunArdublockly/RunArdublockly_2.png" width="1024">

----

5. The terminal should look like this (depending on the operating system) as the following picture:

  <img src="images/RunArdublockly/RunArdublockly_3.png" width="640"/>

----

6. After a few moments, the web browser should open and the Ardublockly IDE will appear.

  <img src="images/RunArdublockly/RunArdublockly_4.png" width="960"/>

----

7. Press the icon above left, which opens the main menu. Choose **Settings**.

  <img src="images/RunArdublockly/RunArdublockly_5.png" width="960"/>

----

8. Add the installation path to the already installed Arduino IDE software. If you want to specify a special sketch directory in which your blocks are automatically saved, you can set this under **Sketch Folder**.

  <img src="images/RunArdublockly/RunArdublockly_6.png" width="512"/>

----

9. Next, select the Arduino Board to be used. In our case this is **Uno**.

  <img src="images/RunArdublockly/RunArdublockly_7.png" width="512"/>

----

10. The next setting should be done while the board is connected via USB to the computer to see it in the settings. **TTYACM0** or **TTYUSB0** are common labels for the Arduino Board under Linux.

  <img src="images/RunArdublockly/RunArdublockly_8.png" width="512"/>

----

11. Press the double arrow icon at the bottom left. This will show the Arduino terminal.

  <img src="images/RunArdublockly/RunArdublockly_9.png" width="128"/>

----

12. Go with the mouse over the triangle or hook until you can read ***Verify the Sketch*** in a pop-up message. Press the appropriate symbol.

  <img src="images/RunArdublockly/RunArdublockly_10.png" width="256"/>

----

13. If everything works, Ardublockly should verify idle success.

  <img src="images/RunArdublockly/RunArdublockly_11.png" width="960"/>

----

14. Next, upload the empty code to the board. Press the symbol that displays **Upload sketch** in the info message.

  <img src="images/RunArdublockly/RunArdublockly_12.png" width="256"/>

----

15. As a result, the Arduino terminal should have the following message. During the uploading you can see the LEDs of the Arduino flashing.

  <img src="images/RunArdublockly/RunArdublockly_13.png" width="960"/>

----

16. If everything worked, everything is clear to the start >>

#### *Ready to go!*

---

#### [Back To Index](#index)

### 2.2. Installation procedure for Android Tablet <a name="installationTablet"></a>


1. Install the following 3 Android applications using Google Play.

* [Pydroid3](https://play.google.com/store/apps/details?id=ru.iiec.pydroid3)

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_1.png" width="512">


* [ArduinoDroid](https://play.google.com/store/apps/details?id=name.antonsmirnov.android.arduinodroid2)

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_2.png" width="512">


* [File Manager](https://play.google.com/store/apps/details?id=com.alphainventor.filemanager)

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_3.png" width="512">


----

2. Start the program **File Manager**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_4.png" width="512">

----

3. Use the OTG adapter USB A jack on Micro USB B plug to connect the USB dongle with the downloaded [Ardublockly Software](https://gitlab.com/digitalstages/arddublockly) and the tablet. The USB dongle should now be visible in the File Manager.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_5.png" width="960"/>

----

4. Hold the finger as long as the Ardublockly icon until the lower menu opens. Tap on **Copy**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_6.png" width="960"/>

----

5. Go back and tap **Main Memory**. Then tap **paste** to copy Ardublockly to the tablet.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_7.png" width="960"/>

----

6. Have some patience, the copy process can take a few minutes.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_8.png" width="960"/>

----

7. Leave **File Manager** and start **ArduinoDroid**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_9.png" width="512"/>

----

8. Now you will install the libraries Ardublockly needs to function properly. Type on the top right of the symbol with the line from 3 points. Then click on **Libraries**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_10.png" width="960"/>

----

9. In **Libraries** press **Add .ZIP library**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_11.png" width="960"/>

----

10. Search the Ardublockly directory and select the first ZIP archive under **arduino-libraries**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_12.png" width="960"/>

----

11. If everything worked, a message should appear similarly as below:

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_13.png" width="960"/>

----

12. After installing all the target archive, it's time to select the Arduino Board with **Board type**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_14.png" width="960"/>

----

13. The Arduino, which is used in the Elegoo Complete Set, is Arduino **Uno**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_15.png" width="960"/>

----

14. Now return to the FileManager application and go there to the directory **ardublockly**. Hold the finger on **start.py** until the menu opens. Choose then **More >> Open with**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_16.png" width="960"/>

----

15. Open the Python file with **Pydroid 3**. Choose **ALWAYS**, so you only need to tap the file next time.

<img src="images/RunArdublockly/RunArduBlocklyOnAndroid_17.png" width="960"/>

----

16. The program shows the Python file in a text editor. You only need to tap the yellow round button at the bottom right. Thereafter, the program will start, an internal web server get established and open the web browser with Ardublockly running.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_18.png" width="960"/>

----

17. Let us test, if you can upload a simple code to the Arduino. Drag the basic function from **Functions** onto the workplace.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_19.png" width="960"/>

----

18. Since on the tablet, Ardublockly is currently not automatically uploading the code to the Arduino. You will need to save the code as Arduino file with .ino extension, then compile and upload it with ArduinoDroid. To save tap the icon for the main menu.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_20.png" width="960"/>

----

19. Then tap **Save Sketch**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_22.png" width="960"/>

----

20. Select the name and directory. Then tap **Download**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_23.png" width="960"/>

----

>Now it is time to pull the USB dongle out of the adapter on your tablet, and replace it with the USB cable that is connected to your Arduino.

----

21. Change to ArduinoDroid and Tap **Sketch >> Open from**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_24.png" width="960"/>

----


22. Tap **Device**, which means your tablet.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_25.png" width="960"/>

----

23. Search the stored Arduino file with *.ino* extension, choose it and tap **Done**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_26.png" width="960"/>

----

24. You should now see the code with an empty ***setup()***, and an empty ***loop()*** function. Go back to the main menu above right, and choose **Actions** this time.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_27.png" width="960"/>

----

25. To be able to upload the code to the Arduino, it must first be compiled. Tap **Compile**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_28.png" width="960"/>

----

26. After being successful you should see the message ***Compilation finished***. Now you can upload the code to the Arduino.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_29.png" width="960"/>

----

27. To do this, go to the main menu again and tap on **Actions**. Then tap **Upload**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_30.png" width="960"/>

----

28. Tap **Upload over USB**.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_31.png" width="960"/>

----

29. If the code has been successfully uploaded, the message **Sketch uploaded** should display.

  <img src="images/RunArdublockly/RunArduBlocklyOnAndroid_32.png" width="960"/>

----

> I hope that in the future we will have a direct connection from Ardublockly to the Arduino IDE (ArduinoDroid) for Android OS. Then the upload from the Android tablet will be as easy as with the laptop / pc.

---

#### OK. All preparations are completed, the equipment is ready to start. Ready to go ....
### ...Let's start the journey!

---

#### [Back to Index](#index)


## 3. Tutorials <a name="tutorials"></a>



### 3.1. Hello my name is Arduino <a name="let-me-introduce"></a>

> Hello, my name is Arduino. I am a microcontroller! May I introduce myself?

  <img src="images/Lesson1/Lesson1_Intro_10.png" width="960"/>

| *Name*        | *Description* |
|---------------|---------------|
| **Atmega328 Microcontroller**            | >> That's my brain. Everything is directed, controlled and switched here.   |
| **Power Supply**                         | >> This allows you to supply enough energy to my brain (9 volts mhmm ... delicious). |
| **USB Plug**                             | >> The USB connection has electricity (5 volts). This is the channel to your computer or tablet on which you can give me your tasks (the program). |
| **Reset Button**                         | >> The push button tells me that I should start the task from the beginning. |
| **Analog Reference Pin (AREF)**          | >> With this PIN, you can set a maximum energy (0 - 5 Volt) for my analog inputs. (We will not use this) |
| **Digital Ground / Ground Pins**         | >> Earth protects me like a lightning rod the house. |
| **Digital I/O**                          | >> My digital inputs and outputs >> The symbol ~ means PWM, which stands for Pulsating connection (PWM) so that you can also control the speed of a motor digitally (0 = OFF, 1 =). |
| **Serial OUT (TX)**                      | >> This pin is also one of my digital I/O pins (D1). So you can talk to me with another device. TX is the communication input |
| **Serial IN (RX)**                       | >> This pin is also one of my digital I/O pins (D0). So I can talk to another device. Rx is the communication output|
| **In-Circuit-Serial Programmer**         | >> Through this I can be used as an extension in larger projects (we will not need). |
| **Analog I/O**                           | >> My analog inputs and outputs are unlike digital states 0 (power off) and 1 (stream). They have 1024 stages, similar to a dimming switch. |
| **Voltage in Pin**                       | >> Here I can give extra energy to sensors or engines, or get extra me energy if the energy is not enough via the 9V battery.|
| **3.3 / 5 Volt Power Pin**               | >> These are my power outputs, once with 3.3 volts and once with 5 volts. |
| **Reset Pin**                            | >> Is like the reset button, if you will connect a push button to this pin. |

---

#### [Back to Index](#index)

### 3.2. My Blinker <a name="my-blinker"></a>

>My turn signal is an LED light on my board. Find it, it is called "L"!

  <img src="images/Lesson2/Lesson2_MyBlinker_1.png" width="600"/>

>Connect me now with your computer / tablet using the USB cable!

---

After the Arduino is connected via USB to the computer, we should now start Ardublockly. Make sure under Settings your Arduino is set to the right COM port.

This is the code that will make the LED blink.

<img src="images/Lesson2/Lesson2_MyBlinker.png" width="960"/>

---

#### What parts do you need?

|*Count*|*Name* | *Image* |
|----------|-------------|--------|
| 1 | **Arduino Uno + USB cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> |

#### New components

***Arduino Uno:***

* is a microcontroller board based on the atmega328p chip.
* has 14 digital input / output pins (of which 6 are used as PWM outputs), 6 analog inputs, a 16 MHz ceramic resonator (CSTCE16M0V53-R0), a USB port, a power socket, an ICSP header and a reset Button.
* contains everything that is needed to support the microcontroller.
* just connect it to a computer with a USB cable or you operate it with an AC-to-DC adapter or battery to get it started.

---

#### Block for Block


1. Under **Input/Output** we find the block for the LED **Set Built-in Builtin_1 to**. Click/Tap this block and take it to the workplace.

  ***If you have pulled the wrong part on the workplace, you can pull this over the menu. So you will delete it.***

  <img src="images/Lesson2/Lesson2_MyBlinker_2.png" width="960"/><br>

2. **How long should the LED light up?** Under **Time** you will find the block for it. Click/Tap the block **wait 1000 milliseconds** and take it on the workplace.

  ***1000 milliseconds are ....?***

  <img src="images/Lesson2/Lesson2_MyBlinker_3.png" width="960"/><br>

3. Repeat the previous 2 steps, this time the setting of the LED **Builtin_1** is set to ***LOW***. This means the LED remains off ... for another second.

  <img src="images/Lesson2/Lesson2_MyBlinker_4.png" width="960"/><br>

4. That's all. Now you have to compile the code block. Go to the icon with the hook, and Click/Tap the icon that displays **Verify Sketch**. You should see **Successfully Verified Sketch** in the Arduino IDE Output.

  **Currently, under the exclusive use of the Android tablets is necessary to compile and upload the code in ArduinoDroid. Use "Save Sketch" to save the converted code in the .ino format.**

  ***This does not apply to computers and laptops in which the Arduino software automatically compiles and uploads, and you pointed at the Arduino IDE executable. Therefore, it can go on now.***

  <img src="images/Lesson2/Lesson2_MyBlinker_5.png" width="960"/><br>

5. Give your code block a name.

  <img src="images/Lesson2/Lesson2_MyBlinker_6.png" width="960"/><br>

6. Save the code block of your project. Use **SAVE** to save the Code block for Ardublockly, which will have a .xml extension.

  <img src="images/Lesson2/Lesson2_MyBlinker_7.png" width="960"/><br>

7. Time to upload. Click/Tap on the large green button (or left next to it) to upload the code to the Arduino. The note will be **Upload Sketch to Arduino**.

  <img src="images/Lesson2/Lesson2_MyBlinker_8.png" width="960"/><br>

  If uploading was successful, the message **Successfully uploaded Sketch** will shows up.

>Naaaa .... am I flashing??? Change the time intervals. :-D

>Can you let me blink me a Morsecode?

---

### 3.3. Let There Be Light <a name="let-there-be-light"></a>

The more energy flows through a LED, the brighter it will be. However, the LED can be destroyed with too high energy. Therefore, we will use resistors to set the energy flow. The higher the resistance, the less energy will flow.

<img src="images/Lesson3/Lesson3_LED_1.png" width="960"/><br>

#### What parts do you need?

| *Count* | *Description* | *Image*  |*Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino UNO + USB cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Red LED** |<img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/>    | 1 | **220 ohm Resistor** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>|
| 1 | **1K ohm Resistor**             |<img src="images/Parts/Part_Resistor_1KOhm.png" width="256"/> | 1 | **10k ohm Resistor** |<img src="images/Parts/Part_Resistor_10KOhm.png" width="256"/>|
| 3 | **M-M Cable**                     |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***Breadboard***

* allows to test circuits without soldering.
* has numerous sizes and models.
* is often a plastic block with integrated metal strips that make the electrical connections between the contact points.

  <img src="images/Lesson3/Lesson3_LED_3.png" width="320"/><br>

* has slot holes marked as red and blue lines, which are connected in each case in series (red/blue).
* has columns of 5 connectors in the upper and lower body, which are also connected to each other (green).
* is interrupted in the middle part (between E and F) so that the upper and lower part can be used independently fro each other.

***LED or light emitting diode:***

* is a small light that needs very little electricity and holds forever. However, it can also break if you can go through too much energy.
* The current will heat the LED so far until it blows. To prevent that, we use resistors. <br>
* The LED has a positive and a negative connection. The positive connection is longer than the negative.

***Resistor:***

* is small element that retains the current flow.
* There are resistors with different units, which are called "ohms".
* The Greek letter Ω omega is the short symbol. A "K" in front of Ohm means kilo or 1000.
* Resistors have color rings from which one can calculate the ohm number. There are resistors with 4 or 5 farming.
* The resistors have no poles + or -. Therefore, it does not matter which direction of resistance is used.

  In this exercise we will use 3 resistors. You should find resistors with 5 circuits in your kit. Each of the strips in the kit have number on the color rings.

  ***Please take only a resistor, so you can later compare the loosen resistors and find out the ohm count.***

  220 Ohm Resistor: <br>
  Red | red | black | black | brown

  1 kOhm Resistor (1000 Ohm):  <br>
  Brown | black | black | brown | brown

  10 kOhm Widerstand (10000 Ohm): <br>
  Brown | black | black | red | brown


#### Circuit diagram

  <img src="images/Lesson3/Lesson3_LED_2.png" width="960"/><br>


#### Assembly instructions

***Info: The following instructions are shortened by a >> character. This means here:***<br>

***Connect part A with that part B, or part A >> Part B!***

1. Cable 1: Arduino GND >> Blue row of Breadboard
2. Cable 2: 5V Output >> Red row of Breadboard
3. LED: Short end of the LED >> Blue row of Breadboard
4. LED: Longer end of the LED >> Hole of the middle part
5. 220 Ohm Resistor: In row of "+" series (red) >> middle section
6. 1 kOhm Resistance: In row of "+" series (red) >> middle section
7. 10 kOhm Resistance: In row of "+" series (red) >> middle section
8. Cable 3: In row of LED on middle part >> in row of 220 ohm resistance to middle part

#### *Start your Arduino into the USB socket of your computer / tablet*

>And it will light! Try the other resistors!

>Or how would a different color? (Do not forget where the longer foot belongs)

---

#### [Back to Index](#index)

### 3.4. More Colors <a name="more-colors"></a>

Let us build a lighting show with an LED. For this we need an RGB LED.

<img src="images/Parts/Part_LED_RGB_5mm.jpg" width="512"/><br>

#### What parts do you need?

| *Count* | *Description* | *Image*  |*Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino UNO + USB cable**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Breadboard**                   |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 1 | **RGB LED**                      |<img src="images/Parts/Part_LED_RGB_5mm.jpg" width="256"/>    | 3 | **220 Ohm Resistor**            |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>|
| 4 | **M-M Cable**                     |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***The RGB LED:***

* looks first like any other diode. But in reality it has 3 diodes installed.
* means red, green, blue. For each color there is an LED installed. That's why the RGB LED has 4 wire legs.
* Has a wire leg for **+** each color (anode) and a leg **-** for grounding (cathode).

  In the following picture, the different colors are shown by the mixture of light colors red, green and blue.

  <img src="images/Lesson4/Lesson4_RGBLED_4.png" width="960"/><br>

* The light intensity has a span of 0 to 255. With 0, these are 256 settings.
* The colors red, green and blue are generated by fully turning the respective LED while the LEDs of the other two colors are turned off. So in the case of red, the red LED gets the value 255, the green and blue LED the value 0.

  ***You certainly know that from the TV or monitor.***


#### Circuit diagram

<img src="images/Lesson4/Lesson4_RGBLED_3.png" width="960"/><br>


#### Assembly instructions

To protect the RGB LED from damage, we will again use a resistor (220 ohms) per LED.

Each of the 3 LEDs gets a slot at the Arduino. In addition, we need a grounding or GND (Ground). The wire leg is the longest leg on the RGB LED. Therefore, you can also see which leg is red, green and blue.

On the Arduino you can see a wave symbol ~ in the digital slots. This indicates that the connection has over *PWM*.

In order to be able to adjust the brightness of our LED, we need the technique of pulse width modulation.

The digital slot without *PWM* can be only 0 or 1, so power off or current. So that we can use 256 levels, our Arduino controls the electrical power of the connection.

The Arduino switches the switch in a super fast pulse. 500 times per second to be accurate. However, our Arduino can determine how long this switch remains:

* At the setting 255, the current flow remains the complete time between the pulses.
* When setting 0, the switch is immediately displayed again for each pulse.
* The setting 128 means exactly half of the time distance between 2 pulses.

Due to the PWM technology, the digital connection (~ symbol) can also be used as an analog slot.

***Info: The following instructions are shortened by a >> character. This means here:***<br>

***Connect part A with that part B, or part A >> Part B!***

1. RGB LED: 4 LED wire legs >> side by side in the upper middle section.
2. Cable 1:  Arduino GND >> Midsection hole before long wire leg
3. 3 Resistor: Before every foot in the upper middle section >> Length position in the lower middle part
4. Cable 2: Arduino ~6 >> Against resistance to the left of the GND cable in the lower middle part
5. Cable 3: Arduino ~5 >> Against resistance to the right of the GND cable in the lower middle part
6. Cable 4: Arduino ~3 >> Against resistance to the right next to 3rd cables in the lower middle part

  **Compare your version with the circuit diagram before we continue.**


#### Block for Block

This is the complete code block, which we will be together. As a result, the RGB LED color will change from red over green to blue and then change back to red. In between you can see the other colors. A journey through the rainbow colors.

<img src="images/Lesson4/Lesson4_RGBLED.png" width="960"/><br>

1. First, the 2 basic functions lead and run-forever loop. **Functions >> Arduino run first, Arduino loop forever**.

  <img src="images/Lesson4/Lesson4_RGBLED_5.png" width="960"/><br>

2. Now we log in each of the 3 LEDs at Arduino. **Input/Output >> set digital pin# 0**.

  <img src="images/Lesson4/Lesson4_RGBLED_6.png" width="960"/><br>

3. Draw 3 times this part in the lead. Change the pinsummer for the red LED ***6***, green LED to ***5*** and blue LED to ***3***. Our Arduino should turn on the red LED at the beginning with **high**, which means so much wit 1 or electricity. The electricity of the green and blue slot will be issued by selecting ***Low***.

  **What if I started all 3?**

  <img src="images/Lesson4/Lesson4_RGBLED_7.png" width="960"/><br>

4. Now we will generate an element which sets the setting of the red LED as a variable (changeable value). **Variables >> Set Item to**.

  <img src="images/Lesson4/Lesson4_RGBLED_8.png" width="960"/><br>

5. Before we change the name, let us add the setting as a number. **Math >> 0**. Place the block in the variable **Set Item to**.

  <img src="images/Lesson4/Lesson4_RGBLED_9.png" width="960"/><br>

6. Click / Tap **Element** and choose **rename**, then give this variable the name ***red***. Change the number to ***255***.

  <img src="images/Lesson4/Lesson4_RGBLED_10.png" width="960"/><br>

7. Repeat the steps of 4th to 6th twice, each for green and blue. Use the right mouse / pad button (laptop / PC) or hold the finger a few seconds on the block (tablet) to open a pop-up window. Choose **Duplicate**.

  <img src="images/Lesson4/Lesson4_RGBLED_11.png" width="960"/><br>

8. Click **red** to open the menu. Choose **New variable ...**, and change it once to ***green*** and the other time to ***blue***.

  <img src="images/Lesson4/Lesson4_RGBLED_12.png" width="960"/><br>

9. Change the numbers of **green** and **blue** to ***0***.

  <img src="images/Lesson4/Lesson4_RGBLED_13.png" width="960"/><br>

10. Drag a **set digital pin# 0** block from **Input/Output**, and change the pin number ***6***. Click on the **HIGH** block and delete them. Then draw a **item** block from **Variables** and put it on the spot where **HIGH** was before. Click **Element** and choose ***red***.

  <img src="images/Lesson4/Lesson4_RGBLED_15.png" width="960"/><br>

11. Click on the currently created block and copy this 2 times. As before, the pin nummer change once to ***5*** and the element to ***green*** and the other time the pin number ***3*** and the element to ***blue*** .

  <img src="images/Lesson4/Lesson4_RGBLED_16.png" width="960"/><br>

  **So far, the LED lights up only in a color.**

  **Let us insert a time, then we will copy all 7 parts twice.**

12. Draw a **wait 1000 milliseconds** block from **Time**, and put it to the bottom of the endless loop.

  <img src="images/Lesson4/Lesson4_RGBLED_17.png" width="960"/><br>

13. Now everything copies in the endless loop twice and put it with each other in the same order as the first group.
14. Then change the numbers of **set item to** in the first section as had **red** to ***255***, **green** to ***0***, **blue** to ***0***. In the second section put the numbers of **red** ***0***, **green** to ***255***, **blue** to ***0***, and In the third section to **red** to ***0***, **green** to ***0***, **blue** to ***255***.

  <img src="images/Lesson4/Lesson4_RGBLED_18.png" width="960"/><br>

15. Verify your code and upload it to your Arduino.

  >Red, green, blue, red, green, blue, red, green, blue, red, green, blue...

  **Try a few other colors!**

  ***See the picture again at the beginning, there are some examples.***

  **But how can you let different colors flow into each other?**

16. Draw a **count with i from 1 to 10 by 1** block from **Loops** on your workplace.

  **With this loop we can change things automatically until a certain condition is reached.**

  <img src="images/Lesson4/Lesson4_RGBLED_19.png" width="960"/><br>

17. Change **to** to ***255***, which means the loop circulates 255 times and adds with each loop one 1. We will use this to influence the brightness of the respective LED.

  <img src="images/Lesson4/Lesson4_RGBLED_20.png" width="960"/><br>

18. Take two **set Item to** from **Variables** on your workspace.

  <img src="images/Lesson4/Lesson4_RGBLED_21.png" width="960"/><br>

19. Take Two **" " + " "** from **Math** in the workspace and connect them to the **Set Item to** Blocks in the loop.

  <img src="images/Lesson4/Lesson4_RGBLED_22.png" width="960"/><br>

20. Take two **Item** from **Variables** on your workstation. Connect them with the first position in both **" "+" "**.

  <img src="images/Lesson4/Lesson4_RGBLED_23.png" width="960"/><br>

21. Change the value of the first **item** block to ***red*** and the **+** sign to ***-***. The value of the second **item** block becomes ***green***.

  <img src="images/Lesson4/Lesson4_RGBLED_24.png" width="960"/><br>

22. Now draw two **0** **Math** in the second position of the **" " - / + " "** blocks.

  <img src="images/Lesson4/Lesson4_RGBLED_25.png" width="960"/><br>

23. Both numbers in the **0** blocks should be **1**.

  **The loop will repeat 255 times, with each loop reducing the value of *red* by one and increases the value of *green* by one. The final result for *red* is 1 and for *green* is 255.**

  <img src="images/Lesson4/Lesson4_RGBLED_26.png" width="960"/><br>

  **To brake the speed of the loops, we use a time break of 10 milliseconds every cycle.**

24. Drag a **wait 1000 milliseconds** block **Time** in the last position of *count with i from 1 to 255 by 1*, and change the value **1000** ***10***.

  <img src="images/Lesson4/Lesson4_RGBLED_27.png" width="960"/><br>

25. Now take the entire block and put it below the first time break **wait 1000 milliseconds** the endless loop.

  **Compile and invite the code to your Arduino to see what happens through the loop.**

  <img src="images/Lesson4/Lesson4_RGBLED_28.png" width="960"/><br>

  **Now we want to do this with the other two LEDs. So first darken the green LED while brightening the blue LED, then darken the blue LED while brightening the red LED.**

26. To do this, click on the *count with i from 1 to 255 by 1* block and duplicate it twice. Then, in the first copy, change the value of **item** from **red** to ***green***, and **green** to ***blue***. In the second copy, the value of **item** block from **red** to ***blue***, and **green** to ***red***.

  <img src="images/Lesson4/Lesson4_RGBLED_29.png" width="960"/><br>

27. Now add both block groups *count with i from 1 to 255 by 1** each under the second and third **wait 1000 milliseconds** block of the run-forever loop.
28. Give the Project a name, compile and invite the code to your Arduino.

  <img src="images/Lesson4/Lesson4_RGBLED_30.png" width="960"/><br>

29. Save your project to do not lose your own tasks.

  **Experiment with colors you like!**

  ***For example, if one of the colors only has to reach only 128 instead of 255 in the loop, then change the number 1 in the second position of the "color +/- 1" to 0.5.***

  >Life is so colorful as you like it. :-D


---

#### [Back to Index](#index)

### 3.5. Button Fun <a name="button-fun"></a>

In this exercise you will learn to learn with the Arduino, a push button and a LED luminaire as you use digital inputs. We will turn on the light, which starts again after a while.

>I am the house light master!

<img src="images/Lesson5/Lesson5_DigitaleEingaenge_1.png" width="960"/>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino UNO + USB cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Red LED** | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/> | 1 | **220 Ohm Resistor** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>|
| 1 | **10k Ohm Resistor** |<img src="images/Parts/Part_Resistor_10KOhm.png" width="256"/>| 3 | **Pushbutton** |<img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/> |
| 3 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***The Push Button:***

* is a switch that connects the contacts when pressing so that the electric current can flow through.
* The two pins on each side are one connection.


#### Circuit diagram

<img src="images/Lesson5/Lesson5_DigitaleEingaenge_2.png" width="512"/><br>

#### Assembly instructions

1. RGB LED:  2 LED wire legs >> Side by side in the lower middle part
2. Push button:  2 x 2 Pin >> In the center of the middle part with 2 pins above and 2 pins below
3. 220 Ohm Resistor: In row of "-" row (Blue) >> Lower middle section below the short leg of the LED
4. 10 kOhm Resistor: In row of "-" row (Blue) >> Lower middle part below the right pin of the push button
5. Kabel 1: GND (Ground) >> In "-" row (Blue)
6. Kabel 2: 5V >> In "+" row (Red)
7. Kabel 3: Arduino Pin 13 >> Lower middle part below the leg of the LED
8. Kabel 4: In "+" row (Red) >> Lower middle part below the left pin of the push button
9. Kabel 5: Arduino Pin 2 >> Upper middle section above the right pin of the push button

  **Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson5/Lesson5_DigitaleEingaenge.png" width="960"/><br>

1. First, drag the standard block **Arduino run first:, Arduino loop forever:** from **Functions** to your workspace.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_3.png" width="960"/><br>

  **Next we will prepare the LED.**

2. Pull **set digital pin# 0** from **Input/Output** in **Arduino run first:**. Change the pin number to ***13*** and **HIGH** to **LOW**.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_4.png" width="960"/><br>

  **LOW means the LED light is off for the time being.**

  **Now let's keep the button press as a variable.**

3. Drag in a **set item to** block below the **set digital pin#**.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_5.png" width="960"/><br>

4. Change **item** to **switch** by choosing **Rename variable...**.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_6.png" width="960"/><br>

5. Drag the **read digital pin#** block and connect it to the **set switch to**.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_7.png" width="960"/><br>

6. Change the pin number to ***2***. Then copy the **set switch to read digital pin#2** block and paste it into the first position of **Arduino loop forever:**.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_8.png" width="960"/><br>

  **When we press the button, the light should turn on.**

  ***We use a new block for this. This method is often used in programming and is called an "if else" statement.***

  ***This means: If something happens, then do the following!***

7. Drag an **if do** block from **Logic** below **set switch to read digital pin#2** into the run-forever loop.

  **We need another block that does a comparison.**

8. Drag in **" "=" "** from **Logic** and connect the block to **if** of the **if do** block.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_9.png" width="960"/><br>

  **We want to know if the push button is pressed. So we need the variable with the button's print status.**

9. Get an **item** block from **Variables** and put it in the first position of **" "=" "**. Click on **item** and choose the name **switch**.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_10.png" width="960"/><br>

  **For comparison, we want to know whether the current flows through the switch, i.e. the button is pressed.**

10. To do this, use the **HIGH** block from **Input/Output** as the second position of **switch = " "**.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_11.png" width="960"/><br>

  **When the switch is pressed, the light should go on and off.**

11. Now duplicate the **set digital pin# 13** block twice, and put both in the **do** space.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_12.png" width="960"/><br>

12. Change **LOW** of the first **set digital pin# 13** block to **HIGH**.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_13.png" width="960"/><br>

  **Now, of course, you have to set a pause between switching on and off so that the light doesn't go out again immediately when you press the button.**

13. Extract a **wait 1000 milliseconds** block from **Time** and place it between the two **set digital pin# 13** blocks.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_14.png" width="960"/><br>

  **Well, one second isn't enough to get into the next room.**

  **So let's take 8 seconds instead.**

14. Change the value **1000** in **wait 1000 milliseconds** to ***8000***.

  <img src="images/Lesson5/Lesson5_DigitaleEingaenge_15.png" width="960"/><br>

15. Give your project a name and save the block code.
16. Compile and upload the code to your Arduino.

>Little light, little light burn... until I'm asleep!

---

#### [Back to Index](#index)

### 3.6. Beat Master <a name="let-beat-a-rhythm"></a>

In this example we will build a metronome that plays the entered beats per minute as a tone.


<img src="images/Lesson6/Lesson6_AktiverBuzzer_1.png" width="960"/>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Active Buzzer**                   |<img src="images/Parts/Part_Electronic_Buzzer.jpg" width="256"/>   |
| 2 | **W-M Cable**                      | <img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/>    |  |  |  |

#### New components

***The active buzzer:***

* is an electronic buzzer.
* is operated with direct current.
* is commonly used in phones, electronic toys, alarm systems, printers, computers and more.
* has a built-in signal source and therefore only needs power to make a sound.

***The W-M Cable:***

* has a pin on one side and a socket on the other.
* comes in the starter kit with several of these cables side-by-side.

***W-M Cable >> Pull a pair, i.e. two cables together, from the broadband.***

#### Assembly instructions

1. W-M Cable 1:  Arduino Pin 12 >> Buzzer "+"
2. W-M Cable 2:  Arduino GND >> Buzzer "-"

#### Block for Block

<img src="images/Lesson6/Lesson6_AktiverBuzzer.png" width="960"/><br>

1. First, drag the **Arduino run first:, Arduino loop forever:** standard block from **Functions** onto your workspace.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_2.png" width="960"/><br>

2. Pull **set digital pin#** from **Input/Output** into **Arduino run first:**. Change the pin number to ***12*** and **HIGH** to **LOW**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_3.png" width="960"/><br>

3. Drag in a **set item to** block below the **set digital pin#**.
4. Change **item** to ***BPM*** by choosing **Rename variable...**.

  **In modern music one uses BPM, which means Beats Per Minute.**

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_4.png" width="960"/><br>

5. Now connect a **0** block from **Math** to the **set BPM to** block.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_5.png" width="960"/><br>

6. Change the value from **0** to ***120***.
7. Right click or hold your finger on the **set BPM to** block with ***120***, then select **Duplicate**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_6.png" width="960"/><br>

8. Click on **BPM** of the copy and select **New variable...**.
9. Click **i** again and this time select **Rename variable...** and enter ***MilliPB***.

  **This means milliseconds per beat, or milliseconds per beat**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_7.png" width="960"/><br>

10. Now remove the **120** block from the **set MilliPB to** block, and replace it with a **" "+" "** block from **Math**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_8.png" width="960"/><br>

11. Now put the block **120** in the first position of **" "+" "**.
12. Then change the value from **120** to **60000**.

  **1000 milliseconds is 1 second plus 60 seconds makes 60000 milliseconds.**

  **Next we take the beats per minute and divide by 60000 milliseconds to get the milliseconds per beat.**

13. Draw an **item** block from **Variables** and put it in the second position of the **60000 + " "** block.
14. Then click on the **+** icon and select ***/***.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_9.png" width="960"/><br>

15. Position the **set MilliPB to 60000** in the lowest position of **Arduino run first:**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_10.png" width="960"/><br>

  **Now it's time to learn a new block. This one is also a loop. It repeats the contents of the loop the specified number of times.**

  **We use this loop to create a buzz tone.**

16. Drag the block **repeat 10 times** from **Loops** and place it in **Arudino loop forever:**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_11.png" width="960"/><br>

17. Make **10** to ***50*** loops.
18. Then choose **set digital pin# 12** and duplicate this block two times.
19. Position both duplicates in the **repeat 50 times** loop, changing the value of the first duplicate from **LOW** to ***HIGH***.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_12.png" width="960"/><br>

20. Drag a block **wait 1000 milliseconds** from **Time**, and change the value from **1000** to ***2***.
21. Then create 2 copies of this block and position one block each under **set digital pin# 12 HIGH** and **set digital pin# 12 LOW**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_13.png" width="960"/><br>

22. The third **wait 2 milliseconds** blocks position is below the **repeat 50 times** loop in **Arudino loop forever:**.

  **The repeat loop goes 50 cycles of 2 milliseconds on and 2 milliseconds off.**

  **How many milliseconds is this in total?**

23. Now pull the **2** from the **wait 2 milliseconds** block below the repeat loop.

  **We just want buzzers to play briefly on each bar. Therefore it must remain switched off in the meantime.**

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_14.png" width="960"/><br>

24. Now take a **" " + " "** block from **Math** and replace it with the **2** milliseconds block.
25. The symbol of **" " + " "** needs to be changed to ***-*** because we want to subtract the buzzer's playing time from the milliseconds between beats.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_15.png" width="960"/><br>

  **The playing time in milliseconds is 200 (50 cycles * 2 milliseconds on * 2 milliseconds off).**

26. Change the value **2** to ***200*** and put this block in the second position of the **" " - " "** block.
27. Then drag **item** from **Variables** once and put it in the first position of the **" " - 200** block.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_16.png" width="960"/><br>

28. Click on **item** in **item - 200** and select **MilliPB**.

  <img src="images/Lesson6/Lesson6_AktiverBuzzer_17.png" width="960"/><br>

29. Save your project.
30. Verify and upload it to your Arduino.

#### *Try different beats per minute!*

***Don't forget to verify and upload! :-)***

---

#### [Back to Index](#index)

### 3.7. Mini Piano <a name="mini-piano"></a>

>I'll play a little song for you!

In this example we will build a small piano out of 5 push buttons and a passive buzzer, that you can use to set and play different notes.

<img src="images/Lesson7/Lesson7_PassiverBuzzer_1.png" width="960"/>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable**       |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/>   | 1 | **Breadboard**                   |<img src="images/Parts/Part_Breadboard.jpg" width="256"/>   |
| 1 | **Passive Buzzer**                      | <img src="images/Parts/Part_PassiveBuzzer.jpg" width="256"/>    | 5 | **Press Button**            |<img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/>|
| 13 | **M-M Cable**            |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/>| | | |


#### New components

***Passive Buzzer:***

* generates audio signals through a pulse (PWM) that causes the air to vibrate.
* vibrates at the Hertz frequency that produces the respective sound.
* must not use the analog pin as it is fixed at 500Hz.


#### Circuit diagram

<img src="images/Lesson7/Lesson7_PassiverBuzzer_2.png" width="960"/><br>

#### Assembly instructions

1. Passive Buzzer:  **+** and **-** Pin >> Horizontally in the upper middle part
2. 5 Push buttons next to each other (3 holes apart): About dividing line between upper and lower center part
2. M-M Cable 1: Upper middle section above buzzer **+** >> Arduino Pin 4
3. M-M Cable 2: Upper middle section above buzzer **-** >> Top blue row of Breadboard
4. M-M Cable 3: Top blue row of Breadboard >> Arduino GND (Ground)
5. M-M Cable 4: Above the left pin of button 1 in the upper middle section >> Top blue row of Breadboard
6. M-M Cable 5: Above the left pin of button 2 in the upper middle section >> Top blue row of Breadboard
7. M-M Cable 6: Above the left pin of button 3 in the upper middle section >> Top blue row of Breadboard
8. M-M Cable 7: Above the left pin of button 4 in the upper middle section >> Top blue row of Breadboard
9. M-M Cable 8: Above the left pin of button 5 in the upper middle section >> Top blue row of Breadboard
10. M-M Cable 9: Above the right pin of button 1 in the upper middle section >> Arduino Pin 7
11. M-M Cable 10: Above the right pin of button 2 in the upper middle section >> Arduino Pin 8
12. M-M Cable 11: Above the right pin of button 3 in the upper middle section >> Arduino Pin ~9
13. M-M Cable 12: Above the right pin of button 4 in the upper middle section >> Arduino Pin ~11
14. M-M Cable 13: Above the right pin of button 5 in the upper middle section >> Arduino Pin 12

**Compare your version to the schematic photo before we continue.**


#### Block for Block

<img src="images/Lesson7/Lesson7_PassiverBuzzer.png" width="960"/><br>

1. First, drag the **Arduino run first:, Arduino loop forever:** standard function block from **Functions** onto your workspace.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_3.png" width="960"/><br>

  **First, we're going to represent the notes we want to play as variables.**

2. Drag a **set item to** block into the section of run-first loop.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_4.png" width="960"/><br>

3. Next we need a number **0** from **Math** as the value for our variable.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_5.png" width="960"/><br>

4. Rename the variable **item** to ***Note1***.

  ***As before, click on "Item" and select "Rename variable...".***

5. Change the number to ***262***.

  **262 is the frequency for middle C in music.**

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_6.png" width="960"/><br>

  **We now want to determine notes for the other 4 keys.**

6. To do this, copy **Set Note1 to 262** four times.

  ***As before, right click on the block or hold your finger on it for 2 seconds and select "Duplicate".***

7. Then click on **Note1** of each copy and press **New variable...**.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_7.png" width="960"/><br>

8. Click again, this time on **i**, **j**, **k**, **m**. Select **Rename variable...** and change the variables to ***Note2***, ***Note3***, ***Note4*** and ***Note5*** one by one.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_8.png" width="960"/><br>

  **The table below shows the musical notes and their frequencies.**

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_9_NoteFreqList.png" width="960"/><br>

  **We will first use the notes of octave 4. C, D, E, G, A**

9. Change the values of the four new notes to ***294***, ***329***, ***392*** and ***440***.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_9.png" width="960"/><br>

  **Now we will assign the actual keys. First as variables, so the Arduino gets the info as soon as you press one of the buttons.**

10. Drag a **set item to** block from **Variables** into the section of the run-forever loop.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_10.png" width="960"/><br>

  **Now we need the block that reads the digital input that the button is connected to.**

11. Connect a **Read digital with PULL_UP mode pin# 0** block from **Input/Output** to the new variable block in the run-forever loop.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_11.png" width="960"/><br>

12. Rename **item** to ***Key1*** and change the value of pin number **0** to ***7***.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_12.png" width="960"/><br>

13. Now duplicate this block of variables four times and insert them one below the other in the run-forever loop.
14. As with the notes, add new variables and rename them to ***Key2***, ***Key3***, ***Key4***, and ***Key5***.

  **Then you have to assign the keys to the respective pin numbers.**

15. Change the pin numbers of each key block to ***8***, ***9***, ***11*** und ***12***.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_13.png" width="960"/><br>

  **Now we will again use the statement block to play the sound when the key is pressed.**

  ***We will add more conditions so we can get all keys at once. The extensions will result in the following logic: if...else if...else if...else.***

16. Drag the **if do** block from **Logic** below the keys in the run-forever loop.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_14.png" width="960"/><br>

17. Now press the small gear icon and drag **else if** four times from left to right below the **if** block.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_15.png" width="960"/><br>

  **The *if do* block in the run-forever loop adds new sections.***

18. To finish, drag **else** again from left to right below the last **else if**.
19. Close the window by clicking the gear icon again.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_16.png" width="960"/><br>

  **As in the previous example with the active buzzer, we compare the state of the button to see if the current flows when it is pressed.**

20. From **Logic** pull the block **" " = " "** and connect it to **if** of the **if do** block.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_17.png" width="960"/><br>

21. Use a **item** block from **Variables** as the first position of **" " = " "**.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_18.png" width="960"/><br>

22. As the second position of **item = " "** drag in a **HIGH** block from **Input/Output**.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_19.png" width="960"/><br>

23. Change **item = HIGH** to ***Key1 = LOW***.
24. Now copy the **Key1 = LOW** block four times.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_20.png" width="960"/><br>

25. Connect each of the new four copies to an **else if** connection.
26. Then change the variable names to keys 2 - 5.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_21.png" width="960"/><br>

  **Now we want to play the appropriate note when the respective key is pressed.**

27. Go to **Audio**, and drag **Set tone on pin# 0 at frequency 220** to the first **do** section.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_22.png" width="960"/><br>

  **The frequency is set to 220 by default. However, we want to assign our note to each key, which we have already set through variables.**

28. Get an **item** block from **Variables** and put that block in place of **220**.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_23.png" width="960"/><br>

29. Change the pin number **pin#** from **0** to ***4***.

  **The buzzer is connected there.**

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_24.png" width="960"/><br>

30. Now copy **Set tone on pin# 4 to Frequency Note1** four times, and paste each one into one of the **do** sections.
31. Change **Note1** to ***Note2***, ***Note3***, ***Note4*** and ***Note5*** respectively.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_25.png" width="960"/><br>

  **If you try it now, the sound would play continuously after you pressed the button.**

  **That is why we have "else". As soon no button is pressed, no tone will be played.**

32. Drag the block **Turn off tone on pin# 0** in the last section **else**.
33. Again, the pin number must be changed from **0** to ***4***.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_26.png" width="960"/><br>

34. Name your piano project and save it.

  <img src="images/Lesson7/Lesson7_PassiverBuzzer_27.png" width="960"/><br>

35. Compile the project and upload it to your Arduino.

#### *Change the frequencies. If you play with two or three other kids, then it is more fun to play different notes.*

***With blocks of math and time, you can even play a sequence with each note. Just add more "set tone..." and "wait ... milliseconds" blocks to each "do" section of "if" and "else if"***

---

#### [Back to Index](#index)

### 3.8. Sam Says 'Move Your Arm!' <a name="sam-says-move-your-arm"></a>

Suppose you have a robotic arm that you want to move when you move your arm. At a certain incline, the motor will begin to move.

>Dance the robot!

<img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_1.png" width="960"/>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Tilt sensor** |<img src="images/Parts/Part_TiltSensor.jpg" width="256"/> |
| 1 | **Servomotor SG90** | <img src="images/Parts/Part_ServoMotor.jpg" width="256"/> | 3 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/>|
| 2 | **W-M Cable** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/>|


#### New components

***Tilt sensor:***

* detects a current orientation or tilt.
* consists of a cavity with a movable conductive material.
* has electrical poles on one side of the cavity.
* connects the poles through the ground at a certain angle or inclination.
* can perceive rough inclinations and vibrations.

***Servo motor:***

* are motors that can be rotated 180 degrees.
* are controlled by sending electronic impulses.
* determine the position by the received impulses.

#### Circuit diagram

<img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_2.png" width="960"/><br>

#### Assembly instructions

1. W-M Cable 1:  *+* Pol Tilt sensor >> Arduino Pin 2
2. W-M Cable 2:  *-* Pol Tilt sensor >> Arduino GND (Ground)
3. M-M Cable 1:  Data Pin Servermotor >> Arduino Pin ~9
4. M-M Cable 2:  *+* Pol Servomotor >> Arduino 5 V
5. M-M Cable 3:  *-* Pol Servomotor >> Arduino GND (Ground)

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor.png" width="960"/><br>

1. Drag and drop the basic function block onto your workspace with run-first and run-forever loop.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_3.png" width="960"/><br>

  **Let's set up the servo motor and give it a starting position.**

2. To do this, drag a **set SERVO from pin 0 to 90 Degrees** block into the run-first loop section of the basic function block.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_4.png" width="960"/><br>

3. Set the pin number to ***9***.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_5.png" width="960"/><br>

  **As with the push button, we want to set the switching through the tilt sensor as a variable.**

4. Go to **Variables** and drag a **set item to** block into the run-forever loop.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_6.png" width="960"/><br>

5. Then pull a **Read digital pin# 0** block from **Input/Output** and connect this block to **Set item to**.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_7.png" width="960"/><br>

6. Rename **item** to ***Tilt*** and select pin number ***2***.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_8.png" width="960"/><br>

  **As in the previous example, we will use a statement method to turn on the motor as soon as the tilt sensor allows current to flow.**

7. Get an **if do** block from **Logic** and add it to the run-forever loop below the **Tilt** variable.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_9.png" width="960"/><br>

  **We need a second section "else" because we want to reverse the motor when the switch is off.**

8. To do this, click the gear icon again, and drag an **else** block from left to right and connect it with **if**.

  ***Click the gear icon again to close the window.***

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_10.png" width="960"/><br>

  **Again we want to check the status of the switch.**

9. So pull a **" " = " "** block out of **Logic** and connect it with the **if** connection.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_11.png" width="960"/><br>

10. Next, put an **item** block from **Variables** in the first position of **" " = " "**.
11. For **item** choose ***Tilt*** from the menu.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_12.png" width="960"/><br>

12. And again let's get in **Input/Output** the block **HIGH** for the second position of **Tilt = " "**.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_13.png" width="960"/><br>

  **When the tilt sensor turns on, the motor should turn 90 degrees. Otherwise it should move back to the starting position.**

13. Duplicate the **set SERVO from pin 9 to 90 Degrees** block twice, and put one block each in the **do** and **else** sections.
14. Change **90** of the block in **do** to ***180***.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_14.png" width="960"/><br>

  **Finally, the loop should wait half a second before re-fetching the tilt angle status. This can be used to bridge small shocks.**

15. Put a **wait 1000 milliseconds** block below **set SERVO from pin 9 to 180 Degrees** in the **do** section.
16. Change the time from **1000** to ***500*** milliseconds.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_15.png" width="960"/><br>

17. Name your project and save the block code.

  <img src="images/Lesson8_9/Lesson8_9_NeigungssensorMotor_16.png" width="960"/><br>

18. Compile the code and upload it to your Arduino.


#### *Dance the robot!*

---

#### [Back to Index](#index)

### 3.9. Lets Make Some More Music! <a name="lets-make-some-more-music"></a>

This time we want to build a new musical instrument that you play in the air with your free hands or with a piece of paper. Your movement is going to change the pitch.

<img src="images/Lesson10/Lesson10_UltraschallSensor_1.png" width="960"/><br>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable**  |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Passive Buzzer** |<img src="images/Parts/Part_PassiveBuzzer.jpg" width="256"/> |
| 1 | **Ultrasonic Sensormodule**  | <img src="images/Parts/Part_UltraSonicSensor.jpg" width="256"/> | 6 | **W-M Cable** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> |


#### New components

***Ultrasonic sensor module:***

* can measure distances from 2-400 cm with an accuracy up to 3mm.
* includes a transmitter, receiver and control circuitry.
* emits 40 kHz sound pulses and checks for a return signal.
* measures the distance by relating the speed of sound to half the time it takes for the signal to be received again.


#### Circuit diagram

<img src="images/Lesson10/Lesson10_UltraschallSensor_2_Schematic.png" width="960"/><br>

#### Assembly instructions

1. W-M Cable 1: *+* Pol Passive Buzzer >> Arduino Pin 4
2. W-M Cable 2: *-* Pol Passive Buzzer >> Arduino GND (Ground)
3. W-M Cable 3: VCC Pin Ultrasonic sensor module >> Arduino 5V
4. W-M Cable 4: Trig Pin Ultrasonic sensor module >> Arduino Pin 12
5. W-M Cable 5: Echo Pin Ultrasonic sensor module >> Arduino Pin ~11
6. W-M Cable 6: GND Pin Ultrasonic sensor module >> Arduino GND (Erde)

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson10/Lesson10_UltraschallSensor.png" width="960"/><br>

1. Start with the basic function from **Functions** containing the run-first and run-forever loop.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_3.png" width="960"/><br>

2. Drag a **Set HC-SR04 with Echo Pin# 0 and Trigger Pin# 0** block from **Sensors** and place it in the header part of the base block.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_4.png" width="960"/><br>

3. Set the echo pin number to ***11*** and the trigger pin number to ***12***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_5.png" width="960"/><br>

  **As a further value we want to set the start frequency of the musical instrument with a variable.**

4. Get a **set item to** block from **Variables** and put it in front of the basic block.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_6.png" width="960"/><br>

5. Rename the variable to ***StartFreq***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_7.png" width="960"/><br>

6. Now get a **0** block from **Math** and connect it with **set StartFreq to**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_8.png" width="960"/><br>

7. Define the frequency by entering number ***400***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_9.png" width="960"/><br>

  **Now we want to update the measured distance from the ultrasonic sensor as a variable.**

8. Draw a new **set item to** block from **Variables** and insert it into the run-forever loop.
9. Rename **item** to ***a***.
10. Go to **Sensors** and get **Read HC-SR04 with Echo Pin# 0** to connect this to **set a to**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_10.png" width="960"/><br>

11. Adjust the echo pin number to ***11***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_11.png" width="960"/><br>

  **Now let's relate the measured distance to a range of frequencies.**

  **This means that the buzzer will not play the measured distance as a frequency, but a higher number relatively to distance.**

12. Here, too, take a **set item to** block from **Variables** as a basis and position it in the run-forever loop.
13. Name this variable ***Note***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_12.png" width="960"/><br>

14. To create the ratio, go to **Math** and connect a **Map** block to **set Note to**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_13.png" width="960"/><br>

  **Map has 5 values or variables with values. The 1st value is the value to be compared with. The 2nd and 3rd values describe the range in which the 1st value will move. The 4th and 5th value is the range in which the 1st value is to be mapped.**

  ***Think of it like a geographical map. There is a scale in which 1 cm on the map is actually 1 km in reality. The area of the map itself is maybe from 0 - 50 cm. Relative to this, the real landscape is 0 - 50 km. The position of your finger tapping on the map at 20 cm is relatively 20 km in reality.***

  **The first value will be the variable a.**

15. Take an **item** block from **Variables** and put it in the first position of **Map**.
16. Choose **a** as the variable name from the menu.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_14.png" width="960"/><br>

  **The 2nd and 3rd value should be the distance you use to make music. For the example you will use the space between 10 - 150 cm, where you can move your hand or paper to make sounds.**

17. Use two **0** blocks of **Math** as 2nd and 3rd position of **Map**.
18. Enter ***10*** as the lowest range value and ***150*** as the highest range value.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_15.png" width="960"/><br>

  **We want to make the 4th and 5th values a bit more flexible so that you can change the frequency more easily later.**

19. Again, get an **item** block from **Variables** and choose the name ***StartFreq*** from the Variables menu.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_16.png" width="960"/><br>

20. Duplicate **StartFreq** once and position this block in the 4th position of **Map**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_17.png" width="960"/><br>

  **In order for the range to shift as the starting frequency changes, we will use a math equation.**

  **In the example we will double the frequency so that we have a full octave range with our instrument.**

21. Put a **" " + " "** block from **Math** as the 5th value of **Map**.
22. Then grab the loose **StartFreq** block left on your workspace and paste it into the first position of **" " + " "**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_18.png" width="960"/><br>

23. Click the **+** icon and choose ***\**** instead.
24. Then use a new **0** block from **Math** as the second position of **StartFreq * " "***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_19.png" width="960"/><br>

25. Change the value from **0** to ***2***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_20.png" width="960"/><br>

  **From this you will get the frequency as a number. Now you're going to feed the number into the buzzer to make the sound.**

  **In addition, the buzzer should stop when your hands or paper are not in the playing area.**

  ***As with the push button and tilt sensor, we will use an if/else statement.***

26. Draw an **if do** block from **Logic** and place it as the bottom block of the run-forever loop.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_21.png" width="960"/><br>

27. Press the gear icon, then draw a **else if** and then a **else** block from left to right.

  ***Close the window by pressing the gear icon again.***

  <img src="images/Lesson10/Lesson10_UltraschallSensor_23.png" width="960"/><br>

28. Again connect a **" " = " "** block from **Logic** with **if**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_24.png" width="960"/><br>

29. Draw a **item** block from **Variables** and use that as the first position of **" " = " "**.
30. Choose ***a*** as variable name.
31. Then draw a **0** block from **Math** for the second position of **a = " "**, and fill in the number ***10***.
32. Now click on the **=** icon and change it to ***<***.

  ***Everything that is less than 10 cm should not produce any sound.***

  <img src="images/Lesson10/Lesson10_UltraschallSensor_25.png" width="960"/><br>

33. Duplicate **a < 10**, and connect the copy to **else if**.
34. Then change the symbol **<** to ***>*** and the value from **10** to ***150***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_26.png" width="960"/><br>

  **To complete this part, we need the block that turns off the sound.**

35. Get a **Turn tone off on pin# 0** from **Audio** and put it in the first **do** section.
36. Define the pin number of the connected buzzer with ***4***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_27.png" width="960"/><br>

37. Duplicate **Turn tone off on pin# 4** and place the copy in the second **do** section.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_28.png" width="960"/><br>

  **Now let's use the actual sound maker.**

38. Drag a **Set tone on pin# 0 by frequency 220** into the **else** section.
39. Change the pin number to ***4***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_29.png" width="960"/><br>

  **Now we get the calculated frequency through the variable Note.**

40. Replace **220** with a **item** block from **Variables**.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_30.png" width="960"/><br>

41. For **item** choose ***Note*** from the variable menu.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_31.png" width="960"/><br>

  **The buzzer works better if you give some time to measure the distance and let the sound play a bit.**

42. Draw a **wait 1000 milliseconds** from **Time**.
43. Reduce time from **1000** to ***100***.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_32.png" width="960"/><br>

44. Give the musical instrument project a name and save it.

  <img src="images/Lesson10/Lesson10_UltraschallSensor_33.png" width="960"/><br>

45. Compile the code and upload it to your Arduino.


#### *Try different starting frequencies, such as half or a quarter apart.*

---

#### [Back to Index](#index)

### 3.10. Sounds Like A Phone! <a name="sounds-like-a-phone"></a>

In the following exercise we will decorate 3 keys of a membrane keypad with 3 different tones.

<img src="images/Lesson11/Lesson11_MembraneKeypad_1.png" width="960"/><br>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Passive Buzzer** |<img src="images/Parts/Part_PassiveBuzzer.jpg" width="256"/> |
| 1 | **4 x 4 Membran Keypad** | <img src="images/Parts/Part_4x4MembraneKeypad.jpg" width="256"/> | 2 | **W-M Cable** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> |
| 8 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***4 x 4 Membrane Keypad:***

* is a membrane keypad used in telephones, fax machines, microwave ovens, ovens, door locks, etc.
* has 16 keys (0-9, A-D and * and #) but only 8 ports. Comparable to a table with rows and columns, the keyboard can be mapped with half the connections.

#### Circuit diagram

<img src="images/Lesson11/Lesson11_MembraneKeypad_2.png" width="960"/><br>

#### Assembly instructions

1. W-M Cable 1: *+* Pol Passiver Buzzer >> Arduino Pin 12
2. W-M Cable 2: *-* Pol Passiver Buzzer >> Arduino GND (Ground)
3. M-M Cable 1: Membrane Keypad Pin 1 >> Arduino Pin ~9
4. M-M Cable 2: Membrane Keypad Pin 2 >> Arduino Pin 8
5. M-M Cable 3: Membrane Keypad Pin 3 >> Arduino Pin 7
6. M-M Cable 4: Membrane Keypad Pin 4 >> Arduino Pin ~6
7. M-M Cable 5: Membrane Keypad Pin 5 >> Arduino Pin ~5
8. M-M Cable 6: Membrane Keypad Pin 6 >> Arduino Pin 4
9. M-M Cable 7: Membrane Keypad Pin 7 >> Arduino Pin ~3
10. M-M Cable 8: Membrane Keypad Pin 8 >> Arduino Pin 2

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson11/Lesson11_MembraneKeypad.png" width="960"/><br>

1. As always, start with the basic function containing the run-first and run-forever loops.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_3.png" width="960"/><br>

  **The membrane keypad requires 8 inputs or 8 pin assignments to function.**

2. Drag a **Setup 4x4 Membrane Keypad** block from **Input/Output** into the run-first loop.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_4.png" width="960"/><br>

3. Click on the respective row assignments (row) and assign ***6**, ***7***, ***8*** and ***9***.
4. Then assign ***2***, ***3***, ***4*** and ***5*** to the columns (col).

  <img src="images/Lesson11/Lesson11_MembraneKeypad_5.png" width="960"/><br>

5. Now drag a **set item to " " as Character** from **Variables** into the run-forever loop.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_6.png" width="960"/><br>

6. Rename **item** to ***Key*** and **Character** to ***Text***.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_7.png" width="960"/><br>

7. Drag a **Receive key from mkpad4x4 on row 1 pin#** from **Input/Output** to " " from **set Key to " " as Text**.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_8.png" width="960"/><br>

8. Change the pin number from **Receive key from mkpad4x4 on row 1 pin#** to ***6***.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_9.png" width="960"/><br>

  **Now we use a statement function again to assign a specific task to the respective keys when they are pressed.**

9. To do this, drag an **if do** block from **Logic** into the run-forever loop.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_10.png" width="960"/><br>

  **In our example, we will only assign tasks to 3 keys.**

  ***You can add more buttons yourself, just add more of the "else if" sections!***

10. Click on the gear icon of the **if do** function, and drag two **else if** and one **else** block to the right below **if**.

  ***Close the menu by clicking the gear icon again!***

  <img src="images/Lesson11/Lesson11_MembraneKeypad_11.png" width="960"/><br>

11. Draw a **" " = " "** block from **Logic** and connect it with **if**.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_12.png" width="960"/><br>

12. Now drag a **item** block from **Variables** to the first position of **" " = " "**.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_13.png" width="960"/><br>

  **We want to compare the pressed key of the membrane keypad with the number or character as text.**

13. Draw a **" "** block from **Text** and set it as the second position of **item = " "**.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_14.png" width="960"/><br>

14. Change **item** to ***Key***, and fill in a 1 into the **" "** text block.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_15.png" width="960"/><br>

  **As with a cell phone, let's make a sound when the key "1" on the membrane keypad is pressed.**

15. To do this, drag the **Set tone on pin# 0 to frequency 220** from **Audio** to the first **do** section.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_16.png" width="960"/><br>

  **The sound should remain switched off if no button on the membrane keyboard is pressed.**

16. Drag a **Turn off tone on pin# 0** from **Audio** and put it in the last section **else**.
17. Change both pin numbers to ***12***.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_17.png" width="960"/><br>

  **The tone should sound for a short time, even if you stop pressing the button.**

18. Draw a **wait 1000 milliseconds** from **Time** below the **Set tone on pin# 12 to frequency 220** block.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_18.png" width="960"/><br>

19. Decrease the value **1000** to ***100***.
20. Now copy everything under **if** and **do** twice and insert it into the respective **else if** connection and its **do** section.
21. Instead of **1** in the text of the **else if** connection, enter ***2*** and ***3*** respectively.
22. Fill in the frequency ***680*** for **2** and ***880*** for **3** of the **Set ton on pin# 12 to frequency 220** blocks.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_19.png" width="960"/><br>

23. Name your project, compile it and upload it to your Arduino.

  <img src="images/Lesson11/Lesson11_MembraneKeypad_20.png" width="960"/><br>

  #### *Add more number keys to the "if do" statement function, and make a little melody out of it!*


---

#### [Back to Index](#index)

### 3.11. How Hot Is It? <a name="how-hot-is-it"></a>

In this exercise, you will learn about a module that allows you to measure air temperature and humidity.

Here we will only use the temperature measurement only.

>Breathe red and blow blue!

<img src="images/Lesson12/Lesson12_TemperatureSensor_1.png" width="960"/><br>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **RGB LED** | <img src="images/Parts/Part_LED_RGB_5mm.jpg" width="256"/> | 3 | **220 Ohm Resistor** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>|
| 1 | **DHT11 Temperature and humidity sensor** |<img src="images/Parts/Part_DHT11_TemperatureHumiditySensor.jpg" width="256"/>| 7 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***DHT11 Temperature and humidity sensor:***

* is a compound sensor that outputs a digital signal with the values for temperature and humidity.
* has an NTC temperature gauge connected to an 8-bit micro controller.
* are used in heaters, ventilators, air conditioners, dehumidifiers, test and inspection equipment, automobile manufacturing, weather stations, home appliances, humidity regulators, and medical devices.


#### Circuit diagram

<img src="images/Lesson12/Lesson12_TemperatureSensor_2.png" width="960"/><br>

#### Assembly instructions

1. RGB LED: 4 LED wire legs >> side by side in the lower middle section.
2. M-M Cable 1: Center section hole in front of long wire leg >> Arduino GND (Ground)
3. 3 Resistors: In front of each foot in the lower middle part >> Longitudinal position in the upper middle part
4. M-M Cable 2: Before resistor to the right of the GND cable in the upper middle part >> Arduino ~6
5. M-M Cable 3: Before resistor to the left of the GND cable in the upper middle part >> Arduino ~5
6. M-M Cable 4: In front of the resistor to the left of the 3rd cable in the upper middle section >> Arduino ~3
7. DHT11 Modul: Side by side in the lower middle section.
8. M-M Cable 5: Above the data pin in the lower center section >> Arduino Pin 2
9. M-M Cable 6: Above the VCC pin (center) in the lower center section >> Arduino 5V
10. M-M Cable 7: Above the GND pin in the lower center section >> Arduino GND

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson12/Lesson12_TemperatureSensor.png" width="960"/><br>

**This time we start with the sensor block. Setup blocks can also be initiated outside on top of the base function.**

1. Get a **Setup DHT11 Sensor with Pin# 0** from **Sensors** onto your workspace.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_3.png" width="960"/><br>

2. Set the pin number to ***2***.
3. Then get the basic function with run-first and run-forever loop from **Functions**.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_4.png" width="960"/><br>

  **Let's initiate the three diodes of the RGB LED.**

4. To do this, drag a **Set digital pin# 0 to HIGH** from **Input/Output** into the run-first loop.
5. Choose the pin number ***3***.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_5.png" width="960"/><br>

6. Duplicate this block twice and place them one below the other in the run-first loop of the basic function.
7. Change the pin numbers to ***5*** and ***6*** while the initiated status becomes ***LOW*** in both of these cases.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_6.png" width="960"/><br>

  **We want to adjust the color produced by the RGB LED in each loop using a variable.**

8. Drag a block **Set analog pin# 0 to** from **Input/Output** onto your workspace.
9. Change the pin number to ***3***.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_7.png" width="960"/><br>

10. Now pull **item** from **Variables** and put it in the connection of **Set analog pin# 3 to**.
11. Rename **item** to ***blue***.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_8.png" width="960"/><br>

12. Repeat the steps 8. - 11. twice. Use pin number ***5*** with variable name ***Green*** and pin number ***6*** with variable name **Red** the second time.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_9.png" width="960"/><br>

  **Now we want to measure the temperature and adjust the color of the RGB LED accordingly.**

  ***Cold >> Blue and Warm >> Red***

13. Get a **Read Temperature with DHT11 from Pin# 0** from **Sensors** and drag it onto your workspace.
14. Change the pin number to ***2***.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_10.png" width="960"/><br>

  **We will set the measurement as a variable.**

15. Take a **set item to " " as Character** block from **Variables** onto your workspace.
16. Drag **Read Temperature with DHT11 from Pin# 2** as **" "** from **set item to " " as Character**.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_12.png" width="960"/><br>

17. Now change **Character** to ***Decimal*** in **set item to Read Temperature with DHT11 from Pin# 2 as Character**.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_13.png" width="960"/><br>

  **Now follows the actual adaptation of the measured temperature to the light intensity of the respective color LED.**

18. Drag a **set item to** block onto your workspace.
19. Name the variable ***Red***.
20. Then duplicate this block twice, create new variables and name them ***Green*** and ***Blue***.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_14.png" width="960"/><br>

  **Since the color is intended to move between blue and red, the green LED will always remain off. Therefore the analog value can be statically 0.**

21. Draw a **0** block from **Math** and connect it to **set Green to**.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_15.png" width="960"/><br>

22. Get a **Map " " fromLow 0 fromHigh 100 toLow 0 toHigh 1000** block for each of the remaining 2 colors and connect them with **set Red to** and **set Blue to** respectively.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_16.png" width="960"/><br>

  **The temperature between 20 and 30 degrees Celsius should determine the brightness between 0 (Off) and 255 (Brightest).**

23. Set the values of the variable **Red** to ***fromLow: 20 fromHigh 30 toLow 0 toHigh 255*** and the variable **Blue** to ***fromLow: 20 fromHigh: 30 toLow: 255 toHigh: 0*** .

  <img src="images/Lesson12/Lesson12_TemperatureSensor_17.png" width="960"/><br>

24. Drag a **item** block from **Variables** into the map value (1st) of **Red** and change it to ***Temperature***.
25. Repeat 24. for **set Blue to**.
26. Drag the entire block of the 3 **set...** blocks as to lowest rows of the run-forever loop.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_18.png" width="960"/><br>

27. Finally, drag another **wait 1000 milliseconds** block into the run-forever loop.

  <img src="images/Lesson12/Lesson12_TemperatureSensor_19.png" width="960"/><br>

28. Compile the code and upload it to the Arduino.
29. Name and save your project.


#### *Breathe in warm breath or blow on the sensor! How about other colors?*

---

#### [Back to Index](#index)

### 3.12. Left, Right, Back And Forth <a name="left-right-back-forth"></a>

This example introduces the joystick, which is also used in game consoles. Instead of a game character, we will represent movement using different colored LEDs.


<img src="images/Lesson13/Lesson13_AnalogJoystick_1.png" width="960"/><br>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Red LED** | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/> | 1 | **Green LED** | <img src="images/Parts/Part_LED_Green_5mm.jpg" width="256"/> |
| 1 | **Yellow LED** | <img src="images/Parts/Part_LED_Yellow_5mm.jpg" width="256"/> | 1 | **Blue LED** | <img src="images/Parts/Part_LED_Blue_5mm.jpg" width="256"/> |
| 4 | **220 Ohm Resistor** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>| 1 | **Joystick Module** |<img src="images/Parts/Part_Analog_Joystick.jpg" width="256"/> |
| 5 | **W-M Cable** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> | 9 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***Joystick Module:***

* has 5 pins, power, ground, X axis, Y axis and push button.
* is analogue and allows precise horizontal and vertical control.
* an integrated pressure switch enables additional functionality.

#### Circuit diagram

<img src="images/Lesson13/Lesson13_AnalogJoystick_2.png" width="960"/><br>

#### Assembly instructions

1. W-M Cable 1: Joystick GND Pin >> Arduino Pin GND
2. W-M Cable 2: Joystick 5V Pin >> Arduino Pin 5V
3. W-M Cable 3: Joystick VRx Pin >> Arduino Pin A0
4. W-M Cable 4: Joystick VRy Pin >> Arduino Pin A1
5. W-M Cable 5: Joystick SW Pin >> Arduino Pin 2
6. Yellow LED: Left side with long leg (+) in upper middle part and short leg in lower part
7. Blue LED: Right side with long leg (+) in upper middle part and short leg in lower part
8. Green LED: In the middle of the upper middle section, in the same row, short leg on the left and long leg on the right
9. Red LED: In the middle of the lower middle section, in the same row, short leg on the left and long leg on the right
10. Widerstand 220 Ohm: In each case above the right leg of the LED >> Same line 4 - 8 plug holes to the right
11. M-M Cable 1: Top blue row of Breadboard >> Arduino GND (Ground)
12. M-M Cable 2: Below the leg of the yellow LED in the lower center section >> Top blue row of Breadboard
13. M-M Cable 3: Below the left leg of the red LED in the lower center section >> Top blue row of Breadboard
14. M-M Cable 4: Below the leg of the blue LED in the lower center section >> Top blue row of Breadboard
15. M-M Cable 5: Above the left leg of the green LED in the upper middle section >> Top blue row of Breadboard
16. M-M Cable 6: Above the free leg of the yellow LED resistor in the upper center section >> Arduino Pin 7
17. M-M Cable 7: Above the free leg of the blue LED resistor in the top center section >> Arduino Pin 8
18. M-M Cable 8: Above the free leg of the blue LED resistor in the top center section >> Arduino Pin 12
19. M-M Cable 9: Above the free leg of the green LED resistor in the top center section >> Arduino Pin 13


**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson13/Lesson13_AnalogJoystick.png" width="960"/><br>

1. To start, drag the base function with run-first and run-forever loop from **Functions** onto your workspace.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_3.png" width="960"/><br>

  **We will use 4 LEDs for the directions. Let's initiate these next.**

2. To do this, drag a **Set digital pin# 0 to HIGH** block from **Input/Output** 4 times into the run-first loop.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_4.png" width="960"/><br>

3. Set the pin numbers to ***7***, ***8***, ***12*** and ***13***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_5.png" width="960"/><br>

  **The joystick is analogue and works in a similar way as 2 potentiometers, which have the middle position as the starting point. So forward and backward is like one potentiometer, left and right is like another potentiometer.**

4. Drag a block **set item to " " as Character** from **Variables** onto your workspace.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_6.png" width="960"/><br>

5. Rename **item** to ***FB*** (Forward/Backward).
6. Then fill **" "** with a **Read analog pin#** block from **Input/Output**.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_7.png" width="960"/><br>

7. Choose the type ***Number*** instead of **Character** and drag the block into the run-forever loop of the base function.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_8.png" width="960"/><br>

8. Duplicate the block, create a new variable with **New variable...** and name it ***LR*** (Left/Right).
9. Change the pin number to ***A1***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_9.png" width="960"/><br>

  **Now we will use the "if do" statement function you are familiar with. We need four sections for each direction, one for button press and one section for other.**

10. Get an **if do** block from **Logic** and put it in the run-forever loop.
11. Now press the gear icon, drag and connect 4 **else if** and 1 **else** block to the right side underneath **if**.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_10.png" width="960"/><br>

12. Duplicate each **Set digital pin#...** block 3 times in the run-first loop.
13. Put the first copy from the block with pin number **7** in the first **do** section, the first copy from the block with pin number **8** in the second **do** section, the first copy from the block with pin number **12** in the third **do** section and the first copy of the block with pin number **13** in the fourth **do** section.
14. Put all second copies into the fifth **do** section.
15. Put all third copies in the **else** section and change all **HIGH** values to ***LOW***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_11.png" width="960"/><br>

  **The four directions have two potentiometers, each having a data range of 0 - 1023 (1024 total). So the resting position for both of them is 511. Anything above 511 is one direction and below is the other direction.**

  **We're going to set a range in which the LED of one direction will light up.**

16. Take a **" " and " "** block **Logic** and connect it with **if**.
17. Then drag 2 **" " + " "** blocks from **Logic** and put each in the first and second position of **" " and " "**.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_13.png" width="960"/><br>

18. Get two **0** blocks of **Math** into your scene and put them in the second position of each **" " + " "** block.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_14.png" width="960"/><br>

19. Now change the first **+** symbol to ***>*** and the second **+** symbol to ***<=***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_16.png" width="960"/><br>

20. Instead of **0** of the **" " > 0** block use ***530***.

  ***The number is higher than the middle position, since the value can fluctuate a little, especially with an analog input. To avoid a lost contact effect, the number is set higher or lower respectively.***

21. Replace **0** of the **" " <= 0** block with ***1023***.
22. For the first position of **" " > 530** and **" " <= 1023**, draw a **item** block from **Variables**.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_17.png" width="960"/><br>

23. Choose ***LR*** as variable name both times.
24. Duplicate the **LR > 530 and LR <= 1023** block and assign the copy to the first **else if** connector.

<img src="images/Lesson13/Lesson13_AnalogJoystick_19.png" width="960"/><br>

25. Modify the symbols and numbers of the block to ***LR < 500 and LR >= 0***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_21.png" width="960"/><br>

26. Make a copy of each of **LR > 530 and LR <= 1023** and **LR < 500 and LR >= 0** and connect those in the same order to the second and third **else if** nodes.
27. Now change all 4 **LR** of the 2 copies to ***FB***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_22.png" width="960"/><br>

  **This should light up the LEDs when we move the joystick knob. But when you press it, all the LEDs should light up.**

28. Drag a new **" " = " "** block from the **Logic** category and assign it to the fourth **else if** connection.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_23.png" width="960"/><br>

29. As the first position of the **" " = " "** we use the **Read digital with PULL_UP mode pin# 0** block from **Input/Output**.
30. Set the pin number ***2***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_24.png" width="960"/><br>

31. As the second position of **Read digital with PULL_UP mode pin# 2 = " "** use a **HIGH** block from **Input/Output**.
32. Change the value from **HIGH** to ***LOW***.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_25.png" width="960"/><br>

33. Name your project and save your sketch.

  <img src="images/Lesson13/Lesson13_AnalogJoystick_26.png" width="960"/><br>

34. Compile the code and upload it to your Arduino.

#### *Game on!!!*

---

#### [Back to Index](#index)

### 3.13. The Starlight Show <a name="star-light-show"></a>

In this project you will learn how to use a remote control to operate a mini display that displays your own small graphics.

>Give me a face and make me smile!

We will only associate 3 graphics in the exercise with 3 buttons on the remote control. You always can expand this on your own.


<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_1.png" width="960"/><br>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Prototyp expansion module** |<img src="images/Parts/Part_PrototypeExpansionModule.jpg" width="256"/> |
| 1 | **InfraRed Module** | <img src="images/Parts/Part_IRModule.jpg" width="256"/> | 1 | **Remote control** |<img src="images/Parts/Part_IRRemoteControl.jpg" width="256"/>|
| 1 | **MAX7219 Module** |<img src="images/Parts/Part_8x8DotMatrixMAX7219.jpg" width="256"/>| 6 | **W-M Cable** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> |
| 3 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***Prototyp Expansion Module:***

* is a plug-able prototype board for the Arduino Uno that comes with a mini breadboard.
* has all Arduino pins + 5 extra GND (Ground) slots + 5 extra 5V slots available.

***InfraRed Module with Remote Control:***

* are small microchips with an infrared receiving photocell.
* often used in TVs and DVD players.
* has a transmitter known as a remote control that broadcasts on a frequency of 38kHz.
* receives infrared patterns from the remote control, which recognizes the message like Morse code and switches the TV on or off, for example.

***MAX7219 8x8 LED Matrix:***

* is a serial display driver that can control a display with up to 8 segments, a bar graph or 64 individual LEDs.
* is arranged as an 8x8 matrix with the already mentioned 64 LEDs, which can be controlled individually.


#### Circuit diagram

<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_2.png" width="960"/><br>

#### Assembly instructions

1. Prototyp Expansion Module: Plugged into matching pin plug holes
2. InfraRed Module: Plugged in a row in the lower area of the mini plug-in board with the diode pointing outwards
3. 3 Resistors: In front of each foot in the upper middle section >> Longitudinal position in the lower middle section
4. M-M Cable 1: IR Module GND >> Arduino GND (Expansion module)
5. M-M Cable 2: IR Module VCC >> Arduino 5V (Expansion modulel)
6. M-M Cable 3: IR Module DATA >> Arduino 13 (Expansion module)
7. W-M Cable 1: MAX7219 VCC >> Arduino 5V (Expansion module)
8. W-M Cable 2: MAX7219 GND >> Arduino GND (Expansion module)
9. W-M Cable 3: MAX7219 DIN >> Arduino 12 (Expansion module)
10. W-M Cable 4: MAX7219 CS >> Arduino 11 (Expansion module)
11. W-M Cable 5: MAX7219 CLK >> Arduino 10 (EExpansion module)

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix.png" width="960"/><br>

1. Of course, we will also start here with the basic function.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_3.png" width="960"/><br>

2. Drag a **Setup IR Remote Control with Pin# 0** from **Input/Output** into the run-first loop.
3. Change the pin number to ***13***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_4.png" width="960"/><br>

  **Now we're going to capture the pressed remote control button as a variable.**

4. Take a **set item to** block from **Variables** and insert it into the run-forever loop.
5. Rename the **item** variable to ***Button***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_5.png" width="960"/><br>

6. Get the **Read value from IR Remote Control on Pin# 0** block from **Input/Output** and connect it to **set Button to**.
7. Change the pin number to ***13***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_7.png" width="960"/><br>

8. Now pull an **if do** block out of **Logic** and add it to the run-forever loop.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_8.png" width="960"/><br>

9. Use the gear icon again to add two **else if** blocks.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_9.png" width="960"/><br>

  **You want to compare, if the pressed button is equal to a certain number.**

10. In **Logic** you will find the **" " = " "** block that you can use for this. Link it to the **if** connection.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_10.png" width="960"/><br>

11. Next fill in the first position of **" " = " "** with a **item** block from **Variables**.
12. For **item** choose ***Button***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_11.png" width="960"/><br>

13. Get a **0** block from **Math** and drag it to the second position of **Button = " "**.
14. Change the value to ***1***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_12.png" width="960"/><br>

  **The MAZ7219 display has its own blocks that regulate the basic setting and the display as well as clearing the display.**

15. From **Displays** drag the block **Setup MAX7219 on DIN Pin...** and place it in the run-first loop.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_13.png" width="960"/><br>

16. Change the pin numbers to ***DIN: 12***, ***CS: 11*** and ***CLK: 10***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_14.png" width="960"/><br>

17. Drag a block **item Set Character MAX7219 DIN Pin# 0** from **Displays** to the first **do** section.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_15.png" width="960"/><br>

18. Rename **item** to ***Heart*** and select pin number ***12***.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_16.png" width="960"/><br>

19. Make 2 copies of the block connected with **if** and associate one of each with an **else if** connection.
20. Change the numbers to ***2*** and ***3***.

  ***With this we will display our characters when you press 1 2 or 3 on the remote control.***

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_17.png" width="960"/><br>

  **The character should be displayed for a few seconds. After that, the display should be deleted.**

21. Draw a **wait 1000 milliseconds** block from **Time** and place the block below **Heart Set Character...**.
22. Increase the value to ***3000*** (3 seconds).

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_18.png" width="960"/><br>

23. To clear the display, drag a **Clear Display MAX7219 DIN Pin# 0** block below the **if do** function into the run-forever loop.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_19.png" width="960"/><br>

24. Pick the pin number ***12***.
25. Make 2 copies each of **Heart Set Character...** and the time block and put them in the second and third column after **do**.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_20.png" width="960"/><br>

26. Click on **Heart** and select **New Variable...**, then **Rename Variable...** to name the second character ***Smile*** and the third character ***A***.
27. Now create the characters for all 3 blocks based on the picture below.

  ***When you click on the checkbox fields, you create small ticks. Each tick will illuminate a LED on your display.***

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_21.png" width="960"/><br>

28. Name your project and save it.

  <img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_22.png" width="960"/><br>

29. Compile the code and upload it to your Arduino.

**Press number 1, 2 and 3 in a row.**

>Ahhhhhh...now I have a smile!

#### *Add your own pictures to the code!*

***The photo below shows the number assignment for your remote control:***

<img src="images/Lesson14/Part_IRRemoteControl.jpg" width="512"/><br>

---

#### [Back to Index](#index)

### 3.14. Animated Animators <a name="get-animated"></a>

As an extension, there are 3 more examples, which open up further possibilities for you.

But here I only give you a few hints and the pictures.

#### *Try to find out these examples yourself.*

Use the previous project as a template.

**Don't forget to choose a different name immediately after you opened the template project. Don't lose it!**

#### On/Off

* In this example you need another **else if** block in the **if do** function.
* Address it to number **10**, which represents the **OFF** button on the remote control.
* Delete the time blocks from all **do** sections.
* Drag the **Clear Display...** into the new column of the **if do** function.

<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_23_OnOff.png" width="960"/><br>

#### Circle animation

* In this example we add a new condition function that belongs to the Boolean logic.
* For example, in conjunction with an **if do** function, something will only be done if it is marked as **true**.
* The animation will only play if a variable has been set to **true**.
* So first create a variable called **Animation**
* Then connect the variable to a **true** block from **Logic**.
* The starting position is **false**.
* If you're using the template project, drag the character and time blocks from the **if do** onto the workspace next to it.
* Reduce the **else if** section to just one extra section.
* Choose a the button assignments ***1*** and ***10*** to switch on/off the animation.
* Add each copy of **set Animation to false** to each **do** section.
* Under Key Number **1** change **false** to ***true***.
* Under key number **10** add the **Clear Display...** block.
* Add a new **if do** block to the run-forever loop.
* Under **if** create a block ***Animation = true***.
* Drag all three character and time blocks into the **do** section.
* Change the names to ***Circle1***, ***Circle2***, and ***Circle3***.
* Follow the tick cast to the image below or try something else.
* The time must be reduced to ***90*** milliseconds in order to make it to an animation.

***This time determines the speed of the animation.***

**Don't forget to save this example with your own name.**

<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_23_Animation.png" width="960"/><br>

#### Space Invaders

* Use the previous example as a template.
* Delete a character block and a time block.
* Increase the time to ***200*** milliseconds.
* Then draw the familiar two poses of the character (see picture below).

<img src="images/Lesson14/Lesson14_IRRemoteLEDMatrix_23_SpaceInvaders.png" width="960"/><br>


---

#### [Back to Index](#index)

### 3.15. Round And Round It Goes <a name="round-and-round-it-goes"></a>

This project is one that you can use to control a small vehicle. You probably already know the gyroscope from your cell phone.
It can perceive rotation and the speed of rotation.

The project has:

* two green LEDs to show the acceleration.
* two red LEDs to indicate braking.
* a stepper motor for steering.

>Let's go!

<img src="images/Lesson15/Lesson15_Gyroscope_1.png" width="960"/><br>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Prototyp Expansion Module**                   |<img src="images/Parts/Part_PrototypeExpansionModule.jpg" width="256"/> |
| 2 | **Red LED** | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/> | 2 | **Green LED** | <img src="images/Parts/Part_LED_Green_5mm.jpg" width="256"/> |
| 4 | **220 Ohm Resistor** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>| 1 | **GY-521 Gyroscope** |<img src="images/Parts/Part_GY-521.jpg" width="256"/> |
| 1 | **Servomotor** |<img src="images/Parts/Part_ServoMotor.jpg" width="256"/> | 7 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |
| 4 | **W-M Cable** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> |

#### Neue Komponenten

***GY-521 Module:***

* includes a MEMS accelerometer and a MEMS gyroscope in a single chip.
* can perform 16-bit analog to digital conversion per channel. Therefore, the module can measure and output the X, Y and Z values at the same time.
* used in smartphones, wearables, game controllers and many other devices.
* attached to an object, it enables the orientation of the object to be determined in 3-dimensional space.
* can also be used to detect motion.

#### Circuit diagram

<img src="images/Lesson15/Lesson15_Gyroscope_2.png" width="960"/><br>

#### Assembly instructions

1. Red LED 1: In the lower middle section in the 5th row with the short leg on the left and the long leg on the right, 2 plug holes in between
2. Red LED 2: In the lower middle section in the 2nd row with the short leg on the left and the long leg on the right, 3 plug holes in between
3. Green LED 1: In the upper middle section in the 2nd row with the short leg on the left and the long leg on the right, 2 plug holes in between
4. Green LED 2: In the upper middle section in the 5th row with the short leg on the left and the long leg on the right, 3 plug holes in between
5. 4 Resistors: In 3rd and 4th row of the top and bottom area above or below the long leg of the LED >> 4 slots in a row offset to the right
6. M-M Cable 1: Above the right leg of the 3rd row resistor (Green LED) >> Arduino Pin 8
7. M-M Cable 2: Above the right leg of the 4th row resistor (Green LED) >> Arduino Pin 7
8. M-M Cable 3: Above the right leg of the resistor in 3rd row (Red LED) >> Arduino Pin 12
9. M-M Cable 4: Above the right leg of the 4th row resistor (Red LED) >> Arduino Pin 13
10. M-M Cable 5: Servermotor GND >> Arduino GND
11. M-M Cable 6: Servermotor VCC >> Arduino 5V
12. M-M Cable 7: Servermotor DATA >> Arduino Pin 9
13. W-M Cable 1: GY-521 VCC >> Arduino 3V Pin
14. W-M Cable 2: GY-521 GND >> Arduino GND
15. W-M Cable 3: GY-521 SCL >> Arduino Pin Analog 5 (A5)
16. W-M Cable 4: GY-521 SDA >> Arduino Pin Analog 4 (A4)

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson15/Lesson15_Gyroscope.png" width="960"/><br>

1. And as always, let's start with the basic function.

  <img src="images/Lesson15/Lesson15_Gyroscope_3.png" width="960"/><br>

  **First, let's take care of the gyroscope and controls.**

2. From **Sensors** drag a **Setup GY-521 Gyroscope with A4|A5** onto your workspace.

  ***Like some others, this setup block can be positioned in the first position outside the run-first loop. The code will still work.***

  <img src="images/Lesson15/Lesson15_Gyroscope_4.png" width="960"/><br>

  **We want to store the 3 axes of a three-dimensional rotation as variables again.**

3. Drag the **GY-521 Measure angles on** from **Sensors** to the run-forever loop.

  <img src="images/Lesson15/Lesson15_Gyroscope_5.png" width="960"/><br>

4. Use a variable for each axis by **New Variable...** and **Rename Variable...** named ***AngleX***, ***AngleY*** and ***AngleZ***.

  **Setup the servo motor.**

5. Draw a **Set SERVO from Pin 0 to 90 Degree** from **Motors**.
6. Change the pin number to ***9***.

  <img src="images/Lesson15/Lesson15_Gyroscope_7.png" width="960"/><br>

7. Pull a **Set digital Pin# 0 to HIGH** from **Input/Output** into the run-first loop four times.
8. Set the pin numbers to ***7***, ***8***, ***12*** and ***13***.

  <img src="images/Lesson15/Lesson15_Gyroscope_8.png" width="960"/><br>

  **Now we will set up the "if do" function with 5 columns. 1 - Accelerating, 2 - Full throttle, 3 - Braking, 4 - Hard braking, 5 - Neither accelerating nor braking.**

9. Drag an **if do** function into the run-forever loop.

  <img src="images/Lesson15/Lesson15_Gyroscope_10.png" width="960"/><br>

10. Use the gear icon to add 4 **else if** blocks to the **if** block.

  **We want to define a range with minimum and maximum again.**

11. Join a **" " and " "** block with **if**.

  <img src="images/Lesson15/Lesson15_Gyroscope_11.png" width="960"/><br>

12. As the first position of **" " and " "** use a **" " = " "** block.

  <img src="images/Lesson15/Lesson15_Gyroscope_12.png" width="960"/><br>

13. The **=** symbol must become ***>***.

  <img src="images/Lesson15/Lesson15_Gyroscope_13.png" width="960"/><br>

14. The first position of the **" " > " "** block becomes an **item** block from **Variables**.
15. Wähle für **item** die Option ***AngleX***.

  <img src="images/Lesson15/Lesson15_Gyroscope_14.png" width="960"/><br>

16. The second position of **AngleX > " "** is a **0** block from **Math**. The number becomes ***10***.
17. Copy **AngleX > 10** for the second position of **AngleX > 10 and " "**.
18. Change the values to ***AngleX < 30***.

  <img src="images/Lesson15/Lesson15_Gyroscope_16.png" width="960"/><br>

19. Make 4 copies of **AngleX > 10 and AngleX < 30** and add them to each **else if** connection.
20. The first copy will be ***AngleX >= 30 and AngleX <= 180***.
21. The second copy will be ***AngleX < -10 and AngleX > -30***.
22. The third copy will be ***AngleX <= -30 and AngleX >= -180***.
23. The fourth copy will be ***AngleX < 10 and AngleX > -10***.

  <img src="images/Lesson15/Lesson15_Gyroscope_17.png" width="960"/><br>

24. Make 5 copies of each of the 4 **Set digital Pin# ...** from the run-first loop, and put them in each of the **do** sections.

  ***Be sure the order of the pins in each column remains 7, 8, 12, and 13.***

25. The values of the first **do** section becomes ***HIGH***, ***LOW***, ***LOW***, ***LOW***.
26. The values of the second **do** section becomes ***HIGH***, ***HIGH***, ***LOW***, ***LOW***.
27. The values of the third **do** section becomes ***LOW***, ***LOW***, ***HIGH***, ***LOW***.
28. The values of the forth **do** section becomes ***LOW***, ***LOW***, ***HIGH***, ***HIGH***.
29. The values of the fifth **do** section becomes ***LOW***, ***LOW***, ***LOW***, ***LOW***.

  <img src="images/Lesson15/Lesson15_Gyroscope_18.png" width="960"/><br>

  **Now you will take care of the steering.**

30. To do this, drag a new **if do** block from **Logic** to the last position of the run-forever loop.
31. Press the gear icon and add 1 **else if** and 1 **else** block to the **if** on the right.

  ***Close the menu by clicking the gear icon again.***

32. Make two copies of the **AngleX < 10 and AngleX > -10** block and put them in the new **if** and **else if** connection, respectively.
33. Change the first copy under **if** to ***AngleZ > 10 and AngleZ < 180***.
34. Change the second copy under **else if** to ***AngleZ < -10 and AngleZ > -180***.

  <img src="images/Lesson15/Lesson15_Gyroscope_19.png" width="960"/><br>

35. Copy 3 times the **Set SERVO from Pin 9 to 90 Degree** block from the run-first loop.
36. Put 1 copy in each **do** section and 1 copy in the **else** section.

  <img src="images/Lesson15/Lesson15_Gyroscope_20.png" width="960"/><br>

  **The motor's zero position under "else" remains 90 degrees.**

  **However, the turns to the left and right must be brought into relation, since when driving a car, the steering does not directly turn the wheels at the same angle as you turn the steering wheel.**

37. Drag a **Map " " fromLow 0 fromHigh 100 toLow 0 to toHigh 1000** block from **Math** into degrees **90** of the **Set SERVO from Pin 9 to 90 Degree** block.
38. Get an **item** block from **Variables** and paste it in the first position of **Map " " fromLow 0 fromHigh 100 toLow 0 to toHigh 1000**.

  <img src="images/Lesson15/Lesson15_Gyroscope_21.png" width="960"/><br>

39. Change the values of the block to ***Map AngleZ fromLow 10 fromHigh 90 toLow 90 toHigh 180***.
40. Copy this block and paste the copy as the degree **90** value in the second **do** section.
41. Change its values to ***Relate AngleZ fromLow -10 fromHigh -90 toLow 90 toHigh 0***.

  <img src="images/Lesson15/Lesson15_Gyroscope_22.png" width="960"/><br>

42. Name your project and save it.

  <img src="images/Lesson15/Lesson15_Gyroscope_23.png" width="960"/><br>

43. Compile your code and upload the project to your Arduino.


#### *But where is the sound when accelerating? Here you can still do something with the passive buzzer!*

---

#### [Back to Index](#index)

### 3.16. Red, yellow, green... and off we go! <a name="red,yellow,green...and-go"></a>

In traffic, there are traffic lights that only switch when a car is at the intersection.

**Let's build such a traffic light!**

<img src="images/Lesson16/Lesson16_Motiondetector_1.png" width="960"/><br>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable**  |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Prototyp Expansion Module** |<img src="images/Parts/Part_PrototypeExpansionModule.jpg" width="256"/> |
| 1 | **Red LED** | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/> | 1 | **Yellow LED** |<img src="images/Parts/Part_LED_Yellow_5mm.jpg" width="256"/> |
| 1 | **Green LED** | <img src="images/Parts/Part_LED_Green_5mm.jpg" width="256"/> | 3 | **220 Ohm Resistor** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/> |
| 1 | **HC-SR501 PIR Motion Detector** |<img src="images/Parts/Part_HC-SR501.jpg" width="256"/> | 3 | **W-M Cable** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> |
| 5 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***HC-SR501 PIR Motion detector:***

* itself has two sensor points, each of which consists of a special material that reacts to infrared light.
* has a lens, which is used here to capture as many rays of light as possible.
* compares the amount of infrared light received that is emitted by the environment.
* registers a change in amount when a warm object, such as a human or animal, changes the amount of infrared light in the area.
* turns on when it increases and turns off when it decreases.
* detects movement at an angle of 110°.
* needs about 1 minute after switching on to initialize. The following settings should only be made after this minute.
* has 2 rotary controls to vary the time interval between 5 seconds and 5 minutes and the sensitivity from 3 to 7 meters.
* has 1 jump switch to set whether the sensor is triggered once or every time. This also toggles the time interval on and off.

#### Circuit diagram

<img src="images/Lesson16/Lesson16_Motiondetector_2.png" width="960"/><br>

#### Assembly instructions

1. Red LED: In the upper middle part in the 4th row with a short leg on the left and a long leg
2. Yellow LED: In the lower middle section in the 1st row with the short leg on the left and the long leg on the right
3. Green LED: In the lower middle section in the 4th row with the short leg on the left and the long leg on the right, 2 plug holes in between
4. 3 Resistors 220 Ohm: Above or below the long leg of each LED >> Second leg of the resistor 4 plug holes on the right
5. M-M Cable 1: Above the left legs of the LEDs at the top >> Arduino GND
6. M-M Cable 2: Below the left leg of the LED at the bottom >> Arduino GND
7. M-M Cable 3: Above the right leg of the red LED resistor at the top >> Arduino Pin 2
8. M-M Cable 4: Above the right leg of the yellow LED resistor at the bottom >> Arduino Pin 4
9. M-M Cable 5: Above the right leg of the green LED resistor at the bottom >> Arduino Pin 8
10. W-M Cable 1: HC-SR501 VCC >> Arduino 5V Pin
11. W-M Cable 2: HC-SR501 DATA >> Arduino Pin 7
12. W-M Cable 3: HC-SR501 GND >> Arduino GND

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson16/Lesson16_Motiondetector.png" width="960"/><br>

1. We start with the basic function.

  <img src="images/Lesson16/Lesson16_Motiondetector_3.png" width="960"/><br>

  **We will use a red, a yellow and a green LED for the traffic lights.**

2. Pull 3 **Set digital pin# 0 to HIGH** blocks from **Input/Output** into the run-first loop.

  <img src="images/Lesson16/Lesson16_Motiondetector_4.png" width="960"/><br>

  **The red LED will be on at power up, so in the Run-first loop the LED is set HIGH while the other two are set LOW.**

3. Choose the pin number ***2***, ***4***, ***8*** and its status ***HIGH***, ***LOW***, ***LOW***.

  <img src="images/Lesson16/Lesson16_Motiondetector_5.png" width="960"/><br>

  **We will use a Boolean operation that works on true and false. When motion is registered, the value switches from false to true.**

4. Get a **set item to** block from **Variables** into the run-first loop.
5. Rename the variable to ***Switch***.
6. Then pull a **true** block from **Logic** and wire it to **set Switch to**.

  <img src="images/Lesson16/Lesson16_Motiondetector_6.png" width="960"/><br>

7. Change the value from **true** to ***false***.
8. Duplicate this block and drag it into the run-forever loop.

  <img src="images/Lesson16/Lesson16_Motiondetector_7.png" width="960"/><br>

  **We will use an "if do" function to change false to true when there is a signal from the motion detector.**

9. Add an **if do** block from **Logic** to the run-forever loop above **set Switch to false**.
10. Next, connect a **" " = " "** block from **Logic** to the **if** slot.

  <img src="images/Lesson16/Lesson16_Motiondetector_8.png" width="960"/><br>

11. As the first position of the **" " = " "** block use a **Read digital Pin# 0** block from **Input/Output**.
12. Dial pin number ***7***.
13. As the second position of **Read digital Pin# 7 = " "** you need a **HIGH** block from **Input/Output**.

  <img src="images/Lesson16/Lesson16_Motiondetector_9.png" width="960"/><br>

14. Copy **set Switch to false** again and drag it into the **do** section.
15. Now change the value **false** to ***true***.

  <img src="images/Lesson16/Lesson16_Motiondetector_10.png" width="960"/><br>

16. Add an **if do** block from **Logic** in the infinite loop above **set Switch to false** and below the first **if do** block.
17. Next, connect a **" " = " "** block of **Logic** to the **if** slot.
18. As the first position of the **" " = " "** block, use an **item** block from **Variables**.
19. Choose the variable name ***Switch***.
20. As the second position of **Switch = " "** you need a **true** block from **Logic**.

  <img src="images/Lesson16/Lesson16_Motiondetector_11.png" width="960"/><br>

  **Now we will create the traffic light sequence.**

  ***We'll start with the yellow LED as the red LED will already be lit.***

21. Drag a **Set digital pin# 0 to HIGH** block into the **do** section of the **if Switch = true** block.
22. Choose pin number ***4***.
23. Then put a **wait 1000 milliseconds** block from **Time** underneath.

  <img src="images/Lesson16/Lesson16_Motiondetector_12.png" width="960"/><br>

24. Enter ***3000*** as time value.

  <img src="images/Lesson16/Lesson16_Motiondetector_13.png" width="960"/><br>

  **After red, yellow-red, comes green.**

25. Copy all 3 **set digital pin#...** blocks and place them in the same order below the time block.
26. Change the value of pin 2 to ***LOW***, pin 4 to ***LOW*** and pin 8 to ***HIGH***.
27. Copy the time block **wait 3000 milliseconds** and drag the copy below **Set digital pin# 8 to HIGH**.
28. Increase the time to ***5000***.

  <img src="images/Lesson16/Lesson16_Motiondetector_14.png" width="960"/><br>

  **After the green phase, the traffic light switches back to yellow.**

29. Copy the **set digital pin#...** block of pin numbers **4** and **8**. Set them one after the other below the time block with 5000 milliseconds.
30. Change the value of pin 4 to ***HIGH*** and pin 8 to ***LOW***.
31. Duplicate the time block with 3000 milliseconds and place it below.

  <img src="images/Lesson16/Lesson16_Motiondetector_15.png" width="960"/><br>

  **After 3 seconds the yellow turns to red.**

32. Copy the **set digital pin#...** block of pin numbers **2** and **4**. Set them one after the other below the time block with 3000 milliseconds.
33. Change the value of pin 2 to ***HIGH*** and pin 4 to ***LOW***.

  <img src="images/Lesson16/Lesson16_Motiondetector_16.png" width="960"/><br>

  **This ends the sequence. It remains red until a movement is detected again.**

34. Name your project and save it.

  <img src="images/Lesson16/Lesson16_Motiondetector_17.png" width="960"/><br>

35. Compile the code and upload it to your Arduino.

---

#### [Back to Index](#index)

### 3.17. What Time Is It? <a name="what-time-is-it"></a>

In this project you will use a real-time clock module and a display to create a clock, that shows you date and time.

<img src="images/Lesson17/Lesson17_Clock_1.png" width="960"/><br>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **DS3231 RTC Module** | <img src="images/Parts/Part_DS3231_RTC_Modul.jpg" width="256"/> | 1 | **LCD1602 Module** |<img src="images/Parts/Part_LCD1602_Modul.jpg" width="256"/> |
| 1 | **Potentiometer 10 kOhm**  |<img src="images/Parts/Part_Potentiometer10kohm.jpg" width="256"/>| 20 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |


#### New components

***DS3231 Real Time Clock Module:***

* is a simple chip that can tell the time.
* has a small built-in battery so the clock will continue to function even when no power is otherwise being supplied by the Arduino.

***LCD 16 x 2 Display:***

* is an alphanumeric liquid crystal display.
* has an LED backlight.
* can display two rows of 16 characters each.

***Potentiometer 10 kOhm***

* is an analogue knob with a value range of 0 - 1024.
* is used as an example as a dimmer switch to vary the light intensity.
* used in many electrical devices.

#### Circuit diagram

<img src="images/Lesson17/Lesson17_Clock_2.png" width="960"/><br>


#### Assembly instructions

1. LCD 16x2 Display: In the upper or lower middle part in row (Right)
2. DS3231 RTC: In the upper or lower middle part in a row (Left)
3. Potentiometer: With two pins in the upper center section via center bridge with one pin in the lower center section (Middle)
4. M-M Cable 1: Top blue row of breadboard  >> Arduino GND
5. M-M Cable 2: Top red row of breadboard >> Arduino 5V
6. M-M Cable 3: Above the left pin (GND) of the potentiometer in the upper middle section >> Top blue row (-) of breadboard
7. M-M Cable 4: Above the right pin (VCC) of the potentiometer upper center section >> Upper red row (+) of the breadboard
8. M-M Cable 5: Below the potentiometer pin in the lower center section >> Above the 3rd pin of the LCD 16x2
9. M-M Cable 6: Above 1st pin of the LCD 16x2 >> Top blue row (-) of the breadboard
10. M-M Cable 7: Above 2nd pin of the LCD 16x2 >> Top red row (+) of the breadboard
11. M-M Cable 8: Above 4th pin of the LCD 16x2 >> Arduino pin 7
12. M-M Cable 9: Above 4th pin of LCD 16x2 >> Top blue row (-) of breadboard
13. M-M Cable 10: Above 5th pin of the LCD 16x2 >> Arduino pin 8
14. M-M Cable 11: Above 11th pin of the LCD 16x2 >> Arduino pin ~9
15. M-M Cable 12: Above 12th pin of the LCD 16x2 >> Arduino pin ~10
16. M-M Cable 13: Above 13th pin of the LCD 16x2 >> Arduino pin ~11
17. M-M Cable 14: Above 14th pin of the LCD 16x2 >> Arduino pin 12
18. M-M Cable 15: Above 15th pin of the LCD 16x2 >> Top red row (+) of the breadboard
19. M-M Cable 16: Above 16th pin of the LCD 16x2 >> Top blue row (-) of the breadboard
20. M-M Cable 17: Above DS3231 RTC GND >> Top red row of breadboard
21. M-M Cable 18: Above DS3231 RTC VCC >> Top blue row of breadboard
22. M-M Cable 19: Above DS3231 RTC SDA >> Arduino Pin Analog 4 (A4)
23. M-M Cable 20: Above DS3231 RTC SDL >> Arduino Pin Analog 5 (A5)

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson17/Lesson17_Clock.png" width="960"/><br>

1. Start with the basic function.

  **Initiate the real time clock and display.**

2. Drag **Setup Real-Time Clock on pin# A4 and A5** from **Time** to the run-first loop.

  <img src="images/Lesson17/Lesson17_Clock_3.png" width="960"/><br>

3. Then get a **Setup LCD 16x2 on digital pins** from **Displays** in the run-first loop.
4. Change the pin numbers to ***DB4: 9, DB5: 10, DB6: 11, DB7: 12***.

  <img src="images/Lesson17/Lesson17_Clock_4.png" width="960"/><br>

  **If you are using the real-time clock for the first time, or if you want to reset the date and time later, we need the block for this.**

5. Drag the **Set clock of RTC manually** block from **Time** into the run-first loop.

  <img src="images/Lesson17/Lesson17_Clock_5.png" width="960"/><br>

  **Store the data from the clock as variables.**

6. Drag a **Read Date and Time from RTC Module** block from **Time** into the run-forever loop.

  <img src="images/Lesson17/Lesson17_Clock_6.png" width="960"/><br>

7. Select the **item** variables one by one, click on **New variable...**. Name these appropriate to the respective data point. Here: ***Year***, ***Month***, ***Day***, ***Hour***, ***Minute***, ***Second***.

  <img src="images/Lesson17/Lesson17_Clock_7.png" width="960"/><br>

  **Now the date and time should be sent to the display.**

8. Drag a **LCD 16x2 >> Write to Display** block from **Displays** into the run-forever loop.
9. Separate both **" "** blocks from the **LCD 16x2 >> Write to Display** blocks.

  ***Keep them on your workspace. You will still need these later.***

  <img src="images/Lesson17/Lesson17_Clock_9.png" width="960"/><br>

  **We have 2 lines in our display. One line will show the date and one line will show the time.**

  **Since we have more than one variable, we will combine them into one text.**

10. Get two **create text with** blocks of **Text** and connect them to **1st Line** and **2nd Line**.

  <img src="images/Lesson17/Lesson17_Clock_10.png" width="960"/><br>

  **Even two connections are not enough.**

11. Press each gear icon and drag 3 more **item** blocks from left to right.

  ***There should now be 5 connections per line.***

  <img src="images/Lesson17/Lesson17_Clock_11.png" width="960"/><br>

12. Now click on **Year**, **Month**, **Day** and link them one after the other with the 1st, 3rd and 5th connection in **Create text with** from **1. Row**.
13. Repeat with **Hour**, **Minute**, **Second** and concatenate these with the 1st, 3rd and 5th links in **Create Text with** of **2. Row**.

  <img src="images/Lesson17/Lesson17_Clock_12.png" width="960"/><br>

  ***Now you can use the " " blocks you dragged onto the workspace.***

14. Copy or get **" "** blocks of **Text** so that the 4 remaining connections of **Create Text with** the **1. Line** and **2. Line** are filled.
15. Fill in ***/*** as text for the spaces in date, i.e. **1. Line**, and ***:*** as text for the distances in time, i.e. **2. Line**.

  <img src="images/Lesson17/Lesson17_Clock_13.png" width="960"/><br>

  **Now the display should be cleared with each loop so that it can be rewritten.**

16. Drag a **Clear LCD 16x2 Display** block from **Displays** to the middle of the run-forever loop.

  <img src="images/Lesson17/Lesson17_Clock_14.png" width="960"/><br>

17. Now enter the date and time into the fields of the **Set clock of RTC manually** block.

  ***Set the time about one minute in the future so you still have time to verify the code. Also expect a few seconds after you trigger the upload to the Arduino.***

  <img src="images/Lesson17/Lesson17_Clock_15.png" width="960"/><br>

18. Verify your code, wait for the time to get a few seconds closer to the default, then upload it to your Arduino.

  <img src="images/Lesson17/Lesson17_Clock_16.png" width="960"/><br>

19. If the time is correct, click the tick in the **Set clock of RTC manually*** block.

  **You can now continue to work on your code, the presentation and other projects.**

  **As long as the small battery in the module has enough power, the clock will keep ticking even if you remove the Arduino from it.**

  <img src="images/Lesson17/Lesson17_Clock_17.png" width="960"/><br>

  #### *Did you notice anything in the display?*

  *If the number of minutes and seconds is less than 10, can you see the frozen number?*

  **Using a new function, let's put a 0 in front of the hours, minutes, and seconds when they fall below 10.**

20. Separate the variables **Hour**, **Minute**, **Second** from **Create Text with** of **2. line** and drop it on the workspace.

  ***You can delete the variable minute and second!***

21. Drag the **Test if true if false** block from **Logic** to the previous position of **Hour**.

  <img src="images/Lesson17/Lesson17_Clock_18.png" width="960"/><br>

  **If it is true that the number is under 10, then add a 0 in front of the number. If wrong, take the number as it comes.**

22. We need the **" " = " "** block from **Logic** as a comparison function in the **Test** connection.

23. Change the **=** symbol to ***<*** and drag **Hour** from the workspace to the first position of **" " < " "**.

  <img src="images/Lesson17/Lesson17_Clock_19.png" width="960"/><br>

24. As the second position of **Hour < " "** we need a **0** block from **Math**.
25. Change the value to ***10***.

  <img src="images/Lesson17/Lesson17_Clock_20.png" width="960"/><br>

  **If true, we want to create a text from a 0 and the corresponding number.**

26. Pull **Create Text with** from **Text** and connect it with **if true**.

  <img src="images/Lesson17/Lesson17_Clock_21.png" width="960"/><br>

  **We already want to make the number 0 available as text.**

27. The 1st position of **Create Text with** becomes a **" "** block from **Text**.
28. Write a ***0*** in it.

  <img src="images/Lesson17/Lesson17_Clock_22.png" width="960"/><br>

29. Now make 2 copies of the variable block **Hour** and put them respectively in the 2nd position of **Create Text with** the **if true** and in the **if false** connection.

  <img src="images/Lesson17/Lesson17_Clock_23.png" width="960"/><br>

30. Now click on the **Test if true if false**, duplicate it twice and connect it to the links where minute and second were placed before.
31. Now change the variables from **Hour** to ***Minute*** or ***Seconds*** depending on the position.

  <img src="images/Lesson17/Lesson17_Clock_24.png" width="960"/><br>

32. Give your project a name and save it.

  ***Check again if the tick in the manual clock setting is deactivated.***

33. Compile the code and upload it to your Arduino.

---

#### [Back to Index](#index)

### 3.18. How Deep Is It? <a name="how-deep-is-it"></a>

For this project it is advisable to provide a small cup with water that we want to measure the level.

The water level detector can be used to prevent something from overflowing or to ensure that plants always have enough water in the tank.

<img src="images/Lesson18/Lesson18_Waterlevelsensor_1.png" width="960"/><br>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Water level module** | <img src="images/Parts/Part_WaterLevelModul.jpg" width="256"/> | 1 | **LCD1602 Module** |<img src="images/Parts/Part_LCD1602_Modul.jpg" width="256"/> |
| 1 | **Potentiometer 10 kOhm** |<img src="images/Parts/Part_Potentiometer10kohm.jpg" width="256"/>| 3 | **W-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |
| 19 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***Water level module***

* was developed to detect, for example, rain, the water level or leaking liquids.
* consists of an electronic connection, a 1 MΩ resistor and several bare conductors or antennas.
* works by connecting the resistor to the leads and the ground to leads. As soon as water comes onto the sensor, they are short-circuited and the current flows.
* can serve as a switch with a digital pin, while it can also determine water level or water volume with an analog pin.

#### Circuit diagram

<img src="images/Lesson18/Lesson18_Waterlevelsensor_2.png" width="960"/><br>

#### Assembly instructions

1. LCD 16x2 Display: In the upper or lower middle part in row (Right)
2. Potentiometer: With two pins in the upper center section via center bridge with one pin in the lower center section (Middle)
3. M-M Cable 1: Top blue row of breadboard  >> Arduino GND
4. M-M Cable 2: Top red row of breadboard >> Arduino 5V
5. M-M Cable 3: Above the left pin (GND) of the potentiometer in the upper middle section >> Top blue row (-) of breadboard
6. M-M Cable 4: Above the right pin (VCC) of the potentiometer upper center section >> Upper red row (+) of the breadboard
7. M-M Cable 5: Below the potentiometer pin in the lower center section >> Above the 3rd pin of the LCD 16x2
8. M-M Cable 6: Above 1st pin of the LCD 16x2 >> Top blue row (-) of the breadboard
9. M-M Cable 7: Above 2nd pin of the LCD 16x2 >> Top red row (+) of the breadboard
10. M-M Cable 8: Above 4th pin of the LCD 16x2 >> Arduino pin 7
11. M-M Cable 9: Above 4th pin of LCD 16x2 >> Top blue row (-) of breadboard
12. M-M Cable 10: Above 5th pin of the LCD 16x2 >> Arduino pin 8
13. M-M Cable 11: Above 11th pin of the LCD 16x2 >> Arduino pin ~9
14. M-M Cable 12: Above 11th pin of the LCD 16x2 >> Arduino pin ~9
15. M-M Cable 13: Above 13th pin of the LCD 16x2 >> Arduino pin ~11
16. M-M Cable 14: Above 14th pin of the LCD 16x2 >> Arduino pin 12
17. M-M Cable 15: Above 15th pin of the LCD 16x2 >> Top red row (+) of the breadboard
18. M-M Cable 16: Above 16th pin of the LCD 16x2 >> Top blue row (-) of the breadboard
19. M-M Cable 17: Above water level module GND cable >> Upper red row of breadboard
20. M-M Cable 18: Above water level module VCC wire >> Top blue row of breadboard
21. M-M Cable 19: Above Water Level Module S Cable >> Arduino Pin Analog 3 (A3)
22. W-M Cable 1: Water level module GND Pin >> Upper middle part of the breadboard
23. W-M Cable 2: Water Level Module VCC Pin >> Upper middle part of the breadboard
24. W-M Cable 3: Water level module S Pin >> Upper center part of the breadboard

**Compare your version to the schematic photo before we continue.**


#### Block for Block

<img src="images/Lesson18/Lesson18_Waterlevelsensor.png" width="960"/><br>

  **After you have dragged in the basic function, lets continue with the display.**

1. Drag the **Set LCD 16x2 on digital pins** setup block into the run-first loop.
2. Use the pin numbers ***9***, ***10***, ***11***, ***12*** in this order.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_3.png" width="960"/><br>

  **The water level indicator is very accurate. You only need the change at certain intervals, the display does not change too often.**

  **Therefore, you will compare the current value with a value that you previously recorded as a variable.**

3. Set the first variable with a **set item to** block from **Variables** connected to a **0** block from **Math**, which end up connected in the run-first loop.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_4.png" width="960"/><br>

4. Change the variable name to ***Value***.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_5.png" width="960"/><br>

5. Duplicate the **set Value to 0** block, put it in the run-first loop.
6. Select **New Variable...** and change the variable name to ***OldValue***.
7. Duplicate the **set Value to 0** block again and put it in the run-forever loop.
8. Replace the **0** of this block with **Read analog pin# A0** from **Input/Output**.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_6.png" width="960"/><br>

9. Change the pin number from **Read analog pin# A0** to ***A3***.
10. Loop an **if do** block from **Logic**.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_7.png" width="960"/><br>

11. Join **if** with a **" " and " "** block from **Logic**.
12. Change **and** to ***or***.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_8.png" width="960"/><br>

13. As the first position of **" " or " "** take a **" " = " "** block from **Logic**.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_9.png" width="960"/><br>

14. Change the **=** to ***>***.
15. Add a **item** block from **Variables** as the first position of **" " > " "**.
16. For **item** choose the variable name ***Value***.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_10.png" width="960"/><br>

17. As the second position of **Value > " "** you need a **" " + " "** block from **Math**.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_11.png" width="960"/><br>

18. Duplicate the **Value** block and drag it to the first position of **" " + " "**.
19. Change the variable name to ***OldValue***.
20. For the second position in **OldValue + " "** you need a **0** block from **Math**.
21. Increase the number to ***2***.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_12.png" width="960"/><br>

22. Now duplicate the complete block **Value > OldValue + 2** and paste it into the second position of **Value > OldValue + 2 or " "**.
23. Now change its values from **>** to ***<*** and **+** to ***-***.

  ***By right-clicking or finger-holding on "Value > OldValue + 2 or Value < OldValue - 2", you can select "External Inputs" from the menu to display this block vertically.***

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_13.png" width="960"/><br>

  **If the new value decreases or increases by 2, the display should be cleared and the old value replaced with the new one.**

24. Drag the **Clear LCD 16x2 Display** block from **Displays** to the **do** section.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_14.png" width="960"/><br>

25. Get a **set item to** and a **item** block from **Variables** onto the workspace.
26. Change the variable name **item** to ***Value*** and connect it with **set item to**.
27. Choose **OldValue** for **item** of the **set item to** block and drag the entire block below the **Clear LCD 16x2 Display** in the **do** section.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_15.png" width="960"/><br>

  **Again, give the display a little time to prevent flickering.**

28. Drag a **wait 100 microseconds** from **Time** into the run-forever loop as the bottom position.
29. Reduce the time to ***10***.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_16.png" width="960"/><br>

  **Now the display only needs to show the value.**

30. Position a block **LCD 16x2 >> Write to Display** below the time block in the run-forever loop.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_17.png" width="960"/><br>

31. In the text block of the first line, write ***WaterLevel***, and delete the text block of the second line.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_18.png" width="960"/><br>

32. Use an **item** block from **Variables** as text for the second line of the **LCD 16x2 >> Write to Display** block.
33. For **item** choose the variable name ***OldValue***.

  <img src="images/Lesson18/Lesson18_Waterlevelsensor_19.png" width="960"/><br>

34. Name your project and save it.
35. Verify the code and upload it to your Arduino.

  ***Now hold the sensor in the water***

>I'm sinking!!!

>Who drank from my cup??!

---

#### [Back to Index](#index)

### 3.19. Let Us Scream <a name="lets-scream"></a>

In this project we want to use a small microphone to display your voice with a graphic level.

<img src="images/Lesson19/Lesson19_MicrophoneSensor_1.png" width="960"/><br>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Sound sensor module** | <img src="images/Parts/Part_SoundSensor.jpg" width="256"/> | 1 | **LCD1602 Module** |<img src="images/Parts/Part_LCD1602_Modul.jpg" width="256"/>|
| 1 | **Potentiometer 10 kOhm** |<img src="images/Parts/Part_Potentiometer10kohm.jpg" width="256"/>| 4 | **W-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |
| 20 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***Sound sensor module:***

* is a transducer that converts sound waves into electrical signals.
* is a microphone, it has the opposite function of a loudspeaker.
* is a condenser microphone used in smartphones and laptops.

A condenser microphone has 2 small plates built in at a small distance.
A plate is fixed and charged with energy. The second plate is flexible and is set in motion by the sound waves.
When these plates touch, the capacitance changes, which then varies with the type of sound and volume.

#### Circuit diagram

<img src="images/Lesson19/Lesson19_MicrophoneSensor_2.png" width="960"/><br>

#### Assembly instructions

1. LCD 16x2 Display: In the upper or lower middle part in row (Right)
2. Potentiometer: With two pins in the upper center section via center bridge with one pin in the lower center section (middle)
3. M-M Cable 1: Top blue row of breadboard  >> Arduino GND
4. M-M Cable 2: Top red row of breadboard >> Arduino 5V
5. M-M Cable 3: Above the left pin (GND) of the potentiometer in the upper middle section >> Top blue row (-) of breadboard
6. M-M Cable 4: Above the right pin (VCC) of the potentiometer upper center section >> Upper red row (+) of the breadboard
7. M-M Cable 5: Unterhalb Pin des Potentiometer im unteren Mittelteil >> Oberhalb 3. Pin der LCD 16x2
8. M-M Cable 6: Above 1st pin of the LCD 16x2 >> Top blue row (-) of the breadboard
9. M-M Cable 7: Above 2nd pin of the LCD 16x2 >> Top red row (+) of the breadboard
10. M-M Cable 8: Oberhalb 4. Pin der LCD 16x2 >> Arduino Pin 7
11. M-M Cable 9: Above 4th pin of LCD 16x2 >> Top blue row (-) of breadboard
12. M-M Cable 10: Above 5th pin of the LCD 16x2 >> Arduino pin 8
13. M-M Cable 11: Above 11th pin of the LCD 16x2 >> Arduino pin ~9
14. M-M Cable 12: Above 12th pin of the LCD 16x2 >> Arduino pin ~10
15. M-M Cable 13: Above 13th pin of the LCD 16x2 >> Arduino pin ~11
16. M-M Cable 14: Above 14th pin of the LCD 16x2 >> Arduino pin 12
17. M-M Cable 15: Above 15th pin of the LCD 16x2 >> Top red row (+) of the breadboard
18. M-M Cable 16: Above 16th pin of the LCD 16x2 >> Top blue row (-) of the breadboard
19. W-M Cable 1: Sound sensor module GND Pin >> Upper middle part of the breadboard
20. W-M Cable 2: Sound sensor module VCC Pin >> Upper center part of the breadboard
21. W-M Cable 3: Sound sensor module D0 Pin >> Upper center part of the breadboard
22. W-M Cable 4: Sound sensor module A0 Pin >> Upper center part of the breadboard
23. M-M Cable 17: Above the sound sensor module GND cable >> Upper red row of the breadboard
24. M-M Cable 18: Above sound sensor module VCC cable >> Upper blue row of breadboard
25. M-M Cable 19: Above sound sensor module D0 cable >> Arduino pin ~3
26. M-M Cable 20: Above Sound Sensor Module A0 cable >> Arduino Pin Analog 0 (A0)

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson19/Lesson19_MicrophoneSensor.png" width="960"/><br>

1. After dropping the base function onto the workspace, drag a **Setup LCD 16x2 on digital pins** block from **Displays** into the run-first loop.
2. Change the pin numbers to ***9***, ***10***, ***11*** and ***12***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_3.png" width="960"/><br>

3. Loop a **Clear LCD 16x2 Display** block from **Displays**.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_4.png" width="960"/><br>

  **When your Arduino receives an audio signal, the built-in LED should light up.**

4. Drag a **Set built-in LED BUILTIN_1 to HIGH** block from **Input/Output** into the run-first loop.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_5.png" width="960"/><br>

  **The display will change depending on the volume signal. The signal has a range of 0 - 1024. Each level representation of loudness consists of 2 characters. The entire line is 16 characters.**

  **Therefore, the 1024 are divided into 8 parts, so 64 per level.**

5. To do this, place a **set item to** block from **Variables** in the run-first loop.
6. Name the variable ***BarValue***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_6.png" width="960"/><br>

7. Concatenate a **0** block from **Math** with the **set BarValue to**.
8. Increase the **0** value to ***64***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_7.png" width="960"/><br>

  **Now you will create the level value text that will later be visible in the display.**

9. Drag another **set item to** block from **Variables** into the run-first loop.
10. Name this block **BarText**.
11. Then draw a **" "** block from **Text** and connect it with **set BarText to**.
12. Trage ***>>*** als Text ein.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_8.png" width="960"/><br>

  **Now you will dedicate a variable to the analoge value of the microphone.**

13. Drag another **set item to** block from **Variables** this time into the run-forever loop.
14. Name the variable **AnalogValue**.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_9.png" width="960"/><br>

15. Connect this block to **Read analog pin# A0** from **Input/Output**.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_10.png" width="960"/><br>

16. Insert another variable block below it called ***DigitalValue*** and connect it to a **Read digital pin# 0** block from **Input/Output**.
17. Choose the digital pin number ***3***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_11.png" width="960"/><br>

  **Use an "if do" function to light up the built-in LED when the audio signal is detected.**

18. Drag an **if do** block from **Logic** into the run-forever loop.
19. Click the gear icon and add the **else** block to the function.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_12.png" width="960"/><br>

20. Join a **" " = " "** block from **Logic** with **if**.
21. Then use an **item** block from **Variables** as the first position of **" " = " "**.
22. Select ***DigitalValue*** as name.
23. As the second position of **DigitalValue = " "** you need a **HIGH** block from **Input/Output**.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_13.png" width="960"/><br>

24. Make two copies of **Set built-in LED BUILTIN_1 to HIGH** and paste them into the sections of **if** and **else** respectively.
25. Change the value of the block in **if** to ***LOW***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_14.png" width="960"/><br>

  **To match the step value text to the incoming audio signal, you need an "if do" function.**

26. Drag an **if do** block from **Logic** into the run-forever loop.
27. Click the gear icon and add 6 **else if** blocks for a total of 8 sections.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_15.png" width="960"/><br>

  **Now, as in previous examples, you will set a range of values by completing a certain task.**

  ***In this example, a specific level value text is to be displayed in a specific volume range.***

28. As a base use a **" " and " "** block from **Logic** and connect it with **if**.
29. Insert a **" " = " "** block from **Logic** for the first position.
30. Change the **=** symbol to ***>=***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_16.png" width="960"/><br>

31. As the first position of **" " >= " "** use a **item** block from **Variables**.
32. The name for it is **AnalogValue**.
33. For the second position you need a **0** block from **Math**.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_17.png" width="960"/><br>

34. Duplicate the **AnalogVaule >= 0** block and use it in the second position of **AnalogValue >= 0 and " "** next to it.
35. Change the symbol **>=** there to ***<=***.
36. Drag the **0** block from **AnalogValue <= 0** onto the workspace and substitute a **" " + " "** block from **Math** for it.
37. Change the symbol from **+** to ***x***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_18.png" width="960"/><br>

38. Use first position of **" " x " "** an **item** block from **Variables** with variable name ***BarValue***.
39. Drag the math block from your workspace to the second position of **BarValue x " "**.
40. Change the value to ***2***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_19.png" width="960"/><br>

41. Duplicate the entire block associated with **if** and place this copy in the first **else if** connection.
42. Make a copy of **BarValue x 2** and put it in the second position of the first position of the entire **else if** connection block instead of **0**.

  ***You can delete the "0" block.***

43. Change the value **2** of the **BarValue x 2** block in the second position of the entire **else if** connection block to ***4***.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_20.png" width="960"/><br>

44. Now create 6 duplicates of the block connected to the first **else if** slot.
45. Put a copy in each of the remaining **else if** slots.
46. Now increase the two values **2** and **4** by ***2*** in each following **else if**.

  ***1. Copy >> 1. Value 1: 4, Value 2: 6***<br>
  ***2. Copy >> 1. Value 1: 6, Value 2: 8***<br>
  ***3. Copy >> 1. Value 1: 8, Value 2: 10***<br>
  ***4. Copy >> 1. Value 1: 10, Value 2: 12***<br>
  ***5. Copy >> 1. Value 1: 12, Value 2: 14***<br>
  ***6. Copy >> 1. Value 1: 14, Value 2: 16***

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_21.png" width="960"/><br>

47. Make 8 copies of **Set BarText to >>** from the run-first loop and put one copy in each **do** section.
48. The text remains **>>** in the first **do** section, while in each subsequent **do** section, ***>>*** is added once to the preceding text.

  ***1. BarText: >>***<br>
  ***2. BarText: >>>>***<br>
  ***3. BarText: >>>>>>***<br>
  ***4. BarText: >>>>>>>>***<br>
  ***5. BarText: >>>>>>>>>>***<br>
  ***6. BarText: >>>>>>>>>>>>***<br>
  ***7. BarText: >>>>>>>>>>>>>>***<br>
  ***8. BarText: >>>>>>>>>>>>>>>>***<br>

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_22.png" width="960"/><br>

  **Now you will have the step value text which can represent the received signal from the microphone. Now send it to the display.**

49. Drag a **LCD 16x2 >> Write to display** block from **Displays** to the bottom of the run-forever loop.
50. Use two **item** blocks of **Variables** instead of **" "** text blocks.

  ***You can delete the text blocks.***

51. Choose ***BarText*** for both variable names.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_23.png" width="960"/><br>

  **Finally, the display should have some time to display the text.**

52. Use a **wait 1000 milliseconds** block from **Time** for the last position of the run-forever loop.
53. Change the value to ***100***.
54. Name your project and save it.

  <img src="images/Lesson19/Lesson19_MicrophoneSensor_24.png" width="960"/><br>

55. Compile the code and upload it to your Arduino.

#### *Be loud allowed! How well does it work?*


---

#### [Back to Index](#index)

### 3.20. 3 Times The Same Please <a name="3-times-the-same"></a>

In our second temperature example, we want to display the temperature correctly. Not only that! You will create equations to convert degrees Celsius to Fahrenheit and from there to Kelvin.

You can then use the push button to switch through the 3 temperature displays.

<img src="images/Lesson20/Lesson20_Thermometer_1.png" width="960"/><br>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Thermistor** | <img src="images/Parts/Part_10kThermistor.jpg" width="256"/> | 1 | **Potentiometer 10 kOhm** |<img src="images/Parts/Part_Potentiometer10kohm.jpg" width="256"/> |
| 1 | **10k Ohm Resistor** |<img src="images/Parts/Part_Resistor_10KOhm.png" width="256"/>| 1 | **Push button** |<img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/> |
| 1 | **LCD1602 Module**     |<img src="images/Parts/Part_LCD1602_Modul.jpg" width="256"/>| 20 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***Thermistor:***

* is a thermal resistance that affects the flow of energy through temperature.
* has a gain at which 1 degree Celsius changes the resistance by more than 100 ohms.

There are two forms of thermistors. One is called NTC (Negative Temperature Coefficient) and one is PTC
(Positive temperature coefficient).

NTC thermistors are typically used to measure temperature.

PTCs, on the other hand, are often used as reusable fuses, as the flow of energy increases
the temperature is rising. If it gets too hot, the fuse interrupts the flow of energy.

#### Circuit diagram

<img src="images/Lesson20/Lesson20_Thermometer_2.png" width="960"/><br>

#### Assembly instructions

1. LCD 16x2 Display: In the upper or lower middle part in row (Right)
2. Potentiometer: With two pins in the upper center section via center bridge with one pin in the lower center section (Middle)
3. Thermistor: In the upper middle part in the 4th row (Left)
4. Press button: Above the dividing line with 2 pins in the upper middle part and 2 pins in the lower middle part (Left outside)
5. Resistor 10 kOhm: Above left thermistor pin in 2nd row >> Top blue row (-) of breadboard
6. M-M Cable 1: Top blue row of breadboard >> Arduino GND
7. M-M Cable 2: Obere rote Reihe der Steckplatte >> Arduino 5V
8. M-M Cable 3: Above the left pin (GND) of the potentiometer, upper middle section >> Upper blue row (-) of the plug-in board
9. M-M Cable 4: Above the right pin (VCC) of the potentiometer upper center section >> Upper red row (+) of the breadboard
10. M-M Cable 5: Below the potentiometer pin in the lower center section >> Above the 3rd pin of the LCD 16x2
11. M-M Cable 6: Above 1st pin of the LCD 16x2 >> Top blue row (-) of the breadboard
12. M-M Cable 7: Above 2nd pin of the LCD 16x2 >> Top red row (+) of the breadboard
13. M-M Cable 8: Above 4th pin of the LCD 16x2 >> Arduino pin 7
14. M-M Cable 9: Above 4th pin of LCD 16x2 >> Top blue row (-) of breadboard
15. M-M Cable 10: Above 5th pin of the LCD 16x2 >> Arduino pin 8
16. M-M Cable 11: Above 11th pin of the LCD 16x2 >> Arduino pin ~9
17. M-M Cable 12: Above 12th pin of the LCD 16x2 >> Arduino pin ~10
18. M-M Cable 13: Above 13th pin of the LCD 16x2 >> Arduino pin ~11
19. M-M Cable 14: Above 14th pin of the LCD 16x2 >> Arduino pin 12
20. M-M Cable 15: Above 15th pin of the LCD 16x2 >> Top red row (+) of the breadboard
21. M-M Cable 16: Above 16th pin of the LCD 16x2 >> Top blue row (-) of the breadboard
22. M-M Cable 17: Above the left push button pin >> Top blue row of breadboard
23. M-M Cable 18: Above the right push button pin >> Arduino 2
24. M-M Cable 19: Above the left thermistor pin in 3rd row below the resistor >> Arduino Pin Analog 0 (A0)
25. M-M Cable 20: Above right thermistor pin >> Top red row (+) of breadboard

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson20/Lesson20_Thermometer.png" width="960"/><br>

  **Immediately after providing the basic function, let's start with the conversion equations. As a first step, you will initiate the value of the thermistor as a variable, which will be the base value for formulas.**

1. Start the run-forever loop with a **Set item to " " to Character** from **Variables**.
2. For **" "** connection use **Read analog pin# A0** of **Input/Output**.
3. Give **item** the variable name ***Temperature***.
4. Change **Character** to ***Decimal***.

  <img src="images/Lesson20/Lesson20_Thermometer_3.png" width="960"/><br>

  **Now you will create two equations that convert the temperature data to Kelvin.**

5. Drag **Set item to " " to Character** from **Variables** to the run-forever loop.
6. For the **" "** connection use **square root** from **Math**.

  <img src="images/Lesson20/Lesson20_Thermometer_4.png" width="960"/><br>

7. Set **square root** to ***ln***.
8. Give **item** the variable name ***Kelvin***.
9. Change **Character** to ***Double***.

  <img src="images/Lesson20/Lesson20_Thermometer_5.png" width="960"/><br>

10. Now connect a **" " + " "** block from **Math** to **ln**.
11. Choose ***x*** for the **+** symbol.
12. For the first position of **" " x " "** use a **0** block from **Math**.
13. You increase the **0** value to ***10000***.
14. Draw another block **" " + " "** as the second position of **10000 x " "**.
15. Change the **+** symbol to ***-***.
16. The second position of this **" " - " "** block becomes a **0** block from **Math** with ***1*** as the value.
17. As the first position of **" " - 1**, again get a **" " + " "** block in the scene.
18. Choose ***/*** for the **+** symbol.
19. Again, the first position of **" " / " "** becomes a **0** block with ***1024*** as the value.
20. The second position of **1024 / " "** is an **item** block from **Variables**.
21. Change **item** to ***Temperature***.

  <img src="images/Lesson20/Lesson20_Thermometer_6.png" width="960"/><br>

  **This was the first part of the formula to calculate Kelvin. Let's continue with the second part.**

22. Drag a **Set item to " " to Character** block from **Variables** into the run-forever loop below the previous formula block.
23. Give **item** the variable name ***Kelvin***.
24. Change **Character** to ***Double***.

  ***Now continue directly on your workspace. We will later insert this block into the block we just created.***

25. Drag a **" " + " "** block from **Math** onto your workspace below the base function.
26. Change the **+** symbol to ***x***.
27. As the second position use an **item** block from **Variables** with the variable name ***Kelvin***.
28. The first position will be a **0** block from **Math** with a value of ***0.0000000876741***.

  ***When you enter this number, the representation of the number will change to 8.76741e-8. The 8 at the end means that the dot separator moves 8 digits to the left.***

  <img src="images/Lesson20/Lesson20_Thermometer_7.png" width="960"/><br>

29. Now use this block as the first position of a new **" " + " "** block that you also drag onto the workspace.
30. Here, too, the symbol ***x*** must be used.
31. Again, the second position is an **item** block from **Variables** with the variable name ***Kelvin***.
32. This **8.76741e-8 x Kelvin x Kelvin** block will in turn become the second position of a new **" " + " "** block.
33. The first position is a block of numbers **0** with the value ***0.000234125***.

  <img src="images/Lesson20/Lesson20_Thermometer_8.png" width="960"/><br>

34. This entire block now becomes the second position of a **" " + " "** block that you get from **Math**.
35. The first position is again a block of numbers **0** with the value ***0.001129148***.
36. This giant block now becomes the second position of the last **" " + " "** part of the Kelvin formula you will get from **Math**.
37. The symbol of this block must become ***/***.
38. The first position is a **0** block with the value ***1***.

  <img src="images/Lesson20/Lesson20_Thermometer_9.png" width="960"/><br>

39. Now drag everything into the **set Kelvin to " " as Double** block.

  <img src="images/Lesson20/Lesson20_Thermometer_10.png" width="960"/><br>

  **With Kelvin you can now calculate the temperature in degrees Celsius.**

40. Continue with a **Set item to " " to Character** block from **Variables** in the run-forever loop below the previous formula block.
41. Give **item** the variable name ***Celsius***.
42. Change **Character** to ***Decimal***.
43. Drag a **" " + " "** block into the **" "** gap.
44. The symbol becomes ***-***.
45. The first position of **" " - " "** becomes an **item** block with the variable name ***Kelvin***.
46. The second position will be a block of numbers **0** with the value ***273.15***.

  <img src="images/Lesson20/Lesson20_Thermometer_11.png" width="960"/><br>

  **The third formula calculates Fahrenheit and will be derived from degrees Celsius.**

47. Drag a **Set item to " " to Character** block from **Variables** into the infinite loop below the "Celsius" block.
48. Give **item** the variable name ***Fahrenheit***.
49. Change **Character** to ***Decimal***.
50. Drag a **" " + " "** block into the **" "** gap.
51. The second position becomes a number ***32*** by a numeric block from **Math**.
52. The first position is another **" " + " "** block, but its symbol is changed to ***/***.
53. The second position of this block will also be a block of numbers with ***5*** as the value.
54. And also this block gets a **" " + " "** block as first position.
55. The symbol in this case becomes ***x***.
56. The second position, like the previous blocks, will be a number with the value ***9***.
57. The first position is now an **item** block from **Variables** with the variable name ***Celsius***.

  <img src="images/Lesson20/Lesson20_Thermometer_12.png" width="960"/><br>

  **That's all with the formulas. Now you will send the temperature to the display.**

58. Now insert a **LCD 16x2 >> Write for display** block from **Displays** under the last formula block.
59. Write ***Temperature is*** in the first line.
60. Drag the text block of the second line out of the connection, place it next to it on your workspace and write ***Celsius***.

  ***Add a space before the C of Celsius to leave room between the number and Celsius.***

61. Drag a **Create text with** block from **Text** into the second line connector.
62. Again use an **item** block with the variable name ***Celsius*** for the first connection.
63. As the second position, use the block of text you dropped onto your workspace.

  **We want to put some time into this display as well, since the temperature won't change that rapidly. You then delete the display for the next reading.**

64. Drag a **wait 1000 milliseconds** block from **Time** to the bottom of the run-forever loop and change the time value to ***500***.
65. Then place a **Clear LCD 16x2 Display** block from **Displays** underneath.

  **Before we continue with the push button and the various temperature displays.... try it first!**

  ***Compile the code and upload it to your Arduino.***

  <img src="images/Lesson20/Lesson20_Thermometer_13.png" width="960"/><br>

  #### *Does it work?*

  **Okay, let's move on to the push button.**

  **Since we only use one button, the temperature should be displayed in Celsius, Fahrenheit and Kelvin in succession with each button press. After Kelvin, it's time for Celsius again.**

66. Drag a **set item to** block from **Variables** with variable name **BtnVal** into the run-first loop.
67. Connect this to a number block **0**.

  **This is the status of the push button 0 = Pressed once, 1 = Pressed twice, 2 = Pressed three times.**

  <img src="images/Lesson20/Lesson20_Thermometer_14.png" width="960"/><br>

  **Now you have to arrange that the status number increases with each button press and when pressed three times, starts again at 0.**

68. Drag an **if do** block from **Logic** to the top position of the run-forever loop.
69. Use the gear icon to add an **else if** block.
70. Connect a **" " and " "** block from **Logic** with **if**.
71. Add a **" " = " "** block from **Logic** to the first and second positions of **" " and " "**, respectively.
72. Change the symbol **=** of the **" " = " "** block of the second position to ***\<***.
73. The first position of the first **" " = " "** block is a **Read digital with PULL_UP mode Pin#0** from **Input/Output**.
74. Choose pin number ***2***.
75. The second position of the first **" " = " "** block becomes a **HIGH** block also from **Input/Output**.
76. Change the value to ***LOW***.
77. The first position of the second part of the **" " = " "** block ist a **item** block from **Variables** with the variable name ***BtnVal***.
78. The second position of the second **" " = " "** block is a block from numbers **0** with a modified value of ***2***.
79. Now duplicate the entire block **Read digital with PULL_UP mode Pin# 2 = LOW and BtnVal < 0** and connect this with the **else if** slot.
80. The only difference between the two blocks will be the **<** character, which must become ***=*** again in the **else if** section.

  <img src="images/Lesson20/Lesson20_Thermometer_15.png" width="960"/><br>

  **If the key value is below 2, this value should now be added by 1.**

  **However, if the key value is 2, the value should return to 0.**

81. Duplicate the **set BtnVal to 0** block from the run-first loop twice and paste the copy into the **do** section of **if** and **else if**.
82. Drag the number block **0** under **if** onto your workspace and change the value to ***1***.
83. Join **set BtnVal to** in **if** with a **" " + " "** block from **Math**.
84. As the first position of **" " + " "** use a **item** block from **Variables** with variable name ***BtnVal***.
85. Now drag the number block **1** from your workspace to the second position of **BtnVal + " "**.

  <img src="images/Lesson20/Lesson20_Thermometer_16.png" width="960"/><br>

  **Finally, we want to display the respective temperatures depending on the button value.**

86. Drag the entire **LCD 16x2 >> Write to Display** from below the formula blocks from the run-forever loop onto your workspace.

  ***The time block and display erase block will remain associated with the block. That's ok, you're about to withdraw those blocks.***

87. Now place an **if do** block from **Logic** below the formula blocks in the run-forever loop.
88. Use the gear icon to add 2 **else if** blocks.
89. Now click on the time block on the workspace and drag it back into the run-forever loop below the new **if do** block.

  ***The display eraser block should be included.***

90. Now get a **" " = " "** block from **Logic** and connect it with **if**.
91. The first position of **" " = " "** becomes an **item** block with variable names ***BtnVal***.
92. The second position of **BtnVal = " "** becomes a block from numbers **0**.
93. Duplicate the **BtnVal = 0** twice and put each in an **else if** slot.
94. The value of the numerical value changes to ***1*** in the first **else if** and to ***2*** in the second **else if**.

  <img src="images/Lesson20/Lesson20_Thermometer_17.png" width="960"/><br>

  **A temperature display for each button value.**

95. Drag the **LCD 16x2 >> Write to Display** from your workspace to the first **do** section.
96. Duplicate **LCD 16x2 >> Write to Display** 2 times and paste each one into the other **do** section.
97. Now change the variable name and test the second **do** section to ***Fahrenheit*** and the second **do** section to ***Kelvin***.

  <img src="images/Lesson20/Lesson20_Thermometer_18.png" width="960"/><br>

98. Give your project a name and save.

  <img src="images/Lesson20/Lesson20_Thermometer_19.png" width="960"/><br>

99. Compile the code and upload it to your Arduino.

#### *how hot is it*

***...It's so hot that the farmers feed their chicken ice cream so they don't lay fried eggs!***

---

#### [Back to Index](#index)

### 3.21. You shall pass! <a name="you-shall-pass"></a>

This example shows you how to program a reader/writer module. You may find these at the school on the copy machine or as doors locks.
It works via radio waves, so direct contact is not necessary.

Du wirst die LCD 16x2 Anzeige nutzen, um die Resultate anzuzeigen. Es kann auch ein Türschloss sein.

<img src="images/Lesson21/Lesson21_RFIDScanner_1.png" width="960"/><br>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **RC522 RFID-Module**  | <img src="images/Parts/Part_RFID_RC522.jpg" width="256"/> | 1 | **LCD1602 Module** |<img src="images/Parts/Part_LCD1602_Modul.jpg" width="256"/> |
| 1 | **Potentiometer 10 kOhm** |<img src="images/Parts/Part_Potentiometer10kohm.jpg" width="256"/>| 7 | **W-M Cable** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/>|
| 23 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***RC522 RFID-Module with card and dongle:***

* is a highly integrated read/write device for contact-less communication at 13.56 MHz.
* has a transmitter that can operate an antenna for communication with ISO/IEC 14443A or MIFARE® cards and transponders without these cards requiring an external power supply.
* has a receiver that contains a circuit for demodulating and decoding the signal from ISO/IEC 14443A/MIFARE® compatible cards and transponders.
* supports contact-less communication with transmission rates of up to 848 kbit/s in both directions.


#### Circuit diagram

<img src="images/Lesson21/Lesson21_RFIDScanner_2.png" width="960"/><br>

#### Assembly instructions

1. LCD 16x2 Display: In the upper or lower middle part in row (Right)
2. Potentiometer: With two pins in the upper center section via center bridge with one pin in the lower center section (Middle).
3. M-M Cable 1: Top blue row of breadboard >> Arduino GND
4. M-M Cable 2: Obere rote Reihe der Steckplatte >> Arduino 5V
5. M-M Cable 3: Above the left pin (GND) of the potentiometer, upper middle section >> Upper blue row (-) of the breadboard board
6. M-M Cable 4: Above the right pin (VCC) of the potentiometer upper center section >> Upper red row (+) of the breadboard
7. M-M Cable 5: Below the potentiometer pin in the lower center section >> Above the 3rd pin of the LCD 16x2
8. M-M Cable 6: Above 1st pin of the LCD 16x2 >> Top blue row (-) of the breadboard
9. M-M Cable 7: Above 2nd pin of the LCD 16x2 >> Top red row (+) of the breadboard
10. M-M Cable 8: Above 4th pin of the LCD 16x2 >> Arduino pin 7
11. M-M Cable 9: Above 4th pin of LCD 16x2 >> Top blue row (-) of breadboard
12. M-M Cable 10: Above 5th pin of the LCD 16x2 >> Arduino pin 8
13. M-M Cable 11: Above 11th pin of the LCD 16x2 >> Arduino pin ~9
14. M-M Cable 12: Above 12th pin of the LCD 16x2 >> Arduino pin ~10
15. M-M Cable 13: Above 13th pin of the LCD 16x2 >> Arduino pin ~11
16. M-M Cable 14: Above 14th pin of the LCD 16x2 >> Arduino pin 12
17. M-M Cable 15: Above 15th pin of the LCD 16x2 >> Top red row (+) of the breadboard
18. M-M Cable 16: Above 16th pin of the LCD 16x2 >> Top blue row (-) of the breadboard
19. W-M Cable 1: RFID-Modul VCC Pin >> Upper middle part of the breadboard board
20. W-M Cable 2: RFID-Modul RST Pin >> Upper middle part of the breadboard board
21. W-M Cable 3: RFID-Modul GND Pin >> Upper middle part of the breadboard board
22. W-M Cable 4: RFID-Modul MISO Pin >> Upper middle part of the breadboard board
23. W-M Cable 5: RFID-Modul SOMI Pin >> Upper middle part of the breadboard board
24. W-M Cable 6: RFID-Modul SCK Pin >> Upper middle part of the breadboard board
25. W-M Cable 7: RFID-Modul SDA Pin >> Upper middle part of the breadboard board
23. M-M Cable 17: Above RFID module VCC cable >> Arduino 3.3V
24. M-M Cable 18: Above RFID module RST cable >> Arduino pin ~9
25. M-M Cable 19: Above RFID module GND cable >> Upper blue row (-) of the breadboard
26. M-M Cable 20: Above RFID module MISO cable >> Arduino pin 12
27. M-M Cable 21: Above RFID module SOMI cable >> Arduino pin ~11
28. M-M Cable 22: Above RFID module SCK cable >> Arduino pin 13
29. M-M Cable 23: Above RFID module SDA cable >> Arduino pin ~10

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson21/Lesson21_RFIDScanner.png" width="960"/><br>

With the basic function on your workspace, you will now insert the basic blocks of the reader/writer and the display.

1. Drag a **Setup RFID with Pin# 9 and SS Pin# 10** block from **Input/Output** into the run-first loop.

  <img src="images/Lesson21/Lesson21_RFIDScanner_3.png" width="960"/><br>

2. Also get a **Setup LCD 16x2 on digital pins** from **Displays** and place it in the run-first loop.

  <img src="images/Lesson21/Lesson21_RFIDScanner_4.png" width="960"/><br>

3. Change the pin numbers to ***3***, ***5***, ***6***, and ***2***.

  <img src="images/Lesson21/Lesson21_RFIDScanner_5.png" width="960"/><br>

  **Each card or dongle has its own identification. You have to find this out first. The code uses a Boolean (true/false) function to represent new identification or check cards/dongle for identity.**

4. Use a **set item to** block from **Variables** together with a **true** block from **Logic**, which are placed in the run-first loop.

  <img src="images/Lesson21/Lesson21_RFIDScanner_6.png" width="960"/><br>

5. Rename **item** to ***NewID***.

  <img src="images/Lesson21/Lesson21_RFIDScanner_7.png" width="960"/><br>

  **First you will now create a welcome text for the display, which will appear after you switched on the Arduino.**

6. To do this, drag a **LCD 16x2 >> Write to Display** from **Displays** into the run-forever loop.

  ***Use your own text. Remember....16 characters per line!***

7. Add ***>>>Welcome to<<<*** for the first line and ***>DIGITAL STAGES<*** for the second line.

  <img src="images/Lesson21/Lesson21_RFIDScanner_8.png" width="960"/><br>

  **Now you will use an "if do" function to build a part for reading a new card or dongle. This is the part that is triggered when NewID is true.**

8. Use an **if do** block from **Logic** and place it below the welcome text block.

9. Press the gear icon and add an **else if** block to the function.

  <img src="images/Lesson21/Lesson21_RFIDScanner_9.png" width="960"/><br>

10. Connect a **" " + " "** block to **if** of the function block.
11. The first position will be an **item** block from **Variables**, which will be given the variable name ***NewID***.
12. The second position of **NewID = " "** becomes a **true** block from **Logic**.
13. Duplicate your **NewID = true** block and connect it with **else if**.
14. However, change **true** to ***false*** here.

  <img src="images/Lesson21/Lesson21_RFIDScanner_10.png" width="960"/><br>

  **Now we will read the actual ID from the device.***

15. Drag a **RFID RC522 >> Get UID item** block from **Input/Output** into the section of **do** under **if**.
16. Rename **item** to ***GetID***.

  <img src="images/Lesson21/Lesson21_RFIDScanner_11.png" width="960"/><br>

  **Next we want to show the identity.**

17. Below the read block with **GetID**, position a **LCD 16x2 >> Write to Display** block from **Displays**.

  <img src="images/Lesson21/Lesson21_RFIDScanner_12.png" width="960"/><br>

18. In the text block of the first line, write ***New ID:***.
19. Delete the second block of text and replace it with a **item** block from **Variables**.
20. Change the variable name to ***GetID***.

  <img src="images/Lesson21/Lesson21_RFIDScanner_13.png" width="960"/><br>

  ***Make sure that NewID is set to "true" in the run-first loop.***

  ***Now compile the code and upload it to your Arduino.***

  <img src="images/Lesson21/Lesson21_RFIDScanner_14.png" width="960"/><br>

  #### *Time to test!*

  **After the welcome text has been displayed, hold the dongle and card over the reader module.**

  ***Write down the identity of your card and dongle!***

  **Ok, now that you know the identity of your card/dongle, you can now query this.**

21. Duplicate the **RFID RC522 >> Get UID GetID** from the **do** section of **if** and paste the copy into the **do** section of **else if**.
22. Below that place a new **if do** block.

  **Here you can add many identities later. To do this, add an "else if" part per identity.**

  **You're just going to add an identity now so you also have a bad example.**

  <img src="images/Lesson21/Lesson21_RFIDScanner_15.png" width="960"/><br>

23. Click on the gear icon and add the **else** block to the function block.
24. Connect a **" " = " "** block from **Logic** with the **if** slot of this block.
25. The first position of **" " = " "** becomes a **item** block from **Variables** with variable name ***GetID***.
26. The second position will be a block of text **" "** of **Text** with the identity of your card or dongle.

  <img src="images/Lesson21/Lesson21_RFIDScanner_16.png" width="960"/><br>

  **In the "do" column of "if" or "else if" now comes the code that is triggered when the specific identity is recognized.**

  **This can be a door opener, light, sound or a specific text such as the cardholder's name.**

27. The first block in the **do** column of **if** becomes the **Clear LCD 16x2 Display** block from **Displays**.
28. Then drag a block **LCD 16x2 >> Write to Display** from **Displays** underneath.
29. Write *++++ID ACCEPTED++*** for the first line and ***++Access Granted+*** for the second line.

  ***This also can be your own text. However keep in mind, there are only 16 characters per line.***

  <img src="images/Lesson21/Lesson21_RFIDScanner_17.png" width="960"/><br>

  **Give a time before the display goes off again.**

30. Now pull a **wait 1000 milliseconds** block from **Time** below the **LCD 16x2 >> Write to Display** block and increase the time to ***3000*** milliseconds.
31. Now place a **Clear LCD 16x2 Display** block from **Displays** underneath to finish.

  <img src="images/Lesson21/Lesson21_RFIDScanner_18.png" width="960"/><br>

32. Now duplicate each individual block from the **do** column of **if** and paste them one by one into the **do** column of **else**.

  ***You can also do this with any expanded "else if" section in the future.***

33. Now change the text of the first line to ***====WRONG ID====*** and the second line to ***==Access Denied=***.

  <img src="images/Lesson21/Lesson21_RFIDScanner_19.png" width="960"/><br>

34. Save your project after naming it.
35. Make sure the value of **NewID** is set to ***false***.
36. Compile the code and upload it.

#### *Take your card and stand near the reader. Then try your dongle.*


---

#### [Back to Index](#index)

### 3.22. Blink Blink Blink <a name="blink-blink-blink"></a>

The bigger and more complicated your projects get, the more pins you need for it. Well, your Arduino has a certain number of these. Now what if you need more inputs and/or outputs?

This is where the shift register comes into play. This can manage different connections through a time pulse, whereby it only needs 3 pins (plus power and ground). Not only that, because the shift register can be connected in series and thus many more connections.


<img src="images/Lesson22/Lesson22_ShiftRegister_1.png" width="960"/><br>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 2 | **Red LED** | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/> | 2 | **Yellow LED** | <img src="images/Parts/Part_LED_Yellow_5mm.jpg" width="256"/> |
| 2 | **Green LED** | <img src="images/Parts/Part_LED_Green_5mm.jpg" width="256"/> | 2 | **Blue LED** | <img src="images/Parts/Part_LED_Blue_5mm.jpg" width="256"/> |
| 8 | **220 Ohm Resistor** | <img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>| 1 | **Shift register 74HC595N** | <img src="images/Parts/Part_74HC595N.jpg" width="256"/> |
| 18 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***Shift register 74HC595N:***

* is a chip that has eight memory locations, each of which can store either a 1 or a 0.
* changes the values via the "Data" and the "Clock" pin of the chip.
* regularly receives 8 pulses from the Arduino via the time pin (clock) and checks each of the data pins (data) to see whether they are "HIGH" or "LOW", i.e. 1 or 0.
* has a "latch" pin that is activated once the data pin values have been checked to store them in the register.
* also has an "Output Enable" (OE) pin, which can be used to turn all outputs on or off at once.


#### Circuit diagram

<img src="images/Lesson22/Lesson22_ShiftRegister_2.png" width="960"/><br>

#### Assembly instructions

1. 2 green, 2 red, 2 yellow and 2 blue LEDs at a distance of 2 plug holes: Short leg in upper blue row (-) of the breadboard >> Long leg 1st row upper middle part
2. Resistor 220 Ohm: 4th row of the column of the respective LED in the upper middle part >> 1st row of the respective same column in the lower middle part
3. 74HC595N: Above the dividing line of the middle part, 8 pins in the upper middle part and 8 pins in the lower middle part with Karbe on chip pointing to the left.
4. M-M Cable 1: Top blue row (-) of breadboard >> Arduino GND
5. M-M Cable 2: Top blue row (-) of breadboard >> Bottom blue row (-) of breadboard
6. M-M Cable 3: Bottom red row (+) of breadboard >> Arduino 5V
7. M-M Cable 4: Below resistor plug hole in column 1 in the lower middle part >> Above shift register pin 2 in the upper middle part
8. M-M Cable 5: Below resistor plug hole in column 2 in the lower middle part >> Below shift register pin 1 in the lower middle part
9. M-M Cable 6: Below resistor plug hole in column 3 in the lower middle part >> Below shift register pin 2 in the lower middle part
10. M-M Cable 7: Below resistor plug hole in column 4 in the lower middle part >> Below shift register pin 3 in the lower middle part
11. M-M Cable 8: Below resistor plug hole in column 5 in the lower middle part >> Below shift register pin 4 in the lower middle part
12. M-M Cable 9: Below resistor plug hole in column 6 in the lower middle part >> Below shift register pin 5 in the lower middle part
13. M-M Cable 10: Below resistor plug hole in column 7 in the lower middle part >> Below shift register pin 6 in the lower middle part
14. M-M Cable 11: Below resistor plug hole in column 8 in the lower middle part >> Below shift register pin 7 in the lower middle part
15. M-M Cable 12: Below shift register Pin 8 in the lower center section >> Lower blue row (-) of the breadboard
16. M-M Cable 13: Above shift register pin 1 in the upper middle section >> Lower red row (+) of the breadboard
17. M-M Cable 14: Above shift register pin 3 in the upper middle part >> Arduino pin 12
18. M-M Cable 15: Above shift register pin 4 in the upper middle section >> Upper blue row (-) of the breadboard
19. M-M Cable 16: Above shift register pin 5 in the upper middle part >> Arduino pin ~11
20. M-M Cable 17: Above shift register pin 6 in the upper middle part >> Arduino pin ~9
21. M-M Cable 18: Above shift register pin 7 in the upper middle section >> Lower red row (+) of the breadboard

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson22/Lesson22_ShiftRegister.png" width="960"/><br>

1. Drag the base function onto the workspace.

  **If you want to use a set of code over and over again, you can use a function that combines them into a new block.**

2. To do this, pull a **to do something** block from **Functions** and place it above the base function.
3. Name this function ***updateShiftRegister***.

  <img src="images/Lesson22/Lesson22_ShiftRegister_3.png" width="960"/><br>

4. Drag 4 **set digital pin# to HIGH** blocks from **Input/Output** onto your workspace.
5. Place 2 in the **updateShiftRegister** function and 2 in the run-first loop of the base function.
6. Change the pin numbers of the blocks in **updateShiftRegister** both to ***11*** while the **HIGH** value of the first block becomes ***LOW***.
7. The pin numbers of the two blocks in the first-run loop become ***9*** and ***12***, respectively.

  <img src="images/Lesson22/Lesson22_ShiftRegister_4.png" width="960"/><br>

  **Now you integrate the functionality of the shift register.**

8. Place a **shiftOut >> Data# 0 Clk# 0 Order MSBFIRST byteVal item** from **Various** between the two blocks in **updateShiftRegister**.

  <img src="images/Lesson22/Lesson22_ShiftRegister_5.png" width="960"/><br>

9. Change the data pin to ***12***, the clk pin to ***9***, the order to ***LSBFIRST*** and rename the byteVal **item** to ***LEDs*** .

  <img src="images/Lesson22/Lesson22_ShiftRegister_6.png" width="960"/><br>

10. Drag a **set item to " " to Character** block into the run-first loop.
11. Add a **0** block from **Math** into the **" "** gap.
12. Choose the variable ***LEDs*** for **item** and the format ***Byte*** for **Character**.
13. Duplicate this block and drag the copy into the run-forever loop.

  <img src="images/Lesson22/Lesson22_ShiftRegister_7.png" width="960"/><br>

  **Now you will insert your specially created function as a block.**

14. Place the **updateShiftRegister** block now available in **Functions** into the run-forever loop.

  <img src="images/Lesson22/Lesson22_ShiftRegister_8.png" width="960"/><br>

15. Below that, put a **wait 1000 milliseconds** block from **Time** in the run-forever loop and reduce the time to ***100*** milliseconds.
16. Duplicate the time block you just created and leave it on your workspace for now.

  <img src="images/Lesson22/Lesson22_ShiftRegister_9.png" width="960"/><br>

  **The shift register uses a byte consisting of 8 bits to organize the 8 ports.**

  **In this example you have 8 LEDs that can be addressed by the byteVal variable.**

  **Using a loop function, we will now step through the 8 bits of the byte, flashing the LED for 100 milliseconds at a time.**

17. Now drag the loop function **count with i from 0 to 10 by 1** from **Loops** into the run-forever loop.
18. Change the **10** to ***7***.

  <img src="images/Lesson22/Lesson22_ShiftRegister_10.png" width="960"/><br>

19. Now duplicate **updateShiftRegister** and paste it into the **do** section.
20. Position the time block from the work area below.

  <img src="images/Lesson22/Lesson22_ShiftRegister_11.png" width="960"/><br>

  **Nun fehlt nur noch die Zuweisung des Bits des byteVal LEDs.**

21. Place the **Set variable of item to bit item** block from **Various** in the first position in **do** of the loop function.

  <img src="images/Lesson22/Lesson22_ShiftRegister_12.png" width="960"/><br>

22. Choose ***LEDs*** as first **item** variable name and ***i*** as second variable.
23. Name your project and save.

  <img src="images/Lesson22/Lesson22_ShiftRegister_13.png" width="960"/><br>

24. Compile your code and upload it to your Arduino.


  #### *Change the time, the order (LSBFIRST) and the "at" number of the loop function.*


---

#### [Zurück zum Index](#index)

### 3.23. Bright, Lighter, Light Brighter <a name="brighter,lighter,light-brighter"></a>

In this example you will see how the shift register bits are controlled directly depending on the analog sensor. Here we will use a photo cell. The more direct you shine light onto the photo cell, the more LEDs will light up.

<img src="images/Lesson23/Lesson23_PhotoCell_1.png" width="960"/><br>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 2 | **Red LED** | <img src="images/Parts/Part_LED_Red_5mm.jpg" width="256"/> | 2 | **Yellow LED** | <img src="images/Parts/Part_LED_Yellow_5mm.jpg" width="256"/> |
| 2 | **Green LED** | <img src="images/Parts/Part_LED_Green_5mm.jpg" width="256"/> | 2 | **Blue LED** | <img src="images/Parts/Part_LED_Blue_5mm.jpg" width="256"/> |
| 8 | **220 Ohm Resistor** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/> | 1 | **Shift register 74HC595N** |<img src="images/Parts/Part_74HC595N.jpg" width="256"/> |
| 1 | **1 kOhm Resistor** |<img src="images/Parts/Part_Resistor_1KOhm.png" width="256"/> | 1 | **Photo cell** |<img src="images/Parts/Part_Photocell.jpg" width="256"/> |
| 20 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***Photo cell***

* is a resistor that is affected by light.
* sometimes also called LDR.
* has a resistance of 50 kOhm in complete darkness.
* has a resistance of about 500 ohms in bright light.
* can be used like a potentiometer via an analog pin from the Arduino and a 1 kOhm resistor in addition.

#### Circuit diagr

<img src="images/Lesson23/Lesson23_PhotoCell_2.png" width="960"/><br>

#### Assembly instructions

1. 2 green, 2 red, 2 yellow and 2 blue LEDs at a distance of 2 plug holes: Short leg in upper blue row (-) of the breadboard >> Long leg 1st row upper middle part
2. Resistor 220 Ohm: 4. Row of the column of the respective LED in the upper middle part >> 1. Row of the respective same column in the lower middle part
3. 74HC595N: Above the dividing line of the middle part, 8 pins in the upper middle part and 8 pins in the lower middle part with kerf on the chip pointing to the left.
4. M-M Cable 1: Top blue row (-) of breadboard >> Arduino GND
5. M-M Cable 2: Top blue row (-) of breadboard >> Bottom blue row (-) of breadboard
6. M-M Cable 3: Bottom red row (+) of breadboard >> Arduino 5V
7. M-M Cable 4: Below resistor plug hole in column 1 in the lower middle part >> Above shift register pin 2 in the upper middle part
8. M-M Cable 5: Below resistor plug hole in column 2 in the lower middle part >> Below shift register pin 1 in the lower middle part
9. M-M Cable 6: Below resistor plug hole in column 3 in the lower middle part >> Below shift register pin 2 in the lower middle part
10. M-M Cable 7: Below resistor plug hole in column 4 in the lower middle part >> Below shift register pin 3 in the lower middle part
11. M-M Cable 8: Below resistor plug hole in column 5 in the lower middle part >> Below shift register pin 4 in the lower middle part
12. M-M Cable 9: Below resistor plug hole in column 6 in the lower middle part >> Below shift register pin 5 in the lower middle part
13. M-M Cable 10: Below resistor plug hole in column 7 in the lower middle part >> Below shift register pin 6 in the lower middle part
14. M-M Cable 11: Below resistor plug hole in column 8 in the lower middle part >> Below shift register pin 7 in the lower middle part
15. M-M Cable 12: Below shift register Pin 8 in the lower center section >> Lower blue row (-) of the breadboard
16. M-M Cable 13: Above shift register pin 1 in the upper middle section >> Lower red row (+) of the breadboard
17. M-M Cable 14: Above shift register pin 3 in the upper middle part >> Arduino pin 12
18. M-M Cable 15: Above shift register pin 4 in the upper middle section >> Upper blue row (-) of the breadboard
19. M-M Cable 16: Above shift register pin 5 in the upper middle part >> Arduino pin ~11
20. M-M Cable 17: Above shift register pin 6 in the upper middle part >> Arduino pin ~9
21. M-M Cable 18: Above shift register pin 7 in the upper middle section >> Lower red row (+) of the breadboard
22. Photo cell: Above the dividing line of the middle section to the right of the shift register
23. 1 kOhm Resistor: 1. Photocell Column Row >> Top blue row (-) of breadboard
24. M-M Cable 19: Below the photocell in the lower center section >> Lower red row (+) of the breadboard
25. M-M Cable 20: Between resistor and photocell in the same column in the upper middle part >> Arduino analog pin A0


**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson23/Lesson23_PhotoCell.png" width="960"/><br>

  **This time we'll start with the code from the last project.**

1. Open the shift register project and delete everything from the loop function.
2. Then delete the time block in the run-forever loop and the two **0 as Byte** blocks connected to **set LEDs to**.

  <img src="images/Lesson23/Lesson23_PhotoCell_3.png" width="960"/><br>

3. Now pull the loop function out of the run-forever loop and place it back above the **updateShiftRegister** block.
4. Create a duplicate of **set LEDs to** and place them in the 1st row of the run-forever loop.
5. Click on the block's **LEDs** and then on **New Variable...** and rename the variable to ***ReadLight***.
6. Now connect a **0** block from **Math** to the **set LEDs to** of the run-first and run-forever loops.
7. Delete the number block **7** from the loop function and replace it with an **item** block from **Variables**.
8. Change the variable name to ***ReadLight***.

  <img src="images/Lesson23/Lesson23_PhotoCell_4.png" width="960"/><br>

9. Join a **" " + " "** block from **Math** with **set ReadLight to**.
10. Change the **+** symbol to ***/***.
11. For the first position of **" " / " "** use a **Read digital pin# 0** from **Input/Output**.
12. The second position is a **0** block from **Math** with a value of ***57***.

  <img src="images/Lesson23/Lesson23_PhotoCell_5.png" width="960"/><br>

  **The light value of the photo cell should determine the number of 8 LEDs.**

  **If the light value is greater than 512 (512 - 1024) all 8 LEDs should be on.**

13. Drag an **if do** block from **Logic** and place it between **set ReadLight to** and **set LEDs to**.
14. Connect **if** to a **" " = " "** block from **Logic**.
15. Change the symbol **=** to ***>***.
16. The first position of **" " > " "** becomes an **item** block from **Variables** with variable names ***ReadLight***.
17. The second position will be a block of numbers **0** with value ***8***.

  <img src="images/Lesson23/Lesson23_PhotoCell_6.png" width="960"/><br>

18. Drag a **set item to** block into the **do** section of the **if ReadLight > 8** function.
19. Choose the variable name ***ReadLight*** and connect a block of numbers **0** from **Math** with the value ***8*** to it.

  <img src="images/Lesson23/Lesson23_PhotoCell_7.png" width="960"/><br>

20. In the **count with i from 0 to ReadLight by 1** function, place a **set item to** block with variable name ***LEDs***.
21. Join this block with a **" " + " "** block from **Math**.
22. The first item is an **item** block from **Variables** with variable name ***LEDs***.

  <img src="images/Lesson23/Lesson23_PhotoCell_8.png" width="960"/><br>

23. The second position of **LEDs + " "** becomes a **" " & ""** block from **Various**.
24. Change the symbol **&** to **<<**.
25. Again the first position here will be a block of numbers **0** with the value ***1***, and the second will be a block of **item** with ***i*** as the variable name.

  <img src="images/Lesson23/Lesson23_PhotoCell_9.png" width="960"/><br>

26. Give the project a different name and save it.
27. Then compile and upload it to the Arduino.

  #### *Use your flashlight from your phone or tablet and shine it on the photocell!*

---

#### [Zurück zum Index](#index)

### 3.24. 9.8.7.6.5.4.3.2.1.0...Start! <a name="countdown...9...0"></a>

In this project, you will learn how to use a 7 segment (plus dot) single digit display.

We will program a countdown from 9 to 0.


<img src="images/Lesson24/Lesson24_7SegmentDisplay_1.png" width="960"/><br>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 8 | **220 Ohm Resistor** |<img src="images/Parts/Part_Resistor_220Ohm.png" width="256"/>| 1 | **Shift register 74HC595N** |<img src="images/Parts/Part_74HC595N.jpg" width="256"/> |
| 1 | **7 Segment Display** |<img src="images/Parts/Part_7SegmentDisplay.jpg" width="256"/>| 28 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Neue Komponenten

**7 Segment Display**

* consists of 8 LEDs, which 7 segments are used for a digit and a point.
* has 10 pins with 2 grounds and 1 pin each for an LED.
* is an alternative to the more complex dot matrix displays.

The number 8 will be displayed when all segments are powered and if you disconnect power for 'G' the number 0 will be displayed. In a seven segment display, current (or voltage) can be applied to different pins at the same time, so we can make combinations of the numeric display from 0 to 9. Because seven-segment displays cannot form an alphabet like X and Z, it cannot be used for alphabet and only to display decimal numeric quantities. However, seven-segment displays can form the letters A, B, C, D, E, and F, so they can also be used to represent hexadecimal digits.

<img src="images/Lesson25/Lesson25_7SegmentDisplay_SegCharacters.png" width="128"/><br>

#### Circuit diagram

<img src="images/Lesson24/Lesson24_7SegmentDisplay_2.png" width="960"/><br>

#### Assembly instructions

1. Seven-segment display: Above the dividing line of the middle section with upper pins in the upper middle section to the left of resistors >> Lower pins in the lower middle section
2. Resistor 220 Ohm: Across the dividing line of the center section at a distance of 2 pinholes in a column with one leg in the upper center section and one leg in the lower center section
3. 74HC595N:Above the dividing line of the middle part, 8 pins in the upper middle part and 8 pins in the lower middle part with Karbe on chip pointing to the left.
4. M-M Cable 1: Top blue row (-) of breadboard >> Arduino GND
5. M-M Cable 2: Top blue row (-) of pegboard >> Bottom blue row (-) of breadboard
6. M-M Cable 3: Bottom red row (+) of breadboard >> Arduino 5V
7. M-M Cable 4: Below resistor plug hole in column 1 in the lower middle part >> Above shift register pin 2 in the upper middle part
8. M-M Cable 5: Below resistor plug hole in column 2 in the lower middle part >> Below shift register pin 1 in the lower middle part
9. M-M Cable 6: Below resistor plug hole in column 3 in the lower middle part >> Below shift register pin 2 in the lower middle part
10. M-M Cable 7: Below resistor plug hole in column 4 in the lower middle part >> Below shift register pin 3 in the lower middle part
11. M-M Cable 8: Below resistor plug hole in column 5 in the lower middle part >> Below shift register pin 4 in the lower middle part
12. M-M Cable 9: Below resistor plug hole in column 6 in the lower middle part >> Below shift register pin 5 in the lower middle part
13. M-M Cable 10: Below resistor plug hole in column 7 in the lower middle part >> Below shift register pin 6 in the lower middle part
14. M-M Cable 11: Below resistor plug hole in column 8 in the lower middle part >> Below shift register pin 7 in the lower middle part
15. M-M Cable 12: Below shift register Pin 8 in the lower center section >> Lower blue row (-) of the breadboard
16. M-M Cable 13: Above shift register pin 1 in the upper middle section >> Lower red row (+) of the breadboard
17. M-M Cable 14: Above shift register pin 3 in the upper middle section >> Arduino pin 2
18. M-M Cable 15: Above shift register pin 4 in the upper middle section >> Upper blue row (-) of the breadboard
19. M-M Cable 16: Above shift register pin 5 in the upper middle part >> Arduino pin ~3
20. M-M Cable 17: Above shift register pin 6 in the upper middle part >> Arduino pin 4
21. M-M Cable 18: Above shift register pin 7 in the upper middle section >> Lower red row (+) of the breadboard
22. M-M Cable 19: Above seven-segment display Pin 3 in the upper middle section >> Upper blue row (-) of the plug-in board
23. M-M Cable 20: Below seven segment display Pin 3 in the lower center section >> Lower blue row (-) of the breadboard
24. M-M Cable 21: Above the seven-segment display, pin 1 in the upper middle section >> Above the resistor plug hole in column 7 in the upper middle section
25. M-M Cable 22: Above the seven-segment display, pin 2 in the upper middle section >> Above the resistance plug hole in column 6 in the upper middle section
26. M-M Cable 23: Above the seven-segment display, pin 4 in the upper middle section >> Above the resistor plug hole in column 1 in the upper middle section
27. M-M Cable 24: Above the seven-segment display, pin 5 in the upper middle section >> Above the resistance plug hole in column 2 in the upper middle section
28. M-M Cable 25: Below the seven-segment display, pin 1 in the lower middle section >> above the resistance plug hole in column 5 in the upper middle section
29. M-M Cable 26: Below the seven-segment display, pin 2, in the lower middle section >> Above the resistor, plug hole in column 4 in the upper middle section
30. M-M Cable 27: Below the seven-segment display pin 4 in the lower middle section >> above the resistance plug hole in column 3 in the upper middle section
31. M-M Cable 28: Below the seven-segment display, pin 5 in the lower middle section >> Above the resistance plug hole in column 8 in the upper middle section

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson24/Lesson24_7SegmentDisplay.png" width="960"/><br>

  **Since we're using the shift register again, you can use one of the two previous projects.**

  ***Remember, it's always better to give the project a new name first when using other projects as a template.***

1. Completely delete everything from the run-forever loop.
2. In the run-first loop, the two **sets of digital pins#** blocks remain.
3. Drag the **LEDs** variable from **shiftOut >> ...** of the **updateShiftRegister** function onto the workspace.
4. Rename **LEDs** to ***Digit***.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_3.png" width="960"/><br>

  **The display needs an initiation.**

5. Drag a **Setup 7 Segment Display** block to the first position of the run-first loop.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_4.png" width="960"/><br>

6. For the empty space of **shiftOut >> ...** in **updateShiftRegister** we use block **7 Segment Display >> Write Digit** from **Displays**.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_5.png" width="960"/><br>

7. Now fill the empty space of **7 Segment Display >> Write Digit** with the **Digit** variable block from the workspace.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_6.png" width="960"/><br>

8. Drag a **count with i from 0 to 10 by 1** block from **Loops** into the run-forever loop.
9. Swap the **10** block with the **0** block, or enter the numbers using the keyboard.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_7.png" width="960"/><br>

10. Now add 2 **wait 1000 milliseconds** blocks from **Time**, one in **do** of the loop function and one below as the last block in the run-forever loop.
11. Increase the time of the time block in the last row of the run-forever loop to ***3000***.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_8.png" width="960"/><br>

12. Pull a **updateShiftRegister** block from **Functions** in the **do** section of the loop function.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_9.png" width="960"/><br>

  **Now you have to set the digit or the number that should be displayed.**

13. Place a **set item to** block from **Variables** in the first row of the **do** section in the loop function.
14. Change the variable name to ***Digit***.
15. Join this block with a **" " + " "** block from **Math**.
16. Change the **+** symbol to ***-***.
17. The first position will be an **item** block with variable name ***i***.
18. The second position is a **0** number block with value ***1***.

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_10.png" width="960"/><br>

19. Save the project after naming it (in case you didn't do it initially).

  <img src="images/Lesson24/Lesson24_7SegmentDisplay_11.png" width="960"/><br>

20. Compile your code and upload it to your Arduino.

#### *Live long and prosper! start in 9.......*


---

#### [Back to Index](#index)

### 3.25. And 1, 2, 3, 4 <a name="1..2..3..4"></a>

We already had a metronome, but now you are going to build a visual version, which can also be enriched with sound later.

Anyway, you can keep the beat even if you can't hear the metronome.

With the two push buttons you can set the BPM (beats per minute).

<img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_1.png" width="960"/><br>

The display with the 7 segments and the point have a specific assignment, which are defined with letters. Each segment is an LED, even if you use more than one display, as in this case.

The 7 segment displays placed next to each other are controlled by time pulses.

Since we are not using a shift register for this display, we will route the display LED pins directly to the Arduino.

<img src="images/Lesson25/Lesson25_7SegmentDisplay_SegCharacters.png" width="128"/><br>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 2 | **Press button**  | <img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/> | 1 | **4-digit, seven-segment display** | <img src="images/Parts/Part_4Digit7SegmentDisplay.jpg" width="256"/> |
| 19 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***4-digit, seven-segment display:***

* is a combination of 4 x 1 digit 7 segment displays with 4 extra pins to turn each digit on and off.
* used for digital clocks, clock radios, calculators, wristwatches, speedometers, automobile odometers, radio frequency displays, etc.

#### Circuit diagram

<img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_2.png" width="960"/><br>

#### Assembly instructions

1. 4 x seven-segment display: Above the dividing line of the middle section with upper pins in the upper middle section to the left of resistors >> lower pins in the lower middle section
2. Push button: Above the dividing line of the middle part at a distance of 2 plug holes
3. M-M Cable 1: Top blue row (-) of breadboard >> Arduino GND
4. M-M Cable 2: Below seven-segment display Pin 1 in the lower center section >> Arduino Analog Pin A0
5. M-M Cable 3: Below seven-segment display pin 2 in the lower middle section >> Arduino pin ~5
6. M-M Cable 4: Below seven-segment display pin 3 in the lower middle section >> Arduino pin 1
7. M-M Cable 5: Below seven-segment display pin 4 in the lower middle part >> Arduino pin 4
8. M-M Cable 6: Below seven-segment display pin 5 in the lower center section >> Arduino pin 8
9. M-M Cable 7: Below seven-segment display pin 6 in the lower center section >> Arduino pin 11
10. M-M Cable 8: Above seven-segment display pin 1 in the upper middle part >> Arduino pin ~6
11. M-M Cable 9: Above seven-segment display pin 2 in the upper middle part >> Arduino pin 2
12. M-M Cable 10: Above seven-segment display pin 3 in the upper middle section >> Arduino pin 7
13. M-M Cable 11: Above seven-segment display pin 4 in the upper middle part >> Arduino pin ~9
14. M-M Cable 12: Above seven-segment display Pin 5 in the upper middle part >> Arduino Pin ~10
15. M-M Cable 13: Above seven-segment display Pin 6 in the upper middle part >> Arduino Pin ~3
16. M-M Cable 14: Above push button 1 left pin 1 in the upper middle part >> Arduino pin 12
17. M-M Cable 15: Above push button 2 left pin 1 in upper middle part >> Arduino pin 13
18. M-M Cable 16: Above push button 1 right pin 1 in the upper middle section >> Upper blue row (-) of the breadboard
19. M-M Cable 17: Above push button 2 right pin 1 in the upper middle section >> Upper blue row (-) of the breadboard


**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay.png" width="960"/><br>

  **After you drag base function onto the workspace, you will adjust the display. All individual pins must be specified here.**

1. Drag a **Setup 4 x 7 Display to pin#** from **Displays** into the run-first loop.
2. Change the pin numbers to ***A:2, B:3, C:4, D:5, E:A0, F:7, G:8, DP:1, D1:6, D2:9, D3:10 , D4:11***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_3.png" width="960"/><br>

3. Place 2 **set item to** blocks from **Variables** in the run-first loop.
4. Name the variables ***BPM*** and ***ShowBPM***.
5. Connect **BPM** to a block of numbers **0** from **Math** with the value ***120***.
6. Connect**ShowBPM** with a **true** block from **Logic** and change the value to ***false***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_4.png" width="960"/><br>

7. Drag 2 more **set item to** blocks from **Variables** this time into the run-forever loop.
8. Name the variables ***valDwn*** and ***valUp***.
9. Connect both blocks with a **Read digital with PULL_UP mode Pin# 0** from **Input/Output**.
10. Set the pin number for **valDwn** to ***12*** and for **valUp** to ***13***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_5.png" width="960"/><br>

  **When you press one of the push buttons, the display should automatically show the "beats per minute".**

11. Position an **if do** block from **Logic** in the run-forever loop.
12. Join **if** with a **" " and " "** block from **Logic**.
13. Change **and** to ***or***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_6.png" width="960"/><br>

14. The first position gets a **" " = " "** block from **Logic** with a **item** block from **Variables** as the first position and a **HIGH** block from **Input /Output** as the second position.
15. **item** becomes ***valDwn*** and **HIGH** becomes ***LOW***.
16. Duplicate **valDwn = Low** and paste the copy in the second position of **valDwn = LOW or " "**.
17. Change the variable name of the copy to ***valUp***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_7.png" width="960"/><br>

18. Make a copy of **set ShowBPM to false** and drag the block into the **do** section of **if do** in the run-forever loop.
19. Change **false** to ***true***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_8.png" width="960"/><br>

20. Drag **if do** block from **Logic** into run-forever loop.
21. Join a **" " = " "** block from **Logic** with **if**.
22. As the first position of **" " = " "** insert an **item** block and choose ***ShowBPM*** as variable name.
23. For the second position of **ShowBPM = " "** insert a **True** block from **Logic**.
24. Make a full copy of the **if do** block and place it below the original.
25. Change the value from **true** to ***false*** here.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_9.png" width="960"/><br>

  **When the "beats per minute" is shown, one should be able to use the push buttons to increase or decrease the number. After 3 seconds the display will automatically switch back to the metronome counter.**

26. Add a **if do** block to the **do** section of the **if ShowBPM = true** function.
27. Use the gear icon to add 2 **else if**.
28. Get a copy of the **valDwn = LOW** block from the **if do** function above and connect it to **if** of the newly created **if do** function.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_10.png" width="960"/><br>

29. Take a copy of **valUp = LOW** from the **if do** function above and connect it to the first **else if** slot.
30. Also connect a **" " = " "** block from **Logic** for the second **else if** section.
31. Change the **=** symbol to ***>***.
32. As the first position you also need an **item** from **Variables**.
33. Name the variable ***curTime***.
34. The second position will be a block of numbers **0** with the value ***3000***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_11.png" width="960"/><br>

  **Now comes the part where the number is added or subtracted. The 3 second waiting time is always reset to 0.**

35. Drag a **set item to** into the **do** section of **if valDwn = LOW** and choose the variable name ***BPM***.
36. Concatenate a **" " + " "** block from **Math** with **set BPM to**.
37. Drag the **item** block from **Variables** into the first position and select ***BPM*** here as well.
38. Change the icon from **+** to ***-***.
39. Then put a block of numbers **0** with the value ***1*** in the second position.
40. Under **set BPM to BPM - 1** place a time block **wait 1000 milliseconds** with the value ***200***.
41. Below that comes another **set item to** from **Variables** and give the variable the name ***startTime***.
42. Connect this block to **current elapsed Time (milliseconds)** from **Time**.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_12.png" width="960"/><br>

43. Make a duplicate of each of the 3 block combinations of the **do** section of **if valDwn = LOW** and put this in the same order in the **else if** section below.
44. Here you only need to change the **-** symbol back to ***+***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_13.png" width="960"/><br>

45. Copy the block **set showBPM = true** from the top **if do** function in the run-forever loop and put it in the **do** section of the second **else if** section **else if cutTime > 3000**.
46. Then change the **true** value to ***false***.

  **Below the embedded "if do" function, we now need two more block combinations, which end up in the "if ShowBPM = true" at the bottom.**

47. Drag a **4 x 7 Segment Display >> Write Digit/Char " "** block from **Displays** below the entire **if valDwn = LOW ...** function.
48. Put an **item** block in the gap and choose ***BPM*** as the variable name.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_14.png" width="960"/><br>

  **Now calculate the current time to get the 3 seconds for automatic switch-off.**

49. As the bottom block entry of the **if ShowBPM = true**, put a copy of **Set startTime to current elapsed Time (milliseconds)**.
50. Change the variable name **startTime** to ***curTime***.
51. Separate the block **current elapsed Time (milliseconds)** from **set curTime to** and put it onto your work space.
52. Connect **set curTime to** to a **" " + " "** block from **Math**.
53. Change the **+** symbol to ***-***.
54. As the first position of **" " - " "** retrieve the **current elapsed Time (milliseconds)** block from the workspace.
55. For the second position, use an **item** block from **Variables** and choose ***startTime*** as the name.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_15.png" width="960"/><br>

  **Now comes the part that shows the numbers 1, 2, 3 and 4 in the rhythm of "beats per minute".**  

  **This all takes place in the "ShowBPM = false" function.**

56. Drag a **set item to** block from **Variables** into the **do** section of the **ShowBPM = false** function.
57. Rename the variable name to ***Beat***.
58. Then join this block with a **" " + " "** block from **Math** and change the **+** symbol to ***x***.
59. In the second position of **Set Beat to " " + " "** you need a number block **0** from **Math** with the value ***100***.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_16.png" width="960"/><br>

60. A new **" " + " "** block from **Math** is required as the first position.
61. The symbol must be changed to **/**.
62. First position of **" " / " "** becomes a number block **0** from **Math** with value ***600***.
63. The second position of **600 / " "** this time becomes an **item** block from **Variables** with the variable name ***BPM***.
64. Underneath it, drag an **item D1 4 x 7 Segment Display >> Create Char** block from **Displays**.
65. Rename **item** to ***StepOne*** and click on the tick boxes so that a one will be displayed.

  ***Use the graphic representation of the 7 segment display at the beginning of this project for help!***

  **Then the number should be displayed in time of bpm.**

66. Get a **wait 1000 milliseconds** block from **Time** and place it below the display block.
67. Delete the **1000** block and replace it with an **item** block, which will be named ***Beat***.

  **After the display has shown the bar number, it should be deleted.**

68. Drag a **4 x 7 Segment Display >> Clear Display** block from **Displays** below the time block.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_17.png" width="960"/><br>

  **Currently only 1 would be displayed.**

69. Repeat 64. - 68. three times with the following variable names and pin numbers:
  * ***StepTwo D2*** <br>
  * ***StepThree D3*** <br>
  * ***StepFour D4*** <br>


70. Use the tick boxes to represent the number 2, 3, and 4 respectively.

  <img src="images/Lesson25/Lesson25_4Digit7SegmentDisplay_18.png" width="960"/><br>

71. Name your project and save the project.
72. Compile the code and upload it to your Arduino.

  #### *And now: always stay in tune!*


---

#### [Back to Index](#index)

### 3.26. Let Us Make Some Wind <a name="lets-make-some-wind"></a>

In this example you will control a DC motor + rotor with 2 push buttons. Adjusting the push buttons will change the direction of the motor and its speed.

<img src="images/Lesson26/Lesson26_DCMotor_1.png" width="960"/><br>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Breadboard power supply module** | <img src="images/Parts/Part_BreadBoardPowerSupply.jpg" width="256"/> | 1 | **L293D IC Motor controller** |<img src="images/Parts/Part_MotorcontrollerL293D.jpg" width="256"/> |
| 2 | **Push button** |<img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/> | 1 | **3-6V DC motor with fan blades** |<img src="images/Parts/Part_DCMotorWithBlades.jpg" width="256"/> |
| 8 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***Breadboard power supply module***

* is required if, for example, you have to supply a motor with energy.
* serves to protect the Arduino.
* can determine the upper and lower output voltage via jumpers (push button switches). Possible voltages are 3.3 volts or 5 volts.
* has a switch to turn the power on and off.

***Make sure you place the power supply module correctly on the breadboard. Note the *+* and *-* signs to identify the correct position.***

***L293D IC motor controller***

* is a versatile chip.
* can control two motors independently.
* is a 4-channel high-current driver that can generate bidirectional currents of up to 1000 mA at voltages from 4.5V to 36V.
* can operate relays, solenoids, DC and bipolar stepper motors with high current or voltage requirements.
* has various pins fitted into ground pins, power flow switch pins, 3.3 and 5 volt inputs and direction pins.
* can change speed by using PWM pulses from the Arduino.

  <img src="images/Lesson26/N295L_MotorController.png" width="512"/><br>

***3-6V DC Motor with fan blades***

* is a DC motor and therefore the most frequently used motor.
* has two leads, one positive and one negative.

If you connect these two cables directly to a battery, the motor spins. If you swap the leads, the motor will rotate in the opposite direction.

#### ***Warning − Do not drive this motor directly from Arduino board pins! This can damage the circuit board!***

#### Circuit diagram

<img src="images/Lesson26/Lesson26_DCMotor_2.png" width="960"/><br>

#### Assembly instructions

1. Power supply module: Connect the **+** and **-** pins to the blue (+) and red (-) rows, respectively.
2. L293D: Above the dividing line of the middle part, 8 pins in the upper middle part and 8 pins in the lower middle part with Karbe on chip pointing to the left.
3. Push button: Above the dividing line of the middle part at a distance of 2 plug holes
4. M-M Cable 1: Bottom blue row (-) of breadboard >> Arduino GND
5. M-M Cable 2: Above push button 1 left pin 1 in the upper middle part >> Arduino pin 7
6. M-M Cable 3: Above push button 2 left pin 1 in upper middle part >> Arduino pin 8
7. M-M Cable 4: Above push button 1 right pin 1 in the upper center section >> Bottom blue row (-) of the breadboard
8. M-M Cable 5: Above push button 2 right pin 1 in upper center section >> Bottom blue row (-) of breadboard
9. M-M Cable 6: Below L293D pin 1 in the lower middle section >> Arduino pin ~5
10. M-M Cable 7: Below L293D pin 2 in the lower middle section >> Arduino pin 4
11. M-M Cable 8: Below L293D Pin 4 in the lower middle section >> Lower blue row (-) of the breadboard
12. M-M Cable 9: Below L293D pin 7 in the lower middle section >> Arduino pin ~3
13. M-M Cable 8: Below L293D Pin 8 in the lower middle section >> Lower red row (+) of the breadboard
14. DC motor red cable **+**: Below L293D pin 3 in the lower center section
15. DC motor black cable **-**: Below L293D pin 6 in the lower center section


**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson26/Lesson26_DCMotor.png" width="960"/><br>

1. Place a **set item to** block from **Variables** in the run-first loop.
2. Rename **item** to ***Speed***.
3. Then connect **set Speed to** to a **0** number block from **Math** and change the value to ***255***.

  <img src="images/Lesson26/Lesson26_DCMotor_3.png" width="960"/><br>

  **We have 2 pins from the motor controller to determine the direction of rotation.**

4. Pull 2 **set digital pin# 0 to** blocks from **Input/Output** into the run-first loop.
5. The first block gets the pin number ***3***.
6. The second block gets the pin number ***4*** and a status ***LOW***.

  <img src="images/Lesson26/Lesson26_DCMotor_4.png" width="960"/><br>

  **Use a Boolean operator to determine whether the direction of rotation should change.**

7. Drag a **set item to** block from **Variables** into the run-first loop and rename it to ***revDir***.
8. Add a **true** block from **Logic** to **set revDir to** and set the value to ***false***.

  <img src="images/Lesson26/Lesson26_DCMotor_5.png" width="960"/><br>

  **Use a variable with numbers to hold the current speed level.**

9. Place a **set item to** block from **Variables** in the run-first loop and rename it to ***SpeedLevel***.
10. Add a **0** block from **Math** to **set SpeedLevel to** and set the value to ***2***.

  <img src="images/Lesson26/Lesson26_DCMotor_6.png" width="960"/><br>

  **You need two variables that will listen to the push buttons all the time if one is pressed.**

11. Loop a **set item to** block from **Variables** this time and rename it to ***Btn1***.
12. Add to **set Btn1 to** a **Read digital with PULL_UP mode Pin# 0** block from **Input/Output** and set the pin number to ***7***.
13. Now copy the entire block combination **set Btn1 to ...** and place the duplicate below.
14. Give the copy a new variable named ***Btn2*** and the pin number ***8***.

  <img src="images/Lesson26/Lesson26_DCMotor_7.png" width="960"/><br>

  **Now you're going to calculate the turning speed based on the speed level.**

15. Copy the **set Speed to 255** block from the run-first loop and drag the duplicate into the run-forever loop.
16. Now separate the **255** block and reduce the number to ***150***.
17. Now connect **set Speed to** with a **" " + " "** block from **Math** and use the block **150** in the first position.

  <img src="images/Lesson26/Lesson26_DCMotor_8.png" width="960"/><br>

18. The second position of **150 + " "** needs a **" " + " "** block from **Math** with the symbol ***x***.
19. Again, the first item consists of an **item** block from **Variables** named ***SpeedLevel***.
20. The second position is a number block with the value ***50***.

  <img src="images/Lesson26/Lesson26_DCMotor_9.png" width="960"/><br>

  **In order to control the speed via the motor controller, you have to send the speed via an analogue pin.**

21. Drag a **Set analog pin# 0 to** block from **Input/Output** into the run-forever loop.
22. The pin number must be ***5***.
23. Connect an **item** block from **Variables** to the block and substitute the variable name ***Speed***.

  <img src="images/Lesson26/Lesson26_DCMotor_10.png" width="960"/><br>

  **If we want the direction of rotation to change, we need the function that responds to true and false of the Boolean operator.**

24. To do this, use an **if do** block from **Logic**, which you position in the run-forever loop.
25. Use the gear icon to add an **else if** part.
26. Then put a **" " = " "** block from **Logic** after **if**.
27. As first position select an **item** block from **Variables** with variable name ***revDir***.
28. In the second position of **revDir = " "** you need a **true** block from **Logic**.
29. Change the value to ***false***.

  <img src="images/Lesson26/Lesson26_DCMotor_11.png" width="960"/><br>

30. Now make a copy of **revDir = false** and connect it with **else if**.
31. Change the value **false** back to ***true***.

  <img src="images/Lesson26/Lesson26_DCMotor_12.png" width="960"/><br>

32. Copy both **set digital pin#...** blocks from the run-first loop and put them in the **do** section of **if revDir = false**.

  <img src="images/Lesson26/Lesson26_DCMotor_13.png" width="960"/><br>

33. Once again, copy both **set digital pin#...** blocks from the run-first loop and put them in the **do** section of **else if revDir = true**.
34. This time change **LOW** to ***HIGH*** and **HIGH** to ***LOW***.

  <img src="images/Lesson26/Lesson26_DCMotor_14.png" width="960"/><br>

  **Next you will create the code that will be triggered each time you press one of the push buttons.**

35. To do this, drag a new **if do** block from **Logic** into the run-forever loop.
36. Again, use the gear icon to add an **else if** part.
37. Now draw another **" " = " "** block from **Logic** and connect it to **if**.
38. The first item will be an **item** block from **Variables** with variable name ***Btn1***.
39. As second block use **HIGH** from **Input/Output**.
40. The value of **HIGH** must change to ***LOW***.
41. Create a duplicate of **Btn1 = LOW** and connect the copy with **else if**.
42. Change the variable name of the copy to ***Btn2***.

  <img src="images/Lesson26/Lesson26_DCMotor_15.png" width="960"/><br>

43. Make a copy of **set revDir to false** from the run-first loop and drag it into the **do** section of **if Btn1 = LOW**.
44. Now use the **change item by 1** block from **Math** with the variable name ***SpeedLevel*** in **if Btn1 = LOW**.

  <img src="images/Lesson26/Lesson26_DCMotor_16.png" width="960"/><br>

  **Each time you press a button, 1 will add up. But you only need 0, 1 and 2 as the speed level. So we need a method here to go back to 0 when it goes above 2.**

45. Therefore, drag another **if do** block from **Logic** into the **do** section of **if Btn1 = LOW**.
46. Substitute a **" " = " "** block from **Logic** as the **if** condition.
47. The **=** symbol changes to ***>***.
48. The first item is an **item** block from **Variables** with the variable name ***SpeedLevel***.
49. The second position is a number block ***2***.
50. For the **do** part, copy in the **set SpeedLevel to 2** block from the run-first loop.
51. Then reduce the number **2** to ***0***.

  <img src="images/Lesson26/Lesson26_DCMotor_17.png" width="960"/><br>

  **After each button press, the program should wait briefly before continuing, so that a double press counts as a single press.**

52. Draw a **wait 1000 milliseconds** below the **if SpeedLevel > 2**.
53. Change the value to **200**.

  <img src="images/Lesson26/Lesson26_DCMotor_18.png" width="960"/><br>

  **Now it is clear as with button 1, now the 2nd button needs the same blocks with the exception of the Boolean value.**

54. Copy all blocks of the **do** section under **if Btn1 = LOW** and paste them in the same order into the **do** section under **else if Btn2 = LOW**.
55. The only value that changes is **false** to ***true*** to change the direction of rotation.

  <img src="images/Lesson26/Lesson26_DCMotor_19.png" width="960"/><br>

56. Name your project and save it.

  <img src="images/Lesson26/Lesson26_DCMotor_20.png" width="960"/><br>

57. Compile the code and upload the project to your Arduino.

  ***Make sure the breadboard power module is plugged into the power outlet and the switch on it is pressed.***

---

#### [Back to Index](#index)

### 3.27. Tik Tak Tok <a name="tic-tac-toc"></a>

Imagine you want to control a green house with water supply, door opener, window opener, ventilation, heating, etc. using your Arduino. Many of the devices should be switched on and off automatically, including those that require significantly more energy (e.g. 220V Lights, big pumps, huge motors s.o.).

This is where the relay comes into play. You can operate this electrical switch with your Arduino, in this case here a 6V motor.

*With other relay switches you can even control devices with 220 volts. However, you only should consider this with under advice of an experienced person.*


<img src="images/Lesson27/Lesson27_Relay_1.png" width="960"/><br>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Breadboard Power supply module** | <img src="images/Parts/Part_BreadBoardPowerSupply.jpg" width="256"/> | 1 | **L293D IC motor controller** |<img src="images/Parts/Part_MotorcontrollerL293D.jpg" width="256"/> |
| 1 | **5V Relais** |<img src="images/Parts/Part_5VRelais.jpg" width="256"/> | 1 | **3-6V DC motor with fan blades** |<img src="images/Parts/Part_DCMotorWithBlades.jpg" width="256"/> |
| 4 | **W-M Cable** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> | 6 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***5V Relais:***

* is a switch triggered by electricity.
* often use an electromagnet that mechanically completes the circuit when voltage is applied.
* is used if you want to switch the circuit with a weaker control circuit or if you want to switch many circuits at once with one control signal. The control circuit and the circuit to be controlled are electrically completely isolated from each other.

#### Circuit diagram

<img src="images/Lesson27/Lesson27_Relay_2.png" width="960"/><br>

#### Assembly instruction

1. Power supply module: Connect the **+** and **-** pins to the blue (+) and red (-) rows, respectively
2. L293D: Above the dividing line of the middle part, 8 pins in the upper middle part and 8 pins in the lower middle part with kerf on chip pointing to the left
3. M-M Cable 1: Bottom blue row (-) of breadboard >> Arduino GND
4. M-M Cable 2: Below L293D pin 1 in the lower middle section >> Arduino pin ~5
5. M-M Cable 3: Below L293D pin 2 in the lower middle section >> Arduino pin 4
6. M-M Cable 4: Below L293D Pin 4 in the lower middle section >> Lower blue row (-) of the breadboard
7. M-M Cable 5: Below L293D pin 7 in the lower middle section >> Arduino pin ~3
8. M-M Cable 6: Below L293D Pin 8 in the lower middle section >> Lower red row (+) of the breadboard
9. W-M Cable 1: Bottom center position of 5V relay >> Bottom red row (+) of breadboard
10. W-M Cable 2: Lower position to the left of the 5V relay >> Below L293D pin 3 in the lower center section
11. W-M Cable 3: Lower Position Right of 5V Relay >> Below L293D Pin 6 in lower center section
12. W-M Cable 4: Upper position To the right of the 5V relay >> In the upper middle section
13. DC Motor red cable **+**: Below W-M cable 4 on upper middle part
14. DC Motor black cable **-**: Top blue row (-) of breadboard

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson27/Lesson27_Relay.png" width="960"/><br>

1. As always, drag the base function from **Functions** onto your workspace.
2. Then pull 3 **set digital pin# 0 to HIGH** from **Input/Output** into the run-first loop.
3. Select the following pin number and status: ***5 >> HIGH***, ***3 >> HIGH*** and ***4 >> LOW***.

  <img src="images/Lesson27/Lesson27_Relay_3.png" width="960"/><br>

4. Duplicate **set digital pin# 5 to HIGH** and drag the copy into the run-forever loop.
5. Get a **repeat 10 times do** block from **Loops** into the run-forever loop.
6. Change the number to ***5***.
7. Now duplicate **set digital pin# 3 to HIGH** and **set digital pin# 4 to LOW**.
8. Drag 2 copies into the **do** section of **repeat 5 times**.
9. Below the copies, place a **wait 1000 milliseconds** block from **Time** and reduce the value to ***500***.
10. Now duplicate these 3 blocks again and place them directly below the original time block.
11. Then change the status from pin number **3** to ***LOW*** and from pin number **4** to ***HIGH***.

  <img src="images/Lesson27/Lesson27_Relay_5.png" width="960"/><br>

12. Make a copy of **set digital pin# 5 to HIGH** and drag it below the **repeat 5 times** loop in the run-forever loop.
13. Change the status to ***LOW***.
14. Then add another time block below **wait 1000 milliseconds** and increase the value to ***3000***.

  <img src="images/Lesson27/Lesson27_Relay_6.png" width="960"/><br>

15. Now duplicate all blocks of the run-forever loop and put them in the same order as the second part under the original part.

  ***For example, now change the wait times in the retry loop to 750.***

  <img src="images/Lesson27/Lesson27_Relay_7.png" width="960"/><br>

16. Name the project, save, compile and upload to your Arduino.

  #### *With the magic of the relay switch, you can now give the fan a rhythm!*

  ***Try different rep counts and wait times.***

---

#### [Back to Index](#index)

### 3.28. Brumm! <a name="brummm"></a>

In this project you will use a motor, which is used for example for an axis in a 3D printer.

You will control the motor through the push button rotary decoder, where the direction of rotation of the rotary decoder determines the direction of rotation of the motor. The more you turn in one direction, the more the motor turns in that direction.

By pressing the button, the motor rotates back to the starting position.


<img src="images/Lesson28/Lesson28_StepperMRotaryPot_1.png" width="960"/><br>


#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Breadboard Power supply module** | <img src="images/Parts/Part_BreadBoardPowerSupply.jpg" width="256"/> | 1 | **ULN2003 Stepper motor driver module** |<img src="images/Parts/Part_ULN2003_StepperModule.jpg" width="256"/>|
| 1 | **Stepper motor** |<img src="images/Parts/Part_StepperMotor.jpg" width="256"/>| 1 | **Rotary encoder module** |<img src="images/Parts/Part_RotaryEncoder.jpg" width="256"/> |
| 11 | **F-M Cable** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> | 1 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***ULN2003 Stepper motor driver module:***

* The easiest way to make a uni-polar stepper motor using an Arduino to control a connection in the transistor array of the ULN2003A is to be bridged.
* The ULN2003A chip contains seven Darlington transistors and has an additional seven more TIP120 transistors on board. It can draw up to 500mA per channel
output and has an internal voltage drop of about 1V when switched on Status.
* It also contains internal clamping diodes to reduce voltage spikes 209 / 223 shift when inductive loads are operated. To the stepper motor too
must control each of the coils with voltage at a specific frequency be taken care of.

***Stepper motor:***

* is an electro mechanical device that converts electrical pulses into discrete mechanical movements.
* rotates in a way that is highly dependent on the input signal or pulses from the Arduino.
* uses a sequence of the pulses that directly determines the direction of the rotation, while the length of the rotation depends on the number and frequency of the pulses.


***Rotary encoder module:***

* is a sensor for the angle of rotation.
* also called a rotary decoder.
* is digital in contrast to the potentiomenter.
* has a push button.

#### Circuit diagram

<img src="images/Lesson28/Lesson28_StepperMRotaryPot_2.png" width="960"/><br>

#### Assembly instructions

1. Stepper motor: Connect motor cable connector to ULN2003 socket
2. ULN2003: Above the dividing line of the middle part 8 pins in the upper middle part and 8 pins in the lower middle part with kerf on chip pointing to the left
3. M-M Cable 1: Bottom blue row (-) of breadboard >> Arduino GND
4. W-M Cable 1: ULN2003 Pin In1 >> Arduino Pin ~11
5. W-M Cable 2: ULN2003 Pin In2 >> Arduino Pin ~10
6. W-M Cable 3: ULN2003 Pin In3 >> Arduino Pin ~9
7. W-M Cable 4: ULN2003 Pin In4 >> Arduino Pin 8
8. W-M Cable 5: ULN2003 Pin **-** >> Bottom blue row (-) of breadboard
9. W-M Cable 6: ULN2003 Pin **+** >> Bottom red row (+) of breadboard
10. W-M Cable 7: Encoder Pin GND >> Arduino GND
11. W-M Cable 8: Encoder Pin 5V >> Arduino 5V
12. W-M Cable 9: Encoder pin SW >> Arduino pin 4
13. W-M Cable 10: Encoder Pin DT >> Arduino Pin ~3
14. W-M Cable 11: Encoder Pin CLK >> Arduino Pin 2

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson28/Lesson28_StepperMRotaryPot.png" width="960"/><br>

  **You will immediately start with your own module or function.**

1. After you have dragged the basic function onto the work area, you continue with your own function.

  **ISR (Interrupt Service Routine) is a hardware interrupt that constantly waits for a pin change. In this case it is pin number 2 to which the rotary decoder is connected.**

2. Drag a **to do something** block from **Functions** above the base function onto your workspace.
3. Call the function ***isr***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_3.png" width="960"/><br>

4. In the first row in the function, place a **wait 100 milliseconds** of **Time** with the value ***4***.
5. Then put a **if do** block from **Logic** underneath and use the gear icon to add **else**.
6. Connect a **Read digital pin# 0** block from **Input/Output** with the **if** slot.
7. Change the pin number to ***2***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_4.png" width="960"/><br>

8. Add a **set item to** block from **Variables** to the **do** section.
9. Name the variable ***RotDir*** and connect it to a copy of **Read digital pin# 2**.
10. Choose the pin number ***3***.
11. Create a duplicate of **set RotDir to Read digital pin# 3** and put the copy in the **else** section.
12. Add a **not** block from **Logic** between **set RotDir to** and **Read digtal pin# 3**.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_5.png" width="960"/><br>

13. Now position another **set item to** variable below the **if Read digital pin# 2** function and give it the name ***TurnDetected***.
14. Connect these to a **true** block from **Logic**.

  **Now continue with the run-first loop.**

15. Pull an **Attach on Interrupt# 0** from **Various** into the run-first loop.
16. Change the mode to ***FALLING***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_6.png" width="960"/><br>

17. Now drag your function **isr** from **Functions** into the function section of **Attach on Interrupt# 0**.
18. Under this function drag a **Setup stepper motor MyStepper** from **Motors** into the run-first loop.
19. After setting the number of pins to ***4***, enter the following pin numbers: ***Pin1#: 8 Pin2#: 10 Pin3#: 9 Pin4#: 11***.
20. The steps per revolution becomes ***700*** and the speed becomes ***32***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_7.png" width="960"/><br>

21. Copy **set TurnDetected to true** from the **isr** function and paste the duplicate into the run-first loop.
22. Then change the value to ***false***.
23. Now make a duplicate of this block combination and position it directly below in the run-first loop.
24. The variable name becomes ***RotDir*** and the value again becomes **true**.
25. The last entry in the run-first loop becomes another variable block **set item to** from **Variables**.
26. Rename the variable to ***RotPos*** and connect it to a block of numbers **0** from **Math**.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_8.png" width="960"/><br>

  **First you will insert the reset function of the rotary movement into the run-forever loop. When you press the rotary decoder button, the motor automatically rotates back to the starting position.**

27. Start with a **if do** block from **Logic**.
28. Connect **if** first to a **not** block from **Logic**, then to a **Read digital with PULL_UP mode Pin# 0**.
29. The pin number becomes ***4***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_9.png" width="960"/><br>

30. Drag another **if do** block from **Logic** into the **do** section of **if not Read digital with PULL_UP mode Pin# 4**.
31. Now use the gear icon to add **else** in the new function.
32. Join **if** with a **" " = " "** block from **Logic**.
33. You fill the first position with an **item** from **Variables**, which is given the name ***RotPos***.
34. The second digit becomes a block of numbers **0** from **Math**.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_10.png" width="960"/><br>

  **In this role you will only use the "else" column. The "do" column remains empty, so nothing will happen in case the rotation position is 0.**

35. Make a copy of **set RotPos to 0** from the run-first loop and place the duplicate in **else** of the **if RotPos = 0** function.
36. Above that, place a **move stepper MyStepper " " steps** in the **else** section.
37. Drag the numeric block **10** onto your workspace and replace it with a **" " + " "** block from **Math**.
38. Change the symbol **+** to ***x***.
39. Now use the numeric keypad **10** in the second position of **" " x " "** and change the value to ***-1***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_11.png" width="960"/><br>

40. The first position of **" " x -1** starts with another **" " + " "** block from **Math**.
41. Again, change the symbol **+** to ***x***.
42. The first position is an **item** block with the variable name ***RotPos***.
43. The second position becomes a number block **0** with value ***50***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_12.png" width="960"/><br>

  **As soon as the interrupter registers a signal and the Boolean value changes to "true" as a result, the rotation position should be updated with the current position.**

44. Draw a **if do** block from **Logic** below the preceding **if not Read digital with PULL_UP mode Pin# 4** function.
45. Connect a **" " = " "** block from **Logic** to **if** of the new **if do** function.
46. Use an **item** block from **Variables** as the first position of **if " " = " "** and choose ***TurnDetected*** as the variable name.
47. The second position becomes a **true** block from **Logic**.
48. For the **do** section, drag in a **set item to** block connected to a **item** block from **Variables**.
49. Create a variable name ***PrevPos*** for **set item to** and choose the variable name ***RotPos*** for the connected block.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_13.png" width="960"/><br>

50. Below that, drag another **if do** function from **Logic** into the **if TurnDetected = true** function and press the gear icon to add the **else** block to the function.
51. Join a **" " = " "** block from **Logic** with **if**.
52. The first position will be an element block with chosen variable name ***RotDir***, while the second one should be a **true** block.
53. In the **do** section, put a copy of **set RotPos to 0** from the run-first loop.
54. Separate the number block, place it on the workspace and increase the number to ***1***.
55. Then connect a **" " + " "** block from **Math** with ***RotPos*** as variable block in first position and number block **1** in second position.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_14.png" width="960"/><br>

56. Make a copy of **set RotPos to Rotpos + 1** and drag it to the **else** section and change the symbol **+** to ***-***.
57. Then position a **set item to** block below the **if RotDir = true** function.
58. Connect a **true** block from **Logic** and change the value to ***false***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_15.png" width="960"/><br>

  **Now add two more "if do" functions to do the actual motor movement.**

59. Position a new **if do** block from **Logic** in the bottom row of the run-forever loop.
60. Draw a **" " = " "** block from **Logic** and connect it with **if**.
61. The second position becomes an element block with variable name ***RotPos***.
62. The first position takes a **" " + " "** block with a number block in the second position, both from **Math**.
63. Change the number to ***1*** and set an item block with variable name ***PrevPos***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_16.png" width="960"/><br>

64. Drag a **move stepper MyStepper 10 steps** into the **do** section.
65. Change the number block value to ***50***.
66. Now make a complete copy of the **if PrevPos + 1 = RotPos** and place it directly below.
67. Change the **+** symbol to ***-*** and the number block value from **50** to ***-50***.

  <img src="images/Lesson28/Lesson28_StepperMRotaryPot_17.png" width="960"/><br>

68. Give the project a name and save it.
69. Compile the code and upload it to the Arduino.

#### *Turn the rotary decoder in both directions, then press the rotary control.*

***You can change the steps to affect how long the stepper motor moves.***

---

#### [Back to Index](#index)

### 3.29. Last But Not Least <a name="last-but-not-least"></a>

In the last example you will operate the stepper motor with the remote control.

<img src="images/Lesson29/Lesson29_StepperMIRRemote_1.png" width="960"/><br>

#### What parts do you need?

| *Count* | *Description* | *Image*  | *Count*  | *Description*  | *Image* |
|----------|--------------|---------|----------|---------------|--------|
| 1 | **Arduino Uno + USB Cable** |<img src="images/Parts/Part_ArduinoUnoR3.jpg" width="256"/> | 1 | **Breadboard** |<img src="images/Parts/Part_Breadboard.jpg" width="256"/> |
| 1 | **Breadboard power supply module** | <img src="images/Parts/Part_BreadBoardPowerSupply.jpg" width="256"/> | 1 | **ULN2003 stepper motor driver module** |<img src="images/Parts/Part_ULN2003_StepperModule.jpg" width="256"/>|
| 1 | **Stepper motor**  |<img src="images/Parts/Part_StepperMotor.jpg" width="256"/>| 1 | **IR-Receiver module** |<img src="images/Parts/Part_IRModule.jpg" width="256"/> |
| 1 | **IR-Remote control** | <img src="images/Parts/Part_IRRemoteControl.jpg" width="256"/> | 6 | **F-M Cable** |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> |
| 4 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### New components

***None***

#### Circuit diagram

<img src="images/Lesson29/Lesson29_StepperMIRRemote_2.png" width="960"/><br>

#### Assembly instructions

1. Stepper motor: Connect motor cable plug to ULN2003 socket
2. ULN2003: Above the dividing line of the middle part, 8 pins in the upper middle part and 8 pins in the lower middle part with kerf on the chip pointing to the left
3. M-M Cable 1: Bottom blue row (-) of breadboard >> Arduino GND
4. W-M Cable 1: ULN2003 Pin In1 >> Arduino Pin ~11
5. W-M Cable 2: ULN2003 Pin In2 >> Arduino Pin ~10
6. W-M Cable 3: ULN2003 Pin In3 >> Arduino Pin ~9
7. W-M Cable 4: ULN2003 Pin In4 >> Arduino Pin 8
8. W-M Cable 5: ULN2003 Pin **-** >> Bottom blue row (-) of breadboard
9. W-M Cable 6: ULN2003 Pin **+** >> Bottom red row (+) of breadboard
10. W-M Cable 7: IR Module Pin GND >> Bottom blue row (-) of breadboard
11. W-M Cable 8: IR Module Pin 5V >> Bottom red row (+) of breadboard
12. W-M Cable 9: IR Module Pin DATA >> Arduino Pin 12

**Compare your version to the schematic photo before we continue.**

#### Block for Block

<img src="images/Lesson29/Lesson29_StepperMIRRemote.png" width="960"/><br>

  **You will program button 13, 14 and 15 to operate the stepper motor.**

  <img src="images/Lesson29/Part_IRRemoteControl.jpg" width="512"/><br>

1. After dropping the base function onto the workspace.
2. Drag a **Setup stepper motor MyStepper stepper motor** into the run-first loop.
3. Set the number of pins to ***4***, the pin numbers to ***8***, ***10***, ***9*** and ***11***.
4. The steps per revolution ***900*** and the speed to ***32***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_3.png" width="960"/><br>

5. Place a **set item to** block with variable name ***RotPos*** connected to a block of numbers **0**.
6. Drag a **Setup IR Remote Control with Pin# 0** block from **Input/Output** into the run-first loop.
7. Change the pin number to ***12***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_4.png" width="960"/><br>

  **The first part of the infinite loop, like the previous example, will bring the motor to the home position. This time, however, with the push of a button on the remote control.**

8. Drag a **if do** function block from **Logic** into the run-forever loop and connect a **" " = " "** block and connect it with **if**.
9. The first position will be a **Read Value from IR Remote Control on Pin# 0** block from **Input/Output** with pin number ***12***.
10. The second position will be a block of numbers with the value ***14***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_5.png" width="960"/><br>

11. Put another **if do** function from **Logic** in the **do** section.
12. Use the gear icon to add the **else** part.
13. Connect a **" " = " "** block to **if** and use an item block with variable name ***RotPos*** in first position.
14. For the second position use a number block **0**.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_6.png" width="960"/><br>

  **Again, the "do" column remains empty. Proceed to the "else" column.**

15. Drag a **move stepper MyStepper 10 steps** block from **Motors** to the **else** section.
16. Separate the number block **10** and place it on the workspace.
17. Instead, put in a **" " + " "** block from **Math** and change the symbol from **+** to ***x***.
18. The second position is a number block with ***-1*** as the value.
19. The first position needs another **" " + " "** block with the **+** symbol changed to ***x***.
20. For the second position, drag in the number block **10** from the workspace with increased value ***100***.
21. As the first position of **" " x 100** use an item block with the variable name ***RotPos***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_7.png" width="960"/><br>

22. Create a duplicate of the **set RotPos to 0** from the run-first loop and position the copy in the **else** column of the **if RotPos = 0** function.
23. Now drag a **set item to** connected to a **item** block into the run-forever loop.
24. Change the first variable to ***PrevPos*** and the second variable to ***RotPos***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_8.png" width="960"/><br>

  **Now you will program the other two keys.**

25. Drag an **if do** function block from **Logic** into the run-forever loop and use the gear icon to add an **else if** block.
26. Connect a **" " = " "** block with **if** and use **Read Value from IR Remote Control on Pin# 0** in first position.
27. Select pin number ***12*** and put a number block with value ***15*** as the second position of **if Read Value from IR Remote Control on Pin# 12 = " "**.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_9.png" width="960"/><br>

28. Make a copy of **if Read Value from IR Remote Control on Pin# 12 = 15** and connect the duplicate with **else if** below it.
29. Change the value **15** to ***13***.
30. Drag a **set item to** block from **Variables** into the **do** section of **if Read Value from IR Remote Control on Pin# 12 = 15**.
31. Give the new block the variable name ***RotPos*** and connect the block with a **" " + " "** block from **Math**.
32. The first position will be an item block with variable name ***RotPos***.
33. The second position will be a block of numbers with the value ***1***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_10.png" width="960"/><br>

34. Make a copy of **set RotPos to RotPos + 1** and put the duplicate in the **do** section of **else if Read Value from IR Remote Control on Pin# 12 = 13**.
35. Ändere das **+** Symbol zu ***-***.
36. Drag another **if do** function into the run-forever loop.
37. Connect a **" " = " "** block from **Logic** to **if** of the new function block.
38. Change the **=** symbol to **<**.
39. The first and second positions become variable block **item** with variable names 1. ***PrevPos*** and 2. ***RotPos***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_11.png" width="960"/><br>

40. Take a **move stepper MyStepper 10 steps** block from **Motors** and put it in the **do** section of **if PrevPos < RotPos**.
41. Drag the **10** numeric block from **move stepper MyStepper 10 steps** onto the workspace and replace it with a **" " + " "** block from **Math**.
42. Change the **+** symbol to ***x*** and drag the **10** number block back to the second position.
43. Increase the value **10** to ***100***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_12.png" width="960"/><br>

44. The first position of **move stepper MyStepper " " x 100 steps** becomes a **" " + " "** block from **Math**.
45. Both positions become variable blocks **item** with variable names 1. ***RotPos*** and 2. ***PrevPos***.
46. Add a **wait 1000 milliseconds** to the **do** section of **if PrevPos < RotPos** and change the time value to ***100***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_13.png" width="960"/><br>

47. Make a complete copy of the **if PrevPos < RotPos** function and position it just below the original.
48. Instead of the **<** symbol, choose ***>***.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_14.png" width="960"/><br>

49. Save your project after naming it.

  <img src="images/Lesson29/Lesson29_StepperMIRRemote_15.png" width="960"/><br>

50. Compile it and load it on your Arduino.

***Join the two lower "if do" functions using " " or " "***.

***Expand the controls with more buttons and different rotation duration.***

---

#### [Back to Index](#index)

## 4. Robot project "Ardubot"<a name="robotproject"></a>

This project is an example project in which parts of your previously gained knowledge finds a more complex practical application.

Like all previous examples, this project is to increase your basic knowledge. As you've probably already noticed, each example opens a door to your own projects.

Whether you mix and match examples or create entirely new projects from your own ideas, you'll learn that programming the Arduino has different paths to achieve the same goal.

The little robot you are about to build and program is just another example. The difference, however, is that this robot provides a fantastic base for trying out your own code later on. You will be able to experience it directly through the resulting behavior of your ArduBot robot.

<img src="images/RobotBuild/RobotBuild.png" width="600"/>

----

### Robot project BOM (Bill of Materials)

The following list shows you which parts from your Elegoo kit **The Most Complete Starter Kit** that is used in the *ArduBot* project:

| *Part of Elegoo Starter Kit* | *Count* | *Image* | *Part of Elegoo Starter Kit* | *Count* | *Image* |
|------------------------------|---------|---------|------------------------------|---------|---------|
| **Arduino Board Uno R3** | 1 | <img src="images/Parts/Part_ArduinoUnoR3.jpg" width="128"/> | **Arduino prototyp expansion module** | 1 | <img src="images/Parts/Part_PrototypeExpansionModule.jpg" width="128"/> |
| **Servo motor** | 1 | <img src="images/Parts/Part_ServoMotor.jpg" width="128"/> | **Ultrasonic module** | 1 | <img src="images/Parts/Part_UltraSonicSensor.jpg" width="128"/> |
| **Press button** | 1 | <img src="images/Parts/Part_PushButton_4Pin.jpg" width="128"/> | **Passive Buzzer** | 1 | <img src="images/Parts/Part_PassiveBuzzer.jpg" width="128"/> |
| **9 Volt Battery** | 1 | <img src="images/Parts/Part_9VBattery.jpg" width="128"/> | **M-M cable** | 18 | <img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="128"/> |
| **Dupont MxM Cable** | 10 | <img src="images/Parts/Part_Cable_Dupont_MM.jpg" width="128"/> |  | | |


In order to be able to build the robot, additional parts are required, which I list under the name **ArduBot Kit**. You can assemble this kit yourself (see instructions below), or order it from me.


| *Part of Elegoo Starter Kit* | *Count* | *Image* | *Part of Elegoo Starter Kit* | *Count* | *Image* |
|------------------------------|---------|---------|------------------------------|---------|---------|
| **Chassis parts** | 1     | <img src="images/RobotBuild/RobotBuild_CNC_2.jpg" width="128"/> | **DC motor with wheel** | 2 | <img src="images/Parts/Part_DCMotorWheel.jpg" width="128"/> |
| **9V Battery box with switch and plug** | 1 | <img src="images/Parts/Part_9VBatterycase4ArduinoDCPlug.jpg" width="128"/> | **InfraRed sensor module** | 2 | <img src="images/Parts/Part_IRSensorModule.jpg" width="128"/> |
| **Rubber band** | 1 | <img src="images/Parts/Part_Rubberband.jpg" width="128"/> | **Roller wheel** | 1 | <img src="images/Parts/Part_RollerWheel.jpg" width="128"/> |
| **Arudino screw M3** | 4 | <img src="images/Parts/Part_ArduinoScrew_M3_5m.jpg" width="128"/>  | **PCB mount hexagonal 6 x 6 mm** | 8 | <img src="images/Parts/Part_PCBMount_6mm.jpg" width="128"/> |
| **PCB mount hexagonal 30 x 6 mm** | 4 | <img src="images/Parts/Part_PCBMount_30mm.jpg" width="128"/> | **Roller wheel screw M3 5mm** | 4 | <img src="images/Parts/Part_Screw_M3_5mm.jpg" width="128"/> |
| **DC motor screw M3 30mm** | 4 | <img src="images/Parts/Part_DCScrew_M3_30mm.jpg" width="128"/> | **PCB mount screw M3 8mm** | 4 | <img src="images/Parts/Part_PCBMount_Screw_M3_8mm.jpg" width="128"/> |
| **Nut M3** | 16 | <img src="images/Parts/Part_Nut_M3.jpg" width="128"/> |


The 2WD Smart Car Chassis Set can be used as an alternative to the ArduBot kit. Here are the necessary parts:

| *Part* | *Count* | *Image* | *Part* | *Count* | *Image* |
|--------|---------|---------| -------|---------|---------|
| **Smart Car chassis with 2 engines** | 1 | <img src="images/Parts/Part_2WDSmartCarChassis.png" width="128"/> | **Cable Dupont FxF** | 2 | <img src="images/Parts/Part_Cable_Dupont_MM.jpg" width="128"/> |
| **Rubber band** | 1 | <img src="images/Parts/Part_Rubberband.jpg" width="128"/> | **Arduino screw M3 5 mm** | 4 | <img src="images/Parts/Part_ArduinoScrew_M3_5m.jpg" width="128"/> |
| **9V Battery box with switch and plug** | 1 | <img src="images/Parts/Part_9VBatterycase4ArduinoDCPlug.jpg" width="128"/> | **InfraRed sensor module** | 2 | <img src="images/Parts/Part_IRSensorModule.jpg" width="128"/>  |

---

#### [Zurück zum Index](#index)

### 4.1. Vorbereitung <a name="rp-preparation"></a>

Ok, first of all you have to make preparations depending on the set.

#### *Ardubot kit - Only if you want to build it yourself*

If you have a CNC machine (also possible with a 3D printer), you can create the base plates and other parts yourself. In the case of a 3D printer, you can use the STL files available through the link below.

The following illustration illustrates the use of the GCode with a CNC milling machine.

| 1. Download the data for the Ardubot elements. | **2. The GCode is intended for an MDF board 3 mm x 200 mm x 300 mm.** |
|-----------------------------|------------------------------|
| ***[D.I.Y CNC Designs](/utilities/ArduBot_CNCParts.zip)*** | <img src="images/Parts/Part_MDFBoard_20x30cm.jpg" width="512"/> |
| **3. After the parts have been milled, loosen, clean and straighten the individual elements.** | **4. Paint the elements with one or more colors of your choice.** |
| <img src="images/RobotBuild/RobotBuild_CNC_1.jpg" width="600"/> | <img src="images/RobotBuild/RobotBuild_CNC_2.jpg" width="600"/> |

---

#### *2WD Smart Car Chassis*

When you purchase this kit you will receive motors with no cables attached. The cables must therefore be soldered to the motors.

| 1. Halve the two cables FxF. | 2. Solder the wires with one color for "+" and one color for "-".|
|------------------------------|------------------------------------------------------------------|
| <img src="images/RobotBuild/RobotBuild_Part1_1.jpg" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_2.jpg" width="600"/> |

---

#### [Back to Index](#index)

### 4.2. Assembly Part 1<a name="rp-assemblyP1"></a>

After all the preparations have been made, you can assemble the first part of the robot. This is the chassis itself and all components that are directly connected to the chassis.

#### *ArduBot kit*

| 1. Slide a 2-hole T-bar into the slot and hold the second T-bar in the slot in the bottom panel. | 2. Insert a motor with a wheel in between and screw it to both T-parts (2 x DC motor screw M3 30mm + 2 x nut M3) |
|-------------------------------|--------------------------------|
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_1.png" width="512"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_2.png" width="600"/> |
| **3. Slide a 2-hole T-bar into the slot and hold the second T-bar in the slot on the other side.** | **4. Insert the second motor with the wheel in between and screw it to both T-parts (2 x DC motor screw M3 30mm + 2 x nut M3).** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_3.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_4.png" width="600"/>|
| **5. Now screw the scooter wheel with 4 x PCB Mount Hexagonal 6 x 6 mm and 4 x roller wheel screw M3 5mm.** | **6. Insert the 2 infrared sensor modules into the front wide slots and carefully clamp them with the T-parts.** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_5.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_6.png" width="600"/>|
| **7. Now take the top round plate and slide the two T-pieces with grooves into the front slots with the grooves facing backwards.** | **8. Insert the servo motor facing towards front and carefully slide the U-piece into the grooves.** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_7.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_8.png" width="600"/>|
| **9. Screw the U part to the servo motor.** | **10. Slide both smiley pieces and slide them onto the wide T-piece with a hole.** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_9.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_10.png" width="600"/>|
| **11. Insert the ultrasonic module into the holes in the smiley pieces with the pins facing up. Carefully place the entire part onto the servo motor shaft.** | **12. Now screw the Arduino to the top plate with 4 x Arudino screws M3, 4 x PCB mounts hexagonal 6 x 6 mm and 4 x nuts M3. Then use the rubber band to connect the battery box (including the 9V battery) to the top plate.** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_11.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_DIY_12.png" width="600"/> |
| **13. Now screw the upper platform to the lower one with 4 x PCB mount screws M3 8mm, 4 x PCB mount hexagonal 30 x 6 mm and 4 x nuts M3.** | **14. Feed the wires through the back holes of the top platform and connect the battery box wire to the Arduino.** |
| <img src="images/RobotBuild/RobotBuild_Part1_DIY_13.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_CNC_3.jpg" width="600"/> |


#### *2WD Smart Car Chassis*


| 1. Slide a 2-hole T-bar into the slot and hold the second T-bar in the slot in the bottom panel. | 2. Insert a motor with a wheel in between and screw it to both T-parts (2 x DC motor screw M3 30mm + 2 x nut M3). |
|--------------------------|-------------------------------------|
| <img src="images/RobotBuild/RobotBuild_Part1_3.jpg" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_4.jpg" width="600"/> |
| **3. Repeat with the second motor, then snap the wheels onto the motor axles.** | **4. Now screw the scooter wheels with 8 x PCB Mount Hexagonal 6 x 6 mm and 8 x scooter wheel screw M3 5mm.** |
| <img src="images/RobotBuild/RobotBuild_Part1_5.jpg" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_6.jpg" width="600"/> |
| **5. Route the motor cables through the rear slots of the upper platform.** | **6. Now screw the Arduino to the top plate with 3 x Arudino screws M3 and 3 x nuts M3.** |
| <img src="images/RobotBuild/RobotBuild_Part1_7.png" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_8.jpg" width="600"/> |
| **7. Use the rubber to connect the battery box (including the 9V battery) to the top plate.** | **8. Connect the battery box cable to the Arduino.** |
| <img src="images/RobotBuild/RobotBuild_Part1_9.jpg" width="600"/> | <img src="images/RobotBuild/RobotBuild_Part1_10.jpg" width="600"/> |

---

#### [Back to Index](#index)

### 4.3. Assembly Part 2<a name="rp-assemblyP2"></a>

In this part you will give the little robot its first driving lessons. You will program the Arduino so that the robot goes in one direction for a few seconds, stops, turns 180 degrees, backs up, turns 180 degrees again at the starting point, and then loops again.

<img src="images/RobotBuild/RobotBuild_Part2_DCMotors_1.jpg" width="512"/><br>

#### What new parts do you need?

| *Part* | *Count* | *Image* | *Part* | *Count* | *Image* |
|--------|---------|---------| -------|---------|---------|
| 1 | **Arduino prototype expansion module (APEM)** |<img src="images/Parts/Part_PrototypeExpansionModule.jpg" width="256"/> | 2 | **DC motor with wheel**  | <img src="images/Parts/Part_DCMotorWheel.jpg" width="256"/>   |
| 1 | **Press button** | <img src="images/Parts/Part_PushButton_4Pin.jpg" width="256"/> | 1 | **Motor controller L293D** |<img src="images/Parts/Part_MotorcontrollerL293D.jpg" width="256"/>|
| 18 | **M-M Cable** |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

#### Circuit diagram

<img src="images/RobotBuild/RobotBuild_Part2_DCMotors_2.jpg" width="960"/><br>

**Chip pin distribution:**

<img src="images/RobotBuild/RobotBuild_Part2_DCMotors_2_Chip.jpg" width="512"/><br>

#### Assembly instructions

1. Arduino Prototype Expansion Module: Plug the module onto the Arduino
2. Motor controller L293D: Insert the chip into the frontmost holes over the center part of the expansion module with the notch facing backwards
3. Push button: Insert this into the rear holes over the middle section of the APEM
4. M-M Cable 1: Upper part column 1 row 4 >> APEM GND expansion module
5. M-M Cable 2: Upper part column 3 row 4 >> APEM pin 11
6. M-M Cable 3: Top part Column 10 Row 3 >> Bottom part Column 10 Row 3
7. M-M Cable 4: Top part Column 10 Row 2 >> Top part Column 17 Row 2
8. M-M Cable 5: Top part Column 13 Row 3 >> Top part Column 14 Row 3
9. M-M Cable 6: Bottom Column 13 Row 3 >> Bottom Column 14 Row 3
10. M-M Cable 7: Top part Column 14 Row 4 >> Bottom part Column 14 Row 2
11. M-M Cable 8: Upper part column 11 row 3 >> APEM pin 5
12. M-M Cable 9: Upper part column 16 row 3 >> APEM pin 6
13. M-M Cable 10: Lower part column 10 row 4 >> APEM 5V
14. M-M Cable 11: Lower part column 11 row 3 >> APEM pin 9
15. M-M Cable 12: Lower part column 13 row 5 >> APEM GND
16. M-M Cable 13: Lower part column 16 row 3 >> APEM pin 10
17. M-M Cable 14: Lower part column 17 row 3 >> APEM 5V
18. M-M Cable 15: DC Motor Left Pole 1 >> Top Column 12 Row 3
19. M-M Cable 16: DC Motor Left Pole 2 >> Top Column 15 Row 3
20. M-M Cable 17: DC Motor Right Pole 1 >> Bottom Column 15 Row 3
21. M-M Cable 18: DC Motor Right Pole 2 >> Top Column 12 Row 3


**Compare your version to the schematic photo before we continue.**


#### Block for Block

<img src="images/RobotBuild/RobotBuild_Part2_DCMotors.jpg" width="960"/><br>

  **Let's start with the motor pins. 2 pins for each direction per motor.**

1. After pulling in the base function, pull 4 **Set digital pin# 0 to HIGH** blocks from **Input/Output** into the run-first loop.
2. Choose the pin numbers ***5***, ***6***, ***9*** and ***10****.
3. Then change all **HIGH** values to ***LOW***.

  **All motors and directions are off.**

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_3.jpg" width="960"/><br>

  **Now we will use the push button, which takes over the start and stop function.**

4. Drag a **Set item to** block from **Variables** into the run-forever loop.
5. Rename **item** to ***StartBtn***.
6. Connect **Set StartBtn to** to a **Read digital with PULL_UP mode Pin# 0** block from **Input/Output**.
7. Change the pin number to ***11***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_4.jpg" width="960"/><br>

  **For each movement forward, backward, left and right we will now create a block function.**

8. Get a **to do something** block from **Functions** on your workspace.
9. Call the function ***Forward***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_5.jpg" width="960"/><br>

10. Duplicate all 4 **Set digital pin#...** blocks from the forward loop and put them in the **to Forward** function.
11. Change the value of pin number **5** and **10** to ***HIGH***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_6.jpg" width="960"/><br>

12. Create 4 duplicates of the complete **to Forward** function and position them one below the other on your workspace.
13. Name the first copy ***Backward*** and set the values of Pin# 5 to ***LOW***, Pin# 6 to ***HIGH***, Pin# 9 to ***HIGH*** and Pin# 10 to ***LOW***.
14. Name the first copy ***Stop*** and set the values of Pin# 5 to ***LOW***, Pin# 6 to ***LOW***, Pin# 9 to ***LOW*** and Pin# 10 to ***LOW***.
15. Name the first copy ***Right*** and set the values of Pin# 5 to ***HIGH***, Pin# 6 to ***LOW***, Pin# 9 to ***HIGH*** and Pin# 10 to ***LOW***.
16. Name the first copy ***Left*** and set the values of Pin# 5 to ***LOW***, Pin# 6 to ***HIGH***, Pin# 9 to ***LOW*** and Pin# 10 to ***HIGH***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_7.jpg" width="960"/><br>

  **After you press the start button, the robot is supposed to go through a certain driving protocol and then wait until you press the button again to repeat the process.**

17. To do this, use a loop block **repeat while do** from **Loops**, which you drag into the run-forever loop.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_8.jpg" width="960"/><br>

18. Connect the loop block with **" " = " "** from **Logic**.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_9.jpg" width="960"/><br>

19. As first position insert an **item** variable block with variable name ***StartBtn***.
20. The second position of **StartBtn = " "** becomes a **HIGH** block from **Input/Output**.
21. Change the value to ***LOW***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_10.jpg" width="960"/><br>

  **Now let's integrate the ride log.**

22. Pull a **Forward** block from **Functions** into the **repeat while StartBtn = LOW** loop.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_11.jpg" width="960"/><br>

  **Now the travel time for the forward motion.**

23. Below **Forward** comes a **wait 1000 milliseconds** block from **Time** and change the time to ***2000***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_12.jpg" width="960"/><br>

24. Repeat steps 22. and 23. with the following functions and time values:

  **Stop**, ***500***, **Right**, ***700***, **Stop**, ***500***, **Forward**, ***2000***, **Stop**, ***500***, **Left**, ***1400***, **Stop**, ***500***, **Backward**, ***2000***, **Stop**, ***500***, **Right**, ***700***, **Stop**, ***500***, **Backward**, ***2000***, **Stop**, ***500***.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_13.jpg" width="960"/><br>

  **After the ride log expires, you want to end the loop so that the loop doesn't finish until you press the start button again.**

25. The last block in the **repeat while StartBtn = LOW** loop becomes a **break out of loop** block of **Loops**.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_14.jpg" width="960"/><br>

26. Name your project and save it.

  ***Connect your ArduBot Arduino to your Tablet/PC via USB.***

27. Verify and upload it to your Arduino.

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_15.jpg" width="960"/><br>

  ***Remove the USB cable. Make sure the battery box is set to "On" so your Arduino gets power.***

  <img src="images/RobotBuild/RobotBuild_Part2_DCMotors_16.jpg" width="960"/><br>

***Put the robot on the ground and position it in the direction you want it to move. Then press the push button.***

**If your ArduBot turns left or right too much or too little, turn off the power, connect him/her to your computer and change the time value "700" of the left turn, right turn, or both.**

***Reload the code onto your Arduino and test the movement. Repeat the procedure until you are happy with the twist.***

#### *Write your own ride logs!*


---

#### [Back to Index](#index)

### 4.4. Assembly Part 3<a name="rp-assemblyP3"></a>

Now the robot will learn to look for dark stripes on the floor. The left and right infrared sensor modules will then help the robot follow this path.

> Is it destiny or predetermined? I follow the path, sometimes it breaks out, then I follow my path again!


<img src="images/RobotBuild/RobotBuild_Part3_IRSensors_1.jpg" width="960"/><br>

#### Notice:

* ***In the case of the ArduBot, the infrared modules are already inserted.***
* ***Otherwise, use 4 wires to connect the holes in the modules and the small slots in the front part of the lower platform (see photo above).***
* ***Put black tape in the shape of a T on white paper. You will use this to calibrate the infrared sensors (see photo below).***
* ***Find or craft a small box or cylinder that you can use to elevate the robot so the wheels are about 1cm off the ground. We also need this for the calibration of the sensors.***

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_TCalib.jpg" width="512"/><br>

#### What new parts do you need?

| *Part* | *Count* | *Image* | *Part* | *Count* | *Image* |
|--------|---------|---------| -------|---------|---------|
| 2 | **Infrared sensor module** |<img src="images/Parts/Part_IRSensorModule.jpg" width="256"/> | 6 | **W-M Cable**  |<img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/>   |

#### Circuit diagram

<img src="images/RobotBuild/RobotBuild_Part3_IRSensors_2.jpg" width="960"/><br>

#### Assembly instructions

1. W-M Cable 1: Left infrared sensor OUT >> APEM pin A1
5. W-M Cable 2: Left infrared sensor GND >> APEM GND
6. W-M Cable 3: Left infrared sensor VCC >> APEM 5V
7. W-M Cable 4: Right infrared sensor OUT >> APEM Pin A0
8. W-M Cable 5: Right infrared sensor GND >> APEM GND
9. W-M Cable 6: Right infrared sensor VCC >> APEM 5V

**Compare your version to the schematic photo before we continue.**


#### Block for Block

<img src="images/RobotBuild/RobotBuild_Part3_IRSensors.jpg" width="960"/><br>

  **Start with the previous project.**

1. Clear the entire **repeat while** loop.
2. Give the project a new name (Assembly_Part_3 or something).

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_3.jpg" width="960"/><br>

  **Since you are not programming a prepared driving course this time, you will use the start button with the help of Boolean operators.**

3. Drag a **set item to** block of **Variables** into the run-first loop.
4. Name the variable **item** as ***On/Off***.
5. Now connect this block to a **true** block from **Logic** and change the value to ***false***.

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_4.jpg" width="960"/><br>

  **Now you will create the relation between button press and condition on/off.**

6. Use an **if do** block from **Logic** in the run-forever loop.
7. Press the gear icon and drag an **else if** block under the **if** block.
8. Connect a **" " and " "** block from **Logic** with **if**.
9. In the first position put a **" " = " "** block from **Logic**.
10. Again the first position is an **item** block with variable name ***StartBtn***.
11. For the second position of **StartBtn = " "** use a **HIGH** block of **Input/Output** and change the value to ***LOW***.
12. Copy the block **StartBtn = LOW** and paste the duplicate in the second position of **StartBtn = LOW and " "**.
13. There select the new variable name ***OnOff***, delete the **LOW** block and replace it with **true** from **Logic**.
14. Change the value from **true** to ***false***.
15. Now create a copy of the block **StartBtn = LOW and OnOff = false** and connect it with **else if**.
16. Now change the value **false** to ***true***.
17. Now duplicate the **Set OnOff to false** block from the run-first loop and put a copy in each of the **do** sections.
18. Change the **false** value of **Set OnOff to False** in the **do** section from **if** to ***true***.

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_5.jpg" width="960"/><br>

  **Next you will record the data from the infrared sensors as variables.**

19. Put 2 **set item to** blocks from **Variables** in the first rows of the run-forever loop and call the variables ***LeftIR*** and ***RightIR***.
20. Then connect both blocks with a **Read digital pin# 0** block from **Input/Output**.
21. Give **LeftIR** the pin number ***A1*** and **RightIR** the pin number ***A0***.

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_6.jpg" width="960"/><br>

  **When ArduBot is turned on with the push button, it should follow a black line.**

22. Drag a block **if do** block from **Logic** above the existing **if do** function into the run-forever loop.
23. Connect to **if** a copy of **OnOff = true** from the underlying **else if** connection.
24. In the **do** section of **if OnOff = true** drag another **if do** block from **Logic**.
25. Press the gear icon and add 3 **else if** blocks to **if**.

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_7.jpg" width="960"/><br>

26. Now connect a block combination **LeftIR = 0 and RightIR = 0** with **if** in the **do** section of **if OnOff = true**.
27. Join the 3 **else if** slots with **LeftIR = 1 and RightIR = 0**, **LeftIR = 0 and RightIR = 1** and **LeftIR = 1 and RightIR = 1**.
28. Fill in the **do** sections one by one with one of the following **Functions**: **Forward**, **Left**, **Right** and **Stop**.

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_8.jpg" width="960"/><br>

29. Save the project and verify it.
30. Connect the USB cable and load it on your Ardubot Arduino.

  <img src="images/RobotBuild/RobotBuild_Part3_IRSensors_9.jpg" width="960"/><br>

***Place the robot on the box or cylinder written on the white paper with the T on it. After turning on the power and pressing the push button, slowly turn the white knob of each sensor with a screw driver until the wheels just turn on in that position.***

**If everything went well with the configuration, your ArduBot should stop as soon as the top bar of the T symbol is reached. It also should stop moving, when you lift it up from the ground.**

***Now take a large sheet of paper and use a black marker pen to draw a path for your robot to follow. You can also use black tape.***


#### *Staying grounded and on track, ArduBot!*

---

#### [Zurück zum Index](#index)

### 4.5. Assembly Part 4<a name="rp-assemblyP4"></a>

In this part we will connect the servo motor and the ultrasonic sensor to the Arduino. The program part will only use the ultrasonic sensor for the time being. As a result, the robot will stop as soon as an obstacle appears in the direction of travel.

> Head and neck on the torso to the heart trump. Now I can see!

<img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_1.png" width="640"/><br>

#### What new parts do you need?

| *Part* | *Count* | *Image* | *Part* | *Count* | *Image* |
|--------|---------|---------| -------|---------|---------|
| 1 | **Ultrasonic sensor** |<img src="images/Parts/Part_UltraSonicSensor.jpg" width="256"/> | 1 | **Servo motor**  |<img src="images/Parts/Part_ServoMotor.jpg" width="256"/>   |
| 4 | **W-M Cable**  | <img src="images/Parts/Part_Cable_Dupont_FM.jpg" width="256"/> | 5 | **M-M Cable**  |<img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/> |

***Note: In the case of the ArduBot, the servo motor and the ultrasonic module are already installed.***

#### Circuit diagram

<img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_2.jpg" width="960"/><br>

#### Assembly instructions

1. M-M Cable 1: Servo motor OUT >> APEM pin 8
2. M-M Cable 2: Servo motor VCC >> Upper part column 5 row 3
3. M-M Cable 3: Servomotor GND >> Upper part column 6 row 3
4. M-M Cable 1: Ultrasonic sensor GND >> Upper part column 6 row 2
5. W-M Cable 2: Ultrasonic sensor ECHO >> APEM pin 13
6. W-M Cable 3: Ultrasonic sensor TRIG >> APEM pin 12
7. W-M Cable 4: Ultrasonic sensor VCC >> Upper part column 5 row 2
8. M-M Cable 4: Upper part column 5 row 1 >> APEM 5V
9. M-M Cable 5: Upper part column 6 row 4 >> APEM GND

**Compare your version to the schematic photo before we continue.**


#### Block for Block

<img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_3.jpg" width="960"/><br>

  **You start with the last project.**

1. Import the last project and give it a new name.

  **Initiate the ultrasonic sensor.**

2. Drag a **Setup HC-SR04 with Echo Pin# 0 and Trigger Pin# 0** from **Sensors** to the top row of the run-first loop.
3. Change the Echo Pin# to ***13*** and the Trigger Pin# to ***12***.

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_4.jpg" width="960"/><br>

4. In the first row of the first-forever loop add a **set item to** from **Variables** with variable name ***Distance***.
5. Connect this variable block to **Read HC-SR04 from Echo Pin# 0** from **Sensors**.

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_5.jpg" width="960"/><br>

6. Change the Echo Pin# to ***13***.

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_6.jpg" width="960"/><br>

  **Now ArduBot should stop moving forward and reverse as long as there is an obstacle within 20 cm.**

7. Draw a **if do** block from **Logic** below the **Forward** function in the **do** section of **if LeftIR = 0 and RightIR = 0**.
8. Use the gear icon and add the **else** section.
9. Then copy the block combination **LeftIR = 0 and RightIR = 0** and connect the duplicate with **do** below the **Forward** function.
10. Change the block combination to ***Distance <= 20 and Distance > 0*** and drag a **Stop** block from **Functions** into the **do** section below.

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_7.jpg" width="960"/><br>

11. Now drag the **Forward** block above **if Distance <= 20 and Distance > 0** into the do section of **else**.

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_8.jpg" width="960"/><br>

12. Save the project and verify your code.
13. Connect your ArduBot to the computer with USB and upload the code.

  <img src="images/RobotBuild/RobotBuild_Part4_UltrasonicSensor_9.jpg" width="960"/><br>

  ***Turn on your ArduBot and place it on the line track. Then press the start button.***

  ***Now hold your hand in front of the eyes (ultrasonic sensor) of your robot.***


#### *Keep your distance, ArbuBot!*

---

#### [Back to Index](#index)

### 4.6. Assembly Part 5<a name="rp-assemblyP5"></a>

This section focuses on the servo motor. This allows the robot to scan a larger section for obstacles. The robot will turn in the direction where there are no obstacles or they are further away.

> Now I can turn my head and look around! Then I'll go where I have space!

<img src="images/RobotBuild/RobotBuild_Part5_ServoMotor.jpg" width="640"/><br>

#### What new parts do you need?

No new parts are needed as you are now programming the servo motor which is already installed and wired.

#### Block for Block

<img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_3.jpg" width="960"/><br>

  **Here you continue the previous project. If ArduBot has an obstacle in front of him/her, he/she should stop, look around and decide in which direction to move on to avoid it.**

1. Start with previous project and change the name.
2. Drag the **Set SERVO with Pin# 0 to 90 Degrees** block from **Motors** to the first row of the run-first loop.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_4.jpg" width="960"/><br>

3. Choose pin number ***8***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_5.jpg" width="960"/><br>

4. Drag 2 **set item to** from **Variables** to the last row of the run-first loop.
5. Name the variables ***rightDistance*** and ***leftDistance***.
6. Connect both blocks with a number block **0** from **Math**.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_6.jpg" width="960"/><br>

7. Place a **to do something** block from **Functions** on your workspace.
8. Name the function ***lookRight***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_7.jpg" width="960"/><br>

9. Create 2 duplicates of **Set SERVO with Pin# 8 to 90 Degrees** from the run-first loop and put them in the function **lookRight**.
10. Change the value **90** of the number block of the top copy to ***45***.
11. Drag 2 **wait 1000 milliseconds** blocks from **Time** between the two **Set SERVO from Pin 8...** blocks.
12. Decrease the time value of the first block to ***500*** and the second block to ***100***.
13. Between the two blocks of time put a **set item to** block from **Variables** named ***rightDistance***.
14. Connect this block to **Read HC-SR04 from Echo Pin# 0** from **Sensors**.
15. Choose pin number ***13***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_8.jpg" width="960"/><br>

16. Duplicate the entire **lookRight** function and call it ***lookLeft***.
17. Now change the number block **45** to ***135***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_9.jpg" width="960"/><br>

18. Take a **lookRight** and **lookLeft** block from **Functions** and place them one below the other in the last row of the run-first loop.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_11.jpg" width="960"/><br>

  **If the robot encounters an obstacle in front of it, it should stop and look around.**

19. Now set the following functions and time blocks below the **Stop** function in the embedded **if Distance <= 20 and Distance > 0** logic block: ***300***, **Backward**, ***400***, **Stop**, ***300***, **lookRight**, ***300***, **lookLeft**, ***300****.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_12.jpg" width="960"/><br>

  **Compared to both distances, ArduBot decides in which direction to proceed.**

20. Below the last time block with value **300**, position an **if do** block from **Logic** and use the gear icon to add an **else if** block.
21. Join **if** with a **" " = " "** block from **Logic** and choose the symbol ***>=*** instead of **=**.
22. Both positions become **item** blocks from **Variables** named in the first position ***leftDistance*** and the second position ***rightDistance***.
23. Duplicate **leftDistance >= rightDistance** and join the copy with **else if**.
24. Here change the symbol to ***>***, the first variable **leftDistance** to ***rightDistance*** and the second variable **rightDistance** to ***leftDistance***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_13.jpg" width="960"/><br>

25. Add the following functions and time blocks into the **do** sections of **if leftDistance >= rightDistance**: **Left**, ***500***, **Stop**, ***500***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_14.jpg" width="960"/><br>

26. Then add the following functions and time blocks into the **do** sections of **if rightDistance > leftDistance**: **Right**, ***500***, **Stop**, ***500***.

  <img src="images/RobotBuild/RobotBuild_Part5_ServoMotor_15.jpg" width="960"/><br>

27. Save the project, verify it and upload it to your Arduino.

***You can influence the behavior of your ArduBot by changing the values in the number blocks.***

#### *Now you can send your robot to travel through your room.*

---

#### [Back to Index](#index)

### 4.7. Assembly Part 6<a name="rp-assemblyP6"></a>

In the final part, we want to give the robot a voice. The robot reports when it can see something.

> Dideldidu....I see something, do you see it too!?

<img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_1.jpg" width="640"/><br>

#### What new parts do you need?

| *Part* | *Count* | *Image* | *Part* | *Count* | *Image* |
|--------|---------|---------| -------|---------|---------|
| 1 | **Passive Buzzer** |<img src="images/Parts/Part_PassiveBuzzer.jpg" width="256"/> | 2 | **M-M Cable**  | <img src="images/Parts/Part_Breadboard_Jumpwire_MM.jpg" width="256"/>  |

#### Circuit diagram

<img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_2.jpg" width="960"/><br>

#### Assembly instructions

1. Passive Buzzer: Stick the buzzer **+** in column 4 row 5 and **-** in column 7 row 5 of the bottom piece.
2. M-M Cable 1: Lower part column 4 row 2 >> APEM 5V
3. M-M Cable 2: Bottom Column 7 Row 2 >> Top Column 6 Row 5

**Compare your version to the schematic photo before we continue.**

#### Block for Block

1. Proceed with the last project, it is always better to give the project a new name to keep the old one.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_3.jpg" width="960"/><br>

  **Start with a variable that holds the starting frequency of the tone.**

2. Drag a **set item to** block from **Variables** to the bottom row of the run-first loop.
3. Rename **item** to ***RootNote*** and connect the block to a number block from **Math**.
4. Increase the number to ***440***.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_4.jpg" width="960"/><br>

  **ArduBot is designed to make a sound when an obstacle is between 20 and 50 cm.**

5. Add a **if do** block from **Logic** to the first row of the **do** section of **if On/Off = true**.
6. Use the gear icon to add **else**.
7. Now connect **do** to a **" " and " "** block from **Logic**.
8. In the first position, add a **" " + " "** block from **Math**.
9. Change the **+** symbol to ***<***.
10. The first position of **" " < " "** becomes an **item** block with variable names **Distance**.
11. The second position will be a block of numbers **0** from **Math** with the incremented value ***50***.
12. Now make a copy of **Distance < 50** and paste it into the second position of **if Distance < 50 and " "**.
13. The symbol **<** becomes ***>*** and the number value changes to ***20***.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_5.jpg" width="960"/><br>

  **Now the part that produces the actual tones.**

14. Add to the **do** section of **if Distance < 50 and Distance > 20** a **Set tone on pin# 0 to 220** block from **Audio** and choose the pin number ***4***.
15. On the other hand, in the **do** section of **else if** comes a **Turn off tone on pin# 0** from **Audio** with pin number ***4***.
16. Drag a variable block **item** onto your workspace and choose the variable name ***RootNote***.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_6.jpg" width="960"/><br>

  **If you only play the base note, your ArduBot would sound very monotonous and lifeless. So incorporate some randomness into it.**

17. Drag 2 **" " + " "** blocks from **Math** onto your workspace.
18. Change the symbol of the first block to ***x*** and drag the second block to the second position of this first block.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_8.jpg" width="960"/><br>

19. The first position of the second block **" " + " "** will be a **random integer from 1 to 100** from **Math**, whereby the number **100** becomes ***4***.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_9.jpg" width="960"/><br>

20. The second position of the second block **random integer from 1 to 4 + " "** becomes a **random fraction** block of **Math**.
21. Now drag the variable block **RootNote** from your workspace to the first position of the first block **" " x random integer from 1 to 4 + random fraction**.
22. Finally replace the value **220** of **Set tone on pin# 4 to 220** with the complete block combination of **RootNote x random integer from 1 to 4 + random fraction**.

  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_10.jpg" width="960"/><br>

23. Save the latest version of the ArduBot project, compile it and upload it to your Arduino.


  <img src="images/RobotBuild/RobotBuild_Part6_PassiveBuzzer_11.jpg" width="960"/><br>

### *Made It! :-D*

---

<img src="images/RobotBuild/RobotBuild_FrontSideTopView.png" width="960"/><br>


### [*ArduBot Video*](images/RobotBuild/BuildARobot.mp4)

#### *Have fun with your ArduBot!*

From here on you can integrate your own ideas. I'll give you a few more suggestions along the way.

For example:

* avoiding the obstacle that lies on the path and then finding back to the path again.
* different melodies for different situations.
* LEDs to indicate what direction your robot is going next.
* the LED matrix, which then makes faces depend on the situation.
* ...

---
## END

### I hope you enjoyed this course. Have fun being a "Maker". :-)

***Thank you for taking the time, and if you have any feedback and/or suggestions I'll greatly appreciate them!***

#### contact@jensmeisner.net
